; RUN: llc -march=nuplus < %s | FileCheck %s

define i8 @selectcc_i8(i1 %cond, i8 %true, i8 %false) nounwind readnone {
; CHECK-LABEL: selectcc_i8
; CHECK: cmpnei s0, s0, 0
; CHECK: bnez s0, .LBB0_2

; CHECK: move_i32 s1, s2

; CHECK-LABEL: .LBB0_2:
; CHECK: move_i32 s0, s1
; CHECK: jret
  %ret = select i1 %cond, i8 %true, i8 %false
  ret i8 %ret
}

define i16 @selectcc_i16(i1 %cond, i16 %true, i16 %false) nounwind readnone {
; CHECK-LABEL: selectcc_i16
; CHECK: cmpnei s0, s0, 0
; CHECK: bnez s0, .LBB1_2

; CHECK: move_i32 s1, s2

; CHECK-LABEL: .LBB1_2:
; CHECK: move_i32 s0, s1
; CHECK: jret
  %ret = select i1 %cond, i16 %true, i16 %false
  ret i16 %ret
}

define i32 @selectcc_i32(i1 %cond, i32 %true, i32 %false) nounwind readnone {
; CHECK-LABEL: selectcc_i32
; CHECK: cmpnei s0, s0, 0
; CHECK: bnez s0, .LBB2_2

; CHECK: move_i32 s1, s2

; CHECK-LABEL: .LBB2_2:
; CHECK: move_i32 s0, s1
; CHECK: jret
  %ret = select i1 %cond, i32 %true, i32 %false
  ret i32 %ret
}

define i64 @selectcc_i64(i1 %cond, i64 %true, i64 %false) nounwind readnone {
; CHECK-LABEL: selectcc_i64
; CHECK: cmpnei s0, s0, 0
; CHECK: bnez s0, .LBB3_2

; CHECK: move_i64 s2_64, s4_64

; CHECK-LABEL: .LBB3_2:
; CHECK: move_i64 s0_64, s2_64
; CHECK: jret
  %ret = select i1 %cond, i64 %true, i64 %false
  ret i64 %ret
}
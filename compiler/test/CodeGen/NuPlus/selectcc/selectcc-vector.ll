; RUN: llc -march=nuplus < %s | FileCheck %s

define<16 x i32> @selectcc_v16i32(i1 %cond,<16 x i32> %true,<16 x i32> %false) nounwind readnone {
; CHECK-LABEL: selectcc_v16i32
; CHECK: cmpnei s0, s0, 0
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, v0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v0, v1
; CHECK: move_i32 rm, s1

  %ret = select i1 %cond,<16 x i32> %true,<16 x i32> %false
  ret<16 x i32> %ret
}

define<8 x i64> @selectcc_v8i64(i1 %cond,<8 x i64> %true,<8 x i64> %false) nounwind readnone {
; CHECK-LABEL: selectcc_v8i64
; CHECK: cmpnei s0, s0, 0
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v0, v0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v0, v1
; CHECK: move_i32 rm, s1

  %ret = select i1 %cond,<8 x i64> %true,<8 x i64> %false
  ret<8 x i64> %ret
}


define<16 x i32> @vselectcc_v16i32(<16 x i1> %cond,<16 x i32> %true,<16 x i32> %false) nounwind readnone {
; CHECK-LABEL: vselectcc_v16i32
; CHECK: moveih s0, 0
; CHECK: moveil s0, 0
; CHECK: move_i32 v3, s0
; CHECK: cmpne_i32 s0, v0, v3
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v0, v2
; CHECK: move_i32 rm, s1

  %ret = select <16 x i1> %cond,<16 x i32> %true,<16 x i32> %false
  ret<16 x i32> %ret
}

define<8 x i64> @vselectcc_v8i64(<8 x i1> %cond,<8 x i64> %true,<8 x i64> %false) nounwind readnone {
; CHECK-LABEL: vselectcc_v8i64
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 s0, s1
; CHECK: move_i64 v3, s0_64
; CHECK: cmpne_i32 s0, v0, v3
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v0, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v0, v2
; CHECK: move_i32 rm, s1

  %ret = select <8 x i1> %cond,<8 x i64> %true,<8 x i64> %false
  ret<8 x i64> %ret
}
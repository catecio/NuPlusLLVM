; RUN: llc -march=nuplus < %s | FileCheck %s

define<16 x float> @selectcc_v16f32(i1 %cond,<16 x float> %true,<16 x float> %false) nounwind readnone {
; CHECK-LABEL: selectcc_v16f32
; CHECK: cmpnei s0, s0, 0
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, v0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v0, v1
; CHECK: move_i32 rm, s1

  %ret = select i1 %cond,<16 x float> %true,<16 x float> %false
  ret<16 x float> %ret
}

define<8 x double> @selectcc_v8f64(i1 %cond,<8 x double> %true,<8 x double> %false) nounwind readnone {
; CHECK-LABEL: selectcc_v8f64
; CHECK: cmpnei s0, s0, 0
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v0, v0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v0, v1
; CHECK: move_i32 rm, s1

  %ret = select i1 %cond,<8 x double> %true,<8 x double> %false
  ret<8 x double> %ret
}

define<16 x float> @vselectcc_v16f32(<16 x i1> %cond,<16 x float> %true,<16 x float> %false) nounwind readnone {
; CHECK-LABEL: vselectcc_v16f32
; CHECK: moveih s0, 0
; CHECK: moveil s0, 0
; CHECK: move_i32 v3, s0
; CHECK: cmpne_i32 s0, v0, v3
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v0, v2
; CHECK: move_i32 rm, s1

  %ret = select <16 x i1> %cond,<16 x float> %true,<16 x float> %false
  ret<16 x float> %ret
}

define<8 x double> @vselectcc_v8f64(<8 x i1> %cond,<8 x double> %true,<8 x double> %false) nounwind readnone {
; CHECK-LABEL: vselectcc_v8f64
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 s0, s1
; CHECK: move_i64 v3, s0_64
; CHECK: cmpne_i32 s0, v0, v3
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v0, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v0, v2
; CHECK: move_i32 rm, s1

  %ret = select <8 x i1> %cond,<8 x double> %true,<8 x double> %false
  ret<8 x double> %ret
}
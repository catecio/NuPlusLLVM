; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i8> @selectccv16i8(i1 %cond, <16 x i8> %true, <16 x i8> %false) nounwind readnone {
; CHECK-LABEL: selectccv16i8
; CHECK: cmpnei s0, s0, 0
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, v0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v0, v1
; CHECK: move_i32 rm, s1

  %ret = select i1 %cond, <16 x i8> %true, <16 x i8> %false
  ret <16 x i8> %ret
}

define <16 x i16>@selectccv16i16(i1 %cond, <16 x i16>%true, <16 x i16>%false) nounwind readnone {
; CHECK-LABEL: selectccv16i16
; CHECK: cmpnei s0, s0, 0
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, v0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v0, v1
; CHECK: move_i32 rm, s1

  %ret = select i1 %cond, <16 x i16>%true, <16 x i16>%false
  ret <16 x i16>%ret
}

define <8 x i8> @selectccv8i8(i1 %cond, <8 x i8> %true, <8 x i8> %false) nounwind readnone {
; CHECK-LABEL: selectccv8i8
; CHECK: cmpnei s0, s0, 0
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v0, v0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v0, v1
; CHECK: move_i32 rm, s1

  %ret = select i1 %cond, <8 x i8> %true, <8 x i8> %false
  ret <8 x i8> %ret
}

define <8 x i16>@selectccv8i16(i1 %cond, <8 x i16>%true, <8 x i16>%false) nounwind readnone {
; CHECK-LABEL: selectccv8i16
; CHECK: cmpnei s0, s0, 0
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v0, v0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v0, v1
; CHECK: move_i32 rm, s1

  %ret = select i1 %cond, <8 x i16>%true, <8 x i16>%false
  ret <8 x i16>%ret
}

define <8 x i32>@selectccv8i32(i1 %cond, <8 x i32>%true, <8 x i32>%false) nounwind readnone {
; CHECK-LABEL: selectccv8i32
; CHECK: cmpnei s0, s0, 0
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v0, v0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v0, v1
; CHECK: move_i32 rm, s1

  %ret = select i1 %cond, <8 x i32>%true, <8 x i32>%false
  ret <8 x i32>%ret
}


define <16 x i8> @vselectccv16i8(<16 x i1> %cond, <16 x i8> %true, <16 x i8> %false) nounwind readnone {
; CHECK-LABEL: vselectccv16i8
; CHECK: moveih s0, 0
; CHECK: moveil s0, 0
; CHECK: move_i32 v3, s0
; CHECK: cmpne_i32 s0, v0, v3
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v0, v2
; CHECK: move_i32 rm, s1

  %ret = select <16 x i1> %cond, <16 x i8> %true, <16 x i8> %false
  ret <16 x i8> %ret
}

define <16 x i16>@vselectccv16i16(<16 x i1> %cond, <16 x i16>%true, <16 x i16>%false) nounwind readnone {
; CHECK-LABEL: vselectccv16i16
; CHECK: moveih s0, 0
; CHECK: moveil s0, 0
; CHECK: move_i32 v3, s0
; CHECK: cmpne_i32 s0, v0, v3
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v0, v2
; CHECK: move_i32 rm, s1

  %ret = select <16 x i1> %cond, <16 x i16>%true, <16 x i16>%false
  ret <16 x i16>%ret
}

define <8 x i8> @vselectccv8i8(<8 x i1> %cond, <8 x i8> %true, <8 x i8> %false) nounwind readnone {
; CHECK-LABEL: vselectccv8i8
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 s0, s1
; CHECK: move_i64 v3, s0_64
; CHECK: cmpne_i32 s0, v0, v3
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v0, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v0, v2
; CHECK: move_i32 rm, s1

  %ret = select <8 x i1> %cond, <8 x i8> %true, <8 x i8> %false
  ret <8 x i8> %ret
}

define <8 x i16>@vselectccv8i16(<8 x i1> %cond, <8 x i16>%true, <8 x i16>%false) nounwind readnone {
; CHECK-LABEL: vselectccv8i16
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 s0, s1
; CHECK: move_i64 v3, s0_64
; CHECK: cmpne_i32 s0, v0, v3
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v0, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v0, v2
; CHECK: move_i32 rm, s1

  %ret = select <8 x i1> %cond, <8 x i16>%true, <8 x i16>%false
  ret <8 x i16>%ret
}

define <8 x i32>@vselectccv8i32(<8 x i1> %cond, <8 x i32>%true, <8 x i32>%false) nounwind readnone {
; CHECK-LABEL: vselectccv8i32
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 s0, s1
; CHECK: move_i64 v3, s0_64
; CHECK: cmpne_i32 s0, v0, v3
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v0, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v0, v2
; CHECK: move_i32 rm, s1

  %ret = select <8 x i1> %cond, <8 x i32>%true, <8 x i32>%false
  ret <8 x i32>%ret
}

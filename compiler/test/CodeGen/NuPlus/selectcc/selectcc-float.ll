; RUN: llc -march=nuplus < %s | FileCheck %s

define float @selectcc_float(i1 %cond, float %true, float %false) nounwind readnone {
; CHECK-LABEL: selectcc_float
; CHECK: cmpnei s0, s0, 0
; CHECK: bnez s0, .LBB0_2

; CHECK: move_i32 s1, s2

; CHECK-LABEL: .LBB0_2:
; CHECK: move_i32 s0, s1
; CHECK: jret
  %ret = select i1 %cond, float %true, float %false
  ret float %ret
}

define double @selectcc_double(i1 %cond, double %true, double %false) nounwind readnone {
; CHECK-LABEL: selectcc_double
; CHECK: cmpnei s0, s0, 0
; CHECK: bnez s0, .LBB1_2

; CHECK: move_i64 s2_64, s4_64

; CHECK-LABEL: .LBB1_2:
; CHECK: move_i64 s0_64, s2_64
; CHECK: jret
  %ret = select i1 %cond, double %true, double %false
  ret double %ret
}
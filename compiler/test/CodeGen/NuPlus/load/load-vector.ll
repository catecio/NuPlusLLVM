; RUN: llc -march=nuplus < %s | FileCheck %s

@v16i32  = global <16 x i32> <i32 1, i32 2, i32 3, i32 4, i32 5, i32 6, i32 7, i32 8, i32 9, i32 10, i32 11, i32 12, i32 13, i32 14, i32 15, i32 16>, align 64

@v8i64 = global <8 x i64> <i64 1, i64 2, i64 3, i64 4, i64 5, i64 6, i64 7, i64 8>, align 64

define <16 x i32> @load_v16i32() {
; CHECK-LABEL: load_v16i32
; CHECK: leah s0, v16i32
; CHECK: leal s0, v16i32
; CHECK: load_v16i32 v0, (s0)

  %data = load <16 x i32>, <16 x i32>* @v16i32, align 64
  ret <16 x i32> %data
}

define <8 x i64> @load_v8i64() {
; CHECK-LABEL: load_v8i64
; CHECK: leah s0, v8i64
; CHECK: leal s0, v8i64
; CHECK: load_v8i64 v0, (s0)

  %data = load <8 x i64>, <8 x i64>* @v8i64, align 64
  ret <8 x i64> %data
}
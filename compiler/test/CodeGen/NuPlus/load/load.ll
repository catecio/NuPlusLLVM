; RUN: llc -march=nuplus < %s | FileCheck %s

@ub = global i8   0, align  1
@b  = global i8   0, align  1

@us = global i16  0, align  2
@s  = global i16  0, align  2

@ui = global i32  0, align  4
@i  = global i32  0, align  4

@ul = global i64  0, align  8
@l  = global i64  0, align  8

define zeroext i8 @load_ub() {
; CHECK-LABEL: load_ub
; CHECK: leah s0, ub
; CHECK: leal s0, ub
; CHECK: load32_u8 s0, (s0)

  %data = load i8, i8* @ub, align 1
  ret i8 %data
}
define signext i8 @load_b() {
; CHECK-LABEL: load_b
; CHECK: leah s0, b
; CHECK: leal s0, b
; CHECK: load32_s8 s0, (s0)

  %data = load i8, i8* @b, align 1
  ret i8 %data
}

define zeroext i16 @load_us() {
; CHECK-LABEL: load_us
; CHECK: leah s0, us
; CHECK: leal s0, us
; CHECK: load32_u16 s0, (s0)

  %data = load i16, i16* @us, align 2
  ret i16 %data
}
define signext i16 @load_s() {
; CHECK-LABEL: load_s
; CHECK: leah s0, s
; CHECK: leal s0, s
; CHECK: load32_s16 s0, (s0)

  %data = load i16, i16* @s, align 2
  ret i16 %data
}

define zeroext i32 @load_ui() {
; CHECK-LABEL: load_ui
; CHECK: leah s0, ui
; CHECK: leal s0, ui
; CHECK: load32 s0, (s0)

  %data = load i32, i32* @ui, align 4
  ret i32 %data
}
define signext i32 @load_i() {
; CHECK-LABEL: load_i
; CHECK: leah s0, i
; CHECK: leal s0, i
; CHECK: load32 s0, (s0)

  %data = load i32, i32* @i, align 4
  ret i32 %data
}

define zeroext i32 @load_ub32() {
; CHECK-LABEL: load_ub32
; CHECK: leah s0, ub
; CHECK: leal s0, ub
; CHECK: load32_u8 s0, (s0)

  %data = load i8, i8* @ub, align 1
  %ext = zext i8 %data to i32
  ret i32 %ext
}
define signext i32 @load_b32() {
; CHECK-LABEL: load_b32
; CHECK: leah s0, b
; CHECK: leal s0, b
; CHECK: load32_s8 s0, (s0)

  %data = load i8, i8* @b, align 1
  %ext = sext i8 %data to i32
  ret i32 %ext
}

define zeroext i32 @load_us32() {
; CHECK-LABEL: load_us32
; CHECK: leah s0, us
; CHECK: leal s0, us
; CHECK: load32_u16 s0, (s0)

  %data = load i16, i16* @us, align 2
  %ext = zext i16 %data to i32
  ret i32 %ext
}
define signext i32 @load_s32() {
; CHECK-LABEL: load_s32
; CHECK: leah s0, s
; CHECK: leal s0, s
; CHECK: load32_s16 s0, (s0)

  %data = load i16, i16* @s, align 2
  %ext = sext i16 %data to i32
  ret i32 %ext
}

define zeroext i64 @load_ul() {
; CHECK-LABEL: load_ul
; CHECK: leah s0, ul
; CHECK: leal s0, ul
; CHECK: load64 s0_64, (s0)

  %data = load i64, i64* @ul, align 8
  ret i64 %data
}
define signext i64 @load_l() {
; CHECK-LABEL: load_l
; CHECK: leah s0, l
; CHECK: leal s0, l
; CHECK: load64 s0_64, (s0)

  %data = load i64, i64* @l, align 8
  ret i64 %data
}

define zeroext i64 @load_ub64() {
; CHECK-LABEL: load_ub64
; CHECK: leah s0, ub
; CHECK: leal s0, ub
; CHECK: load64_u8 s0_64, (s0)

  %data = load i8, i8* @ub, align 1
  %ext = zext i8 %data to i64
  ret i64 %ext
}
define signext i64 @load_b64() {
; CHECK-LABEL: load_b64
; CHECK: leah s0, b
; CHECK: leal s0, b
; CHECK: load64_s8 s0_64, (s0)

  %data = load i8, i8* @b, align 1
  %ext = sext i8 %data to i64
  ret i64 %ext
}

define zeroext i64 @load_us64() {
; CHECK-LABEL: load_us64
; CHECK: leah s0, us
; CHECK: leal s0, us
; CHECK: load64_u16 s0_64, (s0)

  %data = load i16, i16* @us, align 2
  %ext = zext i16 %data to i64
  ret i64 %ext
}
define signext i64 @load_s64() {
; CHECK-LABEL: load_s64
; CHECK: leah s0, s
; CHECK: leal s0, s
; CHECK: load64_s16 s0_64, (s0)

  %data = load i16, i16* @s, align 2
  %ext = sext i16 %data to i64
  ret i64 %ext
}

define zeroext i64 @load_ui64() {
; CHECK-LABEL: load_ui
; CHECK: leah s0, ui
; CHECK: leal s0, ui
; CHECK: load64_u32 s0_64, (s0)

  %data = load i32, i32* @ui, align 4
  %ext = zext i32 %data to i64
  ret i64 %ext
}
define signext i64 @load_i64() {
; CHECK-LABEL: load_i
; CHECK: leah s0, i
; CHECK: leal s0, i
; CHECK: load64_s32 s0_64, (s0)

  %data = load i32, i32* @i, align 4
  %ext = sext i32 %data to i64
  ret i64 %ext
}

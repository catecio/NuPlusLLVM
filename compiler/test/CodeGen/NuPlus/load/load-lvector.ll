; RUN: llc -march=nuplus < %s | FileCheck %s

@v16i8  = global <16 x i8> <i8 1, i8 2, i8 3, i8 4, i8 5, i8 6, i8 7, i8 8, i8 9, i8 10, i8 11, i8 12, i8 13, i8 14, i8 15, i8 16>, align 16
@v16i16  = global <16 x i16> <i16 1, i16 2, i16 3, i16 4, i16 5, i16 6, i16 7, i16 8, i16 9, i16 10, i16 11, i16 12, i16 13, i16 14, i16 15, i16 16>, align 32

@v8i8 = global <8 x i8> <i8 1, i8 2, i8 3, i8 4, i8 5, i8 6, i8 7, i8 8>, align 8
@v8i16 = global <8 x i16> <i16 1, i16 2, i16 3, i16 4, i16 5, i16 6, i16 7, i16 8>, align 16
@v8i32 = global <8 x i32> <i32 1, i32 2, i32 3, i32 4, i32 5, i32 6, i32 7, i32 8>, align 32

define <16 x i32> @load_v16i8() {
; CHECK-LABEL: load_v16i8
; CHECK: leah s0, v16i8
; CHECK: leal s0, v16i8
; CHECK: load_v16i8 v0, (s0)

  %data = load <16 x i8>, <16 x i8>* @v16i8, align 16
  %ext = sext <16 x i8> %data to <16 x i32>
  ret <16 x i32> %ext
}
define <16 x i32> @load_v16u8() {
; CHECK-LABEL: load_v16u8
; CHECK: leah s0, v16i8
; CHECK: leal s0, v16i8
; CHECK: load_v16u8 v0, (s0)

  %data = load <16 x i8>, <16 x i8>* @v16i8, align 16
  %ext = zext <16 x i8> %data to <16 x i32>
  ret <16 x i32> %ext
}

define <16 x i32> @load_v16i16() {
; CHECK-LABEL: load_v16i16
; CHECK: leah s0, v16i16
; CHECK: leal s0, v16i16
; CHECK: load_v16i16 v0, (s0)

  %data = load <16 x i16>, <16 x i16>* @v16i16, align 32
  %ext = sext <16 x i16> %data to <16 x i32>
  ret <16 x i32> %ext
}
define <16 x i32> @load_v16u16() {
; CHECK-LABEL: load_v16u16
; CHECK: leah s0, v16i16
; CHECK: leal s0, v16i16
; CHECK: load_v16u16 v0, (s0)

  %data = load <16 x i16>, <16 x i16>* @v16i16, align 32
  %ext = zext <16 x i16> %data to <16 x i32>
  ret <16 x i32> %ext
}

define <8 x i64> @load_v8i8() {
; CHECK-LABEL: load_v8i8
; CHECK: leah s0, v8i8
; CHECK: leal s0, v8i8
; CHECK: load_v8i8 v0, (s0)

  %data = load <8 x i8>, <8 x i8>* @v8i8, align 8
  %ext = sext <8 x i8> %data to <8 x i64>
  ret <8 x i64> %ext
}
define <8 x i64> @load_v8u8() {
; CHECK-LABEL: load_v8u8
; CHECK: leah s0, v8i8
; CHECK: leal s0, v8i8
; CHECK: load_v8u8 v0, (s0)

  %data = load <8 x i8>, <8 x i8>* @v8i8, align 8
  %ext = zext <8 x i8> %data to <8 x i64>
  ret <8 x i64> %ext
}

define <8 x i64> @load_v8i16() {
; CHECK-LABEL: load_v8i16
; CHECK: leah s0, v8i16
; CHECK: leal s0, v8i16
; CHECK: load_v8i16 v0, (s0)

  %data = load <8 x i16>, <8 x i16>* @v8i16, align 16
  %ext = sext <8 x i16> %data to <8 x i64>
  ret <8 x i64> %ext
}
define <8 x i64> @load_v8u16() {
; CHECK-LABEL: load_v8u16
; CHECK: leah s0, v8i16
; CHECK: leal s0, v8i16
; CHECK: load_v8u16 v0, (s0)

  %data = load <8 x i16>, <8 x i16>* @v8i16, align 16
  %ext = zext <8 x i16> %data to <8 x i64>
  ret <8 x i64> %ext
}

define <8 x i64> @load_v8i32() {
; CHECK-LABEL: load_v8i32
; CHECK: leah s0, v8i32
; CHECK: leal s0, v8i32
; CHECK: load_v8i32 v0, (s0)

  %data = load <8 x i32>, <8 x i32>* @v8i32, align 32
  %ext = sext <8 x i32> %data to <8 x i64>
  ret <8 x i64> %ext
}
define <8 x i64> @load_v8u32() {
; CHECK-LABEL: load_v8u32
; CHECK: leah s0, v8i32
; CHECK: leal s0, v8i32
; CHECK: load_v8u32 v0, (s0)

  %data = load <8 x i32>, <8 x i32>* @v8i32, align 32
  %ext = zext <8 x i32> %data to <8 x i64>
  ret <8 x i64> %ext
}

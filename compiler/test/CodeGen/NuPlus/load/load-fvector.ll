; RUN: llc -march=nuplus < %s | FileCheck %s

@v16f32  = global <16 x float> <float 1.0, float 2.0, float 3.0, float 4.0, float 5.0, float 6.0, float 7.0, float 8.0, float 9.0, float 10.0, float 11.0, float 12.0, float 13.0, float 14.0, float 15.0, float 16.0>, align 64

@v8f64 = global <8 x double> <double 1.0, double 2.0, double 3.0, double 4.0, double 5.0, double 6.0, double 7.0, double 8.0>, align 64

define <16 x float> @load_v16f32() {
; CHECK-LABEL: load_v16f32
; CHECK: leah s0, v16f32
; CHECK: leal s0, v16f32
; CHECK: load_v16i32 v0, (s0)

  %data = load <16 x float>, <16 x float>* @v16f32, align 64
  ret <16 x float> %data
}

define <8 x double> @load_v8f64() {
; CHECK-LABEL: load_v8f64
; CHECK: leah s0, v8f64
; CHECK: leal s0, v8f64
; CHECK: load_v8i64 v0, (s0)

  %data = load <8 x double>, <8 x double>* @v8f64, align 64
  ret <8 x double> %data
}
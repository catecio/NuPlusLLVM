; RUN: llc -march=nuplus < %s | FileCheck %s

@f  = global float   0.0, align  4

@d  = global double  0.0, align  8

define float @load_float() {
; CHECK-LABEL: load_float
; CHECK: leah s0, f
; CHECK: leal s0, f
; CHECK: load32 s0, (s0)

  %data = load float, float* @f, align 4
  ret float %data
}

define double @load_double() {
; CHECK-LABEL: load_double
; CHECK: leah s0, d
; CHECK: leal s0, d
; CHECK: load64 s0_64, (s0)

  %data = load double, double* @d, align 8
  ret double %data
}
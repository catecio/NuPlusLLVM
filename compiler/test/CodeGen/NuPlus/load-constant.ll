; RUN: llc -march=nuplus < %s | FileCheck %s


define i32 @loadI32() nounwind readnone  {
  ; CHECK-LABEL: loadI32
  ; CHECK:  moveih s0, 65535
  ; CHECK:  moveil s0, 65535
  ret i32 4294967295
}

define i64 @loadI64() nounwind readnone  {
  ; CHECK-LABEL: loadI64
  ; CHECK:  moveih s1, 65535
	; CHECK:  moveil s1, 65535
	; CHECK:  move_i32 s0, s1
  ret i64 18446744073709551615
}

define float @loadF32() nounwind readnone  {
  ; CHECK-LABEL: loadF32
  ; CHECK:  moveih s0, 16256
  ; CHECK:  moveil s0, 0
  ret float 1.000000e+00
}

define double @loadF64() nounwind readnone  {
  ; CHECK-LABEL: loadF64
  ; CHECK:  moveih s1, 16368
  ; CHECK:  moveil s1, 0
  ; CHECK:  moveih s0, 0
  ; CHECK:  moveil s0, 0
  ret double 1.000000e+00
}
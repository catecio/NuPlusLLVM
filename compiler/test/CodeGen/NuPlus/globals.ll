; RUN: llc < %s -march=nuplus | FileCheck %s

@a = global i32 1, align 4

; Function Attrs: norecurse nounwind uwtable
define i32 @loadstorea() #0 {
  store i32 2, i32* @a, align 4
  %tmp = load i32, i32* @a
  ret i32 %tmp
}
; CHECK-LABEL: loadstorea
; CHECK:	leah s1, a
; CHECK:	leal s1, a
; CHECK:	moveih s0, 0
; CHECK:	moveil s0, 2
; CHECK:	store32 s0, (s1)
; CHECK:	jret
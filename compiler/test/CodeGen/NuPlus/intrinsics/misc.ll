; RUN: llc -march=nuplus < %s | FileCheck %s

declare void @llvm.nuplus.__builtin_nuplus_barrier (i32, i32) nounwind
define void @barrier (i32 %a, i32 %b) nounwind {
; CHECK-LABEL: barrier 
; CHECK: barrier s0, s1
    call void @llvm.nuplus.__builtin_nuplus_barrier (i32 %a, i32 %b)
    ret void
}

declare void @llvm.nuplus.__builtin_nuplus_flush (i32) nounwind
define void @flush (i32 %a) nounwind {
; CHECK-LABEL: flush 
; CHECK: flush s0
    call void @llvm.nuplus.__builtin_nuplus_flush (i32 %a)
    ret void
}

declare i32 @llvm.nuplus.__builtin_nuplus_createmaskv16i32(<16 x i32>) nounwind
define i32 @createmask_v16i32(<16 x i32> %vmask) nounwind {
; CHECK-LABEL: createmask_v16i32
; CHECK: crt_mask_i32 s0, v0
    %smask = call i32 @llvm.nuplus.__builtin_nuplus_createmaskv16i32(<16 x i32> %vmask)
    ret i32 %smask 
}
declare i32 @llvm.nuplus.__builtin_nuplus_createmaskv8i64(<8 x i64>) nounwind
define i32 @createmask_v8i64(<8 x i64> %vmask) nounwind {
; CHECK-LABEL: createmask_v8i64
; CHECK: crt_mask_i64 s0, v0
    %smask = call i32 @llvm.nuplus.__builtin_nuplus_createmaskv8i64(<8 x i64> %vmask)
    ret i32 %smask 
}

declare void @llvm.nuplus.__builtin_nuplus_write_mask_regv16i32(<16 x i32>) nounwind
define void @writemask_v16i32(<16 x i32> %vmask) nounwind {
; CHECK-LABEL: writemask_v16i32
; CHECK: crt_mask_i32 s0, v0
; CHECK: write_mr s0
    call void @llvm.nuplus.__builtin_nuplus_write_mask_regv16i32(<16 x i32> %vmask)
    ret void
}
declare void @llvm.nuplus.__builtin_nuplus_write_mask_regv8i64(<8 x i64>) nounwind
define void @writemask_v8i64(<8 x i64> %vmask) nounwind {
; CHECK-LABEL: writemask_v8i64
; CHECK: crt_mask_i64 s0, v0
; CHECK: write_mr s0
    call void @llvm.nuplus.__builtin_nuplus_write_mask_regv8i64(<8 x i64> %vmask)
    ret void
}

declare void @llvm.nuplus.__builtin_nuplus_write_mask_reg(i32) nounwind
define void @write_mask_reg(i32 %a) nounwind {
; CHECK-LABEL: write_mask_reg
; CHECK: write_mr s0
    call void @llvm.nuplus.__builtin_nuplus_write_mask_reg(i32 %a)
    ret void
}

declare i32 @llvm.nuplus.__builtin_nuplus_read_mask_reg () nounwind
define i32 @read_mask_reg () nounwind {
; CHECK-LABEL: read_mask_reg 
; CHECK: read_mr s0
    %a = call i32 @llvm.nuplus.__builtin_nuplus_read_mask_reg ()
    ret i32 %a
}

declare void @llvm.nuplus.__builtin_nuplus_write_control_reg(i32, i32) nounwind
define void @write_control_reg(i32 %a, i32 %b) nounwind {
; CHECK-LABEL: write_control_reg
; CHECK: write_cr s0, s1
    call void @llvm.nuplus.__builtin_nuplus_write_control_reg(i32 %a, i32 %b)
    ret void
}

declare i32 @llvm.nuplus.__builtin_nuplus_read_control_reg (i32) nounwind
define i32 @read_control_reg (i32 %a) nounwind {
; CHECK-LABEL: read_control_reg
; CHECK: read_cr s0, s0
    %b = call i32 @llvm.nuplus.__builtin_nuplus_read_control_reg (i32 %a)
    ret i32 %b
}
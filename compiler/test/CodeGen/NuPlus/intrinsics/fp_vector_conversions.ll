; RUN: llc -march=nuplus < %s | FileCheck %s

declare <16 x float> @llvm.nuplus.__builtin_nuplus_v8f64tov16f32(<8 x double>) nounwind
define <16 x float> @v8f64tov16f32(<8 x double> %a) nounwind {
; CHECK-LABEL: v8f64tov16f32
; CHECK: f64tof32 v0, v0
    %b = call <16 x float> @llvm.nuplus.__builtin_nuplus_v8f64tov16f32(<8 x double> %a)
    ret <16 x float> %b 
}

declare <8 x double> @llvm.nuplus.__builtin_nuplus_v16f32tov8f64(<16 x float>) nounwind
define <8 x double> @v16f32tov8f64 (<16 x float> %a) nounwind {
; CHECK-LABEL: v16f32tov8f64 
; CHECK: f32tof64 v0, v0
	%b = call <8 x double> @llvm.nuplus.__builtin_nuplus_v16f32tov8f64(<16 x float> %a)
    ret <8 x double> %b 
}
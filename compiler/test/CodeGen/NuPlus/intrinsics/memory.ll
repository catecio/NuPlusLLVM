; RUN: llc -march=nuplus < %s | FileCheck %s

declare <16 x i32> @llvm.nuplus.__builtin_nuplus_gather_loadi32_scratchpad(<16 x i32>) nounwind
define <16 x i32> @gather_loadi32_scratchpad (<16 x i32> %a) nounwind {
; CHECK-LABEL: gather_loadi32_scratchpad 
; CHECK: loadg32_scratchpad v0, (v0)
    %b = call <16 x i32> @llvm.nuplus.__builtin_nuplus_gather_loadi32_scratchpad (<16 x i32> %a)
    ret <16 x i32> %b
}

declare <16 x float> @llvm.nuplus.__builtin_nuplus_gather_loadf32_scratchpad(<16 x i32>) nounwind
define <16 x float> @gather_loadf32_scratchpad (<16 x i32> %a) nounwind {
; CHECK-LABEL: gather_loadf32_scratchpad 
; CHECK: loadg32_scratchpad v0, (v0)
    %b = call <16 x float> @llvm.nuplus.__builtin_nuplus_gather_loadf32_scratchpad (<16 x i32> %a)
    ret <16 x float> %b
}

declare <8 x i64> @llvm.nuplus.__builtin_nuplus_gather_loadi64_scratchpad(<16 x i32>) nounwind
define <8 x i64> @gather_loadi64_scratchpad (<16 x i32> %a) nounwind {
; CHECK-LABEL: gather_loadi64_scratchpad 
; CHECK: loadg64_scratchpad v0, (v0)
    %b = call <8 x i64> @llvm.nuplus.__builtin_nuplus_gather_loadi64_scratchpad (<16 x i32> %a)
    ret <8 x i64> %b
}

declare <8 x double> @llvm.nuplus.__builtin_nuplus_gather_loadf64_scratchpad(<16 x i32>) nounwind
define <8 x double> @gather_loadf64_scratchpad (<16 x i32> %a) nounwind {
; CHECK-LABEL: gather_loadf64_scratchpad 
; CHECK: loadg64_scratchpad v0, (v0)
    %b = call <8 x double> @llvm.nuplus.__builtin_nuplus_gather_loadf64_scratchpad (<16 x i32> %a)
    ret <8 x double> %b
}



declare void @llvm.nuplus.__builtin_nuplus_scatter_storei32_scratchpad(<16 x i32>, <16 x i32>) nounwind
define void @storei32_scratchpad (<16 x i32> %a, <16 x i32> %b) nounwind {
; CHECK-LABEL: storei32_scratchpad 
; CHECK: stores32_scratchpad v1, (v0)
    call void @llvm.nuplus.__builtin_nuplus_scatter_storei32_scratchpad (<16 x i32> %a, <16 x i32> %b)
    ret void
}
declare void @llvm.nuplus.__builtin_nuplus_scatter_storef32_scratchpad(<16 x i32>, <16 x float>) nounwind
define void @storef32_scratchpad (<16 x i32> %a, <16 x float> %b) nounwind {
; CHECK-LABEL: storef32_scratchpad 
; CHECK: stores32_scratchpad v1, (v0)
    call void @llvm.nuplus.__builtin_nuplus_scatter_storef32_scratchpad (<16 x i32> %a, <16 x float> %b)
    ret void
}
declare void @llvm.nuplus.__builtin_nuplus_scatter_storei64_scratchpad(<16 x i32>, <8 x i64>) nounwind
define void @storei64_scratchpad (<16 x i32> %a, <8 x i64> %b) nounwind {
; CHECK-LABEL: storei64_scratchpad 
; CHECK: stores64_scratchpad v1, (v0)
    call void @llvm.nuplus.__builtin_nuplus_scatter_storei64_scratchpad (<16 x i32> %a, <8 x i64> %b)
    ret void
}
declare void @llvm.nuplus.__builtin_nuplus_scatter_storef64_scratchpad(<16 x i32>, <8 x double>) nounwind
define void @storef64_scratchpad (<16 x i32> %a, <8 x double> %b) nounwind {
; CHECK-LABEL: storef64_scratchpad 
; CHECK: stores64_scratchpad v1, (v0)
    call void @llvm.nuplus.__builtin_nuplus_scatter_storef64_scratchpad (<16 x i32> %a, <8 x double> %b)
    ret void
}

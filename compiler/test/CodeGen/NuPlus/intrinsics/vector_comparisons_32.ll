; RUN: llc -march=nuplus < %s | FileCheck %s

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_ugt(<16 x i32>, <16 x i32>) nounwind
define i32 @cmpi32_ugt(<16 x i32> %a, <16 x i32> %b) nounwind {
; CHECK-LABEL: cmpi32_ugt
; CHECK: cmpugt_i32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_ugt(<16 x i32> %a, <16 x i32> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_uge(<16 x i32>, <16 x i32>) nounwind
define i32 @cmpi32_uge(<16 x i32> %a, <16 x i32> %b) nounwind {
; CHECK-LABEL: cmpi32_uge
; CHECK: cmpuge_i32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_uge(<16 x i32> %a, <16 x i32> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_ult(<16 x i32>, <16 x i32>) nounwind
define i32 @cmpi32_ult(<16 x i32> %a, <16 x i32> %b) nounwind {
; CHECK-LABEL: cmpi32_ult
; CHECK: cmpult_i32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_ult(<16 x i32> %a, <16 x i32> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_ule(<16 x i32>, <16 x i32>) nounwind
define i32 @cmpi32_ule(<16 x i32> %a, <16 x i32> %b) nounwind {
; CHECK-LABEL: cmpi32_ule
; CHECK: cmpule_i32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_ule(<16 x i32> %a, <16 x i32> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_sgt(<16 x i32>, <16 x i32>) nounwind
define i32 @cmpi32_sgt(<16 x i32> %a, <16 x i32> %b) nounwind {
; CHECK-LABEL: cmpi32_sgt
; CHECK: cmpgt_i32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_sgt(<16 x i32> %a, <16 x i32> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_sge(<16 x i32>, <16 x i32>) nounwind
define i32 @cmpi32_sge(<16 x i32> %a, <16 x i32> %b) nounwind {
; CHECK-LABEL: cmpi32_sge
; CHECK: cmpge_i32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_sge(<16 x i32> %a, <16 x i32> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_slt(<16 x i32>, <16 x i32>) nounwind
define i32 @cmpi32_slt(<16 x i32> %a, <16 x i32> %b) nounwind {
; CHECK-LABEL: cmpi32_slt
; CHECK: cmplt_i32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_slt(<16 x i32> %a, <16 x i32> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_sle(<16 x i32>, <16 x i32>) nounwind
define i32 @cmpi32_sle(<16 x i32> %a, <16 x i32> %b) nounwind {
; CHECK-LABEL: cmpi32_sle
; CHECK: cmple_i32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_sle(<16 x i32> %a, <16 x i32> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_eq(<16 x i32>, <16 x i32>) nounwind
define i32 @cmpi32_eq(<16 x i32> %a, <16 x i32> %b) nounwind {
; CHECK-LABEL: cmpi32_eq
; CHECK: cmpeq_i32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_eq(<16 x i32> %a, <16 x i32> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_ne(<16 x i32>, <16 x i32>) nounwind
define i32 @cmpi32_ne(<16 x i32> %a, <16 x i32> %b) nounwind {
; CHECK-LABEL: cmpi32_ne
; CHECK: cmpne_i32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi32_ne(<16 x i32> %a, <16 x i32> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf32_gt(<16 x float>, <16 x float>) nounwind
define i32 @cmpf32_gt(<16 x float> %a, <16 x float> %b) nounwind {
; CHECK-LABEL: cmpf32_gt
; CHECK: cmpfgt_f32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf32_gt(<16 x float> %a, <16 x float> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf32_ge(<16 x float>, <16 x float>) nounwind
define i32 @cmpf32_ge(<16 x float> %a, <16 x float> %b) nounwind {
; CHECK-LABEL: cmpf32_ge
; CHECK: cmpfge_f32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf32_ge(<16 x float> %a, <16 x float> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf32_lt(<16 x float>, <16 x float>) nounwind
define i32 @cmpf32_lt(<16 x float> %a, <16 x float> %b) nounwind {
; CHECK-LABEL: cmpf32_lt
; CHECK: cmpflt_f32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf32_lt(<16 x float> %a, <16 x float> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf32_le(<16 x float>, <16 x float>) nounwind
define i32 @cmpf32_le(<16 x float> %a, <16 x float> %b) nounwind {
; CHECK-LABEL: cmpf32_le
; CHECK: cmpfle_f32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf32_le(<16 x float> %a, <16 x float> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf32_eq(<16 x float>, <16 x float>) nounwind
define i32 @cmpf32_eq(<16 x float> %a, <16 x float> %b) nounwind {
; CHECK-LABEL: cmpf32_eq
; CHECK: cmpfeq_f32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf32_eq(<16 x float> %a, <16 x float> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf32_ne(<16 x float>, <16 x float>) nounwind
define i32 @cmpf32_ne(<16 x float> %a, <16 x float> %b) nounwind {
; CHECK-LABEL: cmpf32_ne
; CHECK: cmpfne_f32 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf32_ne(<16 x float> %a, <16 x float> %b)
    ret i32 %c
}
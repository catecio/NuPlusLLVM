; RUN: llc -march=nuplus < %s | FileCheck %s

declare <16 x i32> @llvm.nuplus.__builtin_nuplus_shufflei32(<16 x i32>, <16 x i32>) nounwind
define <16 x i32> @shufflei32(<16 x i32> %a, <16 x i32> %ind) nounwind {
; CHECK-LABEL: shufflei32
; CHECK: shuffle_i32 v0, v0, v1
	%b = call <16 x i32> @llvm.nuplus.__builtin_nuplus_shufflei32(<16 x i32> %a, <16 x i32> %ind)
    ret <16 x i32> %b 
}

declare <16 x float> @llvm.nuplus.__builtin_nuplus_shufflef32(<16 x float>, <16 x i32>) nounwind
define <16 x float> @shufflef32(<16 x float> %a, <16 x i32> %ind) nounwind {
; CHECK-LABEL: shufflef32
; CHECK: shuffle_i32 v0, v0, v1
	%b = call <16 x float> @llvm.nuplus.__builtin_nuplus_shufflef32(<16 x float> %a, <16 x i32> %ind)
    ret <16 x float> %b 
}

declare <8 x i64> @llvm.nuplus.__builtin_nuplus_shufflei64(<8 x i64>, <8 x i64>) nounwind
define <8 x i64> @shufflei64(<8 x i64> %a, <8 x i64> %ind) nounwind {
; CHECK-LABEL: shufflei64
; CHECK: shuffle_i64 v0, v0, v1
	%b = call <8 x i64> @llvm.nuplus.__builtin_nuplus_shufflei64(<8 x i64> %a, <8 x i64> %ind)
    ret <8 x i64> %b 
}

declare <8 x double> @llvm.nuplus.__builtin_nuplus_shufflef64(<8 x double>, <8 x i64>) nounwind
define <8 x double> @shufflef64(<8 x double> %a, <8 x i64> %ind) nounwind {
; CHECK-LABEL: shufflef64
; CHECK: shuffle_i64 v0, v0, v1
	%b = call <8 x double> @llvm.nuplus.__builtin_nuplus_shufflef64(<8 x double> %a, <8 x i64> %ind)
    ret <8 x double> %b 
}
; RUN: llc -march=nuplus < %s | FileCheck %s

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_ugt(<8 x i64>, <8 x i64>) nounwind
define i32 @cmpi64_ugt(<8 x i64> %a, <8 x i64> %b) nounwind {
; CHECK-LABEL: cmpi64_ugt
; CHECK: cmpugt_i64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_ugt(<8 x i64> %a, <8 x i64> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_uge(<8 x i64>, <8 x i64>) nounwind
define i32 @cmpi64_uge(<8 x i64> %a, <8 x i64> %b) nounwind {
; CHECK-LABEL: cmpi64_uge
; CHECK: cmpuge_i64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_uge(<8 x i64> %a, <8 x i64> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_ult(<8 x i64>, <8 x i64>) nounwind
define i32 @cmpi64_ult(<8 x i64> %a, <8 x i64> %b) nounwind {
; CHECK-LABEL: cmpi64_ult
; CHECK: cmpult_i64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_ult(<8 x i64> %a, <8 x i64> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_ule(<8 x i64>, <8 x i64>) nounwind
define i32 @cmpi64_ule(<8 x i64> %a, <8 x i64> %b) nounwind {
; CHECK-LABEL: cmpi64_ule
; CHECK: cmpule_i64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_ule(<8 x i64> %a, <8 x i64> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_sgt(<8 x i64>, <8 x i64>) nounwind
define i32 @cmpi64_sgt(<8 x i64> %a, <8 x i64> %b) nounwind {
; CHECK-LABEL: cmpi64_sgt
; CHECK: cmpgt_i64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_sgt(<8 x i64> %a, <8 x i64> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_sge(<8 x i64>, <8 x i64>) nounwind
define i32 @cmpi64_sge(<8 x i64> %a, <8 x i64> %b) nounwind {
; CHECK-LABEL: cmpi64_sge
; CHECK: cmpge_i64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_sge(<8 x i64> %a, <8 x i64> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_slt(<8 x i64>, <8 x i64>) nounwind
define i32 @cmpi64_slt(<8 x i64> %a, <8 x i64> %b) nounwind {
; CHECK-LABEL: cmpi64_slt
; CHECK: cmplt_i64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_slt(<8 x i64> %a, <8 x i64> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_sle(<8 x i64>, <8 x i64>) nounwind
define i32 @cmpi64_sle(<8 x i64> %a, <8 x i64> %b) nounwind {
; CHECK-LABEL: cmpi64_sle
; CHECK: cmple_i64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_sle(<8 x i64> %a, <8 x i64> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_eq(<8 x i64>, <8 x i64>) nounwind
define i32 @cmpi64_eq(<8 x i64> %a, <8 x i64> %b) nounwind {
; CHECK-LABEL: cmpi64_eq
; CHECK: cmpeq_i64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_eq(<8 x i64> %a, <8 x i64> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_ne(<8 x i64>, <8 x i64>) nounwind
define i32 @cmpi64_ne(<8 x i64> %a, <8 x i64> %b) nounwind {
; CHECK-LABEL: cmpi64_ne
; CHECK: cmpne_i64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpi64_ne(<8 x i64> %a, <8 x i64> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf64_gt(<8 x double>, <8 x double>) nounwind
define i32 @cmpf64_gt(<8 x double> %a, <8 x double> %b) nounwind {
; CHECK-LABEL: cmpf64_gt
; CHECK: cmpfgt_f64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf64_gt(<8 x double> %a, <8 x double> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf64_ge(<8 x double>, <8 x double>) nounwind
define i32 @cmpf64_ge(<8 x double> %a, <8 x double> %b) nounwind {
; CHECK-LABEL: cmpf64_ge
; CHECK: cmpfge_f64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf64_ge(<8 x double> %a, <8 x double> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf64_lt(<8 x double>, <8 x double>) nounwind
define i32 @cmpf64_lt(<8 x double> %a, <8 x double> %b) nounwind {
; CHECK-LABEL: cmpf64_lt
; CHECK: cmpflt_f64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf64_lt(<8 x double> %a, <8 x double> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf64_le(<8 x double>, <8 x double>) nounwind
define i32 @cmpf64_le(<8 x double> %a, <8 x double> %b) nounwind {
; CHECK-LABEL: cmpf64_le
; CHECK: cmpfle_f64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf64_le(<8 x double> %a, <8 x double> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf64_eq(<8 x double>, <8 x double>) nounwind
define i32 @cmpf64_eq(<8 x double> %a, <8 x double> %b) nounwind {
; CHECK-LABEL: cmpf64_eq
; CHECK: cmpfeq_f64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf64_eq(<8 x double> %a, <8 x double> %b)
    ret i32 %c
}

declare i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf64_ne(<8 x double>, <8 x double>) nounwind
define i32 @cmpf64_ne(<8 x double> %a, <8 x double> %b) nounwind {
; CHECK-LABEL: cmpf64_ne
; CHECK: cmpfne_f64 s0, v0, v1
    %c = call i32 @llvm.nuplus.__builtin_nuplus_mask_cmpf64_ne(<8 x double> %a, <8 x double> %b)
    ret i32 %c
}
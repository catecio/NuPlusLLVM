; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i8> @and_v16i8(<16 x i8> %a, <16 x i8> %b) {
; CHECK-LABEL: and_v16i8:
; CHECK:       and_i32 v0, v0, v1


  %r = and <16 x i8> %a, %b
  ret <16 x i8> %r
}

define <16 x i16> @and_v16i16(<16 x i16> %a, <16 x i16> %b) {
; CHECK-LABEL: and_v16i16:
; CHECK:       and_i32 v0, v0, v1


  %r = and <16 x i16> %a, %b
  ret <16 x i16> %r
}


define <8 x i8> @and_v8i8(<8 x i8> %a, <8 x i8> %b) {
; CHECK-LABEL: and_v8i8:
; CHECK:       and_i64 v0, v0, v1

  %r = and <8 x i8> %a, %b
  ret <8 x i8> %r
}

define <8 x i16> @and_v8i16(<8 x i16> %a, <8 x i16> %b) {
; CHECK-LABEL: and_v8i16:
; CHECK:       and_i64 v0, v0, v1

  %r = and <8 x i16> %a, %b
  ret <8 x i16> %r
}

define <8 x i32> @and_v8i32(<8 x i32> %a, <8 x i32> %b) {
; CHECK-LABEL: and_v8i32:
; CHECK:       and_i64 v0, v0, v1

  %r = and <8 x i32> %a, %b
  ret <8 x i32> %r
}

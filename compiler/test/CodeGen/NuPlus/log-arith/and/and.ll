; RUN: llc -march=nuplus < %s | FileCheck %s

define signext i1 @and_i1(i1 signext %a, i1 signext %b) {
entry:
; CHECK-LABEL: and_i1:
; CHECK:       and_i32 s0, s0, s1

  %r = and i1 %a, %b
  ret i1 %r
}

define signext i8 @and_i8(i8 signext %a, i8 signext %b) {
entry:
; CHECK-LABEL: and_i8:
; CHECK:       and_i32 s0, s0, s1

  %r = and i8 %a, %b
  ret i8 %r
}

define signext i16 @and_i16(i16 signext %a, i16 signext %b) {
entry:
; CHECK-LABEL: and_i16:
; CHECK:       and_i32 s0, s0, s1

  %r = and i16 %a, %b
  ret i16 %r
}

define signext i32 @and_i32(i32 signext %a, i32 signext %b) {
entry:
; CHECK-LABEL: and_i32:
; CHECK:       and_i32 s0, s0, s1

  %r = and i32 %a, %b
  ret i32 %r
}

define signext i64 @and_i64(i64 signext %a, i64 signext %b) {
entry:
; CHECK-LABEL: and_i64:
; CHECK:       and_i64 s0_64, s0_64, s2_64

  %r = and i64 %a, %b
  ret i64 %r
}


define signext i1 @and_i1_4(i1 signext %b) {
entry:
; CHECK-LABEL: and_i1_4:
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 0

  %r = and i1 4, %b
  ret i1 %r
}

define signext i8 @and_i8_4(i8 signext %b) {
entry:
; CHECK-LABEL: and_i8_4:
; CHECK:       andi s0, s0, 4

  %r = and i8 4, %b
  ret i8 %r
}

define signext i16 @and_i16_4(i16 signext %b) {
entry:
; CHECK-LABEL: and_i16_4:
; CHECK:       andi s0, s0, 4

  %r = and i16 4, %b
  ret i16 %r
}

define signext i32 @and_i32_4(i32 signext %b) {
entry:
; CHECK-LABEL: and_i32_4:
; CHECK:       andi s0, s0, 4

  %r = and i32 4, %b
  ret i32 %r
}

define signext i64 @and_i64_4(i64 signext %b) {
entry:
; CHECK-LABEL: and_i64_4:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 4
; CHECK:       and_i64 s0_64, s0_64, s2_64

  %r = and i64 4, %b
  ret i64 %r
}

define signext i1 @and_i1_31(i1 signext %b) {
entry:
; CHECK-LABEL: and_i1_31:
; CHECK:       jret

  %r = and i1 31, %b
  ret i1 %r
}

define signext i8 @and_i8_31(i8 signext %b) {
entry:
; CHECK-LABEL: and_i8_31:
; CHECK:       andi s0, s0, 31

  %r = and i8 31, %b
  ret i8 %r
}

define signext i16 @and_i16_31(i16 signext %b) {
entry:
; CHECK-LABEL: and_i16_31:
; CHECK:       andi s0, s0, 31

  %r = and i16 31, %b
  ret i16 %r
}

define signext i32 @and_i32_31(i32 signext %b) {
entry:
; CHECK-LABEL: and_i32_31:
; CHECK:       andi s0, s0, 31

  %r = and i32 31, %b
  ret i32 %r
}

define signext i64 @and_i64_31(i64 signext %b) {
entry:
; CHECK-LABEL: and_i64_31:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 31
; CHECK:       and_i64 s0_64, s0_64, s2_64

  %r = and i64 31, %b
  ret i64 %r
}

define signext i1 @and_i1_255(i1 signext %b) {
entry:
; CHECK-LABEL: and_i1_255:
; CHECK:       jret

  %r = and i1 255, %b
  ret i1 %r
}

define signext i8 @and_i8_255(i8 signext %b) {
entry:
; CHECK-LABEL: and_i8_255:
; CHECK:       jret

  %r = and i8 255, %b
  ret i8 %r
}

define signext i16 @and_i16_255(i16 signext %b) {
entry:
; CHECK-LABEL: and_i16_255:
; CHECK:       andi s0, s0, 255

  %r = and i16 255, %b
  ret i16 %r
}

define signext i32 @and_i32_255(i32 signext %b) {
entry:
; CHECK-LABEL: and_i32_255:
; CHECK:       andi s0, s0, 255

  %r = and i32 255, %b
  ret i32 %r
}

define signext i64 @and_i64_255(i64 signext %b) {
entry:
; CHECK-LABEL: and_i64_255:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 255
; CHECK:       and_i64 s0_64, s0_64, s2_64

  %r = and i64 255, %b
  ret i64 %r
}


define signext i1 @and_i1_32768(i1 signext %b) {
entry:
; CHECK-LABEL: and_i1_32768:
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 0

  %r = and i1 32768, %b
  ret i1 %r
}

define signext i8 @and_i8_32768(i8 signext %b) {
entry:
; CHECK-LABEL: and_i8_32768:
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 0

  %r = and i8 32768, %b
  ret i8 %r
}

define signext i16 @and_i16_32768(i16 signext %b) {
entry:
; CHECK-LABEL: and_i16_32768:
; CHECK:       moveih s1, 65535
; CHECK:       moveil s1, 32768
; CHECK:       and_i32 s0, s0, s1

  %r = and i16 32768, %b
  ret i16 %r
}

define signext i32 @and_i32_32768(i32 signext %b) {
entry:
; CHECK-LABEL: and_i32_32768:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 32768
; CHECK:       and_i32 s0, s0, s1

  %r = and i32 32768, %b
  ret i32 %r
}

define signext i64 @and_i64_32768(i64 signext %b) {
entry:
; CHECK-LABEL: and_i64_32768:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 32768
; CHECK:       and_i64 s0_64, s0_64, s2_64

  %r = and i64 32768, %b
  ret i64 %r
}

define signext i1 @and_i1_65(i1 signext %b) {
entry:
; CHECK-LABEL: and_i1_65:
; CHECK:       jret

  %r = and i1 65, %b
  ret i1 %r
}

define signext i8 @and_i8_65(i8 signext %b) {
entry:
; CHECK-LABEL: and_i8_65:
; CHECK:       andi s0, s0, 65

  %r = and i8 65, %b
  ret i8 %r
}

define signext i16 @and_i16_65(i16 signext %b) {
entry:
; CHECK-LABEL: and_i16_65:
; CHECK:       andi s0, s0, 65

  %r = and i16 65, %b
  ret i16 %r
}

define signext i32 @and_i32_65(i32 signext %b) {
entry:
; CHECK-LABEL: and_i32_65:
; CHECK:       andi s0, s0, 65

  %r = and i32 65, %b
  ret i32 %r
}

define signext i64 @and_i64_65(i64 signext %b) {
entry:
; CHECK-LABEL: and_i64_65:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 65
; CHECK:       and_i64 s0_64, s0_64, s2_64

  %r = and i64 65, %b
  ret i64 %r
}


define signext i1 @and_i1_256(i1 signext %b) {
entry:
; CHECK-LABEL: and_i1_256:
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 0

  %r = and i1 256, %b
  ret i1 %r
}

define signext i8 @and_i8_256(i8 signext %b) {
entry:
; CHECK-LABEL: and_i8_256:
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 0

  %r = and i8 256, %b
  ret i8 %r
}

define signext i16 @and_i16_256(i16 signext %b) {
entry:
; CHECK-LABEL: and_i16_256:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 256
; CHECK:       and_i32 s0, s0, s1

  %r = and i16 256, %b
  ret i16 %r
}

define signext i32 @and_i32_256(i32 signext %b) {
entry:
; CHECK-LABEL: and_i32_256:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 256
; CHECK:       and_i32 s0, s0, s1

  %r = and i32 256, %b
  ret i32 %r
}

define signext i64 @and_i64_256(i64 signext %b) {
entry:
; CHECK-LABEL: and_i64_256:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 256
; CHECK:       and_i64 s0_64, s0_64, s2_64

  %r = and i64 256, %b
  ret i64 %r
}
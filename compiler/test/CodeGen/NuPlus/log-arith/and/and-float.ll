; RUN: llc -march=nuplus < %s | FileCheck %s

define signext i1 @and_i1(i1 signext %a, i1 signext %b) {
entry:
; CHECK-LABEL: and_i1:
; CHECK:       and_i32 s0, s0, s1

  %r = and i1 %a, %b
  ret i1 %r
}

define signext i8 @and_i8(i8 signext %a, i8 signext %b) {
entry:
; CHECK-LABEL: and_i8:
; CHECK:       and_i32 s0, s0, s1

  %r = and i8 %a, %b
  ret i8 %r
}
; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x float> @fsub_v16f32(<16 x float> %a, <16 x float> %b) {
; CHECK-LABEL: fsub_v16f32:
; CHECK:       fsub_f32 v0, v0, v1

  %r = fsub <16 x float> %a, %b
  ret <16 x float> %r
}

define <8 x double> @fsub_v8f64(<8 x double> %a, <8 x double> %b) {
; CHECK-LABEL: fsub_v8f64:
; CHECK:       fsub_f64 v0, v0, v1

  %r = fsub <8 x double> %a, %b
  ret <8 x double> %r
}

define <16 x float> @fsub_v16f32_4(<16 x float> %a) {
; CHECK-LABEL: fsub_v16f32_4:
; CHECK:       moveih s0, 16512
; CHECK:       moveil s0, 0
; CHECK:       fsub_f32 v0, v0, s0

  %r = fsub <16 x float> %a, <float 4.000000e+00, float 4.000000e+00, float 4.000000e+00, float 4.000000e+00, float 4.000000e+00, float 4.000000e+00, float 4.000000e+00, float 4.000000e+00, float 4.000000e+00, float 4.000000e+00, float 4.000000e+00, float 4.000000e+00, float 4.000000e+00, float 4.000000e+00, float 4.000000e+00, float 4.000000e+00>
  ret <16 x float> %r
}

define <8 x double> @fsub_v8f64_4(<8 x double> %a) {
; CHECK-LABEL: fsub_v8f64_4:
; CHECK:       moveih s1, 16400
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 0
; CHECK:       fsub_f64 v0, v0, s0_64

  %r = fsub <8 x double> %a, <double 4.000000e+00, double 4.000000e+00, double 4.000000e+00, double 4.000000e+00, double 4.000000e+00, double 4.000000e+00, double 4.000000e+00, double 4.000000e+00>
  ret <8 x double> %r
}

define <16 x float> @fsub_v16f32_3(<16 x float> %a) {
; CHECK-LABEL: fsub_v16f32_3:
; CHECK:       moveih s0, 16448
; CHECK:       moveil s0, 0
; CHECK:       fsub_f32 v0, v0, s0


  %r = fsub <16 x float> %a, <float 3.000000e+00, float 3.000000e+00, float 3.000000e+00, float 3.000000e+00, float 3.000000e+00, float 3.000000e+00, float 3.000000e+00, float 3.000000e+00, float 3.000000e+00, float 3.000000e+00, float 3.000000e+00, float 3.000000e+00, float 3.000000e+00, float 3.000000e+00, float 3.000000e+00, float 3.000000e+00>
  ret <16 x float> %r
}

define <8 x double> @fsub_v8f64_3(<8 x double> %a) {
; CHECK-LABEL: fsub_v8f64_3:
; CHECK:       moveih s1, 16392
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 0
; CHECK:       fsub_f64 v0, v0, s0_64

  %r = fsub <8 x double> %a, <double 3.000000e+00, double 3.000000e+00, double 3.000000e+00, double 3.000000e+00, double 3.000000e+00, double 3.000000e+00, double 3.000000e+00, double 3.000000e+00>
  ret <8 x double> %r
}
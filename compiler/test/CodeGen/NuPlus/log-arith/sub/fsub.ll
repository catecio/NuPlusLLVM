; RUN: llc -march=nuplus < %s | FileCheck %s

define float @fsub_float(float %a, float %b) {
entry:
; CHECK-LABEL: fsub_float:
; CHECK:       fsub_f32 s0, s0, s1

  %r = fsub float %a, %b
  ret float %r
}

define double @fsub_double(double %a, double %b) {
entry:
; CHECK-LABEL: fsub_double:
; CHECK:       fsub_f64 s0_64, s0_64, s2_64

  %r = fsub double %a, %b
  ret double %r
}

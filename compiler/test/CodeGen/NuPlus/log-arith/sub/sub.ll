; RUN: llc -march=nuplus < %s | FileCheck %s

define signext i1 @sub_i1(i1 signext %a, i1 signext %b) {
entry:
; CHECK-LABEL: sub_i1:
; CHECK:       sub_i32 s0, s0, s1
; CHECK:       shli s0, s0, 31
; CHECK:       ashri s0, s0, 31

  %r = sub i1 %a, %b
  ret i1 %r
}

define signext i8 @sub_i8(i8 signext %a, i8 signext %b) {
entry:
; CHECK-LABEL: sub_i8:
; CHECK:       sub_i32 s0, s0, s1
; CHECK:       sext8_i32 s0, s0

  %r = sub i8 %a, %b
  ret i8 %r
}

define signext i16 @sub_i16(i16 signext %a, i16 signext %b) {
entry:
; CHECK-LABEL: sub_i16:
; CHECK:       sub_i32 s0, s0, s1
; CHECK:       sext16_i32 s0, s0

  %r = sub i16 %a, %b
  ret i16 %r
}

define signext i32 @sub_i32(i32 signext %a, i32 signext %b) {
entry:
; CHECK-LABEL: sub_i32:
; CHECK:       sub_i32 s0, s0, s1

  %r = sub i32 %a, %b
  ret i32 %r
}

define signext i64 @sub_i64(i64 signext %a, i64 signext %b) {
entry:
; CHECK-LABEL: sub_i64:
; CHECK:       sub_i64 s0_64, s0_64, s2_64

  %r = sub i64 %a, %b
  ret i64 %r
}

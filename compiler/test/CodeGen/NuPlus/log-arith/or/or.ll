; RUN: llc -march=nuplus < %s | FileCheck %s

define signext i1 @or_i1(i1 signext %a, i1 signext %b) {
entry:
; CHECK-LABEL: or_i1:
; CHECK:       or_i32 s0, s0, s1

  %r = or i1 %a, %b
  ret i1 %r
}

define signext i8 @or_i8(i8 signext %a, i8 signext %b) {
entry:
; CHECK-LABEL: or_i8:
; CHECK:       or_i32 s0, s0, s1

  %r = or i8 %a, %b
  ret i8 %r
}

define signext i16 @or_i16(i16 signext %a, i16 signext %b) {
entry:
; CHECK-LABEL: or_i16:
; CHECK:       or_i32 s0, s0, s1

  %r = or i16 %a, %b
  ret i16 %r
}

define signext i32 @or_i32(i32 signext %a, i32 signext %b) {
entry:
; CHECK-LABEL: or_i32:
; CHECK:       or_i32 s0, s0, s1

  %r = or i32 %a, %b
  ret i32 %r
}

define signext i64 @or_i64(i64 signext %a, i64 signext %b) {
entry:
; CHECK-LABEL: or_i64:
; CHECK:       or_i64 s0_64, s0_64, s2_64

  %r = or i64 %a, %b
  ret i64 %r
}

define signext i1 @or_i1_4(i1 signext %b) {
entry:
; CHECK-LABEL: or_i1_4:
; CHECK:       jret

  %r = or i1 4, %b
  ret i1 %r
}

define signext i8 @or_i8_4(i8 signext %b) {
entry:
; CHECK-LABEL: or_i8_4:
; CHECK:       ori s0, s0, 4

  %r = or i8 4, %b
  ret i8 %r
}

define signext i16 @or_i16_4(i16 signext %b) {
entry:
; CHECK-LABEL: or_i16_4:
; CHECK:       ori s0, s0, 4

  %r = or i16 4, %b
  ret i16 %r
}

define signext i32 @or_i32_4(i32 signext %b) {
entry:
; CHECK-LABEL: or_i32_4:
; CHECK:       ori s0, s0, 4

  %r = or i32 4, %b
  ret i32 %r
}

define signext i64 @or_i64_4(i64 signext %b) {
entry:
; CHECK-LABEL: or_i64_4:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 4
; CHECK:       or_i64 s0_64, s0_64, s2_64

  %r = or i64 4, %b
  ret i64 %r
}

define signext i1 @or_i1_31(i1 signext %b) {
entry:
; CHECK-LABEL: or_i1_31:
; CHECK:       moveih s0, 65535
; CHECK:       moveil s0, 65535

  %r = or i1 31, %b
  ret i1 %r
}

define signext i8 @or_i8_31(i8 signext %b) {
entry:
; CHECK-LABEL: or_i8_31:
; CHECK:       ori s0, s0, 31

  %r = or i8 31, %b
  ret i8 %r
}

define signext i16 @or_i16_31(i16 signext %b) {
entry:
; CHECK-LABEL: or_i16_31:
; CHECK:       ori s0, s0, 31

  %r = or i16 31, %b
  ret i16 %r
}

define signext i32 @or_i32_31(i32 signext %b) {
entry:
; CHECK-LABEL: or_i32_31:
; CHECK:       ori s0, s0, 31

  %r = or i32 31, %b
  ret i32 %r
}

define signext i64 @or_i64_31(i64 signext %b) {
entry:
; CHECK-LABEL: or_i64_31:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 31
; CHECK:       or_i64 s0_64, s0_64, s2_64

  %r = or i64 31, %b
  ret i64 %r
}

define signext i1 @or_i1_255(i1 signext %b) {
entry:
; CHECK-LABEL: or_i1_255:
; CHECK:       moveih s0, 65535
; CHECK:       moveil s0, 65535

  %r = or i1 255, %b
  ret i1 %r
}

define signext i8 @or_i8_255(i8 signext %b) {
entry:
; CHECK-LABEL: or_i8_255:
; CHECK:       moveih s0, 65535
; CHECK:       moveil s0, 65535

  %r = or i8 255, %b
  ret i8 %r
}

define signext i16 @or_i16_255(i16 signext %b) {
entry:
; CHECK-LABEL: or_i16_255:
; CHECK:       ori s0, s0, 255

  %r = or i16 255, %b
  ret i16 %r
}

define signext i32 @or_i32_255(i32 signext %b) {
entry:
; CHECK-LABEL: or_i32_255:
; CHECK:       ori s0, s0, 255

  %r = or i32 255, %b
  ret i32 %r
}

define signext i64 @or_i64_255(i64 signext %b) {
entry:
; CHECK-LABEL: or_i64_255:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 255
; CHECK:       or_i64 s0_64, s0_64, s2_64

  %r = or i64 255, %b
  ret i64 %r
}

define signext i1 @or_i1_32768(i1 signext %b) {
entry:
; CHECK-LABEL: or_i1_32768:
; CHECK:       jret

  %r = or i1 32768, %b
  ret i1 %r
}

define signext i8 @or_i8_32768(i8 signext %b) {
entry:
; CHECK-LABEL: or_i8_32768:
; CHECK:       jret

  %r = or i8 32768, %b
  ret i8 %r
}

define signext i16 @or_i16_32768(i16 signext %b) {
entry:
; CHECK-LABEL: or_i16_32768:
; CHECK:       moveih s1, 65535
; CHECK:       moveil s1, 32768
; CHECK:       or_i32 s0, s0, s1

  %r = or i16 32768, %b
  ret i16 %r
}

define signext i32 @or_i32_32768(i32 signext %b) {
entry:
; CHECK-LABEL: or_i32_32768:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 32768
; CHECK:       or_i32 s0, s0, s1

  %r = or i32 32768, %b
  ret i32 %r
}

define signext i64 @or_i64_32768(i64 signext %b) {
entry:
; CHECK-LABEL: or_i64_32768:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 32768
; CHECK:       or_i64 s0_64, s0_64, s2_64

  %r = or i64 32768, %b
  ret i64 %r
}

define signext i1 @or_i1_65(i1 signext %b) {
entry:
; CHECK-LABEL: or_i1_65:
; CHECK:       moveih s0, 65535
; CHECK:       moveil s0, 65535

  %r = or i1 65, %b
  ret i1 %r
}

define signext i8 @or_i8_65(i8 signext %b) {
entry:
; CHECK-LABEL: or_i8_65:
; CHECK:       ori s0, s0, 65

  %r = or i8 65, %b
  ret i8 %r
}

define signext i16 @or_i16_65(i16 signext %b) {
entry:
; CHECK-LABEL: or_i16_65:
; CHECK:       ori s0, s0, 65

  %r = or i16 65, %b
  ret i16 %r
}

define signext i32 @or_i32_65(i32 signext %b) {
entry:
; CHECK-LABEL: or_i32_65:
; CHECK:       ori s0, s0, 65

  %r = or i32 65, %b
  ret i32 %r
}

define signext i64 @or_i64_65(i64 signext %b) {
entry:
; CHECK-LABEL: or_i64_65:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 65
; CHECK:       or_i64 s0_64, s0_64, s2_64

  %r = or i64 65, %b
  ret i64 %r
}

define signext i1 @or_i1_256(i1 signext %b) {
entry:
; CHECK-LABEL: or_i1_256:
; CHECK:       jret

  %r = or i1 256, %b
  ret i1 %r
}

define signext i8 @or_i8_256(i8 signext %b) {
entry:
; CHECK-LABEL: or_i8_256:
; CHECK:       jret

  %r = or i8 256, %b
  ret i8 %r
}

define signext i16 @or_i16_256(i16 signext %b) {
entry:
; CHECK-LABEL: or_i16_256:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 256
; CHECK:       or_i32 s0, s0, s1

  %r = or i16 256, %b
  ret i16 %r
}

define signext i32 @or_i32_256(i32 signext %b) {
entry:
; CHECK-LABEL: or_i32_256:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 256
; CHECK:       or_i32 s0, s0, s1

  %r = or i32 256, %b
  ret i32 %r
}

define signext i64 @or_i64_256(i64 signext %b) {
entry:
; CHECK-LABEL: or_i64_256:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 256
; CHECK:       or_i64 s0_64, s0_64, s2_64

  %r = or i64 256, %b
  ret i64 %r
}
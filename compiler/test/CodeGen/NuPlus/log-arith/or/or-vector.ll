; RUN: llc -march=nuplus < %s | FileCheck %s

define  <16 x i32> @or_v16i32(<16 x i32>  %a, <16 x i32>  %b) {
entry:
; CHECK-LABEL: or_v16i32:
; CHECK:       or_i32 v0, v0, v1

  %r = or <16 x i32> %a, %b
  ret <16 x i32> %r
}

define  <8 x i64> @or_v8i64(<8 x i64>  %a, <8 x i64>  %b) {
entry:
; CHECK-LABEL: or_v8i64:
; CHECK:       or_i64 v0, v0, v1

  %r = or <8 x i64> %a, %b
  ret <8 x i64> %r
}


define  <16 x i32> @or_v16i32_4(<16 x i32>  %b) {
entry:
; CHECK-LABEL: or_v16i32_4:
; CHECK:       ori v0, v0, 4

  %r = or <16 x i32> <i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4>, %b
  ret <16 x i32> %r
}

define  <8 x i64> @or_v8i64_4(<8 x i64>  %b) {
entry:
; CHECK-LABEL: or_v8i64_4:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 4
; CHECK:       or_i64 v0, v0, s0_64

  %r = or <8 x i64> <i64 4, i64 4, i64 4, i64 4, i64 4, i64 4, i64 4, i64 4>, %b
  ret <8 x i64> %r
}

define  <16 x i32> @or_v16i32_31(<16 x i32>  %b) {
entry:
; CHECK-LABEL: or_v16i32_31:
; CHECK:       ori v0, v0, 31

  %r = or <16 x i32> <i32 31, i32 31, i32 31, i32 31, i32 31, i32 31, i32 31, i32 31, i32 31, i32 31, i32 31, i32 31, i32 31, i32 31, i32 31, i32 31>, %b
  ret <16 x i32> %r
}

define  <8 x i64> @or_v8i64_31(<8 x i64>  %b) {
entry:
; CHECK-LABEL: or_v8i64_31:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 31
; CHECK:       or_i64 v0, v0, s0_64

  %r = or <8 x i64> <i64 31, i64 31, i64 31, i64 31, i64 31, i64 31, i64 31, i64 31>, %b
  ret <8 x i64> %r
}


define  <16 x i32> @or_v16i32_255(<16 x i32>  %b) {
entry:
; CHECK-LABEL: or_v16i32_255:
; CHECK:       ori v0, v0, 255

  %r = or <16 x i32> <i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255, i32 255>, %b
  ret <16 x i32> %r
}

define  <8 x i64> @or_v8i64_255(<8 x i64>  %b) {
entry:
; CHECK-LABEL: or_v8i64_255:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 255
; CHECK:       or_i64 v0, v0, s0_64

  %r = or <8 x i64> <i64 255, i64 255, i64 255, i64 255, i64 255, i64 255, i64 255, i64 255>, %b
  ret <8 x i64> %r
}

define  <16 x i32> @or_v16i32_32768(<16 x i32>  %b) {
entry:
; CHECK-LABEL: or_v16i32_32768:
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 32768
; CHECK:       or_i32 v0, v0, s0

  %r = or <16 x i32> <i32 32768, i32 32768, i32 32768, i32 32768, i32 32768, i32 32768, i32 32768, i32 32768, i32 32768, i32 32768, i32 32768, i32 32768, i32 32768, i32 32768, i32 32768, i32 32768>, %b
  ret <16 x i32> %r
}

define  <8 x i64> @or_v8i64_32768(<8 x i64>  %b) {
entry:
; CHECK-LABEL: or_v8i64_32768:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 32768
; CHECK:       or_i64 v0, v0, s0_64

  %r = or <8 x i64> <i64 32768, i64 32768, i64 32768, i64 32768, i64 32768, i64 32768, i64 32768, i64 32768>, %b
  ret <8 x i64> %r
}

define  <16 x i32> @or_v16i32_65(<16 x i32>  %b) {
entry:
; CHECK-LABEL: or_v16i32_65:
; CHECK:       ori v0, v0, 65

  %r = or <16 x i32> <i32 65, i32 65, i32 65, i32 65, i32 65, i32 65, i32 65, i32 65, i32 65, i32 65, i32 65, i32 65, i32 65, i32 65, i32 65, i32 65>, %b
  ret <16 x i32> %r
}

define  <8 x i64> @or_v8i64_65(<8 x i64>  %b) {
entry:
; CHECK-LABEL: or_v8i64_65:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 65
; CHECK:       or_i64 v0, v0, s0_64

  %r = or <8 x i64> <i64 65, i64 65, i64 65, i64 65, i64 65, i64 65, i64 65, i64 65>, %b
  ret <8 x i64> %r
}

define  <16 x i32> @or_v16i32_256(<16 x i32>  %b) {
entry:
; CHECK-LABEL: or_v16i32_256:
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 256
; CHECK:       or_i32 v0, v0, s0

  %r = or <16 x i32> <i32 256, i32 256, i32 256, i32 256, i32 256, i32 256, i32 256, i32 256, i32 256, i32 256, i32 256, i32 256, i32 256, i32 256, i32 256, i32 256>, %b
  ret <16 x i32> %r
}

define  <8 x i64> @or_v8i64_256(<8 x i64>  %b) {
entry:
; CHECK-LABEL: or_v8i64_256:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 256
; CHECK:       or_i64 v0, v0, s0_64

  %r = or <8 x i64> <i64 256, i64 256, i64 256, i64 256, i64 256, i64 256, i64 256, i64 256>, %b
  ret <8 x i64> %r
}
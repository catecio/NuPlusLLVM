; RUN: llc -march=nuplus < %s | FileCheck %s

define float @fdiv_float(float %a, float %b) {
entry:
; CHECK-LABEL: fdiv_float:
; CHECK:       fdiv_f32 s0, s0, s1

  %r = fdiv float %a, %b
  ret float %r
}

define double @fdiv_double(double %a, double %b) {
entry:
; CHECK-LABEL: fdiv_double:
; CHECK:       fdiv_f64 s0_64, s0_64, s2_64

  %r = fdiv double %a, %b
  ret double %r
}

define float @fdiv_float_4(float %a) {
entry:
; CHECK-LABEL: fdiv_float_4:
; CHECK:       moveih s1, 16512
; CHECK:       moveil s1, 0
; CHECK:       fdiv_f32 s0, s0, s1

  %r = fdiv float %a, 4.000000e+00
  ret float %r
}

define double @fdiv_double_4(double %a) {
entry:
; CHECK-LABEL: fdiv_double_4:
; CHECK:       moveih s3, 16400
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 0
; CHECK:       fdiv_f64 s0_64, s0_64, s2_64

  %r = fdiv double %a, 4.000000e+00
  ret double %r
}
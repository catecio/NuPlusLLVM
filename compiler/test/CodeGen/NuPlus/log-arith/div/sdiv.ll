; RUN: llc -march=nuplus < %s | FileCheck %s

define signext i1 @sdiv_i1(i1 signext %a, i1 signext %b) {
entry:
; CHECK-LABEL: sdiv_i1:
; CHECK:       jmpsr __divsi3
; CHECK:       shli s0, s0, 31
; CHECK:       ashri s0, s0, 31

  %r = sdiv i1 %a, %b
  ret i1 %r
}

define signext i8 @sdiv_i8(i8 signext %a, i8 signext %b) {
entry:
; CHECK-LABEL: sdiv_i8:
; CHECK:       jmpsr __divsi3
; CHECK:       sext8_i32 s0, s0

  %r = sdiv i8 %a, %b
  ret i8 %r
}

define signext i16 @sdiv_i16(i16 signext %a, i16 signext %b) {
entry:
; CHECK-LABEL: sdiv_i16:
; CHECK:       jmpsr __divsi3
; CHECK:       sext16_i32 s0, s0

  %r = sdiv i16 %a, %b
  ret i16 %r
}

define signext i32 @sdiv_i32(i32 signext %a, i32 signext %b) {
entry:
; CHECK-LABEL: sdiv_i32:
; CHECK:       jmpsr __divsi3

  %r = sdiv i32 %a, %b
  ret i32 %r
}

define signext i64 @sdiv_i64(i64 signext %a, i64 signext %b) {
entry:
; CHECK-LABEL: sdiv_i64:
; CHECK:       jmpsr __divdi3

  %r = sdiv i64 %a, %b
  ret i64 %r
}

define signext i1 @sdiv_i1_4(i1 signext %a) {
entry:
; CHECK-LABEL: sdiv_i1_4:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       jmpsr __divsi3
; CHECK:       shli s0, s0, 31
; CHECK:       ashri s0, s0, 31

  %r = sdiv i1 %a, 4
  ret i1 %r
}

define signext i8 @sdiv_i8_4(i8 signext %a) {
entry:
; CHECK-LABEL: sdiv_i8_4:
; CHECK:       shri s1, s0, 13
; CHECK:       andi s1, s1, 3
; CHECK:       add_i32 s0, s0, s1
; CHECK:       sext8_i32 s0, s0
; CHECK:       ashri s0, s0, 2

  %r = sdiv i8 %a, 4
  ret i8 %r
}

define signext i16 @sdiv_i16_4(i16 signext %a) {
entry:
; CHECK-LABEL: sdiv_i16_4:
; CHECK:       shri s1, s0, 29
; CHECK:       andi s1, s1, 3
; CHECK:       add_i32 s0, s0, s1
; CHECK:       sext16_i32 s0, s0
; CHECK:       ashri s0, s0, 2

  %r = sdiv i16 %a, 4
  ret i16 %r
}

define signext i32 @sdiv_i32_4(i32 signext %a) {
entry:
; CHECK-LABEL: sdiv_i32_4:
; CHECK:       ashri s1, s0, 31
; CHECK:       shri s1, s1, 30
; CHECK:       add_i32 s0, s0, s1
; CHECK:       ashri s0, s0, 2

  %r = sdiv i32 %a, 4
  ret i32 %r
}

define signext i64 @sdiv_i64_4(i64 signext %a) {
entry:
; CHECK-LABEL: sdiv_i64_4:
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 63
; CHECK:       ashr_i64 s2_64, s0_64, s2
; CHECK:       moveih s4, 0
; CHECK:       moveil s4, 62
; CHECK:       shr_i64 s2_64, s2_64, s4
; CHECK:       add_i64 s0_64, s0_64, s2_64
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 2
; CHECK:       ashr_i64 s0_64, s0_64, s2

  %r = sdiv i64 %a, 4
  ret i64 %r
}
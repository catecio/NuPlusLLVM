; RUN: llc -march=nuplus < %s | FileCheck %s

define  <16 x float> @fdiv_v16f32(<16 x float>  %a, <16 x float>  %b) {
entry:
; CHECK-LABEL: fdiv_v16f32:
; CHECK:       fdiv_f32 v0, v0, v1

  %r = fdiv <16 x float> %a, %b
  ret <16 x float> %r
}

define  <8 x double> @fdiv_v8f64(<8 x double>  %a, <8 x double>  %b) {
entry:
; CHECK-LABEL: fdiv_v8f64:
; CHECK:       fdiv_f64 v0, v0, v1

  %r = fdiv <8 x double> %a, %b
  ret <8 x double> %r
}

; RUN: llc -march=nuplus < %s | FileCheck %s

define  <16 x i8> @udiv_v16i8(<16 x i8>  %a, <16 x i8>  %b) {
entry:
; CHECK-LABEL: udiv_v16i8:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 316
; CHECK:       add_i32 s3, sp, s3
; CHECK:       store32 s50, (s3)
; CHECK:       moveih s50, 0
; CHECK:       moveil s50, 312
; CHECK:       add_i32 s50, sp, s50
; CHECK:       store32 ra, (s50) 
; CHECK:       store_v16i32 v56, 192(sp)       
; CHECK:       store_v16i32 v57, 128(sp)       
; CHECK:       andi v56, v1, 255
; CHECK:       andi v57, v0, 255
; CHECK:       getlanei s0, v57, 15
; CHECK:       getlanei s1, v56, 15
; CHECK:       jmpsr __udivsi3
; CHECK:       _lea s50, 64(sp)
; CHECK:       ori s1, s50, 60
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 14
; CHECK:       getlanei s1, v56, 14
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 56
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 13
; CHECK:       getlanei s1, v56, 13
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 52
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 12
; CHECK:       getlanei s1, v56, 12
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 48
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 11
; CHECK:       getlanei s1, v56, 11
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 44
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 10
; CHECK:       getlanei s1, v56, 10
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 40
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 9
; CHECK:       getlanei s1, v56, 9
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 36
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 8
; CHECK:       getlanei s1, v56, 8
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 32
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 7
; CHECK:       getlanei s1, v56, 7
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 28
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 6
; CHECK:       getlanei s1, v56, 6
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 24
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 5
; CHECK:       getlanei s1, v56, 5
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 20
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 4
; CHECK:       getlanei s1, v56, 4
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 16
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 3
; CHECK:       getlanei s1, v56, 3
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 12
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 2
; CHECK:       getlanei s1, v56, 2
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 8
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 1
; CHECK:       getlanei s1, v56, 1
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 4
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 0
; CHECK:       getlanei s1, v56, 0
; CHECK:       jmpsr __udivsi3
; CHECK:       store32 s0, 64(sp)
; CHECK:       load_v16i32 v0, 64(sp)
; CHECK:       load_v16i32 v57, 128(sp)        
; CHECK:       load_v16i32 v56, 192(sp)       
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 312
; CHECK:       add_i32 s0, sp, s0
; CHECK:       load32 ra, (s0)
; CHECK:       moveih s4, 0
; CHECK:       moveil s4, 316
; CHECK:       add_i32 s4, sp, s4
; CHECK:       load32 s50, (s4)

  %r = udiv <16 x i8> %a, %b
  ret <16 x i8> %r
}

define  <16 x i16> @udiv_v16i16(<16 x i16>  %a, <16 x i16>  %b) {
entry:
; CHECK-LABEL: udiv_v16i16:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 316
; CHECK:       add_i32 s3, sp, s3
; CHECK:       store32 s50, (s3)
; CHECK:       moveih s50, 0
; CHECK:       moveil s50, 312
; CHECK:       add_i32 s50, sp, s50
; CHECK:       store32 ra, (s50)
; CHECK:       store_v16i32 v56, 192(sp)      
; CHECK:       store_v16i32 v57, 128(sp)       
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 65535
; CHECK:       and_i32 v56, v1, s0
; CHECK:       and_i32 v57, v0, s0
; CHECK:       getlanei s0, v57, 15
; CHECK:       getlanei s1, v56, 15
; CHECK:       jmpsr __udivsi3
; CHECK:       _lea s50, 64(sp)
; CHECK:       ori s1, s50, 60
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 14
; CHECK:       getlanei s1, v56, 14
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 56
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 13
; CHECK:       getlanei s1, v56, 13
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 52
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 12
; CHECK:       getlanei s1, v56, 12
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 48
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 11
; CHECK:       getlanei s1, v56, 11
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 44
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 10
; CHECK:       getlanei s1, v56, 10
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 40
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 9
; CHECK:       getlanei s1, v56, 9
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 36
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 8
; CHECK:       getlanei s1, v56, 8
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 32
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 7
; CHECK:       getlanei s1, v56, 7
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 28
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 6
; CHECK:       getlanei s1, v56, 6
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 24
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 5
; CHECK:       getlanei s1, v56, 5
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 20
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 4
; CHECK:       getlanei s1, v56, 4
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 16
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 3
; CHECK:       getlanei s1, v56, 3
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 12
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 2
; CHECK:       getlanei s1, v56, 2
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 8
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 1
; CHECK:       getlanei s1, v56, 1
; CHECK:       jmpsr __udivsi3
; CHECK:       ori s1, s50, 4
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 0
; CHECK:       getlanei s1, v56, 0
; CHECK:       jmpsr __udivsi3
; CHECK:       store32 s0, 64(sp)
; CHECK:       load_v16i32 v0, 64(sp)
; CHECK:       load_v16i32 v57, 128(sp)        
; CHECK:       load_v16i32 v56, 192(sp)       
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 312
; CHECK:       add_i32 s0, sp, s0
; CHECK:       load32 ra, (s0)
; CHECK:       moveih s4, 0
; CHECK:       moveil s4, 316
; CHECK:       add_i32 s4, sp, s4
; CHECK:       load32 s50, (s4)

  %r = udiv <16 x i16> %a, %b
  ret <16 x i16> %r
}

define  <8 x i8> @udiv_v8i8(<8 x i8>  %a, <8 x i8>  %b) {
entry:
; CHECK-LABEL: udiv_v8i8:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 316
; CHECK:       add_i32 s1, sp, s1
; CHECK:       store32 s50, (s1)
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 312
; CHECK:       add_i32 s3, sp, s3
; CHECK:       store32 s51, (s3)
; CHECK:       moveih s4, 0
; CHECK:       moveil s4, 308
; CHECK:       add_i32 s4, sp, s4
; CHECK:       store32 s52, (s4)
; CHECK:       moveih s52, 0
; CHECK:       moveil s52, 304
; CHECK:       add_i32 s52, sp, s52
; CHECK:       store32 ra, (s52)
; CHECK:       store_v16i32 v56, 192(sp)      
; CHECK:       store_v16i32 v57, 128(sp)       
; CHECK:       moveih s51, 0
; CHECK:       moveil s51, 0
; CHECK:       moveih s50, 0
; CHECK:       moveil s50, 255
; CHECK:       and_i64 v56, v1, s50_64
; CHECK:       and_i64 v57, v0, s50_64
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 7
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       _lea s52, 64(sp)
; CHECK:       ori s2, s52, 56
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 6
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 48
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 5
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 40
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 4
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 32
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 3
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 24
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 2
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 16
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 1
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 8
; CHECK:       store64 s0_64, (s2)
; CHECK:       getlane_i64 s0_64, v57, s51
; CHECK:       getlane_i64 s2_64, v56, s51
; CHECK:       jmpsr __udivdi3
; CHECK:       store64 s0_64, 64(sp)
; CHECK:       load_v8i64 v0, 64(sp)
; CHECK:       load_v16i32 v57, 128(sp)         
; CHECK:       load_v16i32 v56, 192(sp)        
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 304
; CHECK:       add_i32 s0, sp, s0
; CHECK:       load32 ra, (s0)
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 308
; CHECK:       add_i32 s1, sp, s1
; CHECK:       load32 s52, (s1)
; CHECK:       moveih s5, 0
; CHECK:       moveil s5, 312
; CHECK:       add_i32 s5, sp, s5
; CHECK:       load32 s51, (s5)
; CHECK:       moveih s6, 0
; CHECK:       moveil s6, 316
; CHECK:       add_i32 s6, sp, s6
; CHECK:       load32 s50, (s6)

  %r = udiv <8 x i8> %a, %b
  ret <8 x i8> %r
}

define  <8 x i16> @udiv_v8i16(<8 x i16>  %a, <8 x i16>  %b) {
entry:
; CHECK-LABEL: udiv_v8i16:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 316
; CHECK:       add_i32 s1, sp, s1
; CHECK:       store32 s50, (s1)
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 312
; CHECK:       add_i32 s3, sp, s3
; CHECK:       store32 s51, (s3)
; CHECK:       moveih s4, 0
; CHECK:       moveil s4, 308
; CHECK:       add_i32 s4, sp, s4
; CHECK:       store32 s52, (s4)
; CHECK:       moveih s52, 0
; CHECK:       moveil s52, 304
; CHECK:       add_i32 s52, sp, s52
; CHECK:       store32 ra, (s52)
; CHECK:       store_v16i32 v56, 192(sp)      
; CHECK:       store_v16i32 v57, 128(sp)       
; CHECK:       moveih s51, 0
; CHECK:       moveil s51, 0
; CHECK:       moveih s50, 0
; CHECK:       moveil s50, 65535
; CHECK:       and_i64 v56, v1, s50_64
; CHECK:       and_i64 v57, v0, s50_64
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 7
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       _lea s52, 64(sp)
; CHECK:       ori s2, s52, 56
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 6
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 48
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 5
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 40
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 4
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 32
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 3
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 24
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 2
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 16
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 1
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 8
; CHECK:       store64 s0_64, (s2)
; CHECK:       getlane_i64 s0_64, v57, s51
; CHECK:       getlane_i64 s2_64, v56, s51
; CHECK:       jmpsr __udivdi3
; CHECK:       store64 s0_64, 64(sp)
; CHECK:       load_v8i64 v0, 64(sp)
; CHECK:       load_v16i32 v57, 128(sp)         
; CHECK:       load_v16i32 v56, 192(sp)
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 304
; CHECK:       add_i32 s0, sp, s0
; CHECK:       load32 ra, (s0)
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 308
; CHECK:       add_i32 s1, sp, s1
; CHECK:       load32 s52, (s1)
; CHECK:       moveih s5, 0
; CHECK:       moveil s5, 312
; CHECK:       add_i32 s5, sp, s5
; CHECK:       load32 s51, (s5)
; CHECK:       moveih s6, 0
; CHECK:       moveil s6, 316
; CHECK:       add_i32 s6, sp, s6
; CHECK:       load32 s50, (s6)

  %r = udiv <8 x i16> %a, %b
  ret <8 x i16> %r
}

define  <8 x i32> @udiv_v8i32(<8 x i32>  %a, <8 x i32>  %b) {
entry:
; CHECK-LABEL: udiv_v8i32:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 316
; CHECK:       add_i32 s1, sp, s1
; CHECK:       store32 s50, (s1)
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 312
; CHECK:       add_i32 s3, sp, s3
; CHECK:       store32 s51, (s3)
; CHECK:       moveih s4, 0
; CHECK:       moveil s4, 308
; CHECK:       add_i32 s4, sp, s4
; CHECK:       store32 s52, (s4)
; CHECK:       moveih s52, 0
; CHECK:       moveil s52, 304
; CHECK:       add_i32 s52, sp, s52
; CHECK:       store32 ra, (s52)
; CHECK:       store_v16i32 v56, 192(sp)      
; CHECK:       store_v16i32 v57, 128(sp)       
; CHECK:       moveih s51, 0
; CHECK:       moveil s51, 0
; CHECK:       moveih s50, 65535
; CHECK:       moveil s50, 65535
; CHECK:       and_i64 v56, v1, s50_64
; CHECK:       and_i64 v57, v0, s50_64
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 7
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       _lea s52, 64(sp)
; CHECK:       ori s2, s52, 56
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 6
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 48
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 5
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 40
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 4
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 32
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 3
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 24
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 2
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 16
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 1
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __udivdi3
; CHECK:       ori s2, s52, 8
; CHECK:       store64 s0_64, (s2)
; CHECK:       getlane_i64 s0_64, v57, s51
; CHECK:       getlane_i64 s2_64, v56, s51
; CHECK:       jmpsr __udivdi3
; CHECK:       store64 s0_64, 64(sp)
; CHECK:       load_v8i64 v0, 64(sp)
; CHECK:       load_v16i32 v57, 128(sp)        
; CHECK:       load_v16i32 v56, 192(sp)         
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 304
; CHECK:       add_i32 s0, sp, s0
; CHECK:       load32 ra, (s0)
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 308
; CHECK:       add_i32 s1, sp, s1
; CHECK:       load32 s52, (s1)
; CHECK:       moveih s5, 0
; CHECK:       moveil s5, 312
; CHECK:       add_i32 s5, sp, s5
; CHECK:       load32 s51, (s5)
; CHECK:       moveih s6, 0
; CHECK:       moveil s6, 316
; CHECK:       add_i32 s6, sp, s6
; CHECK:       load32 s50, (s6)

  %r = udiv <8 x i32> %a, %b
  ret <8 x i32> %r
}

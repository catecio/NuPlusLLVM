; RUN: llc -march=nuplus < %s | FileCheck %s

define signext i1 @shl_i1(i1 signext %a, i1 signext %b) {
entry:
; CHECK-LABEL: shl_i1:
; CHECK:       jret

  %r = shl i1 %a, %b
  ret i1 %r
}

define zeroext i8 @shl_i8(i8 zeroext %a, i8 zeroext %b) {
entry:
; CHECK-LABEL: shl_i8:
; CHECK:       shl_i32 s0, s0, s1
; CHECK:       andi s0, s0, 255

  %r = shl i8 %a, %b
  ret i8 %r
}

define zeroext i16 @shl_i16(i16 zeroext %a, i16 zeroext %b) {
entry:
; CHECK-LABEL: shl_i16:
; CHECK:       shl_i32 s0, s0, s1
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 65535
; CHECK:       and_i32 s0, s0, s1

  %r = shl i16 %a, %b
  ret i16 %r
}

define signext i32 @shl_i32(i32 signext %a, i32 signext %b) {
entry:
; CHECK-LABEL: shl_i32:
; CHECK:       shl_i32 s0, s0, s1

  %r = shl i32 %a, %b
  ret i32 %r
}

define signext i64 @shl_i64(i64 signext %a, i64 signext %b) {
entry:
; CHECK-LABEL: shl_i64:
; CHECK:       shl_i64 s0_64, s0_64, s2

  %r = shl i64 %a, %b
  ret i64 %r
}
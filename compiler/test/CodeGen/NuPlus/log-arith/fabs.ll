; RUN: llc -march=nuplus < %s | FileCheck %s

define float @foo0(float %a) nounwind readnone {
entry:

; CHECK-LABEL: foo0
; moveh 0x7FFF
; movel 0xFFFF
; CHECK: moveih s1, 32767
; CHECK: moveil s1, 65535
; CHECK: and_i32 s0, s0, s1

  %call = tail call float @fabsf(float %a) nounwind readnone
  ret float %call
}

declare float @fabsf(float) nounwind readnone


define double @foo1(double %a) nounwind readnone {
entry:

; CHECK-LABEL: foo1:
; moveh 0x7FFF
; movel 0xFFFF
; moveh 0xFFFF
; movel 0xFFFF
; CHECK: moveih s3, 32767
; CHECK: moveil s3, 65535
; CHECK: moveih s2, 65535
; CHECK: moveil s2, 65535
; CHECK: and_i64 s0_64, s0_64, s2_64

  %call = tail call double @fabs(double %a) nounwind readnone
  ret double %call
}

declare double @fabs(double) nounwind readnone


define <16 x float> @foo2(<16 x float> %a) nounwind readnone {
entry:

; CHECK-LABEL: foo2:
; moveh 0x7FFF
; movel 0xFFFF
; CHECK: moveih s0, 32767
; CHECK: moveil s0, 65535
; CHECK: and_i32 v0, v0, s0

  %call = tail call <16 x float> @llvm.fabs.v16f32(<16 x float> %a) nounwind readnone
  ret <16 x float> %call
}

declare  <16 x float> @llvm.fabs.v16f32( <16 x float>  %Val) nounwind readnone


define <8 x double> @foo3(<8 x double> %a) nounwind readnone {
entry:

; CHECK-LABEL: foo3:
; moveh 0x7FFF
; movel 0xFFFF
; moveh 0xFFFF
; movel 0xFFFF
; CHECK: moveih s1, 32767
; CHECK: moveil s1, 65535
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: and_i64 v0, v0, s0_64

  %call = tail call <8 x double> @llvm.fabs.v8f64(<8 x double> %a) nounwind readnone
  ret <8 x double> %call
}

declare  <8 x double> @llvm.fabs.v8f64( <8 x double>  %Val) nounwind readnone


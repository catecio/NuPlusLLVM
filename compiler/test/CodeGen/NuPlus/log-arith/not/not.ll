; RUN: llc -march=nuplus < %s | FileCheck %s

define signext i1 @not_i1(i1 signext %a) {
entry:
; CHECK-LABEL: not_i1:
; CHECK:       xori s0, s0, -1

  %r = xor i1 %a, -1
  ret i1 %r
}

define signext i8 @not_i8(i8 signext %a) {
entry:
; CHECK-LABEL: not_i8:
; CHECK:       xori s0, s0, -1

  %r = xor i8 %a, -1
  ret i8 %r
}

define signext i16 @not_i16(i16 signext %a) {
entry:
; CHECK-LABEL: not_i16:
; CHECK:       xori s0, s0, -1

  %r = xor i16 %a, -1
  ret i16 %r
}

define signext i32 @not_i32(i32 signext %a) {
entry:
; CHECK-LABEL: not_i32:
; CHECK:       xori s0, s0, -1

  %r = xor i32 %a, -1
  ret i32 %r
}

define signext i64 @not_i64(i64 signext %a) {
entry:
; CHECK-LABEL: not_i64:
; CHECK:       moveih s3, 65535
; CHECK:       moveil s3, 65535
; CHECK:       move_i32 s2, s3
; CHECK:       xor_i64 s0_64, s0_64, s2_64

  %r = xor i64 %a, -1
  ret i64 %r
}

define signext i1 @nor_i1(i1 signext %a, i1 signext %b) {
entry:
; CHECK-LABEL: nor_i1:
; CHECK:       or_i32 s0, s1, s0
; CHECK:       xori s0, s0, -1

  %or = or i1 %b, %a
  %r = xor i1 %or, -1
  ret i1 %r
}

define signext i8 @nor_i8(i8 signext %a, i8 signext %b) {
entry:
; CHECK-LABEL: nor_i8:
; CHECK:       or_i32 s0, s1, s0
; CHECK:       xori s0, s0, -1

  %or = or i8 %b, %a
  %r = xor i8 %or, -1
  ret i8 %r
}

define signext i16 @nor_i16(i16 signext %a, i16 signext %b) {
entry:
; CHECK-LABEL: nor_i16:
; CHECK:       or_i32 s0, s1, s0
; CHECK:       xori s0, s0, -1

  %or = or i16 %b, %a
  %r = xor i16 %or, -1
  ret i16 %r
}

define signext i32 @nor_i32(i32 signext %a, i32 signext %b) {
entry:
; CHECK-LABEL: nor_i32:
; CHECK:       or_i32 s0, s1, s0
; CHECK:       xori s0, s0, -1

  %or = or i32 %b, %a
  %r = xor i32 %or, -1
  ret i32 %r
}


define signext i64 @nor_i64(i64 signext %a, i64 signext %b) {
entry:
; CHECK-LABEL: nor_i64:
; CHECK:       or_i64 s0_64, s2_64, s0_64
; CHECK:       moveih s3, 65535
; CHECK:       moveil s3, 65535
; CHECK:       move_i32 s2, s3
; CHECK:       xor_i64 s0_64, s0_64, s2_64

  %or = or i64 %b, %a
  %r = xor i64 %or, -1
  ret i64 %r
}

; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i8> @not_v16i8(<16 x i8> %a) {
; CHECK-LABEL: not_v16i8:
; CHECK:       xori v0, v0, 255

  %r = xor <16 x i8> %a, <i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1>
  ret <16 x i8> %r
}

define <16 x i16> @not_v16i16(<16 x i16> %a) {
; CHECK-LABEL: not_v16i16:
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 65535
; CHECK:       xor_i32 v0, v0, s0

  %r = xor <16 x i16> %a, <i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1>
  ret <16 x i16> %r
}

define <8 x i8> @not_v8i8(<8 x i8> %a) {
; CHECK-LABEL: not_v8i8:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 255
; CHECK:       xor_i64 v0, v0, s0_64

  %r = xor <8 x i8> %a, <i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1>
  ret <8 x i8> %r
}

define <8 x i16> @not_v8i16(<8 x i16> %a) {
; CHECK-LABEL: not_v8i16:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 65535
; CHECK:       xor_i64 v0, v0, s0_64

  %r = xor <8 x i16> %a, <i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1>
  ret <8 x i16> %r
}

define <8 x i32> @not_v8i32(<8 x i32> %a) {
; CHECK-LABEL: not_v8i32:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 65535
; CHECK:       moveil s0, 65535
; CHECK:       xor_i64 v0, v0, s0_64

  %r = xor <8 x i32> %a, <i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1>
  ret <8 x i32> %r
}

define <16 x i8> @nor_v16i8(<16 x i8> %a, <16 x i8> %b) {
; CHECK-LABEL: nor_v16i8:
; CHECK:       or_i32 v0, v1, v0
; CHECK:       xori v0, v0, 255

  %or = or <16 x i8> %b, %a
  %r = xor <16 x i8> %or, <i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1>
  ret <16 x i8> %r
}

define <16 x i16> @nor_v16i16(<16 x i16> %a, <16 x i16> %b) {
; CHECK-LABEL: nor_v16i16:
; CHECK:       or_i32 v0, v1, v0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 65535
; CHECK:       xor_i32 v0, v0, s0

  %or = or <16 x i16> %b, %a
  %r = xor <16 x i16> %or, <i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1>
  ret <16 x i16> %r
}

define <8 x i8> @nor_v8i8(<8 x i8> %a, <8 x i8> %b) {
; CHECK-LABEL: nor_v8i8:
; CHECK:       or_i64 v0, v1, v0
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 255
; CHECK:       xor_i64 v0, v0, s0_64

  %or = or <8 x i8> %b, %a
  %r = xor <8 x i8> %or, <i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1, i8 -1>
  ret <8 x i8> %r
}

define <8 x i16> @nor_v8i16(<8 x i16> %a, <8 x i16> %b) {
; CHECK-LABEL: nor_v8i16:
; CHECK:       or_i64 v0, v1, v0
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 65535
; CHECK:       xor_i64 v0, v0, s0_64

  %or = or <8 x i16> %b, %a
  %r = xor <8 x i16> %or, <i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1, i16 -1>
  ret <8 x i16> %r
}

define <8 x i32> @nor_v8i32(<8 x i32> %a, <8 x i32> %b) {
; CHECK-LABEL: nor_v8i32:
; CHECK:       or_i64 v0, v1, v0
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 65535
; CHECK:       moveil s0, 65535
; CHECK:       xor_i64 v0, v0, s0_64

  %or = or <8 x i32> %b, %a
  %r = xor <8 x i32> %or, <i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1>
  ret <8 x i32> %r
}
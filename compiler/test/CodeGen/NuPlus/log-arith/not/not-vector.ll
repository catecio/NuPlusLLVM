; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i32> @not_v16i32(<16 x i32> %a) {
; CHECK-LABEL: not_v16i32:
; CHECK:       xori v0, v0, -1

  %r = xor <16 x i32> %a, <i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1>
  ret <16 x i32> %r
}

define <8 x i64> @not_v8i64(<8 x i64> %a) {
; CHECK-LABEL: not_v8i64:
; CHECK:       moveih s1, 65535
; CHECK:       moveil s1, 65535
; CHECK:       move_i32 s0, s1
; CHECK:       xor_i64 v0, v0, s0_64

  %r = xor <8 x i64> %a, <i64 -1, i64 -1, i64 -1, i64 -1, i64 -1, i64 -1, i64 -1, i64 -1>
  ret <8 x i64> %r
}

define <16 x i32> @nor_v16i32(<16 x i32> %a, <16 x i32> %b) {
; CHECK-LABEL: nor_v16i32:
; CHECK:       or_i32 v0, v1, v0
; CHECK:       xori v0, v0, -1

  %or = or <16 x i32> %b, %a
  %r = xor <16 x i32> %or, <i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1, i32 -1>
  ret <16 x i32> %r
}

define <8 x i64> @nor_v8i64(<8 x i64> %a, <8 x i64> %b) {
; CHECK-LABEL: nor_v8i64:
; CHECK:       or_i64 v0, v1, v0
; CHECK:       moveih s1, 65535
; CHECK:       moveil s1, 65535
; CHECK:       move_i32 s0, s1
; CHECK:       xor_i64 v0, v0, s0_64

  %or = or <8 x i64> %b, %a
  %r = xor <8 x i64> %or, <i64 -1, i64 -1, i64 -1, i64 -1, i64 -1, i64 -1, i64 -1, i64 -1>
  ret <8 x i64> %r
}

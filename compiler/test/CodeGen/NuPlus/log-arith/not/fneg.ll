; RUN: llc -march=nuplus < %s | FileCheck %s

define float @foo0(float %d) nounwind readnone {
entry:
; CHECK-LABEL: foo0:
; moveh 0x8000
; movel 0x0000
; CHECK: moveih s1, 32768
; CHECK: moveil s1, 0
; CHECK: xor_i32 s0, s0, s1

  %sub = fsub float -0.000000e+00, %d
  ret float %sub
}

define double @foo1(double %d) nounwind readnone {
entry:
; CHECK-LABEL: foo1:
; moveh 0x8000
; movel 0x0000
; moveh 0x0000
; movel 0x0000
; CHECK: moveih s3, 32768
; CHECK: moveil s3, 0
; CHECK: moveih s2, 0
; CHECK: moveil s2, 0
; CHECK: xor_i64 s0_64, s0_64, s2_64

  %sub = fsub double -0.000000e+00, %d
  ret double %sub
}

define <16 x float> @foo2(<16 x float> %a) nounwind readnone {
entry:

; CHECK-LABEL: foo2:
; moveh 0x7FFF
; movel 0xFFFF
; CHECK: moveih s0, 32768
; CHECK: moveil s0, 0
; CHECK: xor_i32 v0, v0, s0

  %sub = fsub <16 x float> <float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float -0.000000e+00>,
                           %a
  ret <16 x float> %sub
}

define <8 x double> @foo3(<8 x double> %a) nounwind readnone {
entry:

; CHECK-LABEL: foo3:
; moveh 0x7FFF
; movel 0xFFFF
; moveh 0xFFFF
; movel 0xFFFF
; CHECK: moveih s1, 32768
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 0
; CHECK: xor_i64 v0, v0, s0_64

  %sub = fsub <8 x double> <double -0.000000e+00, double -0.000000e+00, double -0.000000e+00, double -0.000000e+00, double -0.000000e+00, double -0.000000e+00, double -0.000000e+00, double -0.000000e+00>,
                     %a
  ret <8 x double> %sub
}



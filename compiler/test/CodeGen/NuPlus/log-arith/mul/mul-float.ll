; RUN: llc -march=nuplus < %s | FileCheck %s

define float @mul_f32(float %a, float %b) {
; CHECK-LABEL: mul_f32:
; CHECK:       fmul_f32 s0, s0, s1


  %r = fmul float %a, %b
  ret float %r
}

define double @mul_f64(double %a, double %b) {
; CHECK-LABEL: mul_f64:
; CHECK:       fmul_f64 s0_64, s0_64, s2_64

  %r = fmul double %a, %b
  ret double %r
}
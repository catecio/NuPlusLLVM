; RUN: llc -march=nuplus < %s | FileCheck %s

define signext i1 @mul_i1(i1 signext %a, i1 signext %b) {
; CHECK-LABEL: mul_i1:
; CHECK:       mull_i32 s0, s0, s1
; CHECK:       shli s0, s0, 31
; CHECK:       ashri s0, s0, 31

  %r = mul i1 %a, %b
  ret i1 %r
}

define signext i8 @mul_i8(i8 signext %a, i8 signext %b) {
; CHECK-LABEL: mul_i8:
; CHECK:       mull_i32 s0, s0, s1
; CHECK:       sext8_i32 s0, s0

  %r = mul i8 %a, %b
  ret i8 %r
}

define signext i16 @mul_i16(i16 signext %a, i16 signext %b) {
; CHECK-LABEL: mul_i16:
; CHECK:       mull_i32 s0, s0, s1
; CHECK:       sext16_i32 s0, s0

  %r = mul i16 %a, %b
  ret i16 %r
}

define signext i32 @mul_i32(i32 signext %a, i32 signext %b) {
; CHECK-LABEL: mul_i32:
; CHECK:       mull_i32 s0, s0, s1

  %r = mul i32 %a, %b
  ret i32 %r
}

define signext i64 @mul_i64(i64 signext %a, i64 signext %b) {
; CHECK-LABEL: mul_i64:
; CHECK:       mull_i64 s0_64, s0_64, s2_64

  %r = mul i64 %a, %b
  ret i64 %r
}

define signext i1 @mul_i1_4(i1 signext %a) {
; CHECK-LABEL: mul_i1_4:
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 0
; CHECK:       jret

  %r = mul i1 4, %a
  ret i1 %r
}

define signext i8 @mul_i8_4(i8 signext %a) {
; CHECK-LABEL: mul_i8_4:
; CHECK:       shli s0, s0, 2
; CHECK:       sext8_i32 s0, s0

  %r = mul i8 4, %a
  ret i8 %r
}

define signext i16 @mul_i16_4(i16 signext %a) {
; CHECK-LABEL: mul_i16_4:
; CHECK:       shli s0, s0, 2
; CHECK:       sext16_i32 s0, s0

  %r = mul i16 4, %a
  ret i16 %r
}

define signext i32 @mul_i32_4(i32 signext %a) {
; CHECK-LABEL: mul_i32_4:
; CHECK:       shli s0, s0, 2

  %r = mul i32 4, %a
  ret i32 %r
}

define signext i64 @mul_i64_4(i64 signext %a) {
; CHECK-LABEL: mul_i64_4:
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 2
; CHECK:       shl_i64 s0_64, s0_64, s2

  %r = mul i64 4, %a
  ret i64 %r
}

define signext i1 @mul_i1_3(i1 signext %a) {
; CHECK-LABEL: mul_i1_3:
; CHECK:       jret

  %r = mul i1 3, %a
  ret i1 %r
}

define signext i8 @mul_i8_3(i8 signext %a) {
; CHECK-LABEL: mul_i8_3:
; CHECK:       mulli s0, s0, 3
; CHECK:       sext8_i32 s0, s0

  %r = mul i8 3, %a
  ret i8 %r
}

define signext i16 @mul_i16_3(i16 signext %a) {
; CHECK-LABEL: mul_i16_3:
; CHECK:       mulli s0, s0, 3
; CHECK:       sext16_i32 s0, s0

  %r = mul i16 3, %a
  ret i16 %r
}

define signext i32 @mul_i32_3(i32 signext %a) {
; CHECK-LABEL: mul_i32_3:
; CHECK:       mulli s0, s0, 3

  %r = mul i32 3, %a
  ret i32 %r
}

define signext i64 @mul_i64_3(i64 signext %a) {
; CHECK-LABEL: mul_i64_3:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 3
; CHECK:       mull_i64 s0_64, s0_64, s2_64

  %r = mul i64 3, %a
  ret i64 %r
}

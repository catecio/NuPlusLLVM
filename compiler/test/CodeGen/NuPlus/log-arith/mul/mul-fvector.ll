; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x float> @mul_v16f32(<16 x float> %a, <16 x float> %b) {
; CHECK-LABEL: mul_v16f32:
; CHECK:       fmul_f32 v0, v0, v1


  %r = fmul <16 x float> %a, %b
  ret <16 x float> %r
}

define <8 x double> @mul_v8f64(<8 x double> %a, <8 x double> %b) {
; CHECK-LABEL: mul_v8f64:
; CHECK:       fmul_f64 v0, v0, v1

  %r = fmul <8 x double> %a, %b
  ret <8 x double> %r
}
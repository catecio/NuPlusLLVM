; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i8>  @cttz_v16i8(<16 x i8>  %a) #1 {
; CHECK-LABEL: cttz_v16i8
; CHECK: moveih s0, 0
; CHECK: moveil s0, 256
; CHECK: or_i32 v0, v0, s0
; CHECK: ctz_i32 v0, v0

  %ret = call <16 x i8>  @llvm.cttz.v16i8 (<16 x i8>  %a, i1 false)
  ret <16 x i8>  %ret
}

; TODO: (Catello) Same as scalar
define <16 x i8>  @cttz_undef_v16i8(<16 x i8>  %a) #1 {
; CHECK-LABEL: cttz_undef_v16i8
; CHECK: ctz_i32 v0, v0

  %ret = call <16 x i8>  @llvm.cttz.v16i8 (<16 x i8>  %a, i1 true)
  ret <16 x i8>  %ret
}

declare <16 x i8>  @llvm.cttz.v16i8 (<16 x i8> , i1) #1

define <16 x i16>  @cttz_v16i16(<16 x i16>  %a) #1 {
; CHECK-LABEL: cttz_v16i16
; CHECK: moveih s0, 1
; CHECK: moveil s0, 0
; CHECK: or_i32 v0, v0, s0
; CHECK: ctz_i32 v0, v0

  %ret = call <16 x i16>  @llvm.cttz.v16i16 (<16 x i16>  %a, i1 false)
  ret <16 x i16>  %ret
}

define <16 x i16>  @cttz_undef_v16i16(<16 x i16>  %a) #1 {
; CHECK-LABEL: cttz_undef_v16i16
; CHECK: ctz_i32 v0, v0

  %ret = call <16 x i16>  @llvm.cttz.v16i16 (<16 x i16>  %a, i1 true)
  ret <16 x i16>  %ret
}

declare <16 x i16>  @llvm.cttz.v16i16 (<16 x i16> , i1) #1

define <8 x i8>  @cttz_v8i8(<8 x i8>  %a) #1 {
; CHECK-LABEL: cttz_v8i8
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 256
; CHECK: or_i64 v0, v0, s0_64
; CHECK: ctz_i64 v0, v0

  %ret = call <8 x i8>  @llvm.cttz.v8i8(<8 x i8>  %a, i1 false)
  ret <8 x i8>  %ret
}

define <8 x i8>  @cttz_undef_v8i8(<8 x i8>  %a) #1 {
; CHECK-LABEL: cttz_undef_v8i8
; CHECK: ctz_i64 v0, v0

  %ret = call <8 x i8>  @llvm.cttz.v8i8(<8 x i8>  %a, i1 true)
  ret <8 x i8>  %ret
}

declare <8 x i8>  @llvm.cttz.v8i8(<8 x i8> , i1) #1


define <8 x i16>  @cttz_v8i16(<8 x i16>  %a) #1 {
; CHECK-LABEL: cttz_v8i16
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 1
; CHECK: moveil s0, 0
; CHECK: or_i64 v0, v0, s0_64
; CHECK: ctz_i64 v0, v0

  %ret = call <8 x i16>  @llvm.cttz.v8i16(<8 x i16>  %a, i1 false)
  ret <8 x i16>  %ret
}

define <8 x i16>  @cttz_undef_v8i16(<8 x i16>  %a) #1 {
; CHECK-LABEL: cttz_undef_v8i16
; CHECK: ctz_i64 v0, v0

  %ret = call <8 x i16>  @llvm.cttz.v8i16(<8 x i16>  %a, i1 true)
  ret <8 x i16>  %ret
}

declare <8 x i16>  @llvm.cttz.v8i16(<8 x i16> , i1) #1

define <8 x i32>  @cttz_v8i32(<8 x i32>  %a) #1 {
; CHECK-LABEL: cttz_v8i32
; CHECK: moveih s1, 0
; CHECK: moveil s1, 1
; CHECK: moveih s0, 0
; CHECK: moveil s0, 0
; CHECK: or_i64 v0, v0, s0_64
; CHECK: ctz_i64 v0, v0

  %ret = call <8 x i32>  @llvm.cttz.v8i32(<8 x i32>  %a, i1 false)
  ret <8 x i32>  %ret
}

define <8 x i32>  @cttz_undef_v8i32(<8 x i32>  %a) #1 {
; CHECK-LABEL: cttz_undef_v8i32
; CHECK: ctz_i64 v0, v0

  %ret = call <8 x i32>  @llvm.cttz.v8i32(<8 x i32>  %a, i1 true)
  ret <8 x i32>  %ret
}

declare <8 x i32>  @llvm.cttz.v8i32(<8 x i32> , i1) #1

attributes #1 = { nounwind readnone }


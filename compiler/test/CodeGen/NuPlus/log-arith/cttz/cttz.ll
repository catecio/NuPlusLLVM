; RUN: llc -march=nuplus < %s | FileCheck %s


define i8 @cttz_char(i8 %a) #1 {
; CHECK-LABEL: cttz_char
; CHECK: moveih s1, 0
; CHECK: moveil s1, 256
; CHECK: or_i32 s0, s0, s1
; CHECK: ctz_i32 s0, s0

  %ret = call i8 @llvm.cttz.i8(i8 %a, i1 false)
  ret i8 %ret
}

; TODO: (Catello) Check if this is a correct behaviour 
define i8 @cttz_undef_char(i8 %a) #1 {
; CHECK-LABEL: cttz_undef_char
; CHECK: ctz_i32 s0, s0

  %ret = call i8 @llvm.cttz.i8(i8 %a, i1 true)
  ret i8 %ret
}

declare i8 @llvm.cttz.i8(i8, i1) #1


define i16 @cttz_short(i16 %a) #1 {
; CHECK-LABEL: cttz_short
; CHECK: moveih s1, 1
; CHECK: moveil s1, 0
; CHECK: or_i32 s0, s0, s1
; CHECK: ctz_i32 s0, s0

  %ret = call i16 @llvm.cttz.i16(i16 %a, i1 false)
  ret i16 %ret
}

define i16 @cttz_undef_short(i16 %a) #1 {
; CHECK-LABEL: cttz_undef_short
; CHECK: ctz_i32 s0, s0

  %ret = call i16 @llvm.cttz.i16(i16 %a, i1 true)
  ret i16 %ret
}

declare i16 @llvm.cttz.i16(i16, i1) #1


define i32 @cttz_int(i32 %a) #1 {
; CHECK-LABEL: cttz_int
; CHECK: ctz_i32 s0, s0

  %ret = call i32 @llvm.cttz.i32(i32 %a, i1 false)
  ret i32 %ret
}

define i32 @cttz_undef_int(i32 %a) #1 {
; CHECK-LABEL: cttz_undef_int
; CHECK: ctz_i32 s0, s0

  %ret = call i32 @llvm.cttz.i32(i32 %a, i1 true)
  ret i32 %ret
}

declare i32 @llvm.cttz.i32(i32, i1) #1

define i64 @cttz_lint(i64 %a) #1 {
; CHECK-LABEL: cttz_lint
; CHECK: ctz_i64 s0_64, s0_64

  %ret = call i64 @llvm.cttz.i64(i64 %a, i1 false)
  ret i64 %ret
}

define i64 @cttz_undef_lint(i64 %a) #1 {
; CHECK-LABEL: cttz_undef_lint
; CHECK: ctz_i64 s0_64, s0_64

  %ret = call i64 @llvm.cttz.i64(i64 %a, i1 true)
  ret i64 %ret
}

declare i64 @llvm.cttz.i64(i64, i1) #1

attributes #1 = { nounwind readnone }


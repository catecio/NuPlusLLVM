; RUN: llc -march=nuplus < %s | FileCheck %s

define signext i1 @xor_i1(i1 signext %a, i1 signext %b) {
entry:
; CHECK-LABEL: xor_i1:
; CHECK:       xor_i32 s0, s0, s1

  %r = xor i1 %a, %b
  ret i1 %r
}

define signext i8 @xor_i8(i8 signext %a, i8 signext %b) {
entry:
; CHECK-LABEL: xor_i8:
; CHECK:       xor_i32 s0, s0, s1

  %r = xor i8 %a, %b
  ret i8 %r
}

define signext i16 @xor_i16(i16 signext %a, i16 signext %b) {
entry:
; CHECK-LABEL: xor_i16:
; CHECK:       xor_i32 s0, s0, s1

  %r = xor i16 %a, %b
  ret i16 %r
}

define signext i32 @xor_i32(i32 signext %a, i32 signext %b) {
entry:
; CHECK-LABEL: xor_i32:
; CHECK:       xor_i32 s0, s0, s1

  %r = xor i32 %a, %b
  ret i32 %r
}

define signext i64 @xor_i64(i64 signext %a, i64 signext %b) {
entry:
; CHECK-LABEL: xor_i64:
; CHECK:       xor_i64 s0_64, s0_64, s2_64

  %r = xor i64 %a, %b
  ret i64 %r
}

define signext i1 @xor_i1_4(i1 signext %b) {
entry:
; CHECK-LABEL: xor_i1_4:
; CHECK:       jret

  %r = xor i1 4, %b
  ret i1 %r
}

define signext i8 @xor_i8_4(i8 signext %b) {
entry:
; CHECK-LABEL: xor_i8_4:
; CHECK:       xori s0, s0, 4

  %r = xor i8 4, %b
  ret i8 %r
}

define signext i16 @xor_i16_4(i16 signext %b) {
entry:
; CHECK-LABEL: xor_i16_4:
; CHECK:       xori s0, s0, 4

  %r = xor i16 4, %b
  ret i16 %r
}

define signext i32 @xor_i32_4(i32 signext %b) {
entry:
; CHECK-LABEL: xor_i32_4:
; CHECK:       xori s0, s0, 4

  %r = xor i32 4, %b
  ret i32 %r
}

define signext i64 @xor_i64_4(i64 signext %b) {
entry:
; CHECK-LABEL: xor_i64_4:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 4
; CHECK:       xor_i64 s0_64, s0_64, s2_64

  %r = xor i64 4, %b
  ret i64 %r
}
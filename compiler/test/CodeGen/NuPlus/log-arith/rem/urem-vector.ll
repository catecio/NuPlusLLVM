; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i32> @urem_v16i32(<16 x i32> %a, <16 x i32> %b) {
; CHECK-LABEL: urem_v16i32:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 316
; CHECK:       add_i32 s3, sp, s3
; CHECK:       store32 s50, (s3)
; CHECK:       moveih s50, 0
; CHECK:       moveil s50, 312
; CHECK:       add_i32 s50, sp, s50
; CHECK:       store32 ra, (s50)           
; CHECK:       store_v16i32 v56, 192(sp)      
; CHECK:       store_v16i32 v57, 128(sp)       
; CHECK:       move_i32 v56, v1
; CHECK:       move_i32 v57, v0
; CHECK:       getlanei s0, v57, 15
; CHECK:       getlanei s1, v56, 15
; CHECK:       jmpsr __umodsi3
; CHECK:       _lea s50, 64(sp)
; CHECK:       ori s1, s50, 60
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 14
; CHECK:       getlanei s1, v56, 14
; CHECK:       jmpsr __umodsi3
; CHECK:       ori s1, s50, 56
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 13
; CHECK:       getlanei s1, v56, 13
; CHECK:       jmpsr __umodsi3
; CHECK:       ori s1, s50, 52
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 12
; CHECK:       getlanei s1, v56, 12
; CHECK:       jmpsr __umodsi3
; CHECK:       ori s1, s50, 48
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 11
; CHECK:       getlanei s1, v56, 11
; CHECK:       jmpsr __umodsi3
; CHECK:       ori s1, s50, 44
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 10
; CHECK:       getlanei s1, v56, 10
; CHECK:       jmpsr __umodsi3
; CHECK:       ori s1, s50, 40
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 9
; CHECK:       getlanei s1, v56, 9
; CHECK:       jmpsr __umodsi3
; CHECK:       ori s1, s50, 36
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 8
; CHECK:       getlanei s1, v56, 8
; CHECK:       jmpsr __umodsi3
; CHECK:       ori s1, s50, 32
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 7
; CHECK:       getlanei s1, v56, 7
; CHECK:       jmpsr __umodsi3
; CHECK:       ori s1, s50, 28
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 6
; CHECK:       getlanei s1, v56, 6
; CHECK:       jmpsr __umodsi3
; CHECK:       ori s1, s50, 24
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 5
; CHECK:       getlanei s1, v56, 5
; CHECK:       jmpsr __umodsi3
; CHECK:       ori s1, s50, 20
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 4
; CHECK:       getlanei s1, v56, 4
; CHECK:       jmpsr __umodsi3
; CHECK:       ori s1, s50, 16
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 3
; CHECK:       getlanei s1, v56, 3
; CHECK:       jmpsr __umodsi3
; CHECK:       ori s1, s50, 12
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 2
; CHECK:       getlanei s1, v56, 2
; CHECK:       jmpsr __umodsi3
; CHECK:       ori s1, s50, 8
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 1
; CHECK:       getlanei s1, v56, 1
; CHECK:       jmpsr __umodsi3
; CHECK:       ori s1, s50, 4
; CHECK:       store32 s0, (s1)
; CHECK:       getlanei s0, v57, 0
; CHECK:       getlanei s1, v56, 0
; CHECK:       jmpsr __umodsi3
; CHECK:       store32 s0, 64(sp)
; CHECK:       load_v16i32 v0, 64(sp)
; CHECK:       load_v16i32 v57, 128(sp)        
; CHECK:       load_v16i32 v56, 192(sp)       
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 312
; CHECK:       add_i32 s0, sp, s0
; CHECK:       load32 ra, (s0)
; CHECK:       moveih s4, 0
; CHECK:       moveil s4, 316
; CHECK:       add_i32 s4, sp, s4
; CHECK:       load32 s50, (s4)

  %r = urem <16 x i32> %a, %b
  ret <16 x i32> %r
}

define <8 x i64> @urem_v8i64(<8 x i64> %a, <8 x i64> %b) {
; CHECK-LABEL: urem_v8i64:
; CHECK:       moveih s5, 0
; CHECK:       moveil s5, 316
; CHECK:       add_i32 s5, sp, s5
; CHECK:       store32 s50, (s5)
; CHECK:       moveih s50, 0
; CHECK:       moveil s50, 312
; CHECK:       add_i32 s50, sp, s50
; CHECK:       store32 ra, (s50)
; CHECK:       store_v16i32 v56, 192(sp)      
; CHECK:       store_v16i32 v57, 128(sp)       
; CHECK:       move_i32 v56, v1
; CHECK:       move_i32 v57, v0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 7
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __umoddi3
; CHECK:       _lea s50, 64(sp)
; CHECK:       ori s2, s50, 56
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 6
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __umoddi3
; CHECK:       ori s2, s50, 48
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 5
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __umoddi3
; CHECK:       ori s2, s50, 40
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 4
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __umoddi3
; CHECK:       ori s2, s50, 32
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 3
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __umoddi3
; CHECK:       ori s2, s50, 24
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 2
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __umoddi3
; CHECK:       ori s2, s50, 16
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 1
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __umoddi3
; CHECK:       ori s2, s50, 8
; CHECK:       store64 s0_64, (s2)
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 0
; CHECK:       getlane_i64 s0_64, v57, s2
; CHECK:       getlane_i64 s2_64, v56, s2
; CHECK:       jmpsr __umoddi3
; CHECK:       store64 s0_64, 64(sp)
; CHECK:       load_v8i64 v0, 64(sp)
; CHECK:       load_v16i32 v57, 128(sp)       
; CHECK:       load_v16i32 v56, 192(sp)       
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 312
; CHECK:       add_i32 s0, sp, s0
; CHECK:       load32 ra, (s0)
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 316
; CHECK:       add_i32 s1, sp, s1
; CHECK:       load32 s50, (s1)

  %r = urem <8 x i64> %a, %b
  ret <8 x i64> %r
}

define <16 x i32> @urem_v16i32_4(<16 x i32> %a) {
; CHECK-LABEL: urem_v16i32_4:
; CHECK:       andi v0, v0, 3

  %r = urem <16 x i32> %a, <i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4>
  ret <16 x i32> %r
}

define <8 x i64> @urem_v8i64_4(<8 x i64> %a) {
; CHECK-LABEL: urem_v8i64_4:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 3
; CHECK:       and_i64 v0, v0, s0_64

  %r = urem <8 x i64> %a, <i64 4, i64 4, i64 4, i64 4, i64 4, i64 4, i64 4, i64 4>
  ret <8 x i64> %r
}

define <16 x i32> @urem_v16i32_3(<16 x i32> %a) {
; CHECK-LABEL: urem_v16i32_3:
; CHECK:       moveih s0, 43690
; CHECK:       moveil s0, 43691
; CHECK:       mulhu_i32 v1, v0, s0
; CHECK:       shri v1, v1, 1
; CHECK:       mulli v1, v1, 3
; CHECK:       sub_i32 v0, v0, v1


  %r = urem <16 x i32> %a, <i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3>
  ret <16 x i32> %r
}

define <8 x i64> @urem_v8i64_3(<8 x i64> %a) {
; CHECK-LABEL: urem_v8i64_3:
; CHECK:       moveih s1, 43690
; CHECK:       moveil s1, 43690
; CHECK:       moveih s0, 43690
; CHECK:       moveil s0, 43691
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 1
; CHECK:       mulhu_i64 v1, v0, s0_64
; CHECK:       shr_i64 v1, v1, s2_64
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 3
; CHECK:       mull_i64 v1, v1, s2_64
; CHECK:       sub_i64 v0, v0, v1

  %r = urem <8 x i64> %a, <i64 3, i64 3, i64 3, i64 3, i64 3, i64 3, i64 3, i64 3>
  ret <8 x i64> %r
}

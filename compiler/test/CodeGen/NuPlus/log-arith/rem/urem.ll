; RUN: llc -march=nuplus < %s | FileCheck %s

define signext i1 @urem_i1(i1 signext %a, i1 signext %b) {
entry:
; CHECK-LABEL: urem_i1:
; CHECK:       andi s0, s0, 1
; CHECK:       andi s1, s1, 1
; CHECK:       jmpsr __umodsi3
; CHECK:       shli s0, s0, 31
; CHECK:       ashri s0, s0, 31

  %r = urem i1 %a, %b
  ret i1 %r
}

define signext i8 @urem_i8(i8 signext %a, i8 signext %b) {
entry:
; CHECK-LABEL: urem_i8:
; CHECK:       andi s0, s0, 255
; CHECK:       andi s1, s1, 255
; CHECK:       jmpsr __umodsi3
; CHECK:       sext8_i32 s0, s0

  %r = urem i8 %a, %b
  ret i8 %r
}

define signext i16 @urem_i16(i16 signext %a, i16 signext %b) {
entry:
; CHECK-LABEL: urem_i16:
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 65535
; CHECK:       and_i32 s0, s0, s2
; CHECK:       and_i32 s1, s1, s2
; CHECK:       jmpsr __umodsi3
; CHECK:       sext16_i32 s0, s0

  %r = urem i16 %a, %b
  ret i16 %r
}

define signext i32 @urem_i32(i32 signext %a, i32 signext %b) {
entry:
; CHECK-LABEL: urem_i32:
; CHECK:       jmpsr __umodsi3

  %r = urem i32 %a, %b
  ret i32 %r
}

define signext i64 @urem_i64(i64 signext %a, i64 signext %b) {
entry:
; CHECK-LABEL: urem_i64:
; CHECK:       jmpsr __umoddi3

  %r = urem i64 %a, %b
  ret i64 %r
}
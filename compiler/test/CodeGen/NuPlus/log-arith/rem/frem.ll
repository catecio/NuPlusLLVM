; RUN: llc -march=nuplus < %s | FileCheck %s

define float @frem_float(float %a, float %b) {
entry:
; CHECK-LABEL: frem_float:
; CHECK:       jmpsr fmodf

  %r = frem float %a, %b
  ret float %r
}

define double @frem_double(double %a, double %b) {
entry:
; CHECK-LABEL: frem_double:
; CHECK:       jmpsr fmod

  %r = frem double %a, %b
  ret double %r
}
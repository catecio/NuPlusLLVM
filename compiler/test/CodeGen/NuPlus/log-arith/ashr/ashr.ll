; RUN: llc -march=nuplus < %s | FileCheck %s

define signext i1 @ashr_i1(i1 signext %a, i1 signext %b) {
entry:
; CHECK-LABEL: ashr_i1:
; CHECK:       jret

  %r = ashr i1 %a, %b
  ret i1 %r
}

define signext i8 @ashr_i8(i8 signext %a, i8 signext %b) {
entry:
; CHECK-LABEL: ashr_i8:
; CHECK:       andi s1, s1, 255
; CHECK:       ashr_i32 s0, s0, s1

  %r = ashr i8 %a, %b
  ret i8 %r
}

define signext i16 @ashr_i16(i16 signext %a, i16 signext %b) {
entry:
; CHECK-LABEL: ashr_i16:
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 65535
; CHECK:       and_i32 s1, s1, s2
; CHECK:       ashr_i32 s0, s0, s1

  %r = ashr i16 %a, %b
  ret i16 %r
}

define signext i32 @ashr_i32(i32 signext %a, i32 signext %b) {
entry:
; CHECK-LABEL: ashr_i32:
; CHECK:       ashr_i32 s0, s0, s1

  %r = ashr i32 %a, %b
  ret i32 %r
}

define signext i64 @ashr_i64(i64 signext %a, i64 signext %b) {
entry:
; CHECK-LABEL: ashr_i64:
; CHECK:       ashr_i64 s0_64, s0_64, s2

  %r = ashr i64 %a, %b
  ret i64 %r
}

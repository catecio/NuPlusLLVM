; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i8> @ashr_v16i8(<16 x i8> %a, <16 x i8> %b) {
; CHECK-LABEL: ashr_v16i8:
; CHECK:       andi v1, v1, 255
; CHECK:       sext8_i32 v0, v0
; CHECK:       ashr_i32 v0, v0, v1


  %r = ashr <16 x i8> %a, %b
  ret <16 x i8> %r
}

define <16 x i16> @ashr_v16i16(<16 x i16> %a, <16 x i16> %b) {
; CHECK-LABEL: ashr_v16i16:
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 65535
; CHECK:       and_i32 v1, v1, s0
; CHECK:       sext16_i32 v0, v0
; CHECK:       ashr_i32 v0, v0, v1


  %r = ashr <16 x i16> %a, %b
  ret <16 x i16> %r
}


define <8 x i8> @ashr_v8i8(<8 x i8> %a, <8 x i8> %b) {
; CHECK-LABEL: ashr_v8i8:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 255
; CHECK:       and_i64 v1, v1, s0_64
; CHECK:       sext8_i64 v0, v0
; CHECK:       ashr_i64 v0, v0, v1

  %r = ashr <8 x i8> %a, %b
  ret <8 x i8> %r
}

define <8 x i16> @ashr_v8i16(<8 x i16> %a, <8 x i16> %b) {
; CHECK-LABEL: ashr_v8i16:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 65535
; CHECK:       and_i64 v1, v1, s0_64
; CHECK:       sext16_i64 v0, v0
; CHECK:       ashr_i64 v0, v0, v1

  %r = ashr <8 x i16> %a, %b
  ret <8 x i16> %r
}

define <8 x i32> @ashr_v8i32(<8 x i32> %a, <8 x i32> %b) {
; CHECK-LABEL: ashr_v8i32:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 65535
; CHECK:       moveil s0, 65535
; CHECK:       and_i64 v1, v1, s0_64
; CHECK:       sext32_i64 v0, v0
; CHECK:       ashr_i64 v0, v0, v1

  %r = ashr <8 x i32> %a, %b
  ret <8 x i32> %r
}

; RUN: llc -march=nuplus < %s | FileCheck %s


define i8 @ctlz_char(i8 %a) #1 {
; CHECK-LABEL: ctlz_char
; CHECK: andi s0, s0, 255
; CHECK: clz_i32 s0, s0
; CHECK: addi s0, s0, -24

  %ret = call i8 @llvm.ctlz.i8(i8 %a, i1 false)
  ret i8 %ret
}

define i8 @ctlz_undef_char(i8 %a) #1 {
; CHECK-LABEL: ctlz_undef_char
; CHECK: andi s0, s0, 255
; CHECK: clz_i32 s0, s0
; CHECK: addi s0, s0, -24

  %ret = call i8 @llvm.ctlz.i8(i8 %a, i1 true)
  ret i8 %ret
}

declare i8 @llvm.ctlz.i8(i8, i1) #1


define i16 @ctlz_short(i16 %a) #1 {
; CHECK-LABEL: ctlz_short
; CHECK: moveih s1, 0
; CHECK: moveil s1, 65535
; CHECK: and_i32 s0, s0, s1
; CHECK: clz_i32 s0, s0
; CHECK: addi s0, s0, -16

  %ret = call i16 @llvm.ctlz.i16(i16 %a, i1 false)
  ret i16 %ret
}

define i16 @ctlz_undef_short(i16 %a) #1 {
; CHECK-LABEL: ctlz_undef_short
; CHECK: moveih s1, 0
; CHECK: moveil s1, 65535
; CHECK: and_i32 s0, s0, s1
; CHECK: clz_i32 s0, s0
; CHECK: addi s0, s0, -16

  %ret = call i16 @llvm.ctlz.i16(i16 %a, i1 true)
  ret i16 %ret
}

declare i16 @llvm.ctlz.i16(i16, i1) #1


define i32 @ctlz_int(i32 %a) #1 {
; CHECK-LABEL: ctlz_int
; CHECK: clz_i32 s0, s0

  %ret = call i32 @llvm.ctlz.i32(i32 %a, i1 false)
  ret i32 %ret
}

define i32 @ctlz_undef_int(i32 %a) #1 {
; CHECK-LABEL: ctlz_undef_int
; CHECK: clz_i32 s0, s0

  %ret = call i32 @llvm.ctlz.i32(i32 %a, i1 true)
  ret i32 %ret
}

declare i32 @llvm.ctlz.i32(i32, i1) #1

define i64 @ctlz_lint(i64 %a) #1 {
; CHECK-LABEL: ctlz_lint
; CHECK: clz_i64 s0_64, s0_64

  %ret = call i64 @llvm.ctlz.i64(i64 %a, i1 false)
  ret i64 %ret
}

define i64 @ctlz_undef_lint(i64 %a) #1 {
; CHECK-LABEL: ctlz_undef_lint
; CHECK: clz_i64 s0_64, s0_64

  %ret = call i64 @llvm.ctlz.i64(i64 %a, i1 true)
  ret i64 %ret
}

declare i64 @llvm.ctlz.i64(i64, i1) #1

attributes #1 = { nounwind readnone }


; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i32>  @ctlz_v16i32(<16 x i32>  %a) #1 {
; CHECK-LABEL: ctlz_v16i32
; CHECK: clz_i32  v0, v0

  %ret = call <16 x i32>  @llvm.ctlz.v16i32 (<16 x i32>  %a, i1 false)
  ret <16 x i32>  %ret
}

define <16 x i32>  @ctlz_undef_v16i32(<16 x i32>  %a) #1 {
; CHECK-LABEL: ctlz_undef_v16i32
; CHECK: clz_i32  v0, v0

  %ret = call <16 x i32>  @llvm.ctlz.v16i32 (<16 x i32>  %a, i1 true)
  ret <16 x i32>  %ret
}

declare <16 x i32>  @llvm.ctlz.v16i32 (<16 x i32> , i1) #1

define <8 x i64>  @ctlz_v8i64(<8 x i64>  %a) #1 {
; CHECK-LABEL: ctlz_v8i64
; CHECK: clz_i64  v0, v0

  %ret = call <8 x i64>  @llvm.ctlz.v8i64(<8 x i64>  %a, i1 false)
  ret <8 x i64>  %ret
}

define <8 x i64>  @ctlz_undef_v8i64(<8 x i64>  %a) #1 {
; CHECK-LABEL: ctlz_undef_v8i64
; CHECK: clz_i64  v0, v0

  %ret = call <8 x i64>  @llvm.ctlz.v8i64(<8 x i64>  %a, i1 true)
  ret <8 x i64>  %ret
}

declare <8 x i64>  @llvm.ctlz.v8i64(<8 x i64> , i1) #1

attributes #1 = { nounwind readnone }


; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i8>  @ctlz_v16i8(<16 x i8>  %a) #1 {
; CHECK-LABEL: ctlz_v16i8
; CHECK: andi v0, v0, 255
; CHECK: clz_i32 v0, v0
; CHECK: subi v0, v0, 24

  %ret = call <16 x i8>  @llvm.ctlz.v16i8 (<16 x i8>  %a, i1 false)
  ret <16 x i8>  %ret
}

define <16 x i8>  @ctlz_undef_v16i8(<16 x i8>  %a) #1 {
; CHECK-LABEL: ctlz_undef_v16i8
; CHECK: andi v0, v0, 255
; CHECK: clz_i32 v0, v0
; CHECK: subi v0, v0, 24

  %ret = call <16 x i8>  @llvm.ctlz.v16i8 (<16 x i8>  %a, i1 true)
  ret <16 x i8>  %ret
}

declare <16 x i8>  @llvm.ctlz.v16i8 (<16 x i8> , i1) #1

define <16 x i16>  @ctlz_v16i16(<16 x i16>  %a) #1 {
; CHECK-LABEL: ctlz_v16i16
; CHECK: moveih s0, 0
; CHECK: moveil s0, 65535
; CHECK: and_i32 v0, v0, s0
; CHECK: clz_i32 v0, v0
; CHECK: subi v0, v0, 16

  %ret = call <16 x i16>  @llvm.ctlz.v16i16 (<16 x i16>  %a, i1 false)
  ret <16 x i16>  %ret
}

define <16 x i16>  @ctlz_undef_v16i16(<16 x i16>  %a) #1 {
; CHECK-LABEL: ctlz_undef_v16i16
; CHECK: moveih s0, 0
; CHECK: moveil s0, 65535
; CHECK: and_i32 v0, v0, s0
; CHECK: clz_i32 v0, v0
; CHECK: subi v0, v0, 16

  %ret = call <16 x i16>  @llvm.ctlz.v16i16 (<16 x i16>  %a, i1 true)
  ret <16 x i16>  %ret
}

declare <16 x i16>  @llvm.ctlz.v16i16 (<16 x i16> , i1) #1

define <8 x i8>  @ctlz_v8i8(<8 x i8>  %a) #1 {
; CHECK-LABEL: ctlz_v8i8
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 56
; CHECK: moveih s2, 0
; CHECK: moveil s2, 255
; CHECK: move_i64 s4_64, s0_64
; CHECK: move_i32 s4, s2
; CHECK: and_i64 v0, v0, s4_64
; CHECK: clz_i64 v0, v0
; CHECK: sub_i64 v0, v0, s0_64

  %ret = call <8 x i8>  @llvm.ctlz.v8i8(<8 x i8>  %a, i1 false)
  ret <8 x i8>  %ret
}

define <8 x i8>  @ctlz_undef_v8i8(<8 x i8>  %a) #1 {
; CHECK-LABEL: ctlz_undef_v8i8
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 56
; CHECK: moveih s2, 0
; CHECK: moveil s2, 255
; CHECK: move_i64 s4_64, s0_64
; CHECK: move_i32 s4, s2
; CHECK: and_i64 v0, v0, s4_64
; CHECK: clz_i64 v0, v0
; CHECK: sub_i64 v0, v0, s0_64

  %ret = call <8 x i8>  @llvm.ctlz.v8i8(<8 x i8>  %a, i1 true)
  ret <8 x i8>  %ret
}

declare <8 x i8>  @llvm.ctlz.v8i8(<8 x i8> , i1) #1


define <8 x i16>  @ctlz_v8i16(<8 x i16>  %a) #1 {
; CHECK-LABEL: ctlz_v8i16
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 48
; CHECK: moveih s2, 0
; CHECK: moveil s2, 65535
; CHECK: move_i64 s4_64, s0_64
; CHECK: move_i32 s4, s2
; CHECK: and_i64 v0, v0, s4_64
; CHECK: clz_i64 v0, v0
; CHECK: sub_i64 v0, v0, s0_64

  %ret = call <8 x i16>  @llvm.ctlz.v8i16(<8 x i16>  %a, i1 false)
  ret <8 x i16>  %ret
}

define <8 x i16>  @ctlz_undef_v8i16(<8 x i16>  %a) #1 {
; CHECK-LABEL: ctlz_undef_v8i16
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 48
; CHECK: moveih s2, 0
; CHECK: moveil s2, 65535
; CHECK: move_i64 s4_64, s0_64
; CHECK: move_i32 s4, s2
; CHECK: and_i64 v0, v0, s4_64
; CHECK: clz_i64 v0, v0
; CHECK: sub_i64 v0, v0, s0_64

  %ret = call <8 x i16>  @llvm.ctlz.v8i16(<8 x i16>  %a, i1 true)
  ret <8 x i16>  %ret
}

declare <8 x i16>  @llvm.ctlz.v8i16(<8 x i16> , i1) #1

define <8 x i32>  @ctlz_v8i32(<8 x i32>  %a) #1 {
; CHECK-LABEL: ctlz_v8i32
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 32
; CHECK: moveih s2, 65535
; CHECK: moveil s2, 65535
; CHECK: move_i64 s4_64, s0_64
; CHECK: move_i32 s4, s2
; CHECK: and_i64 v0, v0, s4_64
; CHECK: clz_i64 v0, v0
; CHECK: sub_i64 v0, v0, s0_64

  %ret = call <8 x i32>  @llvm.ctlz.v8i32(<8 x i32>  %a, i1 false)
  ret <8 x i32>  %ret
}

define <8 x i32>  @ctlz_undef_v8i32(<8 x i32>  %a) #1 {
; CHECK-LABEL: ctlz_undef_v8i32
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 32
; CHECK: moveih s2, 65535
; CHECK: moveil s2, 65535
; CHECK: move_i64 s4_64, s0_64
; CHECK: move_i32 s4, s2
; CHECK: and_i64 v0, v0, s4_64
; CHECK: clz_i64 v0, v0
; CHECK: sub_i64 v0, v0, s0_64

  %ret = call <8 x i32>  @llvm.ctlz.v8i32(<8 x i32>  %a, i1 true)
  ret <8 x i32>  %ret
}

declare <8 x i32>  @llvm.ctlz.v8i32(<8 x i32> , i1) #1

attributes #1 = { nounwind readnone }


; RUN: llc -march=nuplus < %s | FileCheck %s

define float @fadd32(float %a, float %b) {
; CHECK-LABEL: fadd32:
; CHECK:       fadd_f32 s0, s0, s1


  %r = fadd float %a, %b
  ret float %r
}

define double @fadd64(double %a, double %b) {
; CHECK-LABEL: fadd64:
; CHECK:       fadd_f64 s0_64, s0_64, s2_64

  %r = fadd double %a, %b
  ret double %r
}
; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i8> @add_v16i8(<16 x i8> %a, <16 x i8> %b) {
; CHECK-LABEL: add_v16i8:
; CHECK:       add_i32 v0, v0, v1


  %r = add <16 x i8> %a, %b
  ret <16 x i8> %r
}

define <16 x i16> @add_v16i16(<16 x i16> %a, <16 x i16> %b) {
; CHECK-LABEL: add_v16i16:
; CHECK:       add_i32 v0, v0, v1


  %r = add <16 x i16> %a, %b
  ret <16 x i16> %r
}


define <8 x i8> @add_v8i8(<8 x i8> %a, <8 x i8> %b) {
; CHECK-LABEL: add_v8i8:
; CHECK:       add_i64 v0, v0, v1

  %r = add <8 x i8> %a, %b
  ret <8 x i8> %r
}

define <8 x i16> @add_v8i16(<8 x i16> %a, <8 x i16> %b) {
; CHECK-LABEL: add_v8i16:
; CHECK:       add_i64 v0, v0, v1

  %r = add <8 x i16> %a, %b
  ret <8 x i16> %r
}

define <8 x i32> @add_v8i32(<8 x i32> %a, <8 x i32> %b) {
; CHECK-LABEL: add_v8i32:
; CHECK:       add_i64 v0, v0, v1

  %r = add <8 x i32> %a, %b
  ret <8 x i32> %r
}

; TODO: (Catello) vec8f32 not implemented yet
define <8 x float> @add_v8f32(<8 x float> %a, <8 x float> %b) {
; CHECK-LABEL: add_v8f32:

  %r = fadd <8 x float> %a, %b
  ret <8 x float> %r
}

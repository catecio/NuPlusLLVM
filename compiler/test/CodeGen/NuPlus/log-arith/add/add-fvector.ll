; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x float> @add_v16f32(<16 x float> %a, <16 x float> %b) {
; CHECK-LABEL: add_v16f32:
; CHECK:       fadd_f32 v0, v0, v1


  %r = fadd <16 x float> %a, %b
  ret <16 x float> %r
}

define <8 x double> @add_v8f64(<8 x double> %a, <8 x double> %b) {
; CHECK-LABEL: add_v8f64:
; CHECK:       fadd_f64 v0, v0, v1

  %r = fadd <8 x double> %a, %b
  ret <8 x double> %r
}
; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i32> @add_v16i32(<16 x i32> %a, <16 x i32> %b) {
; CHECK-LABEL: add_v16i32:
; CHECK:       add_i32 v0, v0, v1


  %r = add <16 x i32> %a, %b
  ret <16 x i32> %r
}

define <8 x i64> @add_v8i64(<8 x i64> %a, <8 x i64> %b) {
; CHECK-LABEL: add_v8i64:
; CHECK:       add_i64 v0, v0, v1

  %r = add <8 x i64> %a, %b
  ret <8 x i64> %r
}

define <16 x i32> @add_v16i32_s(<16 x i32> %a, i32 %b) {
; CHECK-LABEL: add_v16i32_s:
; CHECK:       add_i32 v0, v0, s0
  %1 = insertelement <16 x i32> undef, i32 %b, i32 0
  %2 = shufflevector <16 x i32> %1, <16 x i32> undef, <16 x i32> zeroinitializer
  %r = add <16 x i32> %a, %2
  ret <16 x i32> %r
}

define <8 x i64> @add_v8i64_s(<8 x i64> %a, i64 %b) {
; CHECK-LABEL: add_v8i64_s:
; CHECK:       add_i64 v0, v0, s0_64

  %1 = insertelement <8 x i64> undef, i64 %b, i32 0
  %2 = shufflevector <8 x i64> %1, <8 x i64> undef, <8 x i32> zeroinitializer
  %3 = add <8 x i64> %2, %a
  ret <8 x i64> %3
}

define <16 x i32> @add_v16i32_4(<16 x i32> %a) {
; CHECK-LABEL: add_v16i32_4:
; CHECK:       addi v0, v0, 4

  %r = add <16 x i32> %a, <i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4>
  ret <16 x i32> %r
}

define <8 x i64> @add_v8i64_4(<8 x i64> %a) {
; CHECK-LABEL: add_v8i64_4:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 4
; CHECK:       add_i64 v0, v0, s0_64

  %r = add <8 x i64> %a, <i64 4, i64 4, i64 4, i64 4, i64 4, i64 4, i64 4, i64 4>
  ret <8 x i64> %r
}

define <16 x i32> @add_v16i32_3(<16 x i32> %a) {
; CHECK-LABEL: add_v16i32_3:
; CHECK:       addi v0, v0, 3


  %r = add <16 x i32> %a, <i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3>
  ret <16 x i32> %r
}

define <8 x i64> @add_v8i64_3(<8 x i64> %a) {
; CHECK-LABEL: add_v8i64_3:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 3
; CHECK:       add_i64 v0, v0, s0_64

  %r = add <8 x i64> %a, <i64 3, i64 3, i64 3, i64 3, i64 3, i64 3, i64 3, i64 3>
  ret <8 x i64> %r
}
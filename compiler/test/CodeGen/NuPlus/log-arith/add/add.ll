; RUN: llc -march=nuplus < %s | FileCheck %s

define signext i1 @add_i1(i1 signext %a, i1 signext %b) {
; CHECK-LABEL: add_i1:
; CHECK:       add_i32 s0, s0, s1
; CHECK:       shli s0, s0, 31
; CHECK:       ashri s0, s0, 31

  %r = add i1 %a, %b
  ret i1 %r
}

define signext i8 @add_i8(i8 signext %a, i8 signext %b) {
; CHECK-LABEL: add_i8:
; CHECK:       add_i32 s0, s0, s1
; CHECK:       sext8_i32 s0, s0

  %r = add i8 %a, %b
  ret i8 %r
}

define signext i16 @add_i16(i16 signext %a, i16 signext %b) {
; CHECK-LABEL: add_i16:
; CHECK:       add_i32 s0, s0, s1
; CHECK:       sext16_i32 s0, s0

  %r = add i16 %a, %b
  ret i16 %r
}

define signext i32 @add_i32(i32 signext %a, i32 signext %b) {
; CHECK-LABEL: add_i32:
; CHECK:       add_i32 s0, s0, s1

  %r = add i32 %a, %b
  ret i32 %r
}

define signext i64 @add_i64(i64 signext %a, i64 signext %b) {
; CHECK-LABEL: add_i64:
; CHECK:       add_i64 s0_64, s0_64, s2_64

  %r = add i64 %a, %b
  ret i64 %r
}

define signext i1 @add_i1_4(i1 signext %a) {
; CHECK-LABEL: add_i1_4:
; CHECK:       jret

  %r = add i1 4, %a
  ret i1 %r
}

define signext i8 @add_i8_4(i8 signext %a) {
; CHECK-LABEL: add_i8_4:
; CHECK:       addi s0, s0, 4
; CHECK:       sext8_i32 s0, s0

  %r = add i8 4, %a
  ret i8 %r
}

define signext i16 @add_i16_4(i16 signext %a) {
; CHECK-LABEL: add_i16_4:
; CHECK:       addi s0, s0, 4
; CHECK:       sext16_i32 s0, s0

  %r = add i16 4, %a
  ret i16 %r
}

define signext i32 @add_i32_4(i32 signext %a) {
; CHECK-LABEL: add_i32_4:
; CHECK:       addi s0, s0, 4

  %r = add i32 4, %a
  ret i32 %r
}

define signext i64 @add_i64_4(i64 signext %a) {
; CHECK-LABEL: add_i64_4:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 4
; CHECK:       add_i64 s0_64, s0_64, s2_64

  %r = add i64 4, %a
  ret i64 %r
}

define signext i1 @add_i1_3(i1 signext %a) {
; CHECK-LABEL: add_i1_3:
; CHECK:       shli s0, s0, 31
; CHECK:       moveih s1, 32768
; CHECK:       moveil s1, 0
; CHECK:       add_i32 s0, s0, s1
; CHECK:       ashri s0, s0, 31

  %r = add i1 3, %a
  ret i1 %r
}

define signext i8 @add_i8_3(i8 signext %a) {
; CHECK-LABEL: add_i8_3:
; CHECK:       addi s0, s0, 3
; CHECK:       sext8_i32 s0, s0

  %r = add i8 3, %a
  ret i8 %r
}

define signext i16 @add_i16_3(i16 signext %a) {
; CHECK-LABEL: add_i16_3:
; CHECK:       addi s0, s0, 3
; CHECK:       sext16_i32 s0, s0

  %r = add i16 3, %a
  ret i16 %r
}

define signext i32 @add_i32_3(i32 signext %a) {
; CHECK-LABEL: add_i32_3:
; CHECK:       addi s0, s0, 3

  %r = add i32 3, %a
  ret i32 %r
}

define signext i64 @add_i64_3(i64 signext %a) {
; CHECK-LABEL: add_i64_3:
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 0
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 3
; CHECK:       add_i64 s0_64, s0_64, s2_64

  %r = add i64 3, %a
  ret i64 %r
}

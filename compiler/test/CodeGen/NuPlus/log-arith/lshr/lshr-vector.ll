; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i32> @lshr_v16i32(<16 x i32> %a, <16 x i32> %b) {
; CHECK-LABEL: lshr_v16i32:
; CHECK:       shr_i32 v0, v0, v1


  %r = lshr <16 x i32> %a, %b
  ret <16 x i32> %r
}

define <8 x i64> @lshr_v8i64(<8 x i64> %a, <8 x i64> %b) {
; CHECK-LABEL: lshr_v8i64:
; CHECK:       shr_i64 v0, v0, v1

  %r = lshr <8 x i64> %a, %b
  ret <8 x i64> %r
}

define <16 x i32> @lshr_v16i32_4(<16 x i32> %a) {
; CHECK-LABEL: lshr_v16i32_4:
; CHECK:       shri v0, v0, 4

  %r = lshr <16 x i32> %a, <i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4, i32 4>
  ret <16 x i32> %r
}

define <8 x i64> @lshr_v8i64_4(<8 x i64> %a) {
; CHECK-LABEL: lshr_v8i64_4:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 4
; CHECK:       shr_i64 v0, v0, s0_64

  %r = lshr <8 x i64> %a, <i64 4, i64 4, i64 4, i64 4, i64 4, i64 4, i64 4, i64 4>
  ret <8 x i64> %r
}

define <16 x i32> @lshr_v16i32_3(<16 x i32> %a) {
; CHECK-LABEL: lshr_v16i32_3:
; CHECK:       shri v0, v0, 3


  %r = lshr <16 x i32> %a, <i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3, i32 3>
  ret <16 x i32> %r
}

define <8 x i64> @lshr_v8i64_3(<8 x i64> %a) {
; CHECK-LABEL: lshr_v8i64_3:
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 3
; CHECK:       shr_i64 v0, v0, s0_64

  %r = lshr <8 x i64> %a, <i64 3, i64 3, i64 3, i64 3, i64 3, i64 3, i64 3, i64 3>
  ret <8 x i64> %r
}
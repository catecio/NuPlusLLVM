; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x float> @ret_v16f32() nounwind {
  %1 = alloca <16 x float>, align 64
  %2 = load <16 x float>, <16 x float>* %1, align 64
  ret <16 x float> %2
}

; CHECK-LABEL: ret_v16f32
; CHECK:       addi sp, sp, -128
; CHECK:       load_v16i32 v0, 64(sp)
; CHECK:       addi sp, sp, 128

define <8 x double> @ret_v8f64() nounwind {
  %1 = alloca <8 x double>, align 64
  %2 = load <8 x double>, <8 x double>* %1, align 64
  ret <8 x double> %2
}

; CHECK-LABEL: ret_v8f64
; CHECK:       addi sp, sp, -128
; CHECK:       load_v8i64 v0, 64(sp)
; CHECK:       addi sp, sp, 128

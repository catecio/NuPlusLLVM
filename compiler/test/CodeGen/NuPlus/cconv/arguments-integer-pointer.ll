; RUN: llc -march=nuplus < %s | FileCheck %s

define void @pointer_char(i8*) #0 {
  %2 = alloca i8*, align 4
  %3 = alloca i8*, align 4
  store i8* %0, i8** %2, align 4
  %4 = load i8*, i8** %2, align 4
  store i8* %4, i8** %3, align 4
  ret void
}

; CHECK-LABEL: pointer_char
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)            
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       store32 s0, 48(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)             
; CHECK:       addi sp, sp, 64
; CHECK:       jret

define void @pointer_short(i16*) #0 {
  %2 = alloca i16*, align 4
  %3 = alloca i16*, align 4
  store i16* %0, i16** %2, align 4
  %4 = load i16*, i16** %2, align 4
  store i16* %4, i16** %3, align 4
  ret void
}

; CHECK-LABEL: pointer_short
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)            
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       store32 s0, 48(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)             
; CHECK:       addi sp, sp, 64
; CHECK:       jret

define void @pointer_int(i32*) #0 {
  %2 = alloca i32*, align 4
  %3 = alloca i32*, align 4
  store i32* %0, i32** %2, align 4
  %4 = load i32*, i32** %2, align 4
  store i32* %4, i32** %3, align 4
  ret void
}

; CHECK-LABEL: pointer_int
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)            
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       store32 s0, 48(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)             
; CHECK:       addi sp, sp, 64
; CHECK:       jret

define void @pointer_llint(i64*) #0 {
  %2 = alloca i64*, align 4
  %3 = alloca i64*, align 4
  store i64* %0, i64** %2, align 4
  %4 = load i64*, i64** %2, align 4
  store i64* %4, i64** %3, align 4
  ret void
}

; CHECK-LABEL: pointer_llint
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)            
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       store32 s0, 48(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)             
; CHECK:       addi sp, sp, 64
; CHECK:       jret


attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }

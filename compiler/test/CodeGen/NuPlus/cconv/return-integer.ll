; RUN: llc -march=nuplus < %s | FileCheck %s

define signext i8 @ret_char() nounwind {
  %1 = alloca i8, align 1
  %2 = load i8, i8* %1, align 1
  ret i8 %2
}

; CHECK-LABEL: ret_char
; CHECK:       load32_s8 s0, 63(sp)

define signext i16 @ret_short() nounwind {
  %1 = alloca i16, align 2
  %2 = load i16, i16* %1, align 2
  ret i16 %2
}

; CHECK-LABEL: ret_short
; CHECK:       load32_s16 s0, 62(sp)


define i32 @ret_int() nounwind {
  %1 = alloca i32, align 4
  %2 = load i32, i32* %1, align 4
  ret i32 %2
}

; CHECK-LABEL: ret_int
; CHECK:       load32 s0, 60(sp)

define i64 @ret_llint() nounwind {
  %1 = alloca i64, align 8
  %2 = load i64, i64* %1, align 8
  ret i64 %2
}

; CHECK-LABEL: ret_llint
; CHECK:       load64 s0_64, 56(sp)

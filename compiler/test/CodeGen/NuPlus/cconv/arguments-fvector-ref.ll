; RUN: llc -march=nuplus < %s | FileCheck %s

define void @ref_v16f32(<16 x float>* dereferenceable(64)) #0 {
  %2 = alloca <16 x float>*, align 4
  %3 = alloca <16 x float>, align 64
  store <16 x float>* %0, <16 x float>** %2, align 4
  %4 = load <16 x float>*, <16 x float>** %2, align 4
  %5 = load <16 x float>, <16 x float>* %4, align 64
  store <16 x float> %5, <16 x float>* %3, align 64
  ret void
}

; CHECK-LABEL: ref_v16f32
; CHECK:       addi sp, sp, -128
; CHECK:       store32 fp, 124(sp)            
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 116(fp)
; CHECK:       load_v16i32 v0, (s0)
; CHECK:       store_v16i32 v0, (fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 124(sp)            
; CHECK:       addi sp, sp, 128
; CHECK:       jret

define void @ref_v8f64(<8 x double>* dereferenceable(64)) #0 {
  %2 = alloca <8 x double>*, align 4
  %3 = alloca <8 x double>, align 64
  store <8 x double>* %0, <8 x double>** %2, align 4
  %4 = load <8 x double>*, <8 x double>** %2, align 4
  %5 = load <8 x double>, <8 x double>* %4, align 64
  store <8 x double> %5, <8 x double>* %3, align 64
  ret void
}

; CHECK-LABEL: ref_v8f64
; CHECK:       addi sp, sp, -128
; CHECK:       store32 fp, 124(sp)           
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 116(fp)
; CHECK:       load_v8i64 v0, (s0)
; CHECK:       store_v8i64 v0, (fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 124(sp)            
; CHECK:       addi sp, sp, 128
; CHECK:       jret


attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }

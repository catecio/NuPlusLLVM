; RUN: llc -march=nuplus < %s | FileCheck %s

define void @foo_v16f32(<16 x float>, <16 x float>, <16 x float>, <16 x float>, <16 x float>, <16 x float>, <16 x float>, <16 x float>, <16 x float>, <16 x float>) #0 {
  %11 = alloca <16 x float>, align 64
  %12 = alloca <16 x float>, align 64
  %13 = alloca <16 x float>, align 64
  %14 = alloca <16 x float>, align 64
  %15 = alloca <16 x float>, align 64
  %16 = alloca <16 x float>, align 64
  %17 = alloca <16 x float>, align 64
  %18 = alloca <16 x float>, align 64
  %19 = alloca <16 x float>, align 64
  %20 = alloca <16 x float>, align 64
  store <16 x float> %0, <16 x float>* %11, align 64
  store <16 x float> %1, <16 x float>* %12, align 64
  store <16 x float> %2, <16 x float>* %13, align 64
  store <16 x float> %3, <16 x float>* %14, align 64
  store <16 x float> %4, <16 x float>* %15, align 64
  store <16 x float> %5, <16 x float>* %16, align 64
  store <16 x float> %6, <16 x float>* %17, align 64
  store <16 x float> %7, <16 x float>* %18, align 64
  store <16 x float> %8, <16 x float>* %19, align 64
  store <16 x float> %9, <16 x float>* %20, align 64
  ret void
}

; CHECK-LABEL: foo_v16f32
; CHECK:       moveih s0, -1
; CHECK:       moveil s0, 64832
; CHECK:       add_i32 sp, sp, s0
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 700
; CHECK:       add_i32 s1, sp, s1
; CHECK:       store32 fp, (s1)           
; CHECK:       move_i32 fp, sp
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 576
; CHECK:       add_i32 s2, fp, s2
; CHECK:       store_v16i32 v0, (s2)
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 512
; CHECK:       add_i32 s3, fp, s3
; CHECK:       store_v16i32 v1, (s3)
; CHECK:       moveih s4, 0
; CHECK:       moveil s4, 448
; CHECK:       add_i32 s4, fp, s4
; CHECK:       store_v16i32 v2, (s4)
; CHECK:       moveih s5, 0
; CHECK:       moveil s5, 384
; CHECK:       add_i32 s5, fp, s5
; CHECK:       store_v16i32 v3, (s5)
; CHECK:       moveih s6, 0
; CHECK:       moveil s6, 320
; CHECK:       add_i32 s6, fp, s6
; CHECK:       store_v16i32 v4, (s6)
; CHECK:       moveih s7, 0
; CHECK:       moveil s7, 256
; CHECK:       add_i32 s7, fp, s7
; CHECK:       store_v16i32 v5, (s7)
; CHECK:       store_v16i32 v6, 192(fp)
; CHECK:       store_v16i32 v7, 128(fp)
; CHECK:       moveih s8, 0
; CHECK:       moveil s8, 704
; CHECK:       add_i32 s8, fp, s8
; CHECK:       load_v16i32 v0, (s8)
; CHECK:       store_v16i32 v0, 64(fp)
; CHECK:       moveih s9, 0
; CHECK:       moveil s9, 768
; CHECK:       add_i32 s9, fp, s9
; CHECK:       load_v16i32 v0, (s9)
; CHECK:       store_v16i32 v0, (fp)
; CHECK:       move_i32 sp, fp
; CHECK:       moveih s10, 0
; CHECK:       moveil s10, 700
; CHECK:       add_i32 s10, sp, s10
; CHECK:       load32 fp, (s10)             
; CHECK:       moveih s11, 0
; CHECK:       moveil s11, 704
; CHECK:       add_i32 sp, sp, s11
; CHECK:       jret

define void @foo_v8f64(<8 x double>, <8 x double>, <8 x double>, <8 x double>, <8 x double>, <8 x double>, <8 x double>, <8 x double>, <8 x double>, <8 x double>) #0 {
  %11 = alloca <8 x double>, align 64
  %12 = alloca <8 x double>, align 64
  %13 = alloca <8 x double>, align 64
  %14 = alloca <8 x double>, align 64
  %15 = alloca <8 x double>, align 64
  %16 = alloca <8 x double>, align 64
  %17 = alloca <8 x double>, align 64
  %18 = alloca <8 x double>, align 64
  %19 = alloca <8 x double>, align 64
  %20 = alloca <8 x double>, align 64
  store <8 x double> %0, <8 x double>* %11, align 64
  store <8 x double> %1, <8 x double>* %12, align 64
  store <8 x double> %2, <8 x double>* %13, align 64
  store <8 x double> %3, <8 x double>* %14, align 64
  store <8 x double> %4, <8 x double>* %15, align 64
  store <8 x double> %5, <8 x double>* %16, align 64
  store <8 x double> %6, <8 x double>* %17, align 64
  store <8 x double> %7, <8 x double>* %18, align 64
  store <8 x double> %8, <8 x double>* %19, align 64
  store <8 x double> %9, <8 x double>* %20, align 64
  ret void
}

; CHECK-LABEL: foo_v8f64
; CHECK:       moveih s0, -1
; CHECK:       moveil s0, 64832
; CHECK:       add_i32 sp, sp, s0
; CHECK:       moveih s1, 0
; CHECK:       moveil s1, 700
; CHECK:       add_i32 s1, sp, s1
; CHECK:       store32 fp, (s1)              
; CHECK:       move_i32 fp, sp
; CHECK:       moveih s2, 0
; CHECK:       moveil s2, 576
; CHECK:       add_i32 s2, fp, s2
; CHECK:       store_v8i64 v0, (s2)
; CHECK:       moveih s3, 0
; CHECK:       moveil s3, 512
; CHECK:       add_i32 s3, fp, s3
; CHECK:       store_v8i64 v1, (s3)
; CHECK:       moveih s4, 0
; CHECK:       moveil s4, 448
; CHECK:       add_i32 s4, fp, s4
; CHECK:       store_v8i64 v2, (s4)
; CHECK:       moveih s5, 0
; CHECK:       moveil s5, 384
; CHECK:       add_i32 s5, fp, s5
; CHECK:       store_v8i64 v3, (s5)
; CHECK:       moveih s6, 0
; CHECK:       moveil s6, 320
; CHECK:       add_i32 s6, fp, s6
; CHECK:       store_v8i64 v4, (s6)
; CHECK:       moveih s7, 0
; CHECK:       moveil s7, 256
; CHECK:       add_i32 s7, fp, s7
; CHECK:       store_v8i64 v5, (s7)
; CHECK:       store_v8i64 v6, 192(fp)
; CHECK:       store_v8i64 v7, 128(fp)
; CHECK:       moveih s8, 0
; CHECK:       moveil s8, 704
; CHECK:       add_i32 s8, fp, s8
; CHECK:       load_v8i64 v0, (s8)
; CHECK:       store_v8i64 v0, 64(fp)
; CHECK:       moveih s9, 0
; CHECK:       moveil s9, 768
; CHECK:       add_i32 s9, fp, s9
; CHECK:       load_v8i64 v0, (s9)
; CHECK:       store_v8i64 v0, (fp)
; CHECK:       move_i32 sp, fp
; CHECK:       moveih s10, 0
; CHECK:       moveil s10, 700
; CHECK:       add_i32 s10, sp, s10
; CHECK:       load32 fp, (s10)              
; CHECK:       moveih s11, 0
; CHECK:       moveil s11, 704
; CHECK:       add_i32 sp, sp, s11
; CHECK:       jret


attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }
; RUN: llc -march=nuplus < %s | FileCheck %s

define void @char(i8 signext) #0 {
  %2 = alloca i8, align 1
  store i8 %0, i8* %2, align 1
  ret void
}

; CHECK-LABEL: char
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)            
; CHECK:       move_i32 fp, sp
; CHECK:       store32_8 s0, 55(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)             
; CHECK:       addi sp, sp, 64
; CHECK:       jret

define void @short(i16 signext) #0 {
  %2 = alloca i16, align 2
  store i16 %0, i16* %2, align 2
  ret void
}

; CHECK-LABEL: short
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)             
; CHECK:       move_i32 fp, sp
; CHECK:       store32_16 s0, 54(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)              
; CHECK:       addi sp, sp, 64
; CHECK:       jret

define void @integer(i32) #0 {
  %2 = alloca i32, align 4
  store i32 %0, i32* %2, align 4
  ret void
}

; CHECK-LABEL: integer
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)             
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)              
; CHECK:       addi sp, sp, 64
; CHECK:       jret

define void @llinteger(i64) #0 {
  %2 = alloca i64, align 8
  store i64 %0, i64* %2, align 8
  ret void
}

; CHECK-LABEL: llinteger
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)             
; CHECK:       move_i32 fp, sp
; CHECK:       store64 s0_64, 48(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)              
; CHECK:       addi sp, sp, 64
; CHECK:       jret


define void @foo_char(i8 signext, i8 signext, i8 signext, i8 signext, i8 signext, i8 signext, i8 signext, i8 signext, i8 signext, i8 signext) #0 {
  %11 = alloca i8, align 1
  %12 = alloca i8, align 1
  %13 = alloca i8, align 1
  %14 = alloca i8, align 1
  %15 = alloca i8, align 1
  %16 = alloca i8, align 1
  %17 = alloca i8, align 1
  %18 = alloca i8, align 1
  %19 = alloca i8, align 1
  %20 = alloca i8, align 1
  store i8 %0, i8* %11, align 1
  store i8 %1, i8* %12, align 1
  store i8 %2, i8* %13, align 1
  store i8 %3, i8* %14, align 1
  store i8 %4, i8* %15, align 1
  store i8 %5, i8* %16, align 1
  store i8 %6, i8* %17, align 1
  store i8 %7, i8* %18, align 1
  store i8 %8, i8* %19, align 1
  store i8 %9, i8* %20, align 1
  ret void
}

; CHECK-LABEL: foo_char
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)              
; CHECK:       move_i32 fp, sp
; CHECK:       store32_8 s0, 55(fp)
; CHECK:       store32_8 s1, 54(fp)
; CHECK:       store32_8 s2, 53(fp)
; CHECK:       store32_8 s3, 52(fp)
; CHECK:       store32_8 s4, 51(fp)
; CHECK:       store32_8 s5, 50(fp)
; CHECK:       store32_8 s6, 49(fp)
; CHECK:       store32_8 s7, 48(fp)
; CHECK:       load32 s0, 64(fp)
; CHECK:       store32_8 s0, 47(fp)
; CHECK:       load32 s0, 68(fp)
; CHECK:       store32_8 s0, 46(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)               
; CHECK:       addi sp, sp, 64
; CHECK:       jret


define void @foo_short(i16 signext, i16 signext, i16 signext, i16 signext, i16 signext, i16 signext, i16 signext, i16 signext, i16 signext, i16 signext) #0 {
  %11 = alloca i16, align 2
  %12 = alloca i16, align 2
  %13 = alloca i16, align 2
  %14 = alloca i16, align 2
  %15 = alloca i16, align 2
  %16 = alloca i16, align 2
  %17 = alloca i16, align 2
  %18 = alloca i16, align 2
  %19 = alloca i16, align 2
  %20 = alloca i16, align 2
  store i16 %0, i16* %11, align 2
  store i16 %1, i16* %12, align 2
  store i16 %2, i16* %13, align 2
  store i16 %3, i16* %14, align 2
  store i16 %4, i16* %15, align 2
  store i16 %5, i16* %16, align 2
  store i16 %6, i16* %17, align 2
  store i16 %7, i16* %18, align 2
  store i16 %8, i16* %19, align 2
  store i16 %9, i16* %20, align 2
  ret void
}

; CHECK-LABEL: foo_short
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)             
; CHECK:       move_i32 fp, sp
; CHECK:       store32_16 s0, 54(fp)
; CHECK:       store32_16 s1, 52(fp)
; CHECK:       store32_16 s2, 50(fp)
; CHECK:       store32_16 s3, 48(fp)
; CHECK:       store32_16 s4, 46(fp)
; CHECK:       store32_16 s5, 44(fp)
; CHECK:       store32_16 s6, 42(fp)
; CHECK:       store32_16 s7, 40(fp)
; CHECK:       load32 s0, 64(fp)
; CHECK:       store32_16 s0, 38(fp)
; CHECK:       load32 s0, 68(fp)
; CHECK:       store32_16 s0, 36(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)              
; CHECK:       addi sp, sp, 64
; CHECK:       jret

define void @foo_int(i32, i32, i32, i32, i32, i32, i32, i32, i32, i32) #0 {
  %11 = alloca i32, align 4
  %12 = alloca i32, align 4
  %13 = alloca i32, align 4
  %14 = alloca i32, align 4
  %15 = alloca i32, align 4
  %16 = alloca i32, align 4
  %17 = alloca i32, align 4
  %18 = alloca i32, align 4
  %19 = alloca i32, align 4
  %20 = alloca i32, align 4
  store i32 %0, i32* %11, align 4
  store i32 %1, i32* %12, align 4
  store i32 %2, i32* %13, align 4
  store i32 %3, i32* %14, align 4
  store i32 %4, i32* %15, align 4
  store i32 %5, i32* %16, align 4
  store i32 %6, i32* %17, align 4
  store i32 %7, i32* %18, align 4
  store i32 %8, i32* %19, align 4
  store i32 %9, i32* %20, align 4
  ret void
}

; CHECK-LABEL: foo_int
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)             
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       store32 s1, 48(fp)
; CHECK:       store32 s2, 44(fp)
; CHECK:       store32 s3, 40(fp)
; CHECK:       store32 s4, 36(fp)
; CHECK:       store32 s5, 32(fp)
; CHECK:       store32 s6, 28(fp)
; CHECK:       store32 s7, 24(fp)
; CHECK:       load32 s0, 64(fp)
; CHECK:       store32 s0, 20(fp)
; CHECK:       load32 s0, 68(fp)
; CHECK:       store32 s0, 16(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)              
; CHECK:       addi sp, sp, 64
; CHECK:       jret


define void @foo_llint(i64, i64, i64, i64, i64, i64, i64, i64, i64, i64) #0 {
  %11 = alloca i64, align 8
  %12 = alloca i64, align 8
  %13 = alloca i64, align 8
  %14 = alloca i64, align 8
  %15 = alloca i64, align 8
  %16 = alloca i64, align 8
  %17 = alloca i64, align 8
  %18 = alloca i64, align 8
  %19 = alloca i64, align 8
  %20 = alloca i64, align 8
  store i64 %0, i64* %11, align 8
  store i64 %1, i64* %12, align 8
  store i64 %2, i64* %13, align 8
  store i64 %3, i64* %14, align 8
  store i64 %4, i64* %15, align 8
  store i64 %5, i64* %16, align 8
  store i64 %6, i64* %17, align 8
  store i64 %7, i64* %18, align 8
  store i64 %8, i64* %19, align 8
  store i64 %9, i64* %20, align 8
  ret void
}


; CHECK-LABEL: foo_llint
; CHECK:       addi sp, sp, -128
; CHECK:       store32 fp, 124(sp)          
; CHECK:       move_i32 fp, sp
; CHECK:       store64 s0_64, 112(fp)
; CHECK:       store64 s2_64, 104(fp)
; CHECK:       store64 s4_64, 96(fp)
; CHECK:       store64 s6_64, 88(fp)
; CHECK:       store64 s8_64, 80(fp)
; CHECK:       store64 s10_64, 72(fp)
; CHECK:       store64 s12_64, 64(fp)
; CHECK:       store64 s14_64, 56(fp)
; CHECK:       load64 s0_64, 128(fp)
; CHECK:       store64 s0_64, 48(fp)
; CHECK:       load64 s0_64, 136(fp)
; CHECK:       store64 s0_64, 40(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 124(sp)            
; CHECK:       addi sp, sp, 128
; CHECK:       jret


attributes #0 = { #0 uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }

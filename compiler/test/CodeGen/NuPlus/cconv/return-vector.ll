; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i32> @ret_v16i32() nounwind {
  %1 = alloca <16 x i32>, align 64
  %2 = load <16 x i32>, <16 x i32>* %1, align 64
  ret <16 x i32> %2
}

; CHECK-LABEL: ret_v16i32
; CHECK:       addi sp, sp, -128
; CHECK:       load_v16i32 v0, 64(sp)
; CHECK:       addi sp, sp, 128

define <8 x i64> @ret_v8i64() nounwind {
  %1 = alloca <8 x i64>, align 64
  %2 = load <8 x i64>, <8 x i64>* %1, align 64
  ret <8 x i64> %2
}

; CHECK-LABEL: ret_v8i64
; CHECK:       addi sp, sp, -128
; CHECK:       load_v8i64 v0, 64(sp)
; CHECK:       addi sp, sp, 128

; RUN: llc -march=nuplus < %s | FileCheck %s

; TODO: (Catello) returning v8f32 is not supported yet

define <16 x i8> @ret_v16i8() nounwind {
  %1 = alloca <16 x i8>, align 16
  %2 = load <16 x i8>, <16 x i8>* %1, align 16
  ret <16 x i8> %2
}

; CHECK-LABEL: ret_v16i8
; CHECK:       load_v16i8 v0, 48(sp)

define <16 x i16> @ret_v16i16() nounwind {
  %1 = alloca <16 x i16>, align 32
  %2 = load <16 x i16>, <16 x i16>* %1, align 32
  ret <16 x i16> %2
}

; CHECK-LABEL: ret_v16i16
; CHECK:       load_v16i16 v0, 32(sp)

define <8 x i8> @ret_v8i8() nounwind {
  %1 = alloca <8 x i8>, align 8
  %2 = load <8 x i8>, <8 x i8>* %1, align 8
  ret <8 x i8> %2
}

; CHECK-LABEL: ret_v8i8
; CHECK:       load_v8i8 v0, 56(sp)

define <8 x i16> @ret_v8i16() nounwind {
  %1 = alloca <8 x i16>, align 16
  %2 = load <8 x i16>, <8 x i16>* %1, align 16
  ret <8 x i16> %2
}

; CHECK-LABEL: ret_v8i16
; CHECK:       load_v8i16 v0, 48(sp)

define <8 x i32> @ret_v8i32() nounwind {
  %1 = alloca <8 x i32>, align 32
  %2 = load <8 x i32>, <8 x i32>* %1, align 32
  ret <8 x i32> %2
}

; CHECK-LABEL: ret_v8i32
; CHECK:       load_v8i32 v0, 32(sp)

define <8 x float> @ret_v8f32() nounwind {
  %1 = alloca <8 x float>, align 32
  %2 = load <8 x float>, <8 x float>* %1, align 32
  ret <8 x float> %2
}


; CHECK-LABEL: ret_v8f32

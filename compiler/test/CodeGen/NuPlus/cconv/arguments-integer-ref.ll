; RUN: llc -march=nuplus < %s | FileCheck %s

define void @ref_char(i8* dereferenceable(1)) #0 {
  %2 = alloca i8*, align 4
  %3 = alloca i8, align 1
  store i8* %0, i8** %2, align 4
  %4 = load i8*, i8** %2, align 4
  %5 = load i8, i8* %4, align 1
  store i8 %5, i8* %3, align 1
  ret void
}

; CHECK-LABEL: ref_char
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)            
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       load32_u8 s0, (s0)
; CHECK:       store32_8 s0, 51(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)             
; CHECK:       addi sp, sp, 64
; CHECK:       jret

define void @ref_short(i16* dereferenceable(2)) #0 {
  %2 = alloca i16*, align 4
  %3 = alloca i16, align 2
  store i16* %0, i16** %2, align 4
  %4 = load i16*, i16** %2, align 4
  %5 = load i16, i16* %4, align 2
  store i16 %5, i16* %3, align 2
  ret void
}

; CHECK-LABEL: ref_short
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)            
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       load32_u16 s0, (s0)
; CHECK:       store32_16 s0, 50(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)             
; CHECK:       addi sp, sp, 64
; CHECK:       jret

define void @ref_int(i32* dereferenceable(4)) #0 {
  %2 = alloca i32*, align 4
  %3 = alloca i32, align 4
  store i32* %0, i32** %2, align 4
  %4 = load i32*, i32** %2, align 4
  %5 = load i32, i32* %4, align 4
  store i32 %5, i32* %3, align 4
  ret void
}

; CHECK-LABEL: ref_int
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)           
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       load32 s0, (s0)
; CHECK:       store32 s0, 48(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)            
; CHECK:       addi sp, sp, 64
; CHECK:       jret

define void @ref_llint(i64* dereferenceable(8)) #0 {
  %2 = alloca i64*, align 4
  %3 = alloca i64, align 8
  store i64* %0, i64** %2, align 4
  %4 = load i64*, i64** %2, align 4
  %5 = load i64, i64* %4, align 8
  store i64 %5, i64* %3, align 8
  ret void
}

; CHECK-LABEL: ref_llint
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)           
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       load64 s0_64, (s0)
; CHECK:       store64 s0_64, 40(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)            
; CHECK:       addi sp, sp, 64
; CHECK:       jret


attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }

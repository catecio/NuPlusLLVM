; RUN: llc -march=nuplus < %s | FileCheck %s

define void @ref_float(float* dereferenceable(4)) #0 {
  %2 = alloca float*, align 4
  %3 = alloca float, align 4
  store float* %0, float** %2, align 4
  %4 = load float*, float** %2, align 4
  %5 = load float, float* %4, align 4
  store float %5, float* %3, align 4
  ret void
}

; CHECK-LABEL: ref_float
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)              
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       load32 s0, (s0)
; CHECK:       store32 s0, 48(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)              
; CHECK:       addi sp, sp, 64
; CHECK:       jret

define void @ref_double(double* dereferenceable(8)) #0 {
  %2 = alloca double*, align 4
  %3 = alloca double, align 8
  store double* %0, double** %2, align 4
  %4 = load double*, double** %2, align 4
  %5 = load double, double* %4, align 8
  store double %5, double* %3, align 8
  ret void
}

; CHECK-LABEL: ref_double
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)            
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       load64 s0_64, (s0)
; CHECK:       store64 s0_64, 40(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)             
; CHECK:       addi sp, sp, 64
; CHECK:       jret


attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }

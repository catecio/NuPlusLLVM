; RUN: llc -march=nuplus < %s | FileCheck %s

define float @ret_float() nounwind {
  %1 = alloca float, align 4
  %2 = load float, float* %1, align 4
  ret float %2
}

; CHECK-LABEL: ret_float
; CHECK:       load32 s0, 60(sp)

define double @ret_double() nounwind {
  %1 = alloca double, align 8
  %2 = load double, double* %1, align 8
  ret double %2
}

; CHECK-LABEL: ret_double
; CHECK:       load64 s0_64, 56(sp)


; RUN: llc -march=nuplus < %s | FileCheck %s

; TODO: (Catello) add support for vector < 512 bit

define void @foo_v16i8(<16 x i8>) #0 {
  %2 = alloca <16 x i8>, align 16
  %3 = alloca <16 x i8>, align 16
  store <16 x i8> %0, <16 x i8>* %2, align 16
  %4 = load <16 x i8>, <16 x i8>* %2, align 16
  store <16 x i8> %4, <16 x i8>* %3, align 16
  ret void
}

; CHECK-LABEL: foo_v16i8
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)           
; CHECK:       move_i32 fp, sp
; CHECK:       store_v16i8 v0, 32(fp)
; CHECK:       store_v16i8 v0, 16(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)            
; CHECK:       addi sp, sp, 64
; CHECK:       jret


define void @foo_v16i16(<16 x i16>) #0 {
  %2 = alloca <16 x i16>, align 32
  %3 = alloca <16 x i16>, align 32
  store <16 x i16> %0, <16 x i16>* %2, align 32
  %4 = load <16 x i16>, <16 x i16>* %2, align 32
  store <16 x i16> %4, <16 x i16>* %3, align 32
  ret void
}

; CHECK-LABEL: foo_v16i16
; CHECK:       addi sp, sp, -128
; CHECK:       store32 fp, 124(sp)           
; CHECK:       move_i32 fp, sp
; CHECK:       store_v16i16 v0, 64(fp)
; CHECK:       store_v16i16 v0, 32(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 124(sp)            
; CHECK:       addi sp, sp, 128
; CHECK:       jret

define void @foo_v8i8(<8 x i8>) #0 {
  %2 = alloca <8 x i8>, align 8
  %3 = alloca <8 x i8>, align 8
  store <8 x i8> %0, <8 x i8>* %2, align 8
  %4 = load <8 x i8>, <8 x i8>* %2, align 8
  store <8 x i8> %4, <8 x i8>* %3, align 8
  ret void
}

; CHECK-LABEL: foo_v8i8
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)            
; CHECK:       move_i32 fp, sp
; CHECK:       store_v8i8 v0, 48(fp)
; CHECK:       store_v8i8 v0, 40(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)              
; CHECK:       addi sp, sp, 64
; CHECK:       jret

define void @foo_v8i16(<8 x i16>) #0 {
  %2 = alloca <8 x i16>, align 16
  %3 = alloca <8 x i16>, align 16
  store <8 x i16> %0, <8 x i16>* %2, align 16
  %4 = load <8 x i16>, <8 x i16>* %2, align 16
  store <8 x i16> %4, <8 x i16>* %3, align 16
  ret void
}

; CHECK-LABEL: foo_v8i16
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)            
; CHECK:       move_i32 fp, sp
; CHECK:       store_v8i16 v0, 32(fp)
; CHECK:       store_v8i16 v0, 16(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)             
; CHECK:       addi sp, sp, 64
; CHECK:       jret

define void @foo_v8i32(<8 x i32>) #0 {
  %2 = alloca <8 x i32>, align 32
  %3 = alloca <8 x i32>, align 32
  store <8 x i32> %0, <8 x i32>* %2, align 32
  %4 = load <8 x i32>, <8 x i32>* %2, align 32
  store <8 x i32> %4, <8 x i32>* %3, align 32
  ret void
}

; CHECK-LABEL: foo_v8i32
; CHECK:       addi sp, sp, -128
; CHECK:       store32 fp, 124(sp)          
; CHECK:       move_i32 fp, sp
; CHECK:       store_v8i32 v0, 64(fp)
; CHECK:       store_v8i32 v0, 32(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 124(sp)            
; CHECK:       addi sp, sp, 128
; CHECK:       jret

define void @foo_v8f32(<8 x float>) #0 {
  %2 = alloca <8 x float>, align 32
  %3 = alloca <8 x float>, align 32
  store <8 x float> %0, <8 x float>* %2, align 32
  %4 = load <8 x float>, <8 x float>* %2, align 32
  store <8 x float> %4, <8 x float>* %3, align 32
  ret void
}

; CHECK-LABEL: foo_v8f32


attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }
; RUN: llc -march=nuplus < %s | FileCheck %s

define void @pointer_v16f32(<16 x float>*) #0 {
  %2 = alloca <16 x float>*, align 4
  %3 = alloca <16 x float>*, align 4
  store <16 x float>* %0, <16 x float>** %2, align 4
  %4 = load <16 x float>*, <16 x float>** %2, align 4
  store <16 x float>* %4, <16 x float>** %3, align 4
  ret void
}

; CHECK-LABEL: pointer_v16f32
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)            
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       store32 s0, 48(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)              
; CHECK:       addi sp, sp, 64
; CHECK:       jret

define void @pointer_v8f64(<8 x double>*) #0 {
  %2 = alloca <8 x double>*, align 4
  %3 = alloca <8 x double>*, align 4
  store <8 x double>* %0, <8 x double>** %2, align 4
  %4 = load <8 x double>*, <8 x double>** %2, align 4
  store <8 x double>* %4, <8 x double>** %3, align 4
  ret void
}

; CHECK-LABEL: pointer_v8f64
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)            
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       store32 s0, 48(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)              
; CHECK:       addi sp, sp, 64
; CHECK:       jret

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }

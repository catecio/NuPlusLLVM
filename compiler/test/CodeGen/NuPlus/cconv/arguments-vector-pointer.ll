; RUN: llc -march=nuplus < %s | FileCheck %s

define void @pointer_v16i32(<16 x i32>*) #0 {
  %2 = alloca <16 x i32>*, align 4
  %3 = alloca <16 x i32>*, align 4
  store <16 x i32>* %0, <16 x i32>** %2, align 4
  %4 = load <16 x i32>*, <16 x i32>** %2, align 4
  store <16 x i32>* %4, <16 x i32>** %3, align 4
  ret void
}

; CHECK-LABEL: pointer_v16i32
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)             
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       store32 s0, 48(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)              
; CHECK:       addi sp, sp, 64
; CHECK:       jret


define void @pointer_v8i64(<8 x i64>*) #0 {
  %2 = alloca <8 x i64>*, align 4
  %3 = alloca <8 x i64>*, align 4
  store <8 x i64>* %0, <8 x i64>** %2, align 4
  %4 = load <8 x i64>*, <8 x i64>** %2, align 4
  store <8 x i64>* %4, <8 x i64>** %3, align 4
  ret void
}

; CHECK-LABEL: pointer_v8i64
; CHECK:       addi sp, sp, -64
; CHECK:       store32 fp, 60(sp)            
; CHECK:       move_i32 fp, sp
; CHECK:       store32 s0, 52(fp)
; CHECK:       store32 s0, 48(fp)
; CHECK:       move_i32 sp, fp
; CHECK:       load32 fp, 60(sp)              
; CHECK:       addi sp, sp, 64
; CHECK:       jret

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }

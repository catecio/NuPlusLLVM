; RUN: llc -march=nuplus < %s | FileCheck %s


@bytes = global [11 x i8] zeroinitializer
@dwords = global [11 x i64] zeroinitializer
@floats = global [11 x float] zeroinitializer
@doubles = global [11 x double] zeroinitializer

define void @double_args(double %a, double %b, double %c, double %d, double %e,
                         double %f, double %g, double %h, double %i) #0 {
entry:
        %0 = getelementptr [11 x double], [11 x double]* @doubles, i32 0, i32 1
        store volatile double %a, double* %0
        %1 = getelementptr [11 x double], [11 x double]* @doubles, i32 0, i32 2
        store volatile double %b, double* %1
        %2 = getelementptr [11 x double], [11 x double]* @doubles, i32 0, i32 3
        store volatile double %c, double* %2
        %3 = getelementptr [11 x double], [11 x double]* @doubles, i32 0, i32 4
        store volatile double %d, double* %3
        %4 = getelementptr [11 x double], [11 x double]* @doubles, i32 0, i32 5
        store volatile double %e, double* %4
        %5 = getelementptr [11 x double], [11 x double]* @doubles, i32 0, i32 6
        store volatile double %f, double* %5
        %6 = getelementptr [11 x double], [11 x double]* @doubles, i32 0, i32 7
        store volatile double %g, double* %6
        %7 = getelementptr [11 x double], [11 x double]* @doubles, i32 0, i32 8
        store volatile double %h, double* %7
        %8 = getelementptr [11 x double], [11 x double]* @doubles, i32 0, i32 9
        store volatile double %i, double* %8
        ret void
}
; CHECK-LABEL: double_args
; CHECK:        addi sp, sp, -64
; CHECK: 	store32 fp, 60(sp)            
; CHECK: 	move_i32 fp, sp
; CHECK: 	leah s16, doubles
; CHECK: 	leal s16, doubles
; CHECK: 	store64 s0_64, 8(s16)
; CHECK: 	store64 s2_64, 16(s16)
; CHECK: 	store64 s4_64, 24(s16)
; CHECK: 	store64 s6_64, 32(s16)
; CHECK: 	store64 s8_64, 40(s16)
; CHECK: 	store64 s10_64, 48(s16)
; CHECK: 	store64 s12_64, 56(s16)
; CHECK: 	store64 s14_64, 64(s16)
; CHECK: 	load64 s0_64, 64(fp)
; CHECK: 	store64 s0_64, 72(s16)
; CHECK: 	move_i32 sp, fp
; CHECK: 	load32 fp, 60(sp)             
; CHECK: 	addi sp, sp, 64
; CHECK: 	jret


define void @float_args(float %a, float %b, float %c, float %d, float %e,
                        float %f, float %g, float %h, float %i, float %j)
                       #0 {
entry:
        %0 = getelementptr [11 x float], [11 x float]* @floats, i32 0, i32 1
        store volatile float %a, float* %0
        %1 = getelementptr [11 x float], [11 x float]* @floats, i32 0, i32 2
        store volatile float %b, float* %1
        %2 = getelementptr [11 x float], [11 x float]* @floats, i32 0, i32 3
        store volatile float %c, float* %2
        %3 = getelementptr [11 x float], [11 x float]* @floats, i32 0, i32 4
        store volatile float %d, float* %3
        %4 = getelementptr [11 x float], [11 x float]* @floats, i32 0, i32 5
        store volatile float %e, float* %4
        %5 = getelementptr [11 x float], [11 x float]* @floats, i32 0, i32 6
        store volatile float %f, float* %5
        %6 = getelementptr [11 x float], [11 x float]* @floats, i32 0, i32 7
        store volatile float %g, float* %6
        %7 = getelementptr [11 x float], [11 x float]* @floats, i32 0, i32 8
        store volatile float %h, float* %7
        %8 = getelementptr [11 x float], [11 x float]* @floats, i32 0, i32 9
        store volatile float %i, float* %8
        %9 = getelementptr [11 x float], [11 x float]* @floats, i32 0, i32 10
        store volatile float %j, float* %9
        ret void
}

; CHECK-LABEL: float_args
; CHECK: 	addi sp, sp, -64
; CHECK: 	store32 fp, 60(sp)          
; CHECK: 	move_i32 fp, sp
; CHECK: 	leah s8, floats
; CHECK: 	leal s8, floats
; CHECK: 	store32 s0, 4(s8)
; CHECK: 	store32 s1, 8(s8)
; CHECK: 	store32 s2, 12(s8)
; CHECK: 	store32 s3, 16(s8)
; CHECK: 	store32 s4, 20(s8)
; CHECK: 	store32 s5, 24(s8)
; CHECK: 	store32 s6, 28(s8)
; CHECK: 	store32 s7, 32(s8)
; CHECK: 	load32 s0, 64(fp)
; CHECK: 	store32 s0, 36(s8)
; CHECK: 	load32 s0, 68(fp)
; CHECK: 	store32 s0, 40(s8)
; CHECK: 	move_i32 sp, fp
; CHECK: 	load32 fp, 60(sp)          
; CHECK: 	addi sp, sp, 64
; CHECK: 	jret


define void @double_arg2(i8 %a, double %b) #0 {
entry:
        %0 = getelementptr [11 x i8], [11 x i8]* @bytes, i32 0, i32 1
        store volatile i8 %a, i8* %0
        %1 = getelementptr [11 x double], [11 x double]* @doubles, i32 0, i32 1
        store volatile double %b, double* %1
        ret void
}

; CHECK-LABEL: double_arg2
; CHECK:	addi sp, sp, -64
; CHECK: 	store32 fp, 60(sp)             
; CHECK: 	move_i32 fp, sp
; CHECK: 	leah s1, bytes
; CHECK: 	leal s1, bytes
; CHECK: 	store32_8 s0, 1(s1)
; CHECK: 	leah s0, doubles
; CHECK: 	leal s0, doubles
; CHECK: 	store64 s2_64, 8(s0)
; CHECK: 	move_i32 sp, fp
; CHECK: 	load32 fp, 60(sp)             
; CHECK: 	addi sp, sp, 64
; CHECK: 	jret

define void @float_arg2(i8 signext %a, float %b) #0 {
entry:
        %0 = getelementptr [11 x i8], [11 x i8]* @bytes, i32 0, i32 1
        store volatile i8 %a, i8* %0
        %1 = getelementptr [11 x float], [11 x float]* @floats, i32 0, i32 1
        store volatile float %b, float* %1
        ret void
}

; CHECK-LABEL: float_arg2
; CHECK: 	addi sp, sp, -64
; CHECK: 	store32 fp, 60(sp)            
; CHECK: 	move_i32 fp, sp
; CHECK: 	leah s2, bytes
; CHECK: 	leal s2, bytes
; CHECK: 	store32_8 s0, 1(s2)
; CHECK: 	leah s0, floats
; CHECK: 	leal s0, floats
; CHECK: 	store32 s1, 4(s0)
; CHECK: 	move_i32 sp, fp
; CHECK: 	load32 fp, 60(sp)              
; CHECK: 	addi sp, sp, 64
; CHECK: 	jret

define void @foo_double(double, double, double, double, double, double, double, double, double) #0 {
  %10 = alloca double, align 8
  %11 = alloca double, align 8
  %12 = alloca double, align 8
  %13 = alloca double, align 8
  %14 = alloca double, align 8
  %15 = alloca double, align 8
  %16 = alloca double, align 8
  %17 = alloca double, align 8
  %18 = alloca double, align 8
  store double %0, double* %10, align 8
  store double %1, double* %11, align 8
  store double %2, double* %12, align 8
  store double %3, double* %13, align 8
  store double %4, double* %14, align 8
  store double %5, double* %15, align 8
  store double %6, double* %16, align 8
  store double %7, double* %17, align 8
  store double %8, double* %18, align 8
  ret void
}

; CHECK-LABEL: foo_double
; CHECK: 	addi sp, sp, -128
; CHECK: 	store32 fp, 124(sp)           
; CHECK: 	move_i32 fp, sp
; CHECK: 	store64 s0_64, 112(fp)
; CHECK: 	store64 s2_64, 104(fp)
; CHECK: 	store64 s4_64, 96(fp)
; CHECK: 	store64 s6_64, 88(fp)
; CHECK: 	store64 s8_64, 80(fp)
; CHECK: 	store64 s10_64, 72(fp)
; CHECK: 	store64 s12_64, 64(fp)
; CHECK: 	store64 s14_64, 56(fp)
; CHECK: 	load64 s0_64, 128(fp)
; CHECK: 	store64 s0_64, 48(fp)
; CHECK: 	move_i32 sp, fp
; CHECK: 	load32 fp, 124(sp)           
; CHECK: 	addi sp, sp, 128
; CHECK: 	jret

define void @foo_float(float, float, float, float, float, float, float, float, float) #0 {
  %10 = alloca float, align 4
  %11 = alloca float, align 4
  %12 = alloca float, align 4
  %13 = alloca float, align 4
  %14 = alloca float, align 4
  %15 = alloca float, align 4
  %16 = alloca float, align 4
  %17 = alloca float, align 4
  %18 = alloca float, align 4
  store float %0, float* %10, align 4
  store float %1, float* %11, align 4
  store float %2, float* %12, align 4
  store float %3, float* %13, align 4
  store float %4, float* %14, align 4
  store float %5, float* %15, align 4
  store float %6, float* %16, align 4
  store float %7, float* %17, align 4
  store float %8, float* %18, align 4
  ret void
}


; CHECK-LABEL: foo_float
; CHECK:        addi sp, sp, -64
; CHECK: 	store32 fp, 60(sp)            
; CHECK: 	move_i32 fp, sp
; CHECK: 	store32 s0, 52(fp)
; CHECK: 	store32 s1, 48(fp)
; CHECK: 	store32 s2, 44(fp)
; CHECK: 	store32 s3, 40(fp)
; CHECK: 	store32 s4, 36(fp)
; CHECK: 	store32 s5, 32(fp)
; CHECK: 	store32 s6, 28(fp)
; CHECK: 	store32 s7, 24(fp)
; CHECK: 	load32 s0, 64(fp)
; CHECK: 	store32 s0, 20(fp)
; CHECK: 	move_i32 sp, fp
; CHECK: 	load32 fp, 60(sp)              
; CHECK: 	addi sp, sp, 64
; CHECK: 	jret

attributes #0 = { #0 uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="nuplus" "unsafe-fp-math"="false" "use-soft-float"="false" }

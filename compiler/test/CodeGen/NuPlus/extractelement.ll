; RUN: llc -march=nuplus < %s | FileCheck %s

define i32 @extractelt_v16i32(<16 x i32>  %a) nounwind {
; CHECK-LABEL: extractelt_v16i32
; CHECK: getlanei s0, v0, 2

  %ris = extractelement <16 x i32> %a, i32 2
  ret i32 %ris
}

define i16  @extractelt_v16i16(<16 x i16>  %a) nounwind {
; CHECK-LABEL: extractelt_v16i16
; CHECK: getlanei s0, v0, 2

  %ris = extractelement <16 x i16> %a, i32 2
  ret i16 %ris
}

define i8  @extractelt_v16i8(<16 x i8>  %a) nounwind {
; CHECK-LABEL: extractelt_v16i8
; CHECK: getlanei s0, v0, 2

  %ris = extractelement <16 x i8> %a, i32 2
  ret  i8 %ris
}


define i64 @extractelt_v8i64(<8 x i64>  %a) nounwind {
; CHECK-LABEL: extractelt_v8i64
; CHECK: moveih s0, 0
; CHECK: moveil s0, 2
; CHECK: getlane_i64 s0_64, v0, s0

  %ris = extractelement <8 x i64> %a, i32 2
  ret i64 %ris
}

define i32  @extractelt_v8i32(<8 x i32>  %a) nounwind {
; CHECK-LABEL: extractelt_v8i32
; CHECK: moveih s0, 0
; CHECK: moveil s0, 2
; CHECK: getlane_i64 s0_64, v0, s0

  %ris = extractelement <8 x i32> %a, i32 2
  ret i32 %ris
}

define i16  @extractelt_v8i16(<8 x i16>  %a) nounwind {
; CHECK-LABEL: extractelt_v8i16
; CHECK: moveih s0, 0
; CHECK: moveil s0, 2
; CHECK: getlane_i64 s0_64, v0, s0

  %ris = extractelement <8 x i16> %a, i32 2
  ret i16 %ris
}

define i8  @extractelt_v8i8(<8 x i8>  %a) nounwind {
; CHECK-LABEL: extractelt_v8i8
; CHECK: moveih s0, 0
; CHECK: moveil s0, 2
; CHECK: getlane_i64 s0_64, v0, s0

  %ris = extractelement <8 x i8> %a, i32 2
  ret  i8 %ris
}


define float  @extractelt_v16f32(<16 x float>  %a) nounwind {
; CHECK-LABEL: extractelt_v16f32
; CHECK: getlanei s0, v0, 2

  %ris = extractelement <16 x float> %a, i32 2
  ret float %ris
}

define double  @extractelt_v8f64(<8 x double>  %a) nounwind {
; CHECK-LABEL: extractelt_v8f64
; CHECK: moveih s0, 0
; CHECK: moveil s0, 2
; CHECK: getlane_i64 s0_64, v0, s0

  %ris = extractelement <8 x double> %a, i32 2
  ret double %ris
}

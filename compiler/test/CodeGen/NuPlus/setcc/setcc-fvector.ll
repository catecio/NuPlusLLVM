; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i32> @seteq_float(<16 x float> %a, <16 x float> %b) nounwind {
entry:

; CHECK-LABEL: seteq_float
; CHECK: cmpfeq_f32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool  = fcmp oeq  <16 x float> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setne_float(<16 x float> %a, <16 x float> %b) nounwind {
entry:

; CHECK-LABEL: setne_float
; CHECK: cmpfne_f32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = fcmp one  <16 x float> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setueq_float(<16 x float> %a, <16 x float> %b) nounwind {
entry:

; CHECK-LABEL: setueq_float
; CHECK: cmpfeq_f32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool  = fcmp ueq  <16 x float> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setune_float(<16 x float> %a, <16 x float> %b) nounwind {
entry:

; CHECK-LABEL: setune_float
; CHECK: cmpfne_f32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = fcmp une  <16 x float> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setsgt_float(<16 x float> %a, <16 x float> %b) nounwind {
entry:

; CHECK-LABEL: setsgt_float
; CHECK: cmpfgt_f32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = fcmp ogt <16 x float> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setsge_float(<16 x float> %a, <16 x float> %b) nounwind {
entry:

; CHECK-LABEL: setsge_float
; CHECK: cmpfge_f32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = fcmp oge <16 x float> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setslt_float(<16 x float> %a, <16 x float> %b) nounwind {
entry:

; CHECK-LABEL: setslt_float
; CHECK: cmpflt_f32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = fcmp olt <16 x float> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setsle_float(<16 x float> %a, <16 x float> %b) nounwind {
entry:

; CHECK-LABEL: setsle_float
; CHECK: cmpfle_f32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = fcmp ole <16 x float> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}


define <16 x i32> @setuge_float(<16 x float> %a, <16 x float> %b) nounwind {
entry:

; CHECK-LABEL: setuge_float
; CHECK: cmpfge_f32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = fcmp uge <16 x float> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setugt_float(<16 x float> %a, <16 x float> %b) nounwind {
entry:

; CHECK-LABEL: setugt_float
; CHECK: cmpfgt_f32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = fcmp ugt <16 x float> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setule_float(<16 x float> %a, <16 x float> %b) nounwind {
entry:

; CHECK-LABEL: setule_float
; CHECK: cmpfle_f32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = fcmp ule <16 x float> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setult_float(<16 x float> %a, <16 x float> %b) nounwind {
entry:

; CHECK-LABEL: setult_float
; CHECK: cmpflt_f32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = fcmp ult <16 x float> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}


define <8 x i64> @seteq_double(<8 x double> %a, <8 x double> %b) nounwind {
entry:

; CHECK-LABEL: seteq_double
; CHECK: cmpfeq_f64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1


  %bool  = fcmp oeq  <8 x double> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setne_double(<8 x double> %a, <8 x double> %b) nounwind {
entry:

; CHECK-LABEL: setne_double
; CHECK: cmpfne_f64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1


  %bool = fcmp one  <8 x double> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setueq_double(<8 x double> %a, <8 x double> %b) nounwind {
entry:

; CHECK-LABEL: setueq_double
; CHECK: cmpfeq_f64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1


  %bool  = fcmp ueq  <8 x double> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setune_double(<8 x double> %a, <8 x double> %b) nounwind {
entry:

; CHECK-LABEL: setune_double
; CHECK: cmpfne_f64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1


  %bool = fcmp une  <8 x double> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setsgt_double(<8 x double> %a, <8 x double> %b) nounwind {
entry:

; CHECK-LABEL: setsgt_double
; CHECK: cmpfgt_f64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1


  %bool = fcmp ogt <8 x double> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setsge_double(<8 x double> %a, <8 x double> %b) nounwind {
entry:

; CHECK-LABEL: setsge_double
; CHECK: cmpfge_f64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1


  %bool = fcmp oge <8 x double> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setslt_double(<8 x double> %a, <8 x double> %b) nounwind {
entry:

; CHECK-LABEL: setslt_double
; CHECK: cmpflt_f64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1


  %bool = fcmp olt <8 x double> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setsle_double(<8 x double> %a, <8 x double> %b) nounwind {
entry:

; CHECK-LABEL: setsle_double
; CHECK: cmpfle_f64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1


  %bool = fcmp ole <8 x double> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}


define <8 x i64> @setuge_double(<8 x double> %a, <8 x double> %b) nounwind {
entry:

; CHECK-LABEL: setuge_double
; CHECK: cmpfge_f64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1


  %bool = fcmp uge <8 x double> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setugt_double(<8 x double> %a, <8 x double> %b) nounwind {
entry:

; CHECK-LABEL: setugt_double
; CHECK: cmpfgt_f64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1


  %bool = fcmp ugt <8 x double> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setule_double(<8 x double> %a, <8 x double> %b) nounwind {
entry:

; CHECK-LABEL: setule_double
; CHECK: cmpfle_f64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1


  %bool = fcmp ule <8 x double> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setult_double(<8 x double> %a, <8 x double> %b) nounwind {
entry:

; CHECK-LABEL: setult_double
; CHECK: cmpflt_f64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1


  %bool = fcmp ult <8 x double> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

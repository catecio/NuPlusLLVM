; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i32> @seteq_i32(<16 x i32> %a, <16 x i32> %b) nounwind {
entry:

; CHECK-LABEL: seteq_i32
; CHECK: cmpeq_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp eq <16 x i32> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setne_i32(<16 x i32> %a, <16 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setne_i32
; CHECK: cmpne_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp ne  <16 x i32> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setsgt_i32(<16 x i32> %a, <16 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setsgt_i32
; CHECK: cmpgt_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp sgt <16 x i32> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setsge_i32(<16 x i32> %a, <16 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setsge_i32
; CHECK: cmpge_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp sge <16 x i32> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setslt_i32(<16 x i32> %a, <16 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setslt_i32
; CHECK: cmplt_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp slt <16 x i32> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setsle_i32(<16 x i32> %a, <16 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setsle_i32
; CHECK: cmple_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp sle <16 x i32> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}


define <16 x i32> @setuge_i32(<16 x i32> %a, <16 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setuge_i32
; CHECK: cmpuge_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp uge <16 x i32> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setugt_i32(<16 x i32> %a, <16 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setugt_i32
; CHECK: cmpugt_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp ugt <16 x i32> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setule_i32(<16 x i32> %a, <16 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setule_i32
; CHECK: cmpule_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp ule <16 x i32> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setult_i32(<16 x i32> %a, <16 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setult_i32
; CHECK: cmpult_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp ult <16 x i32> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}


define <8 x i64> @seteq_i64(<8 x i64> %a, <8 x i64> %b) nounwind {
entry:

; CHECK-LABEL: seteq_i64
; CHECK: cmpeq_i64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1

  %bool  = icmp eq  <8 x i64> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setne_i64(<8 x i64> %a, <8 x i64> %b) nounwind {
entry:

; CHECK-LABEL: setne_i64
; CHECK: cmpne_i64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp ne  <8 x i64> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setsgt_i64(<8 x i64> %a, <8 x i64> %b) nounwind {
entry:

; CHECK-LABEL: setsgt_i64
; CHECK: cmpgt_i64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp sgt <8 x i64> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setsge_i64(<8 x i64> %a, <8 x i64> %b) nounwind {
entry:

; CHECK-LABEL: setsge_i64
; CHECK: cmpge_i64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp sge <8 x i64> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setslt_i64(<8 x i64> %a, <8 x i64> %b) nounwind {
entry:

; CHECK-LABEL: setslt_i64
; CHECK: cmplt_i64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp slt <8 x i64> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setsle_i64(<8 x i64> %a, <8 x i64> %b) nounwind {
entry:

; CHECK-LABEL: setsle_i64
; CHECK: cmple_i64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp sle <8 x i64> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}


define <8 x i64> @setuge_i64(<8 x i64> %a, <8 x i64> %b) nounwind {
entry:

; CHECK-LABEL: setuge_i64
; CHECK: cmpuge_i64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp uge <8 x i64> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setugt_i64(<8 x i64> %a, <8 x i64> %b) nounwind {
entry:

; CHECK-LABEL: setugt_i64
; CHECK: cmpugt_i64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp ugt <8 x i64> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setule_i64(<8 x i64> %a, <8 x i64> %b) nounwind {
entry:

; CHECK-LABEL: setule_i64
; CHECK: cmpule_i64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp ule <8 x i64> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setult_i64(<8 x i64> %a, <8 x i64> %b) nounwind {
entry:

; CHECK-LABEL: setult_i64
; CHECK: cmpult_i64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp ult <8 x i64> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

; RUN: llc -march=nuplus < %s | FileCheck %s

define zeroext i1 @seteq_i32(i32 %a, i32 %b) nounwind {
entry:

; CHECK-LABEL: seteq_i32
; CHECK: cmpeq_i32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool  = icmp eq  i32 %a, %b
  ret i1 %bool
}

define zeroext i1 @setne_i32(i32 %a, i32 %b) nounwind {
entry:

; CHECK-LABEL: setne_i32
; CHECK: cmpne_i32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool = icmp ne  i32 %a, %b
  ret i1 %bool
}

define zeroext i1 @setsgt_i32(i32 %a, i32 %b) nounwind {
entry:

; CHECK-LABEL: setsgt_i32
; CHECK: cmpgt_i32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool = icmp sgt i32 %a, %b
  ret i1 %bool
}

define zeroext i1 @setsge_i32(i32 %a, i32 %b) nounwind {
entry:

; CHECK-LABEL: setsge_i32
; CHECK: cmpge_i32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool = icmp sge i32 %a, %b
  ret i1 %bool
}

define zeroext i1 @setslt_i32(i32 %a, i32 %b) nounwind {
entry:

; CHECK-LABEL: setslt_i32
; CHECK: cmplt_i32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool = icmp slt i32 %a, %b
  ret i1 %bool
}

define zeroext i1 @setsle_i32(i32 %a, i32 %b) nounwind {
entry:

; CHECK-LABEL: setsle_i32
; CHECK: cmple_i32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool = icmp sle i32 %a, %b
  ret i1 %bool
}


define zeroext i1 @setuge_i32(i32 %a, i32 %b) nounwind {
entry:

; CHECK-LABEL: setuge_i32
; CHECK: cmpuge_i32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool = icmp uge i32 %a, %b
  ret i1 %bool
}

define zeroext i1 @setugt_i32(i32 %a, i32 %b) nounwind {
entry:

; CHECK-LABEL: setugt_i32
; CHECK: cmpugt_i32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool = icmp ugt i32 %a, %b
  ret i1 %bool
}

define zeroext i1 @setule_i32(i32 %a, i32 %b) nounwind {
entry:

; CHECK-LABEL: setule_i32
; CHECK: cmpule_i32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool = icmp ule i32 %a, %b
  ret i1 %bool
}

define zeroext i1 @setult_i32(i32 %a, i32 %b) nounwind {
entry:

; CHECK-LABEL: setult_i32
; CHECK: cmpult_i32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool = icmp ult i32 %a, %b
  ret i1 %bool
}


define zeroext i1 @seteq_i64(i64 %a, i64 %b) nounwind {
entry:

; CHECK-LABEL: seteq_i64
; CHECK: cmpeq_i64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool  = icmp eq  i64 %a, %b
  ret i1 %bool
}

define zeroext i1 @setne_i64(i64 %a, i64 %b) nounwind {
entry:

; CHECK-LABEL: setne_i64
; CHECK: cmpne_i64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool = icmp ne  i64 %a, %b
  ret i1 %bool
}

define zeroext i1 @setsgt_i64(i64 %a, i64 %b) nounwind {
entry:

; CHECK-LABEL: setsgt_i64
; CHECK: cmpgt_i64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool = icmp sgt i64 %a, %b
  ret i1 %bool
}

define zeroext i1 @setsge_i64(i64 %a, i64 %b) nounwind {
entry:

; CHECK-LABEL: setsge_i64
; CHECK: cmpge_i64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool = icmp sge i64 %a, %b
  ret i1 %bool
}

define zeroext i1 @setslt_i64(i64 %a, i64 %b) nounwind {
entry:

; CHECK-LABEL: setslt_i64
; CHECK: cmplt_i64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool = icmp slt i64 %a, %b
  ret i1 %bool
}

define zeroext i1 @setsle_i64(i64 %a, i64 %b) nounwind {
entry:

; CHECK-LABEL: setsle_i64
; CHECK: cmple_i64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool = icmp sle i64 %a, %b
  ret i1 %bool
}


define zeroext i1 @setuge_i64(i64 %a, i64 %b) nounwind {
entry:

; CHECK-LABEL: setuge_i64
; CHECK: cmpuge_i64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool = icmp uge i64 %a, %b
  ret i1 %bool
}

define zeroext i1 @setugt_i64(i64 %a, i64 %b) nounwind {
entry:

; CHECK-LABEL: setugt_i64
; CHECK: cmpugt_i64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool = icmp ugt i64 %a, %b
  ret i1 %bool
}

define zeroext i1 @setule_i64(i64 %a, i64 %b) nounwind {
entry:

; CHECK-LABEL: setule_i64
; CHECK: cmpule_i64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool = icmp ule i64 %a, %b
  ret i1 %bool
}

define zeroext i1 @setult_i64(i64 %a, i64 %b) nounwind {
entry:

; CHECK-LABEL: setult_i64
; CHECK: cmpult_i64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool = icmp ult i64 %a, %b
  ret i1 %bool
}

; RUN: llc -march=nuplus < %s | FileCheck %s

define zeroext i1 @seteq_float(float %a, float %b) nounwind {
entry:

; CHECK-LABEL: seteq_float
; CHECK: cmpfeq_f32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool  = fcmp oeq  float %a, %b
  ret i1 %bool
}

define zeroext i1 @setne_float(float %a, float %b) nounwind {
entry:

; CHECK-LABEL: setne_float
; CHECK: cmpfne_f32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool = fcmp one  float %a, %b
  ret i1 %bool
}

define zeroext i1 @setueq_float(float %a, float %b) nounwind {
entry:

; CHECK-LABEL: setueq_float
; CHECK: cmpfne_f32 s0, s0, s1
; CHECK: xori s0, s0, -1
; CHECK: andi s0, s0, 1

  %bool  = fcmp ueq  float %a, %b
  ret i1 %bool
}

define zeroext i1 @setune_float(float %a, float %b) nounwind {
entry:

; CHECK-LABEL: setune_float
; CHECK: cmpfeq_f32 s0, s0, s1
; CHECK: xori s0, s0, -1
; CHECK: andi s0, s0, 1

  %bool = fcmp une  float %a, %b
  ret i1 %bool
}

define zeroext i1 @setsgt_float(float %a, float %b) nounwind {
entry:

; CHECK-LABEL: setsgt_float
; CHECK: cmpfgt_f32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool = fcmp ogt float %a, %b
  ret i1 %bool
}

define zeroext i1 @setsge_float(float %a, float %b) nounwind {
entry:

; CHECK-LABEL: setsge_float
; CHECK: cmpfge_f32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool = fcmp oge float %a, %b
  ret i1 %bool
}

define zeroext i1 @setslt_float(float %a, float %b) nounwind {
entry:

; CHECK-LABEL: setslt_float
; CHECK: cmpflt_f32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool = fcmp olt float %a, %b
  ret i1 %bool
}

define zeroext i1 @setsle_float(float %a, float %b) nounwind {
entry:

; CHECK-LABEL: setsle_float
; CHECK: cmpfle_f32 s0, s0, s1
; CHECK: andi s0, s0, 1

  %bool = fcmp ole float %a, %b
  ret i1 %bool
}


define zeroext i1 @setuge_float(float %a, float %b) nounwind {
entry:

; CHECK-LABEL: setuge_float
; CHECK: cmpflt_f32 s0, s0, s1
; CHECK: xori s0, s0, -1
; CHECK: andi s0, s0, 1

  %bool = fcmp uge float %a, %b
  ret i1 %bool
}

define zeroext i1 @setugt_float(float %a, float %b) nounwind {
entry:

; CHECK-LABEL: setugt_float
; CHECK: cmpfle_f32 s0, s0, s1
; CHECK: xori s0, s0, -1
; CHECK: andi s0, s0, 1

  %bool = fcmp ugt float %a, %b
  ret i1 %bool
}

define zeroext i1 @setule_float(float %a, float %b) nounwind {
entry:

; CHECK-LABEL: setule_float
; CHECK: cmpfgt_f32 s0, s0, s1
; CHECK: xori s0, s0, -1
; CHECK: andi s0, s0, 1

  %bool = fcmp ule float %a, %b
  ret i1 %bool
}

define zeroext i1 @setult_float(float %a, float %b) nounwind {
entry:

; CHECK-LABEL: setult_float
; CHECK: cmpfge_f32 s0, s0, s1
; CHECK: xori s0, s0, -1
; CHECK: andi s0, s0, 1

  %bool = fcmp ult float %a, %b
  ret i1 %bool
}


define zeroext i1 @seteq_double(double %a, double %b) nounwind {
entry:

; CHECK-LABEL: seteq_double
; CHECK: cmpfeq_f64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool  = fcmp oeq  double %a, %b
  ret i1 %bool
}

define zeroext i1 @setne_double(double %a, double %b) nounwind {
entry:

; CHECK-LABEL: setne_double
; CHECK: cmpfne_f64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool = fcmp one  double %a, %b
  ret i1 %bool
}

define zeroext i1 @setueq_double(double %a, double %b) nounwind {
entry:

; CHECK-LABEL: setueq_double
; CHECK: cmpfne_f64 s0, s0_64, s2_64
; CHECK: xori s0, s0, -1
; CHECK: andi s0, s0, 1

  %bool  = fcmp ueq  double %a, %b
  ret i1 %bool
}

define zeroext i1 @setune_double(double %a, double %b) nounwind {
entry:

; CHECK-LABEL: setune_double
; CHECK: cmpfeq_f64 s0, s0_64, s2_64
; CHECK: xori s0, s0, -1
; CHECK: andi s0, s0, 1

  %bool = fcmp une  double %a, %b
  ret i1 %bool
}

define zeroext i1 @setsgt_double(double %a, double %b) nounwind {
entry:

; CHECK-LABEL: setsgt_double
; CHECK: cmpfgt_f64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool = fcmp ogt double %a, %b
  ret i1 %bool
}

define zeroext i1 @setsge_double(double %a, double %b) nounwind {
entry:

; CHECK-LABEL: setsge_double
; CHECK: cmpfge_f64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool = fcmp oge double %a, %b
  ret i1 %bool
}

define zeroext i1 @setslt_double(double %a, double %b) nounwind {
entry:

; CHECK-LABEL: setslt_double
; CHECK: cmpflt_f64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool = fcmp olt double %a, %b
  ret i1 %bool
}

define zeroext i1 @setsle_double(double %a, double %b) nounwind {
entry:

; CHECK-LABEL: setsle_double
; CHECK: cmpfle_f64 s0, s0_64, s2_64
; CHECK: andi s0, s0, 1

  %bool = fcmp ole double %a, %b
  ret i1 %bool
}


define zeroext i1 @setuge_double(double %a, double %b) nounwind {
entry:

; CHECK-LABEL: setuge_double
; CHECK: cmpflt_f64 s0, s0_64, s2_64
; CHECK: xori s0, s0, -1
; CHECK: andi s0, s0, 1

  %bool = fcmp uge double %a, %b
  ret i1 %bool
}

define zeroext i1 @setugt_double(double %a, double %b) nounwind {
entry:

; CHECK-LABEL: setugt_double
; CHECK: cmpfle_f64 s0, s0_64, s2_64
; CHECK: xori s0, s0, -1
; CHECK: andi s0, s0, 1

  %bool = fcmp ugt double %a, %b
  ret i1 %bool
}

define zeroext i1 @setule_double(double %a, double %b) nounwind {
entry:

; CHECK-LABEL: setule_double
; CHECK: cmpfgt_f64 s0, s0_64, s2_64
; CHECK: xori s0, s0, -1
; CHECK: andi s0, s0, 1

  %bool = fcmp ule double %a, %b
  ret i1 %bool
}

define zeroext i1 @setult_double(double %a, double %b) nounwind {
entry:

; CHECK-LABEL: setult_double
; CHECK: cmpfge_f64 s0, s0_64, s2_64
; CHECK: xori s0, s0, -1
; CHECK: andi s0, s0, 1

  %bool = fcmp ult double %a, %b
  ret i1 %bool
}

; RUN: llc -march=nuplus < %s | FileCheck %s

define <8 x i64> @seteq_i32(<8 x i32> %a, <8 x i32> %b) nounwind {
entry:

; CHECK-LABEL: seteq_i32
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: and_i64 v1, v1, s0_64
; CHECK: and_i64 v0, v0, s0_64
; CHECK: cmpeq_i64 s2, v0, v1
; CHECK: move_i32 s5, s0
; CHECK: move_i32 s4, s0
; CHECK: move_i32 s0, s1
; CHECK: move_i64 v0, s0_64
; CHECK: move_i64 v1, s4_64
; CHECK: move_i32 s3, rm
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v1, v1
; CHECK: moveih s2, 65535
; CHECK: moveil s2, 65535
; CHECK: xor_i32 rm, rm, s2
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s3

  %bool  = icmp eq  <8 x i32> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setne_i32(<8 x i32> %a, <8 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setne_i32
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: and_i64 v1, v1, s0_64
; CHECK: and_i64 v0, v0, s0_64
; CHECK: cmpne_i64 s2, v0, v1
; CHECK: move_i32 s5, s0
; CHECK: move_i32 s4, s0
; CHECK: move_i32 s0, s1
; CHECK: move_i64 v0, s0_64
; CHECK: move_i64 v1, s4_64
; CHECK: move_i32 s3, rm
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v1, v1
; CHECK: moveih s2, 65535
; CHECK: moveil s2, 65535
; CHECK: xor_i32 rm, rm, s2
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s3

  %bool = icmp ne  <8 x i32> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setsgt_i32(<8 x i32> %a, <8 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setsgt_i32
; CHECK: sext32_i64 v1, v1
; CHECK: sext32_i64 v0, v0
; CHECK: cmpgt_i64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp sgt <8 x i32> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setsge_i32(<8 x i32> %a, <8 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setsge_i32
; CHECK: sext32_i64 v1, v1
; CHECK: sext32_i64 v0, v0
; CHECK: cmpge_i64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp sge <8 x i32> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setslt_i32(<8 x i32> %a, <8 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setslt_i32
; CHECK: sext32_i64 v1, v1
; CHECK: sext32_i64 v0, v0
; CHECK: cmplt_i64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp slt <8 x i32> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setsle_i32(<8 x i32> %a, <8 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setsle_i32
; CHECK: sext32_i64 v1, v1
; CHECK: sext32_i64 v0, v0
; CHECK: cmple_i64 s0, v0, v1
; CHECK: moveih s3, 65535
; CHECK: moveil s3, 65535
; CHECK: move_i32 s2, s3
; CHECK: moveih s5, 0
; CHECK: moveil s5, 0
; CHECK: move_i32 s4, s5
; CHECK: move_i64 v0, s4_64
; CHECK: move_i64 v1, s2_64
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i64 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp sle <8 x i32> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}


define <8 x i64> @setuge_i32(<8 x i32> %a, <8 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setuge_i32
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: and_i64 v1, v1, s0_64
; CHECK: and_i64 v0, v0, s0_64
; CHECK: cmpuge_i64 s2, v0, v1
; CHECK: move_i32 s5, s0
; CHECK: move_i32 s4, s0
; CHECK: move_i32 s0, s1
; CHECK: move_i64 v0, s0_64
; CHECK: move_i64 v1, s4_64
; CHECK: move_i32 s3, rm
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v1, v1
; CHECK: moveih s2, 65535
; CHECK: moveil s2, 65535
; CHECK: xor_i32 rm, rm, s2
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s3

  %bool = icmp uge <8 x i32> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setugt_i32(<8 x i32> %a, <8 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setugt_i32
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: and_i64 v1, v1, s0_64
; CHECK: and_i64 v0, v0, s0_64
; CHECK: cmpugt_i64 s2, v0, v1
; CHECK: move_i32 s5, s0
; CHECK: move_i32 s4, s0
; CHECK: move_i32 s0, s1
; CHECK: move_i64 v0, s0_64
; CHECK: move_i64 v1, s4_64
; CHECK: move_i32 s3, rm
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v1, v1
; CHECK: moveih s2, 65535
; CHECK: moveil s2, 65535
; CHECK: xor_i32 rm, rm, s2
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s3

  %bool = icmp ugt <8 x i32> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setule_i32(<8 x i32> %a, <8 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setule_i32
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: and_i64 v1, v1, s0_64
; CHECK: and_i64 v0, v0, s0_64
; CHECK: cmpule_i64 s2, v0, v1
; CHECK: move_i32 s5, s0
; CHECK: move_i32 s4, s0
; CHECK: move_i32 s0, s1
; CHECK: move_i64 v0, s0_64
; CHECK: move_i64 v1, s4_64
; CHECK: move_i32 s3, rm
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v1, v1
; CHECK: moveih s2, 65535
; CHECK: moveil s2, 65535
; CHECK: xor_i32 rm, rm, s2
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s3

  %bool = icmp ule <8 x i32> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @setult_i32(<8 x i32> %a, <8 x i32> %b) nounwind {
entry:

; CHECK-LABEL: setult_i32
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: and_i64 v1, v1, s0_64
; CHECK: and_i64 v0, v0, s0_64
; CHECK: cmpult_i64 s2, v0, v1
; CHECK: move_i32 s5, s0
; CHECK: move_i32 s4, s0
; CHECK: move_i32 s0, s1
; CHECK: move_i64 v0, s0_64
; CHECK: move_i64 v1, s4_64
; CHECK: move_i32 s3, rm
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v1, v1
; CHECK: moveih s2, 65535
; CHECK: moveil s2, 65535
; CHECK: xor_i32 rm, rm, s2
; CHECK: move_i64 v1, v0
; CHECK: move_i32 rm, s3

  %bool = icmp ult <8 x i32> %a, %b
  %ret = sext <8 x i1> %bool to <8 x i64>
  ret <8 x i64> %ret
}

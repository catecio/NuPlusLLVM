; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i32> @seteq_i8(<16 x i8> %a, <16 x i8> %b) nounwind {
entry:

; CHECK-LABEL: seteq_i8
; CHECK: andi v1, v1, 255
; CHECK: andi v0, v0, 255
; CHECK: cmpeq_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK:  move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp eq <16 x i8> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setne_i8(<16 x i8> %a, <16 x i8> %b) nounwind {
entry:

; CHECK-LABEL: setne_i8
; CHECK: andi v1, v1, 255
; CHECK: andi v0, v0, 255
; CHECK: cmpne_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK:  move_i32 v1, v0
; CHECK: move_i32 rm, s1


  %bool = icmp ne  <16 x i8> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setsgt_i8(<16 x i8> %a, <16 x i8> %b) nounwind {
entry:

; CHECK-LABEL: setsgt_i8
; CHECK: sext8_i32 v1, v1
; CHECK: sext8_i32 v0, v0
; CHECK: cmpgt_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK:  move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp sgt <16 x i8> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setsge_i8(<16 x i8> %a, <16 x i8> %b) nounwind {
entry:

; CHECK-LABEL: setsge_i8
; CHECK: sext8_i32 v1, v1
; CHECK: sext8_i32 v0, v0
; CHECK: cmpge_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK:  move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp sge <16 x i8> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setslt_i8(<16 x i8> %a, <16 x i8> %b) nounwind {
entry:

; CHECK-LABEL: setslt_i8
; CHECK: sext8_i32 v1, v1
; CHECK: sext8_i32 v0, v0
; CHECK: cmplt_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK:  move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp slt <16 x i8> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setsle_i8(<16 x i8> %a, <16 x i8> %b) nounwind {
entry:

; CHECK-LABEL: setsle_i8
; CHECK: sext8_i32 v1, v1
; CHECK: sext8_i32 v0, v0
; CHECK: cmple_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK:  move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp sle <16 x i8> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}


define <16 x i32> @setuge_i8(<16 x i8> %a, <16 x i8> %b) nounwind {
entry:

; CHECK-LABEL: setuge_i8
; CHECK: andi v1, v1, 255
; CHECK: andi v0, v0, 255
; CHECK: cmpuge_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK:  move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp uge <16 x i8> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setugt_i8(<16 x i8> %a, <16 x i8> %b) nounwind {
entry:

; CHECK-LABEL: setugt_i8
; CHECK: andi v1, v1, 255
; CHECK: andi v0, v0, 255
; CHECK: cmpugt_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK:  move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp ugt <16 x i8> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setule_i8(<16 x i8> %a, <16 x i8> %b) nounwind {
entry:

; CHECK-LABEL: setule_i8
; CHECK: andi v1, v1, 255
; CHECK: andi v0, v0, 255
; CHECK: cmpule_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK:  move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp ule <16 x i8> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @setult_i8(<16 x i8> %a, <16 x i8> %b) nounwind {
entry:

; CHECK-LABEL: setult_i8
; CHECK: andi v1, v1, 255
; CHECK: andi v0, v0, 255
; CHECK: cmpult_i32 s0, v0, v1
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: move_i32 v0, s1
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: move_i32 v1, s1
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v1, v1
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK:  move_i32 v1, v0
; CHECK: move_i32 rm, s1

  %bool = icmp ult <16 x i8> %a, %b
  %ret = sext <16 x i1> %bool to <16 x i32>
  ret <16 x i32> %ret
}

; RUN: llc -march=nuplus < %s | FileCheck %s

define float @uitofp_char(i8 %a) nounwind readnone {
; CHECK-LABEL: uitofp_char
; CHECK: andi s2, s0, 255
; CHECK: itof_i32 s0, s2
; CHECK: moveih s1, 20352
; CHECK: moveil s1, 0
; CHECK: cmplti s2, s2, 0
; CHECK: bnez s2, .LBB0_1
; CHECK: jmp .LBB0_2

; CHECK-LABEL: .LBB0_1:
; CHECK: fadd_f32 s0, s0, s1

; CHECK-LABEL: .LBB0_2:
; CHECK: jret
  %ret =  uitofp i8 %a to float
  ret float %ret
}

define float @uitofp_short(i16 %a) nounwind readnone {
; CHECK-LABEL: uitofp_short
; CHECK: moveih s1, 0
; CHECK: moveil s1, 65535
; CHECK: and_i32 s2, s0, s1
; CHECK: itof_i32 s0, s2
; CHECK: moveih s1, 20352
; CHECK: moveil s1, 0
; CHECK: cmplti s2, s2, 0
; CHECK: bnez s2, .LBB1_1
; CHECK: jmp .LBB1_2

; CHECK-LABEL: .LBB1_1:
; CHECK: fadd_f32 s0, s0, s1

; CHECK-LABEL: .LBB1_2:
; CHECK: jret

  %ret =  uitofp i16 %a to float
  ret float %ret
}

define float @uitofp_int(i32 %a) nounwind readnone {
; CHECK-LABEL: uitofp_int
; CHECK: move_i32 s1, s0
; CHECK: itof_i32 s0, s1
; CHECK: moveih s2, 20352
; CHECK: moveil s2, 0
; CHECK: cmplti s1, s1, 0
; CHECK: bnez s1, .LBB2_1
; CHECK: jmp .LBB2_2

; CHECK-LABEL: .LBB2_1:
; CHECK: fadd_f32 s0, s0, s2

; CHECK-LABEL: .LBB2_2:
; CHECK: jret

  %ret =  uitofp i32 %a to float
  ret float %ret
}

define float @uitofp_li(i64 %a) nounwind readnone {
; CHECK-LABEL: uitofp_li
; CHECK: moveih s3, 0
; CHECK: moveil s3, 0
; CHECK: move_i32 s2, s3
; CHECK: cmplt_i64 s2, s0_64, s2_64
; CHECK: itof_i32 s0, s0
; CHECK: moveih s1, 20352
; CHECK: moveil s1, 0
; CHECK: bnez s2, .LBB3_1
; CHECK: jmp .LBB3_2

; CHECK-LABEL: .LBB3_1:
; CHECK: fadd_f32 s0, s0, s1

; CHECK-LABEL: .LBB3_2:
; CHECK: jret

  %ret =  uitofp i64 %a to float
  ret float %ret
}


define double @uitofp64char(i8 %a) nounwind readnone {
; CHECK-LABEL: uitofp64char
; CHECK: moveih s3, 17392
; CHECK: moveil s3, 0
; CHECK: moveih s2, 0
; CHECK: moveil s2, 0
; CHECK: andi s4, s0, 255
; CHECK: itof_i32 s0, s4
; CHECK: f32tof64 s0_64, s0
; CHECK: cmplti s4, s4, 0
; CHECK: bnez s4, .LBB4_1
; CHECK: jmp .LBB4_2

; CHECK-LABEL: .LBB4_1:
; CHECK: fadd_f64 s0_64, s0_64, s2_64

; CHECK-LABEL: .LBB4_2:
; CHECK: jret

  %ret =  uitofp i8 %a to double
  ret double %ret
}

define double @uitofp64short(i16 %a) nounwind readnone {
; CHECK-LABEL: uitofp64short
; CHECK: moveih s3, 17392
; CHECK: moveil s3, 0
; CHECK: moveih s2, 0
; CHECK: moveil s2, 0
; CHECK: moveih s1, 0
; CHECK: moveil s1, 65535
; CHECK: and_i32 s4, s0, s1
; CHECK: itof_i32 s0, s4
; CHECK: f32tof64 s0_64, s0
; CHECK: cmplti s4, s4, 0
; CHECK: bnez s4, .LBB5_1
; CHECK: jmp .LBB5_2

; CHECK-LABEL: .LBB5_1:
; CHECK: fadd_f64 s0_64, s0_64, s2_64

; CHECK-LABEL: .LBB5_2:
; CHECK: jret

  %ret =  uitofp i16 %a to double
  ret double %ret
}

define double @uitofp64int(i32 %a) nounwind readnone {
; CHECK-LABEL: uitofp64int
; CHECK: move_i32 s2, s0
; CHECK: moveih s5, 17392
; CHECK: moveil s5, 0
; CHECK: moveih s4, 0
; CHECK: moveil s4, 0
; CHECK: itof_i32 s0, s2
; CHECK: f32tof64 s0_64, s0
; CHECK: cmplti s2, s2, 0
; CHECK: bnez s2, .LBB6_1
; CHECK: jmp .LBB6_2

; CHECK-LABEL: .LBB6_1:
; CHECK: fadd_f64 s0_64, s0_64, s4_64

; CHECK-LABEL: .LBB6_2:
; CHECK: jret

  %ret =  uitofp i32 %a to double
  ret double %ret
}

define double @uitofp64li(i64 %a) nounwind readnone {
; CHECK-LABEL: uitofp64li
; CHECK: moveih s3, 17392
; CHECK: moveil s3, 0
; CHECK: moveih s2, 0
; CHECK: moveil s2, 0
; CHECK: move_i32 s5, s2
; CHECK: move_i32 s4, s2
; CHECK: cmplt_i64 s4, s0_64, s4_64
; CHECK: itof_i64 s0_64, s0_64
; CHECK: bnez s4, .LBB7_1
; CHECK: jmp .LBB7_2

; CHECK-LABEL: .LBB7_1:
; CHECK: fadd_f64 s0_64, s0_64, s2_64

; CHECK-LABEL: .LBB7_2:
; CHECK: jret

  %ret =  uitofp i64 %a to double
  ret double %ret
}

; RUN: llc -march=nuplus < %s | FileCheck %s

define i32 @f32toi32(float %a) nounwind readnone {
; CHECK-LABEL: f32toi32
; CHECK: ftoi_i32 s0, s0
; CHECK: jret

  %ret =  fptosi float %a to i32
  ret i32 %ret
}

define i64 @f32toi64(float %a) nounwind readnone {
; CHECK-LABEL: f32toi64
; CHECK: ftoi_i32 s0, s0
; CHECK: move_i64 s0_64, s0
; CHECK: sext32_i64 s0_64, s0_64
; CHECK: jret

  %ret =  fptosi float %a to i64
  ret i64 %ret
}

define i32 @f64toi32(double %a) nounwind readnone {
; CHECK-LABEL: f64toi32
; CHECK: f64tof32 s0, s0_64
; CHECK: ftoi_i32 s0, s0
; CHECK: jret

  %ret =  fptosi double %a to i32
  ret i32 %ret
}

define i64 @f64toi64(double %a) nounwind readnone {
; CHECK-LABEL: f64toi64
; CHECK: ftoi_i64 s0_64, s0_64
; CHECK: jret

  %ret =  fptosi double %a to i64
  ret i64 %ret
}

; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x float> @uitofp_char(<16 x i8> %a) nounwind readnone {
; CHECK-LABEL: uitofp_char
; CHECK: andi v0, v0, 255
; CHECK: itof_i32 v1, v0
; CHECK: moveih s0, 20352
; CHECK: moveil s0, 0
; CHECK: fadd_f32 v2, v1, s0
; CHECK: cmplti s0, v0, 0
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, v2
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v0, v1
; CHECK: move_i32 rm, s1

  %ret =  uitofp <16 x i8> %a to <16 x float>
  ret <16 x float> %ret
}

define <16 x float> @uitofp_short(<16 x i16> %a) nounwind readnone {
; CHECK-LABEL: uitofp_short
; CHECK: moveih s0, 0
; CHECK: moveil s0, 65535
; CHECK: and_i32 v0, v0, s0
; CHECK: itof_i32 v1, v0
; CHECK: moveih s0, 20352
; CHECK: moveil s0, 0
; CHECK: fadd_f32 v2, v1, s0
; CHECK: cmplti s0, v0, 0
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, v2
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v0, v1
; CHECK: move_i32 rm, s1

  %ret =  uitofp <16 x i16> %a to <16 x float>
  ret <16 x float> %ret
}

define <16 x float> @uitofp_int(<16 x i32> %a) nounwind readnone {
; CHECK-LABEL: uitofp_int
; CHECK: itof_i32 v1, v0
; CHECK: moveih s0, 20352
; CHECK: moveil s0, 0
; CHECK: fadd_f32 v2, v1, s0
; CHECK: cmplti s0, v0, 0
; CHECK: move_i32 s1, rm
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, v2
; CHECK: moveih s0, 65535
; CHECK: moveil s0, 65535
; CHECK: xor_i32 rm, rm, s0
; CHECK: move_i32 v0, v1
; CHECK: move_i32 rm, s1

  %ret =  uitofp <16 x i32> %a to <16 x float>
  ret <16 x float> %ret
}

define <8 x double> @uitofp64_char(<8 x i8> %a) nounwind readnone {
; CHECK-LABEL: uitofp64_char
; CHECK: moveih s1, 17392
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 0
; CHECK: move_i32 s3, s0
; CHECK: move_i32 s2, s0
; CHECK: moveih s4, 0
; CHECK: moveil s4, 255
; CHECK: move_i64 s6_64, s2_64
; CHECK: move_i32 s6, s4
; CHECK: and_i64 v0, v0, s6_64
; CHECK: cmplt_i64 s2, v0, s2_64
; CHECK: itof_i64 v1, v0
; CHECK: fadd_f64 v0, v1, s0_64
; CHECK: move_i32 s0, rm
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v0, v0
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: xor_i32 rm, rm, s1
; CHECK: move_i64 v0, v1
; CHECK: move_i32 rm, s0


  %ret =  uitofp <8 x i8> %a to <8 x double>
  ret <8 x double> %ret
}

define <8 x double> @uitofp64_short(<8 x i16> %a) nounwind readnone {
; CHECK-LABEL: uitofp64_short
; CHECK: moveih s1, 17392
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 0
; CHECK: move_i32 s3, s0
; CHECK: move_i32 s2, s0
; CHECK: moveih s4, 0
; CHECK: moveil s4, 65535
; CHECK: move_i64 s6_64, s2_64
; CHECK: move_i32 s6, s4
; CHECK: and_i64 v0, v0, s6_64
; CHECK: cmplt_i64 s2, v0, s2_64
; CHECK: itof_i64 v1, v0
; CHECK: fadd_f64 v0, v1, s0_64
; CHECK: move_i32 s0, rm
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v0, v0
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: xor_i32 rm, rm, s1
; CHECK: move_i64 v0, v1
; CHECK: move_i32 rm, s0



  %ret =  uitofp <8 x i16> %a to <8 x double>
  ret <8 x double> %ret
}

define <8 x double> @uitofp64_int(<8 x i32> %a) nounwind readnone {
; CHECK-LABEL: uitofp64_int
; CHECK: moveih s1, 17392
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 0
; CHECK: move_i32 s3, s0
; CHECK: move_i32 s2, s0
; CHECK: moveih s4, 65535
; CHECK: moveil s4, 65535
; CHECK: move_i64 s6_64, s2_64
; CHECK: move_i32 s6, s4
; CHECK: and_i64 v0, v0, s6_64
; CHECK: cmplt_i64 s2, v0, s2_64
; CHECK: itof_i64 v1, v0
; CHECK: fadd_f64 v0, v1, s0_64
; CHECK: move_i32 s0, rm
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v0, v0
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: xor_i32 rm, rm, s1
; CHECK: move_i64 v0, v1
; CHECK: move_i32 rm, s0

  %ret =  uitofp <8 x i32> %a to <8 x double>
  ret <8 x double> %ret
}

define <8 x double> @uitofp64_li(<8 x i64> %a) nounwind readnone {
; CHECK-LABEL: uitofp64_li
; CHECK: moveih s1, 17392
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 0
; CHECK: move_i32 s3, s0
; CHECK: move_i32 s2, s0
; CHECK: cmplt_i64 s2, v0, s2_64
; CHECK: itof_i64 v1, v0
; CHECK: fadd_f64 v0, v1, s0_64
; CHECK: move_i32 s0, rm
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v0, v0
; CHECK: moveih s1, 65535
; CHECK: moveil s1, 65535
; CHECK: xor_i32 rm, rm, s1
; CHECK: move_i64 v0, v1
; CHECK: move_i32 rm, s0

  %ret =  uitofp <8 x i64> %a to <8 x double>
  ret <8 x double> %ret
}

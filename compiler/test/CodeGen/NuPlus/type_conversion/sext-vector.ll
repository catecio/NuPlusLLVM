; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i32> @sextv16i8tov16i32(<16 x i8> %a) nounwind readnone {
; CHECK-LABEL: sextv16i8tov16i32
; CHECK: sext8_i32 v0, v0
; CHECK: jret

  %ret = sext <16 x i8> %a to <16 x i32>
  ret <16 x i32> %ret
}

define <16 x i32> @sextv16i16tov16i32(<16 x i16> %a) nounwind readnone {
; CHECK-LABEL: sextv16i16tov16i32
; CHECK: sext16_i32 v0, v0
; CHECK: jret

  %ret = sext <16 x i16> %a to <16 x i32>
  ret <16 x i32> %ret
}

define <8 x i64> @sextv8i8tov8i64(<8 x i8> %a) nounwind readnone {
; CHECK-LABEL: sextv8i8tov8i64
; CHECK: sext8_i64 v0, v0
; CHECK: jret

  %ret = sext <8 x i8> %a to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @sextv8i16tov8i64(<8 x i16> %a) nounwind readnone {
; CHECK-LABEL: sextv8i16tov8i64
; CHECK: sext16_i64 v0, v0
; CHECK: jret

  %ret = sext <8 x i16> %a to <8 x i64>
  ret <8 x i64> %ret
}

define <8 x i64> @sextv8i32tov8i64(<8 x i32> %a) nounwind readnone {
; CHECK-LABEL: sextv8i32tov8i64
; CHECK: sext32_i64 v0, v0
; CHECK: jret

  %ret = sext <8 x i32> %a to <8 x i64>
  ret <8 x i64> %ret
}

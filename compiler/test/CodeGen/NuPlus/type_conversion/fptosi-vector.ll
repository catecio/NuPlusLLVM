; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i32> @v16f32tov16i32(<16 x float> %a) nounwind readnone {
; CHECK-LABEL: v16f32tov16i32
; CHECK: ftoi_i32 v0, v0
; CHECK: jret

  %ret =  fptosi <16 x float> %a to <16 x i32>
  ret <16 x i32> %ret
}

define <8 x i64> @v8f64tov8i64(<8 x double> %a) nounwind readnone {
; CHECK-LABEL: v8f64tov8i64
; CHECK: ftoi_i64 v0, v0
; CHECK: jret

  %ret =  fptosi <8 x double> %a to <8 x i64>
  ret <8 x i64> %ret
}

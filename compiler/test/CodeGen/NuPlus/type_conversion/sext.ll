; RUN: llc -march=nuplus < %s | FileCheck %s

define i32 @i8toi32(i8 %a) nounwind readnone {
; CHECK-LABEL: i8toi32
; CHECK: sext8_i32 s0, s0
; CHECK: jret

  %ret =  sext i8 %a to i32
  ret i32 %ret
}

define i32 @i16toi32(i16 %a) nounwind readnone {
; CHECK-LABEL: i16toi32
; CHECK: sext16_i32 s0, s0
; CHECK: jret

  %ret =  sext i16 %a to i32
  ret i32 %ret
}

define i64 @i8toi64(i8 %a) nounwind readnone {
; CHECK-LABEL: i8toi64
; CHECK: sext32_i64 s0_64, s0_64
; CHECK: sext8_i64 s0_64, s0_64
; CHECK: jret

  %ret =  sext i8 %a to i64
  ret i64 %ret
}

define i64 @i16toi64(i16 %a) nounwind readnone {
; CHECK-LABEL: i16toi64
; CHECK: sext32_i64 s0_64, s0_64
; CHECK: sext16_i64 s0_64, s0_64
; CHECK: jret

  %ret =  sext i16 %a to i64
  ret i64 %ret
}

define i64 @i32toi64(i32 %a) nounwind readnone {
; CHECK-LABEL: i32toi64
; CHECK: sext32_i64 s0_64, s0_64
; CHECK: jret

  %ret =  sext i32 %a to i64
  ret i64 %ret
}

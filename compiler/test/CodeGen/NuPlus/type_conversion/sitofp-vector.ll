; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x float> @sitofp_char(<16 x i8> %a) nounwind readnone {
; CHECK-LABEL: sitofp_char
; CHECK: sext8_i32 v0, v0
; CHECK: itof_i32 v0, v0

  %ret =  sitofp <16 x i8> %a to <16 x float>
  ret <16 x float> %ret
}

define <16 x float> @sitofp_short(<16 x i16> %a) nounwind readnone {
; CHECK-LABEL: sitofp_short
; CHECK: sext16_i32 v0, v0
; CHECK: itof_i32 v0, v0

  %ret =  sitofp <16 x i16> %a to <16 x float>
  ret <16 x float> %ret
}

define <16 x float> @sitofp_int(<16 x i32> %a) nounwind readnone {
; CHECK-LABEL: sitofp_int
; CHECK: itof_i32 v0, v0

  %ret =  sitofp <16 x i32> %a to <16 x float>
  ret <16 x float> %ret
}

define <8 x double> @sitofp64_char(<8 x i8> %a) nounwind readnone {
; CHECK-LABEL: sitofp64_char
; CHECK: sext8_i64 v0, v0
; CHECK: itof_i64 v0, v0

  %ret =  sitofp <8 x i8> %a to <8 x double>
  ret <8 x double> %ret
}

define <8 x double> @sitofp64_short(<8 x i16> %a) nounwind readnone {
; CHECK-LABEL: sitofp64_short
; CHECK: sext16_i64 v0, v0
; CHECK: itof_i64 v0, v0

  %ret =  sitofp <8 x i16> %a to <8 x double>
  ret <8 x double> %ret
}

define <8 x double> @sitofp64_int(<8 x i32> %a) nounwind readnone {
; CHECK-LABEL: sitofp64_int
; CHECK: sext32_i64 v0, v0
; CHECK: itof_i64 v0, v0

  %ret =  sitofp <8 x i32> %a to <8 x double>
  ret <8 x double> %ret
}

define <8 x double> @sitofp64_li(<8 x i64> %a) nounwind readnone {
; CHECK-LABEL: sitofp64_li
; CHECK: itof_i64 v0, v0

  %ret =  sitofp <8 x i64> %a to <8 x double>
  ret <8 x double> %ret
}

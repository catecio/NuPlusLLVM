; RUN: llc -march=nuplus < %s | FileCheck %s

define float @itofp_char(i8 %a) nounwind readnone {
; CHECK-LABEL: itofp_char
; CHECK: sext8_i32 s0, s0
; CHECK: itof_i32 s0, s0
; CHECK: jret
  %ret =  sitofp i8 %a to float
  ret float %ret
}

define float @itofp_short(i16 %a) nounwind readnone {
; CHECK-LABEL: itofp_short
; CHECK: sext16_i32 s0, s0
; CHECK: itof_i32 s0, s0
; CHECK: jret

  %ret =  sitofp i16 %a to float
  ret float %ret
}

define float @itofp_int(i32 %a) nounwind readnone {
; CHECK-LABEL: itofp_int
; CHECK: itof_i32 s0, s0
; CHECK: jret

  %ret =  sitofp i32 %a to float
  ret float %ret
}

define float @itofp_li(i64 %a) nounwind readnone {
; CHECK-LABEL: itofp_li
; CHECK: itof_i32 s0, s0
; CHECK: jret

  %ret =  sitofp i64 %a to float
  ret float %ret
}


define double @itofp64char(i8 %a) nounwind readnone {
; CHECK-LABEL: itofp64char
; CHECK: sext8_i32 s0, s0
; CHECK: itof_i32 s0, s0
; CHECK: f32tof64 s0_64, s0
; CHECK: jret

  %ret =  sitofp i8 %a to double
  ret double %ret
}

define double @itofp64short(i16 %a) nounwind readnone {
; CHECK-LABEL: itofp64short
; CHECK: sext16_i32 s0, s0
; CHECK: itof_i32 s0, s0
; CHECK: f32tof64 s0_64, s0
; CHECK: jret

  %ret =  sitofp i16 %a to double
  ret double %ret
}

define double @itofp64int(i32 %a) nounwind readnone {
; CHECK-LABEL: itofp64int
; CHECK: itof_i32 s0, s0
; CHECK: f32tof64 s0_64, s0
; CHECK: jret

  %ret =  sitofp i32 %a to double
  ret double %ret
}

define double @itofp64li(i64 %a) nounwind readnone {
; CHECK-LABEL: itofp64li
; CHECK: itof_i64 s0_64, s0_64
; CHECK: jret

  %ret =  sitofp i64 %a to double
  ret double %ret
}

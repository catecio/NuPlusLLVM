; RUN: llc -march=nuplus < %s | FileCheck %s

declare void @extern_void_void()
declare i32 @extern_i32_void()
declare float @extern_float_void()

define i32 @call_void_void() {
; CHECK-LABEL: call_void_void:
; CHECK:       store32 ra, 60(sp) 
; CHECK:       jmpsr extern_void_void
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 0
; CHECK:       load32 ra, 60(sp)

  call void @extern_void_void()
  ret i32 0
}

define i32 @call_i32_void() {
; CHECK-LABEL: call_i32_void:
; CHECK:       store32 ra, 60(sp)            
; CHECK:       jmpsr extern_i32_void
; CHECK:       addi s0, s0, 1
; CHECK:       load32 ra, 60(sp)

  %1 = call i32 @extern_i32_void()
  %2 = add i32 %1, 1
  ret i32 %2
}

define float @call_float_void() {
; CHECK-LABEL: call_float_void:
; CHECK:       store32 ra, 60(sp)                  
; CHECK:       jmpsr extern_float_void
; CHECK:       moveih s1, 16256
; CHECK:       moveil s1, 0
; CHECK:       fadd_f32 s0, s0, s1
; CHECK:       load32 ra, 60(sp)


  %1 = call float @extern_float_void()
  %2 = fadd float %1, 1.0
  
  ret float %2
}

define void @musttail_call_void_void() {
; CHECK-LABEL: musttail_call_void_void:
; CHECK:       store32 ra, 60(sp)                    
; CHECK:       jmpsr extern_void_void
; CHECK:       load32 ra, 60(sp)

  musttail call void @extern_void_void()
  ret void
}

define i32 @musttail_call_i32_void() {
; CHECK-LABEL: musttail_call_i32_void:
; CHECK:       store32 ra, 60(sp)                    
; CHECK:       jmpsr extern_i32_void
; CHECK:       load32 ra, 60(sp)

  %1 = musttail call i32 @extern_i32_void()
  ret i32 %1
}

define float @musttail_call_float_void() {
; CHECK-LABEL: musttail_call_float_void:
; CHECK:       store32 ra, 60(sp)                    
; CHECK:       jmpsr extern_float_void
; CHECK:       load32 ra, 60(sp)

  %1 = musttail call float @extern_float_void()
  ret float %1
}

define i32 @indirect_call_void_void(void ()* %addr) {
; CHECK-LABEL: indirect_call_void_void:
; CHECK:       store32 ra, 60(sp)                    
; CHECK:       jmpsr s0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 0             
; CHECK:       load32 ra, 60(sp)

  call void %addr()
  ret i32 0
}

define i32 @indirect_call_i32_void(i32 ()* %addr) {
; CHECK-LABEL: indirect_call_i32_void:
; CHECK:       store32 ra, 60(sp)    
; CHECK:       jmpsr s0
; CHECK:       addi s0, s0, 1
; CHECK:       load32 ra, 60(sp) 


  %1 = call i32 %addr()
  %2 = add i32 %1, 1
  ret i32 %2
}

define float @indirect_call_float_void(float ()* %addr) {
; CHECK-LABEL: indirect_call_float_void:
; CHECK:       store32 ra, 60(sp)    
; CHECK:       jmpsr s0
; CHECK:       moveih s1, 16256
; CHECK:       moveil s1, 0
; CHECK:       fadd_f32 s0, s0, s1
; CHECK:       load32 ra, 60(sp)  

  %1 = call float %addr()
  %2 = fadd float %1, 1.0
  ret float %2
}

; We can't use 'musttail' here because the verifier is too conservative and
; prohibits any prototype difference.
define void @tail_indirect_call_void_void(void ()* %addr) {
; CHECK-LABEL: tail_indirect_call_void_void:
; CHECK:       store32 ra, 60(sp)    
; CHECK:       jmpsr s0
; CHECK:       load32 ra, 60(sp)  

  tail call void %addr()
  ret void
}

define i32 @tail_indirect_call_i32_void(i32 ()* %addr) {
; CHECK-LABEL: tail_indirect_call_i32_void:
; CHECK:       store32 ra, 60(sp)    
; CHECK:       jmpsr s0
; CHECK:       load32 ra, 60(sp)  

  %1 = tail call i32 %addr()
  ret i32 %1
}

define float @tail_indirect_call_float_void(float ()* %addr) {
; CHECK-LABEL: tail_indirect_call_float_void:
; CHECK:       store32 ra, 60(sp)    
; CHECK:       jmpsr s0
; CHECK:       load32 ra, 60(sp)  

  %1 = tail call float %addr()
  ret float %1
}


declare hidden void @undef_double(i32 %this, double %volume) unnamed_addr align 2

define hidden void @thunk_undef_double(i32 %this, double %volume) unnamed_addr align 2 {
; CHECK-LABEL: thunk_undef_double:
; CHECK:       store32 ra, 60(sp)    
; CHECK:       jmpsr undef_double
; CHECK:       load32 ra, 60(sp)  

  tail call void @undef_double(i32 undef, double undef) #8
  ret void
}

; Check that immediate addresses do not use jal.
define i32 @jal_only_allows_symbols() {
; CHECK-LABEL: jal_only_allows_symbols:
; CHECK:       store32 ra, 60(sp)    
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 1234
; CHECK:       jmpsr s0
; CHECK:       moveih s0, 0
; CHECK:       moveil s0, 0
; CHECK:       load32 ra, 60(sp)  

  call void () inttoptr (i32 1234 to void ()*)()
  ret i32 0
}


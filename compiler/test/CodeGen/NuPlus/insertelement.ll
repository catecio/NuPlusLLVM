; RUN: llc -march=nuplus < %s | FileCheck %s

define <16 x i32> @inselt_v16i32(<16 x i32>  %a) nounwind {
; CHECK-LABEL: inselt_v16i32
; CHECK: moveih s0, 0
; CHECK: moveil s0, 2
; CHECK: moveih s1, 0
; CHECK: moveil s1, 5
; CHECK: move_i32 s2, rm
; CHECK: moveih s3, 0
; CHECK: moveil s3, 1
; CHECK: shl_i32 s0, s3, s0
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, s1
; CHECK: move_i32 rm, s2

  %ris = insertelement <16 x i32> %a, i32 5, i32 2
  ret <16 x i32> %ris
}

define <16 x i16>  @inselt_v16i16(<16 x i16>  %a) nounwind {
; CHECK-LABEL: inselt_v16i16
; CHECK: moveih s0, 0
; CHECK: moveil s0, 2
; CHECK: moveih s1, 0
; CHECK: moveil s1, 5
; CHECK: move_i32 s2, rm
; CHECK: moveih s3, 0
; CHECK: moveil s3, 1
; CHECK: shl_i32 s0, s3, s0
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, s1
; CHECK: move_i32 rm, s2

  %ris = insertelement <16 x i16> %a, i16 5, i32 2
  ret <16 x i16> %ris
}

define <16 x i8>  @inselt_v16i8(<16 x i8>  %a) nounwind {
; CHECK-LABEL: inselt_v16i8
; CHECK: moveih s0, 0
; CHECK: moveil s0, 2
; CHECK: moveih s1, 0
; CHECK: moveil s1, 5
; CHECK: move_i32 s2, rm
; CHECK: moveih s3, 0
; CHECK: moveil s3, 1
; CHECK: shl_i32 s0, s3, s0
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, s1
; CHECK: move_i32 rm, s2

  %ris = insertelement <16 x i8> %a, i8 5, i32 2
  ret <16 x i8> %ris
}


define <8 x i64> @inselt_v8i64(<8 x i64>  %a) nounwind {
; CHECK-LABEL: inselt_v8i64
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 5
; CHECK: moveih s2, 0
; CHECK: moveil s2, 2
; CHECK: move_i32 s3, rm
; CHECK: moveih s4, 0
; CHECK: moveil s4, 1
; CHECK: shl_i32 s2, s4, s2
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v0, s0_64
; CHECK: move_i32 rm, s3

  %ris = insertelement <8 x i64> %a, i64 5, i32 2
  ret <8 x i64> %ris
}

define <8 x i32>  @inselt_v8i32(<8 x i32>  %a) nounwind {
; CHECK-LABEL: inselt_v8i32
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 5
; CHECK: moveih s2, 0
; CHECK: moveil s2, 2
; CHECK: move_i32 s3, rm
; CHECK: moveih s4, 0
; CHECK: moveil s4, 1
; CHECK: shl_i32 s2, s4, s2
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v0, s0_64
; CHECK: move_i32 rm, s3

  %ris = insertelement <8 x i32> %a, i32 5, i32 2
  ret <8 x i32> %ris
}

define <8 x i16>  @inselt_v8i16(<8 x i16>  %a) nounwind {
; CHECK-LABEL: inselt_v8i16
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 5
; CHECK: moveih s2, 0
; CHECK: moveil s2, 2
; CHECK: move_i32 s3, rm
; CHECK: moveih s4, 0
; CHECK: moveil s4, 1
; CHECK: shl_i32 s2, s4, s2
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v0, s0_64
; CHECK: move_i32 rm, s3

  %ris = insertelement <8 x i16> %a, i16 5, i32 2
  ret <8 x i16> %ris
}

define <8 x i8>  @inselt_v8i8(<8 x i8>  %a) nounwind {
; CHECK-LABEL: inselt_v8i8
; CHECK: moveih s1, 0
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 5
; CHECK: moveih s2, 0
; CHECK: moveil s2, 2
; CHECK: move_i32 s3, rm
; CHECK: moveih s4, 0
; CHECK: moveil s4, 1
; CHECK: shl_i32 s2, s4, s2
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v0, s0_64
; CHECK: move_i32 rm, s3

  %ris = insertelement <8 x i8> %a, i8 5, i32 2
  ret <8 x i8> %ris
}


define <16 x float>  @inselt_v16f32(<16 x float>  %a) nounwind {
; CHECK-LABEL: inselt_v16f32
; CHECK: moveih s0, 0
; CHECK: moveil s0, 2
; CHECK: moveih s1, 16384
; CHECK: moveil s1, 0
; CHECK: move_i32 s2, rm
; CHECK: moveih s3, 0
; CHECK: moveil s3, 1
; CHECK: shl_i32 s0, s3, s0
; CHECK: move_i32 rm, s0
; CHECK: move_i32 v0, s1
; CHECK: move_i32 rm, s2

  %ris = insertelement <16 x float> %a, float 2.000000e+00, i32 2
  ret <16 x float> %ris
}

define <8 x double>  @inselt_v8f64(<8 x double>  %a) nounwind {
; CHECK-LABEL: inselt_v8f64
; CHECK: moveih s1, 16384
; CHECK: moveil s1, 0
; CHECK: moveih s0, 0
; CHECK: moveil s0, 0
; CHECK: moveih s2, 0
; CHECK: moveil s2, 2
; CHECK: move_i32 s3, rm
; CHECK: moveih s4, 0
; CHECK: moveil s4, 1
; CHECK: shl_i32 s2, s4, s2
; CHECK: move_i32 rm, s2
; CHECK: move_i64 v0, s0_64
; CHECK: move_i32 rm, s3

  %ris = insertelement <8 x double> %a, double 2.000000e+00, i32 2
  ret <8 x double> %ris
}
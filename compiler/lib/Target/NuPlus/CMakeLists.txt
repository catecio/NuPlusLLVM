set(LLVM_TARGET_DEFINITIONS NuPlus.td)

tablegen(LLVM NuPlusGenRegisterInfo.inc -gen-register-info)
tablegen(LLVM NuPlusGenInstrInfo.inc -gen-instr-info)
tablegen(LLVM NuPlusGenDisassemblerTables.inc -gen-disassembler)
tablegen(LLVM NuPlusGenMCCodeEmitter.inc -gen-emitter)
tablegen(LLVM NuPlusGenAsmWriter.inc -gen-asm-writer)
tablegen(LLVM NuPlusGenAsmMatcher.inc -gen-asm-matcher)
tablegen(LLVM NuPlusGenDAGISel.inc -gen-dag-isel)
tablegen(LLVM NuPlusGenSubtargetInfo.inc -gen-subtarget)
tablegen(LLVM NuPlusGenCallingConv.inc -gen-callingconv)
add_public_tablegen_target(NuPlusCommonTableGen)

add_llvm_target(NuPlusCodeGen
  NuPlusAsmPrinter.cpp
  NuPlusInstrInfo.cpp
  NuPlusISelDAGToDAG.cpp
  NuPlusISelLowering.cpp
  NuPlusFrameLowering.cpp
  NuPlusMachineFunctionInfo.cpp
  NuPlusRegisterInfo.cpp
  NuPlusSubtarget.cpp
  NuPlusTargetMachine.cpp
  NuPlusMCInstLower.cpp
  NuPlusTargetObjectFile.cpp
  )

add_dependencies(LLVMNuPlusCodeGen intrinsics_gen)

add_subdirectory(TargetInfo)
add_subdirectory(MCTargetDesc)
add_subdirectory(AsmParser)
add_subdirectory(InstPrinter)
add_subdirectory(Disassembler)


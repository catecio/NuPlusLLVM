//===-- NuPlusELFObjectWriter.cpp - NuPlus ELF Writer -----------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include "MCTargetDesc/NuPlusFixupKinds.h"
#include "MCTargetDesc/NuPlusMCTargetDesc.h"
#include "llvm/MC/MCELFObjectWriter.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/raw_ostream.h"

using namespace llvm;

namespace {
  class NuPlusELFObjectWriter : public MCELFObjectTargetWriter {
  public:
    NuPlusELFObjectWriter(uint8_t OSABI);

  protected:
    unsigned getRelocType(MCContext &Ctx, const MCValue &Target,
                          const MCFixup &Fixup, bool IsPCRel) const override;

  };
}

NuPlusELFObjectWriter::NuPlusELFObjectWriter(uint8_t OSABI)
    : MCELFObjectTargetWriter(/*Is64Bit*/ false, OSABI, ELF::EM_NUPLUS,
                              /*HasRelocationAddend*/ true) {}

unsigned NuPlusELFObjectWriter::getRelocType(MCContext &Ctx,
                                            const MCValue &Target,
                                            const MCFixup &Fixup,
                                            bool IsPCRel) const {

  unsigned Type;
  //switch kind
  switch ((unsigned)Fixup.getKind()) {
  default:
    llvm_unreachable("Invalid fixup kind!");
  case FK_Data_4:
    Type = ELF::R_NUPLUS_ABS32;
    break;

  case NuPlus::fixup_NuPlus_PCRel_Branch:
    Type = ELF::R_NUPLUS_BRANCH;
    break;

  // In normal cases, these types should not be emitted because they can be
  // fixed up immediately (the compiler uses it to access constant pool
  // entries at the beginning of each function). We hit this code path
  // if there is an undefined symbol in assembly code. This will cause
  // an error later during linking.
  case NuPlus::fixup_NuPlus_PCRel_MemAccExt:
    Type = ELF::R_NUPLUS_PCREL_MEM_EXT;
    break;

  case NuPlus::fixup_NuPlus_PCRel_MemAcc:
    Type = ELF::R_NUPLUS_PCREL_MEM;
    break;

  case NuPlus::fixup_NuPlus_PCRel_ComputeLabelAddress:
    Type = ELF::R_NUPLUS_PCREL_LEA;
    break;
  case NuPlus::fixup_NuPlus_ABS_high:
    Type = ELF::R_NUPLUS_ABS_HIGH_LEA;
    break;
  case NuPlus::fixup_NuPlus_ABS_low:
    Type = ELF::R_NUPLUS_ABS_LOW_LEA;
    break;
  }
  return Type;
}


MCObjectWriter *llvm::createNuPlusELFObjectWriter(raw_pwrite_stream &OS,
                                                 uint8_t OSABI) {
  MCELFObjectTargetWriter *MOTW = new NuPlusELFObjectWriter(OSABI);
  return createELFObjectWriter(MOTW, OS, true);
}

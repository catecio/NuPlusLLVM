//===-- NuPlusAsmBackend.cpp - NuPlus Assembler Backend ---------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include "NuPlusFixupKinds.h"
#include "MCTargetDesc/NuPlusMCTargetDesc.h"
#include "llvm/MC/MCAsmBackend.h"
#include "llvm/MC/MCAssembler.h"
#include "llvm/MC/MCDirectives.h"
#include "llvm/MC/MCELFObjectWriter.h"
#include "llvm/MC/MCExpr.h"
#include "llvm/MC/MCFixupKindInfo.h"
#include "llvm/MC/MCObjectWriter.h"
#include "llvm/MC/MCSubtargetInfo.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/raw_ostream.h"

using namespace llvm;

//UNUSED, these PCRel istructions read PC and not PC+4.
static unsigned adjustFixupValue(unsigned Kind, uint64_t Value) {
  switch (Kind) {
  default:
    llvm_unreachable("Unknown fixup kind!");
  case FK_Data_1:
  case FK_Data_2:
  case FK_Data_4:
  case FK_Data_8:
  case NuPlus::fixup_NuPlus_PCRel_MemAccExt:
  case NuPlus::fixup_NuPlus_PCRel_MemAcc:
  case NuPlus::fixup_NuPlus_PCRel_Branch:
  case NuPlus::fixup_NuPlus_PCRel_ComputeLabelAddress:
  case NuPlus::fixup_NuPlus_ABS_high:
  case NuPlus::fixup_NuPlus_ABS_low:
    //Value -= 4; // source location is PC + 4
    break;
  }
  return Value;
}

namespace {
  class NuPlusAsmBackend : public MCAsmBackend {
  Triple::OSType OSType;

  public:
    NuPlusAsmBackend(const Target &T, Triple::OSType OSType)
        : MCAsmBackend(), OSType(OSType) {}


    void applyFixup(const MCFixup &Fixup, char *Data, unsigned DataSize,
                    uint64_t Value, bool IsPCRel) const override {

      MCFixupKind Kind = Fixup.getKind();
      Value = adjustFixupValue((unsigned)Kind, Value);
      
      //TODO: usato in SPARC
      //if (!Value) return;           // Doesn't change encoding.

      unsigned Offset = Fixup.getOffset();

      uint64_t CurVal = 0;
      for (unsigned i = 0; i != 4; ++i) {
        CurVal |= (uint64_t)((uint8_t)Data[Offset + i]) << (i * 8);
      }

      uint64_t Mask = ((uint64_t)(-1) >> (64 - getFixupKindInfo(Kind).TargetSize));
      Value <<= getFixupKindInfo(Kind).TargetOffset;
      Mask <<= getFixupKindInfo(Kind).TargetOffset;
      CurVal |= Value & Mask;

      // For each byte of the fragment that the fixup touches, mask in the bits
      // from the fixup value. The Value has been "split up" into the
      // appropriate bitfields above.
      for (unsigned i = 0; i != 4; ++i) {
        Data[Offset + i] = uint8_t((CurVal >> (i * 8)) & 0xff);
      }
    }

    MCObjectWriter *createObjectWriter(raw_pwrite_stream &OS) const override {
      uint8_t OSABI = MCELFObjectTargetWriter::getOSABI(OSType);
      return createNuPlusELFObjectWriter(OS, OSABI);
    }

    unsigned getNumFixupKinds() const {
      return NuPlus::NumTargetFixupKinds;
    }

    const MCFixupKindInfo &getFixupKindInfo(MCFixupKind Kind) const override {
      const static MCFixupKindInfo Infos[NuPlus::NumTargetFixupKinds] = {
        // This table *must* be in same the order of fixup_* kinds in
        // NuPlusFixupKinds.h.
        //
        // name                          offset  bits  flags
        {"fixup_NuPlus_PCRel_MemAccExt", 3, 9, MCFixupKindInfo::FKF_IsPCRel},
        {"fixup_NuPlus_PCRel_MemAcc", 3, 9, MCFixupKindInfo::FKF_IsPCRel},
        {"fixup_NuPlus_PCRel_Branch", 0, 18, MCFixupKindInfo::FKF_IsPCRel},
        {"fixup_NuPlus_PCRel_ComputeLabelAddress", 3, 9, MCFixupKindInfo::FKF_IsPCRel}
      };

      if (Kind < FirstTargetFixupKind)
        return MCAsmBackend::getFixupKindInfo(Kind);

      assert(unsigned(Kind - FirstTargetFixupKind) < getNumFixupKinds() &&
             "Invalid kind!");
      return Infos[Kind - FirstTargetFixupKind];
    }

    bool mayNeedRelaxation(const MCInst &Inst) const override {
      return false;
    }

    /// fixupNeedsRelaxation - Target specific predicate for whether a given
    /// fixup requires the associated instruction to be relaxed.
    bool fixupNeedsRelaxation(const MCFixup &Fixup,
                              uint64_t Value,
                              const MCRelaxableFragment *DF,
                              const MCAsmLayout &Layout) const override {
      return false;
    }
    void relaxInstruction(const MCInst &Inst, const MCSubtargetInfo &STI,
                          MCInst &Res) const override {
      assert(0 && "relaxInstruction() unimplemented");
    }

    bool writeNopData(uint64_t Count, MCObjectWriter *OW) const override {
      OW->WriteZeros(Count);
      return true;
    }
  };

} // end anonymous namespace

MCAsmBackend *llvm::createNuPlusAsmBackend(const Target &T,
                                          const MCRegisterInfo &MRI,
                                          const Triple &TT, StringRef CPU) {
  return new NuPlusAsmBackend(T, Triple(TT).getOS());
}

//===-- NuPlusMCTargetDesc.cpp - NuPlus Target Descriptions -----------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file provides NuPlus specific target descriptions.
//
//===----------------------------------------------------------------------===//

//TODO: aggiungere MCInstrAnalysis
#include "NuPlusMCAsmInfo.h"
#include "NuPlusMCTargetDesc.h"
#include "InstPrinter/NuPlusInstPrinter.h"
#include "llvm/MC/MCELFStreamer.h"
#include "llvm/MC/MCInstrInfo.h"
#include "llvm/MC/MCRegisterInfo.h"
#include "llvm/MC/MCSubtargetInfo.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/TargetRegistry.h"

using namespace llvm;

#define GET_INSTRINFO_MC_DESC
#include "NuPlusGenInstrInfo.inc"

#define GET_SUBTARGETINFO_MC_DESC
#include "NuPlusGenSubtargetInfo.inc"

#define GET_REGINFO_MC_DESC
#include "NuPlusGenRegisterInfo.inc"

// Put an instruction into the common information entry (CIE), shared
// by all frame description entries (FDE), which indicates the stack
// pointer register (r29) is used to find the canonical frame address (CFA).
static MCAsmInfo *createNuPlusMCAsmInfo(const MCRegisterInfo &MRI,
                                       const Triple &TT) {
  MCAsmInfo *MAI = new NuPlusMCAsmInfo(TT);
  unsigned Reg = MRI.getDwarfRegNum(NuPlus::SP_REG, true);
  MCCFIInstruction Inst = MCCFIInstruction::createDefCfa(nullptr, Reg, 0);
  MAI->addInitialFrameState(Inst);
  return MAI;
}

static MCRegisterInfo *createNuPlusMCRegisterInfo(const Triple &TT) {
  MCRegisterInfo *X = new MCRegisterInfo();
  InitNuPlusMCRegisterInfo(X, NuPlus::RA_REG);
  return X;
}

static MCInstrInfo *createNuPlusMCInstrInfo() {
  MCInstrInfo *X = new MCInstrInfo();
  InitNuPlusMCInstrInfo(X);
  return X;
}

static MCSubtargetInfo *createNuPlusMCSubtargetInfo(const Triple &TT,
                                                    StringRef CPU,
                                                    StringRef FS) {
  return createNuPlusMCSubtargetInfoImpl(TT, CPU, FS);
}

static MCStreamer *createNuPlusMCStreamer(const Triple &T, MCContext &Context,
                                         MCAsmBackend &MAB,
                                         raw_pwrite_stream &OS,
                                         MCCodeEmitter *Emitter,
                                         bool RelaxAll) {
  return createELFStreamer(Context, MAB, OS, Emitter, RelaxAll);
}

static MCInstPrinter *createNuPlusMCInstPrinter(const Triple &T,
                                               unsigned SyntaxVariant,
                                               const MCAsmInfo &MAI,
                                               const MCInstrInfo &MII,
                                               const MCRegisterInfo &MRI) {
  return new NuPlusInstPrinter(MAI, MII, MRI);
}

extern "C" void LLVMInitializeNuPlusTargetMC() {
  // Register the MC asm info.
  RegisterMCAsmInfoFn A(TheNuPlusTarget, createNuPlusMCAsmInfo);

  // Register the MC instruction info.
  TargetRegistry::RegisterMCInstrInfo(TheNuPlusTarget, createNuPlusMCInstrInfo);

  // Register the MC register info.
  TargetRegistry::RegisterMCRegInfo(TheNuPlusTarget, createNuPlusMCRegisterInfo);

  TargetRegistry::RegisterMCCodeEmitter(TheNuPlusTarget, createNuPlusMCCodeEmitter);

  // Register the MC subtarget info.
  TargetRegistry::RegisterMCSubtargetInfo(TheNuPlusTarget, createNuPlusMCSubtargetInfo);

  // Register the ASM Backend
  TargetRegistry::RegisterMCAsmBackend(TheNuPlusTarget, createNuPlusAsmBackend);

  // Register the object streamer
  TargetRegistry::RegisterELFStreamer(TheNuPlusTarget, createNuPlusMCStreamer);

  // MC instruction printer
  TargetRegistry::RegisterMCInstPrinter(TheNuPlusTarget, createNuPlusMCInstPrinter);
}

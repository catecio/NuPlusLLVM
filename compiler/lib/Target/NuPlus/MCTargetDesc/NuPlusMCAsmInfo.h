//===-- NuPlusMCAsmInfo.h - NuPlus asm properties ----------------*- C++ -*--===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the declaration of the NuPlusMCAsmInfo class.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_LIB_TARGET_NUPLUS_MCTARGETDESC_NUPLUSMCASMINFO_H
#define LLVM_LIB_TARGET_NUPLUS_MCTARGETDESC_NUPLUSMCASMINFO_H

#include "llvm/MC/MCAsmInfoELF.h"

namespace llvm {
class Triple;

class NuPlusMCAsmInfo : public MCAsmInfo {
  virtual void anchor();

public:
  explicit NuPlusMCAsmInfo(const Triple &TT);
};

} // namespace llvm

#endif

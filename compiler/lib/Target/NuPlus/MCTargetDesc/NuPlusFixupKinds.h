//===-- NuPlusFixupKinds.h - NuPlus Specific Fixup Entries --------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_LIB_TARGET_NUPLUS_MCTARGETDESC_NUPLUSFIXUPKINDS_H
#define LLVM_LIB_TARGET_NUPLUS_MCTARGETDESC_NUPLUSFIXUPKINDS_H

#include "llvm/MC/MCFixup.h"

namespace llvm {
  namespace NuPlus {
  // Although most of the current fixup types reflect a unique relocation
  // one can have multiple fixup types for a given relocation and thus need
  // to be uniquely named.
  //
  // This table *must* be in the same order of
  // MCFixupKindInfo Infos[NuPlus::NumTargetFixupKinds]
  // in NuPlusAsmBackend.cpp.
    enum Fixups {
	
	// PC relative offset for extended memory access
        fixup_NuPlus_PCRel_MemAccExt = FirstTargetFixupKind, // PC relative offset for extended memory access
        fixup_NuPlus_PCRel_MemAcc,              // PC relative offset for memory access
        fixup_NuPlus_PCRel_Branch,              // PC relative for branch instruction
        fixup_NuPlus_PCRel_ComputeLabelAddress,
        fixup_NuPlus_ABS_high,
        fixup_NuPlus_ABS_low,

      // Marker
      LastTargetFixupKind,
      NumTargetFixupKinds = LastTargetFixupKind - FirstTargetFixupKind
    };
  }
}

#endif

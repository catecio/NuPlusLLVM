//===-- NuPlusMCAsmInfo.cpp - NuPlus asm properties -------------------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the declarations of the NuPlusMCAsmInfo properties.
//
//===----------------------------------------------------------------------===//

#include "NuPlusMCAsmInfo.h"
#include "llvm/ADT/Triple.h"

using namespace llvm;

void NuPlusMCAsmInfo::anchor() {}

NuPlusMCAsmInfo::NuPlusMCAsmInfo(const Triple &TheTriple) {
  IsLittleEndian = true;
  SupportsDebugInformation = true;
  
  //Catello risolto con ExceptionHandling::None;
  ExceptionsType = ExceptionHandling::DwarfCFI;
  //ExceptionsType = ExceptionHandling::None;

  PrivateGlobalPrefix = ".L";
  PrivateLabelPrefix = ".L";
}

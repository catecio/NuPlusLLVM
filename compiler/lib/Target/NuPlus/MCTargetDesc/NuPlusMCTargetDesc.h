//===-- NuPlusMCTargetDesc.h - NuPlus Target Descriptions ---------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file provides NuPlus specific target descriptions.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_LIB_TARGET_NUPLUS_MCTARGETDESC_NUPLUSMCTARGETDESC_H
#define LLVM_LIB_TARGET_NUPLUS_MCTARGETDESC_NUPLUSMCTARGETDESC_H

#include "llvm/Support/DataTypes.h"
#include "llvm/Support/raw_ostream.h"

namespace llvm {
class MCAsmBackend;
class MCCodeEmitter;
class MCContext;
class MCInstrInfo;
class MCObjectWriter;
class MCRegisterInfo;
class MCSubtargetInfo;
class Target;
class Triple;
class StringRef;
class raw_ostream;

extern Target TheNuPlusTarget;

MCCodeEmitter *createNuPlusMCCodeEmitter(const MCInstrInfo &MCII,
                                        const MCRegisterInfo &MRI,
                                        MCContext &Ctx);
MCAsmBackend *createNuPlusAsmBackend(const Target &T, const MCRegisterInfo &MRI,
                                    const Triple &TT, StringRef CPU);
MCObjectWriter *createNuPlusELFObjectWriter(raw_pwrite_stream &OS, uint8_t OSABI);
} // End llvm namespace

// Defines symbolic names for NuPlus registers.  This defines a mapping from
// register name to register number.
//
#define GET_REGINFO_ENUM
#include "NuPlusGenRegisterInfo.inc"

// Defines symbolic names for the NuPlus instructions.
//
#define GET_INSTRINFO_ENUM
#include "NuPlusGenInstrInfo.inc"

#define GET_SUBTARGETINFO_ENUM
#include "NuPlusGenSubtargetInfo.inc"

#endif

//===-- NuPlusFrameLowering.h - Define frame lowering for NuPlus --*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
//
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_LIB_TARGET_NUPLUS_NUPLUSFRAMELOWERING_H
#define LLVM_LIB_TARGET_NUPLUS_NUPLUSFRAMELOWERING_H

#include "NuPlus.h"
#include "llvm/Target/TargetFrameLowering.h"

namespace llvm {
class NuPlusSubtarget;

class NuPlusFrameLowering : public TargetFrameLowering {
public:
  static const NuPlusFrameLowering *create(const NuPlusSubtarget &ST);

  explicit NuPlusFrameLowering(const NuPlusSubtarget &ST)
      : TargetFrameLowering(TargetFrameLowering::StackGrowsDown, 64, 0, 64) {}

  /// emitProlog/emitEpilog - These methods insert prolog and epilog code into
  /// the function.
  void emitPrologue(MachineFunction &MF, MachineBasicBlock &MBB) const override;
  void emitEpilogue(MachineFunction &MF, MachineBasicBlock &MBB) const override;

  MachineBasicBlock::iterator
  eliminateCallFramePseudoInstr(MachineFunction &MF,
                                MachineBasicBlock &MBB,
                                MachineBasicBlock::iterator I) const override;

  bool hasReservedCallFrame(const MachineFunction &MF) const override;
  bool hasFP(const MachineFunction &MF) const override;
  void determineCalleeSaves(MachineFunction &MF, BitVector &SavedRegs,
                            RegScavenger *RS) const override;

private:
  uint64_t getWorstCaseStackSize(const MachineFunction &MF) const;
};

} // End llvm namespace

#endif

//===-- NuPlus.h - Top-level interface for NuPlus representation --*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the entry points for global functions defined in the LLVM
// NuPlus back-end.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_LIB_TARGET_NUPLUS_NUPLUS_H
#define LLVM_LIB_TARGET_NUPLUS_NUPLUS_H

#include "MCTargetDesc/NuPlusMCTargetDesc.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Target/TargetMachine.h"

namespace llvm {
  class FunctionPass;
  class NuPlusTargetMachine;
  class formatted_raw_ostream;

  FunctionPass *createNuPlusISelDag(NuPlusTargetMachine &TM);

} // end namespace llvm;

#endif

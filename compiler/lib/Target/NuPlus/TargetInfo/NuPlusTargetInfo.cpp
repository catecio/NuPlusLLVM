//===-- NuPlusTargetInfo.cpp - NuPlus Target Implementation -----------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include "NuPlus.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/TargetRegistry.h"
using namespace llvm;

Target llvm::TheNuPlusTarget;

//default hasJIT=false - Check if this targets supports the just-in-time compilation
extern "C" void LLVMInitializeNuPlusTargetInfo() {
  RegisterTarget<Triple::nuplus> X(TheNuPlusTarget, "nuplus","NuPlus");
}

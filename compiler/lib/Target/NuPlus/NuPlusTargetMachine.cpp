//===-- NuPlusTargetMachine.cpp - Define TargetMachine for NuPlus -----------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
//
//===----------------------------------------------------------------------===//

#include "NuPlus.h"
#include "NuPlusTargetMachine.h"
#include "llvm/CodeGen/Passes.h"
#include "llvm/CodeGen/TargetPassConfig.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Support/TargetRegistry.h"

using namespace llvm;

static std::string computeDataLayout () {
  // e -> Little-endian, 
  // m:e -> ManglingMode ELF
  // p:32:32 -> puntatori a 32 bit allineati a 32 bit
  return "e-m:e-p:32:32-i64:64:64-i32:32:32-f32:32:32-f64:64:64";

}

extern "C" void LLVMInitializeNuPlusTarget() {
  // Register the target.
  RegisterTargetMachine<NuPlusTargetMachine> X(TheNuPlusTarget);
}

static Reloc::Model getEffectiveRelocModel(const Triple &TT,
                                           Optional<Reloc::Model> RM) {
  if (!RM.hasValue())
    return Reloc::Static;
  return *RM;
}

NuPlusTargetMachine::NuPlusTargetMachine(const Target &T, const Triple &TT,
                                       StringRef CPU, StringRef FS,
                                       const TargetOptions &Options,
                                       Optional<Reloc::Model> RM,
                                       CodeModel::Model CM,
                                       CodeGenOpt::Level OL)
    : LLVMTargetMachine(T, computeDataLayout(), TT, CPU, FS, Options,
                        getEffectiveRelocModel(TT, RM), CM, OL),
      TLOF(make_unique<NuPlusTargetObjectFile>()),
      Subtarget(TT, CPU, FS, *this) {
  initAsmInfo();
}





namespace {
/// NuPlus Code Generator Pass Configuration Options.
class NuPlusPassConfig : public TargetPassConfig {
public:
  NuPlusPassConfig(NuPlusTargetMachine *TM, PassManagerBase &PM)
      : TargetPassConfig(TM, PM) {}

  NuPlusTargetMachine &getNuPlusTargetMachine() const {
    return getTM<NuPlusTargetMachine>();
  }

  bool addInstSelector() override;
};
} // namespace

TargetPassConfig *NuPlusTargetMachine::createPassConfig(PassManagerBase &PM) {
  return new NuPlusPassConfig(this, PM);
}

bool NuPlusPassConfig::addInstSelector() {
  addPass(createNuPlusISelDag(getNuPlusTargetMachine()));
  return false;
}

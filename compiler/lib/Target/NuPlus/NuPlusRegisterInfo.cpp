//===-- NuPlusRegisterInfo.h - NuPlus Register Information Impl ---*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the NuPlus implementation of the TargetRegisterInfo class.
//
//===----------------------------------------------------------------------===//

#include "NuPlusRegisterInfo.h"
#include "NuPlus.h"
#include "NuPlusInstrInfo.h"
#include "NuPlusSubtarget.h"
#include "llvm/ADT/BitVector.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/IR/Type.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Target/TargetFrameLowering.h"
#include "llvm/Target/TargetInstrInfo.h"

#define GET_REGINFO_TARGET_DESC
#include "NuPlusGenRegisterInfo.inc"

using namespace llvm;

NuPlusRegisterInfo::NuPlusRegisterInfo() : NuPlusGenRegisterInfo(NuPlus::FP_REG) {}

//===----------------------------------------------------------------------===//
// Callee Saved Registers methods
//===----------------------------------------------------------------------===//

const uint16_t *
NuPlusRegisterInfo::getCalleeSavedRegs(const MachineFunction *MF) const {
  return NuPlusCSR_SaveList;
}

const uint32_t *
NuPlusRegisterInfo::getCallPreservedMask(const MachineFunction &MF,
                                        CallingConv::ID) const {
  return NuPlusCSR_RegMask;
}

BitVector NuPlusRegisterInfo::getReservedRegs(const MachineFunction &MF) const {
  BitVector Reserved(getNumRegs());
  Reserved.set(NuPlus::TR_REG);
  Reserved.set(NuPlus::MR_REG);
  Reserved.set(NuPlus::FP_REG);
  Reserved.set(NuPlus::SP_REG);
  Reserved.set(NuPlus::RA_REG);
  Reserved.set(NuPlus::PC_REG);
  return Reserved;
}

// Returns a TargetRegisterClass used for pointer values.
const TargetRegisterClass *
NuPlusRegisterInfo::getPointerRegClass(const MachineFunction &MF,
                                      unsigned Kind) const {
  // Use GPR32 RegisterClass to store pointer values.
  return &NuPlus::GPR32RegClass;
}

// FrameIndex represent objects inside a abstract stack.
// We must replace FrameIndex with an stack/frame pointer
// direct reference.
//TODO: cosi non è limitato lo stack (32 bit = 4 GB).
void NuPlusRegisterInfo::eliminateFrameIndex(MachineBasicBlock::iterator II,
                                            int SPAdj, unsigned FIOperandNum,
                                            RegScavenger *RS) const {
// Inserire istruzioni per la traduzione del frame index.

  assert(SPAdj == 0 && "Unexpected");

  MachineInstr &MI = *II;
  DebugLoc dl = MI.getDebugLoc();
  int FrameIndex = MI.getOperand(FIOperandNum).getIndex();
  MachineFunction &MF = *MI.getParent()->getParent();
  const TargetFrameLowering &TFL = *MF.getSubtarget().getFrameLowering();
  MachineFrameInfo *MFI = MF.getFrameInfo();

  // Round stack size to multiple of 64, consistent with frame pointer info.
  int stackSize = alignTo(MFI->getStackSize(), TFL.getStackAlignment());

  // Frame index is relative to where SP is before it is decremented on
  // entry to the function. Need to add stackSize to adjust for this.
  int64_t Offset = MF.getFrameInfo()->getObjectOffset(FrameIndex) +
                   MI.getOperand(FIOperandNum + 1).getImm() + stackSize;

  // Determine where callee saved registers live in the frame
  const std::vector<CalleeSavedInfo> &CSI = MFI->getCalleeSavedInfo();
  int MinCSFI = 0;
  int MaxCSFI = -1;
  if (CSI.size()) {
    MinCSFI = CSI[0].getFrameIdx();
    MaxCSFI = CSI[CSI.size() - 1].getFrameIdx();
  }

  // When we save callee saved registers (which includes FP), we must use
  // the SP reg, because FP is not set up yet.
  unsigned FrameReg;
  if (FrameIndex >= MinCSFI && FrameIndex <= MaxCSFI)
    FrameReg = NuPlus::SP_REG;
  else
    FrameReg = getFrameRegister(MF);

  // Replace frame index with a frame pointer reference.
  if (isInt<9>(Offset)) {
    // If the offset is small enough to fit in the immediate field, directly
    // encode it.
    MI.getOperand(FIOperandNum).ChangeToRegister(FrameReg, false);
    MI.getOperand(FIOperandNum + 1).ChangeToImmediate(Offset);
    //TODO: 32 bit troppi, controllare
  } else if (isInt<25>(Offset)) {
	// with 18 bits offset, 256 kB
    MachineBasicBlock &MBB = *II->getParent();
    const NuPlusInstrInfo &TII =
        *static_cast<const NuPlusInstrInfo *>(MF.getSubtarget().getInstrInfo());

    MachineRegisterInfo &RegInfo = MBB.getParent()->getRegInfo();
    // Crea registro virtuale
    unsigned Reg = RegInfo.createVirtualRegister(&NuPlus::GPR32RegClass);
    // Inserisci l'immediato nel registro ottenuto in precedenza
    // tramite la MOVEIH e la MOVEIL
    BuildMI(MBB, II, dl, TII.get(NuPlus::MOVEIHSI), Reg).addImm((Offset >> 16) & 0xffff);
	  BuildMI(MBB, II, dl, TII.get(NuPlus::MOVEILSI), Reg).addImm(Offset & 0xffff);
    BuildMI(MBB, II, dl, TII.get(NuPlus::ADDSSS_32), Reg).addReg(FrameReg).addReg(Reg);
    MI.getOperand(FIOperandNum).ChangeToRegister(Reg, false);
    MI.getOperand(FIOperandNum + 1).ChangeToImmediate(0);
  } else
    report_fatal_error("frame index out of bounds: frame too large");


}

unsigned NuPlusRegisterInfo::getFrameRegister(const MachineFunction &MF) const {
  const TargetFrameLowering *TFI = MF.getSubtarget().getFrameLowering();
  return TFI->hasFP(MF) ? NuPlus::FP_REG : NuPlus::SP_REG;
}


// We enable the machine register scavenger which provides a mechanism to make
// registers available by evicting them to spill slots.
bool NuPlusRegisterInfo::requiresRegisterScavenging(const MachineFunction &MF) const {
  return true;
}

bool NuPlusRegisterInfo::trackLivenessAfterRegAlloc(const MachineFunction &MF) const {
  return true;
}

bool NuPlusRegisterInfo::requiresFrameIndexScavenging(const MachineFunction &MF) const {
  return true;
}

//===- NuPlusRelocator.cpp  ----------------------------------------------===//
//
//                     The MCLinker Project
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include <mcld/IRBuilder.h>
#include <mcld/LD/ELFFileFormat.h>
#include <mcld/LD/LDSymbol.h>
#include <mcld/LinkerConfig.h>
#include <mcld/Object/ObjectBuilder.h>
#include <mcld/Support/MsgHandling.h>

#include <llvm/ADT/Twine.h>
#include <llvm/Support/DataTypes.h>
#include <llvm/Support/ELF.h>
#include <llvm/Support/Host.h>
#include <mcld/Support/raw_ostream.h>

#include "NuPlusRelocationFunctions.h"
#include "NuPlusRelocationHelpers.h"
#include "NuPlusRelocator.h"

using namespace mcld;

//===----------------------------------------------------------------------===//
// Relocation Functions and Tables
//===----------------------------------------------------------------------===//
DECL_NUPLUS_APPLY_RELOC_FUNCS

/// the prototype of applying function
typedef Relocator::Result (*ApplyFunctionType)(Relocation& pReloc,
                                               NuPlusRelocator& pParent);

// the table entry of applying functions
class ApplyFunctionEntry {
 public:
  ApplyFunctionEntry() {}
  ApplyFunctionEntry(ApplyFunctionType pFunc, const char* pName,
                     size_t pSize = 0)
      : func(pFunc), name(pName), size(pSize) {}
  ApplyFunctionType func;
  const char* name;
  size_t size;
};
typedef std::map<Relocator::Type, ApplyFunctionEntry> ApplyFunctionMap;

static const ApplyFunctionMap::value_type ApplyFunctionList[] = {
    DECL_NUPLUS_APPLY_RELOC_FUNC_PTRS(ApplyFunctionMap::value_type,
                                     ApplyFunctionEntry)};

// declare the table of applying functions
static ApplyFunctionMap ApplyFunctions(ApplyFunctionList,
                                       ApplyFunctionList +
                                           sizeof(ApplyFunctionList) /
                                               sizeof(ApplyFunctionList[0]));

//===----------------------------------------------------------------------===//
// NuPlusRelocator
//===----------------------------------------------------------------------===//
NuPlusRelocator::NuPlusRelocator(NuPlusGNULDBackend& pParent,
                               const LinkerConfig& pConfig)
    : Relocator(pConfig), m_Target(pParent) {}

NuPlusRelocator::~NuPlusRelocator() {}

Relocator::Result NuPlusRelocator::applyRelocation(Relocation& pRelocation) {
  Relocation::Type type = pRelocation.type();
  assert(ApplyFunctions.find(type) != ApplyFunctions.end());
  return ApplyFunctions[type].func(pRelocation, *this);
}

uint32_t NuPlusRelocator::getDebugStringOffset(Relocation& pReloc) const {
  if (pReloc.type() != llvm::ELF::R_NUPLUS_ABS32)
    error(diag::unsupport_reloc_for_debug_string) << getName(pReloc.type());

  return pReloc.symInfo()->outSymbol()->fragRef()->offset() + pReloc.target() +
         pReloc.addend();
}

void NuPlusRelocator::applyDebugStringOffset(Relocation& pReloc,
                                            uint32_t pOffset) {
  pReloc.target() = pOffset;
}

const char* NuPlusRelocator::getName(Relocator::Type pType) const {
  assert(ApplyFunctions.find(pType) != ApplyFunctions.end());
  return ApplyFunctions[pType].name;
}

Relocator::Size NuPlusRelocator::getSize(Relocation::Type pType) const {
  return ApplyFunctions[pType].size;
}

void NuPlusRelocator::scanRelocation(Relocation& pReloc, IRBuilder& pBuilder,
                                    Module& pModule, LDSection& pSection,
                                    Input& pInput) {
  ResolveInfo* rsym = pReloc.symInfo();
  assert(NULL != rsym &&
         "ResolveInfo of relocation not set while scanRelocation");

  assert(NULL != pSection.getLink());
  if (0 == (pSection.getLink()->flag() & llvm::ELF::SHF_ALLOC)) return;

  // check if we shoule issue undefined reference for the relocation target
  // symbol
  if (rsym->isUndef() && !rsym->isDyn() && !rsym->isWeak() && !rsym->isNull())
    issueUndefRef(pReloc, pSection, pInput);
}

//===----------------------------------------------------------------------===//
// Each relocation function implementation
//===----------------------------------------------------------------------===//

Relocator::Result none(Relocation& pReloc, NuPlusRelocator& pParent) {
  return Relocator::OK;
}

Relocator::Result abs(Relocation& pReloc, NuPlusRelocator& pParent) {
  Relocator::DWord A = pReloc.addend();
  Relocator::DWord S = pReloc.symValue();
  pReloc.target() = S + A;

  return Relocator::OK;
}

// modificata per aderire alle istruzioni di tipo JR, anche se potrebbe essere 
// richiamata anche per istruzioni di tipo J
Relocator::Result branch(Relocation& pReloc, NuPlusRelocator& pParent) {
  Relocator::Address S = pReloc.symValue();
  Relocator::Address P = pReloc.place();
  int offset = S - P;

  if (helper_check_signed_overflow(offset, 18)) return Relocator::Overflow;

  pReloc.target() = helper_replace_field(pReloc.target(), offset, 0, 18);

  return Relocator::OK;
}

// modificata per aderire alle istruzioni di tipo M
Relocator::Result mem(Relocation& pReloc, NuPlusRelocator& pParent) {
  Relocator::Address S = pReloc.symValue();
  Relocator::Address P = pReloc.place();
  int offset = S - P;

  if (helper_check_signed_overflow(offset, 9)) return Relocator::Overflow;

  pReloc.target() = helper_replace_field(pReloc.target(), offset, 3, 9);

  return Relocator::OK;
}

// TODO: la seguente è valida esclusivamente per nyuzi
Relocator::Result memext(Relocation& pReloc, NuPlusRelocator& pParent) {
  Relocator::Address S = pReloc.symValue();
  Relocator::Address P = pReloc.place();
  Relocator::Address A = pReloc.addend();
  int offset = (S + A) - P;

  if (helper_check_signed_overflow(offset, 9)) return Relocator::Overflow;

  pReloc.target() = helper_replace_field(pReloc.target(), offset, 3, 9);

  return Relocator::OK;
}

// Modificata per aderire al tipo I
Relocator::Result lea(Relocation& pReloc, NuPlusRelocator& pParent) {

  Relocator::Address S = pReloc.symValue();
  Relocator::Address P = pReloc.place();
  // TODO: questa rilocazione vale se l'indirizzo non sfora i 16 del campo Imm
  // della ADD
  int offset = S - P;

  if (helper_check_unsigned_overflow(offset, 9)) return Relocator::Overflow;

  pReloc.target() = helper_replace_field(pReloc.target(), offset, 3, 9);

  return Relocator::OK;
}

Relocator::Result absh(Relocation& pReloc, NuPlusRelocator& pParent) {

  Relocator::Address S = pReloc.symValue();
  Relocator::DWord A = pReloc.addend();
  
  int offset = (((S+A) >> 16) & 0xFFFF);
  
  if (helper_check_unsigned_overflow(offset, 16)) return Relocator::Overflow;

  pReloc.target() = helper_replace_field(pReloc.target(), offset, 2, 16);

  return Relocator::OK;
}

Relocator::Result absl(Relocation& pReloc, NuPlusRelocator& pParent) {

  Relocator::Address S = pReloc.symValue(); 
  Relocator::DWord A = pReloc.addend();

  int offset = ((S+A) & 0xFFFF);

  if (helper_check_signed_overflow(offset, 16)) return Relocator::Overflow;

  pReloc.target() = helper_replace_field(pReloc.target(), offset, 2, 16);

  return Relocator::OK;
}

//===-  NuPlusRelocator.h ------------------------------------------------===//
//
//                     The MCLinker Project
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#ifndef TARGET_NUPLUS_NUPLUSRELOCATOR_H
#define TARGET_NUPLUS_NUPLUSRELOCATOR_H

#include <mcld/LD/Relocator.h>
#include <mcld/Target/GOT.h>
#include <mcld/Target/KeyEntryMap.h>
#include "NuPlusLDBackend.h"

namespace mcld {

/** \class NuPlusRelocator
 *  \brief NuPlusRelocator creates and destroys the NuPlus relocations.
 *
 */
class NuPlusRelocator : public Relocator {
 public:
  typedef KeyEntryMap<Relocation, Relocation> RelRelMap;

  NuPlusRelocator(NuPlusGNULDBackend& pParent, const LinkerConfig& pConfig);
  ~NuPlusRelocator();

  Result applyRelocation(Relocation& pRelocation);

  uint32_t getDebugStringOffset(Relocation& pReloc) const;
  void applyDebugStringOffset(Relocation& pReloc, uint32_t pOffset);

  NuPlusGNULDBackend& getTarget() { return m_Target; }

  const NuPlusGNULDBackend& getTarget() const { return m_Target; }

  const char* getName(Relocation::Type pType) const;

  Size getSize(Relocation::Type pType) const;

  const RelRelMap& getRelRelMap() const { return m_RelRelMap; }
  RelRelMap& getRelRelMap() { return m_RelRelMap; }

  void scanRelocation(Relocation& pReloc, IRBuilder& pBuilder, Module& pModule,
                      LDSection& pSection, Input& pInput);

 private:
  NuPlusGNULDBackend& m_Target;
  RelRelMap m_RelRelMap;
};

}  // namespace of mcld

#endif

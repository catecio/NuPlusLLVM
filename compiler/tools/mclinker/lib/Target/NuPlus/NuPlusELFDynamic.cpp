//===- NuPlusELFDynamic.cpp ----------------------------------------------===//
//
//                     The MCLinker Project
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#include "NuPlusELFDynamic.h"

#include <mcld/LD/ELFFileFormat.h>
#include <mcld/LinkerConfig.h>

using namespace mcld;

NuPlusELFDynamic::NuPlusELFDynamic(const GNULDBackend& pParent,
                                 const LinkerConfig& pConfig)
    : ELFDynamic(pParent, pConfig) {}

NuPlusELFDynamic::~NuPlusELFDynamic() {}

void NuPlusELFDynamic::reserveTargetEntries(const ELFFileFormat& pFormat) {}

void NuPlusELFDynamic::applyTargetEntries(const ELFFileFormat& pFormat) {}

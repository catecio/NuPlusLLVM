//===- NuPlusGNUInfo.h ---------------------------------------------------===//
//
//                     The MCLinker Project
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#ifndef TARGET_NUPLUS_NUPLUSGNUINFO_H
#define TARGET_NUPLUS_NUPLUSGNUINFO_H
#include <mcld/Target/GNUInfo.h>

#include <llvm/Support/ELF.h>

namespace mcld {

class NuPlusGNUInfo : public GNUInfo {
 public:
  NuPlusGNUInfo(const llvm::Triple& pTriple) : GNUInfo(pTriple) {}

/// The return value of machine() it the same as e_machine in the ELF header
  uint32_t machine() const { return llvm::ELF::EM_NUPLUS; }

  /// abiPageSize - the abi page size of the target machine, and we set it to 4K
  /// here. If target favors the different size, please override this function
  uint64_t abiPageSize() const { return 0x1000; }

  /// defaultTextSegmentAddr - target should specify its own default start
  /// address of the text segment. esp. for exec.
  uint64_t defaultTextSegmentAddr() const { return 0; }

  // There are no processor-specific flags so this field shall contain zero.
  uint64_t flags() const { return 0x0; }
};

}  // namespace of mcld

#endif

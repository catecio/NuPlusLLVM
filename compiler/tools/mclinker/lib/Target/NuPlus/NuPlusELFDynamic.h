//===- NuPlusELFDynamic.h ------------------------------------------------===//
//
//                     The MCLinker Project
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#ifndef TARGET_NUPLUS_NUPLUSELFDYNAMIC_H
#define TARGET_NUPLUS_NUPLUSELFDYNAMIC_H

#include <mcld/Target/ELFDynamic.h>

namespace mcld {

class NuPlusELFDynamic : public ELFDynamic {
 public:
  NuPlusELFDynamic(const GNULDBackend& pParent, const LinkerConfig& pConfig);
  ~NuPlusELFDynamic();

 private:
  void reserveTargetEntries(const ELFFileFormat& pFormat);
  void applyTargetEntries(const ELFFileFormat& pFormat);
};

}  // namespace of mcld

#endif

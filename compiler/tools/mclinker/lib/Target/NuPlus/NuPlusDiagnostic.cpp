//===- NuPlusDiagnostic.cpp ----------------------------------------------===//
//
//                     The MCLinker Project
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#include "NuPlus.h"
#include <mcld/LD/DWARFLineInfo.h>
#include <mcld/Support/TargetRegistry.h>

using namespace mcld;

namespace mcld {
//===----------------------------------------------------------------------===//
// createNuPlusDiagnostic - the help function to create corresponding
// NuPlusDiagnostic
//===----------------------------------------------------------------------===//
DiagnosticLineInfo* createNuPlusDiagLineInfo(const mcld::Target& pTarget,
                                            const std::string& pTriple) {
  return new DWARFLineInfo();
}

}  // namespace of mcld

//===----------------------------------------------------------------------===//
// InitializeNuPlusDiagnostic
//===----------------------------------------------------------------------===//
extern "C" void MCLDInitializeNuPlusDiagnosticLineInfo() {
  // Register the linker frontend
  mcld::TargetRegistry::RegisterDiagnosticLineInfo(TheNuPlusTarget,
                                                   createNuPlusDiagLineInfo);
}

//===- NuPlusLDBackend.cpp -----------------------------------------------===//
//
//                     The MCLinker Project
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
#include "NuPlus.h"
#include "NuPlusELFDynamic.h"
#include "NuPlusGNUInfo.h"
#include "NuPlusLDBackend.h"
#include "NuPlusRelocator.h"

#include <cstring>

#include <llvm/ADT/Triple.h>
#include <llvm/ADT/Twine.h>
#include <llvm/Support/Casting.h>
#include <llvm/Support/ELF.h>

#include <mcld/Fragment/AlignFragment.h>
#include <mcld/Fragment/FillFragment.h>
#include <mcld/Fragment/NullFragment.h>
#include <mcld/Fragment/RegionFragment.h>
#include <mcld/Fragment/Stub.h>
#include <mcld/IRBuilder.h>
#include <mcld/LD/BranchIslandFactory.h>
#include <mcld/LD/ELFFileFormat.h>
#include <mcld/LD/ELFSegment.h>
#include <mcld/LD/ELFSegmentFactory.h>
#include <mcld/LD/LDContext.h>
#include <mcld/LD/StubFactory.h>
#include <mcld/LinkerConfig.h>
#include <mcld/Object/ObjectBuilder.h>
#include <mcld/Support/MemoryArea.h>
#include <mcld/Support/MemoryRegion.h>
#include <mcld/Support/MsgHandling.h>
#include <mcld/Support/TargetRegistry.h>
#include <mcld/Target/ELFAttribute.h>
#include <mcld/Target/GNUInfo.h>

using namespace mcld;

LDSymbol *beginSym = NULL, *endSym = NULL;
LDSection* scratchpadSection = NULL;
const uint64_t scratchpadLimit = 64 * 1024; //64KB

//===----------------------------------------------------------------------===//
// NuPlusGNULDBackend
//===----------------------------------------------------------------------===//
NuPlusGNULDBackend::NuPlusGNULDBackend(const LinkerConfig& pConfig,
                                     GNUInfo* pInfo)
    : GNULDBackend(pConfig, pInfo),
      m_pRelocator(NULL),
      m_pRelaDyn(NULL),
      m_pDynamic(NULL) {}

NuPlusGNULDBackend::~NuPlusGNULDBackend() {
  if (m_pRelocator != NULL) delete m_pRelocator;
  if (m_pRelaDyn != NULL) delete m_pRelaDyn;
  if (m_pDynamic != NULL) delete m_pDynamic;
}

void NuPlusGNULDBackend::initTargetSections(Module& pModule,
                                           ObjectBuilder& pBuilder) {

  if (LinkerConfig::Object != config().codeGenType()) {
    ELFFileFormat* file_format = getOutputFormat();

    // initialize .rela.dyn
    LDSection& reladyn = file_format->getRelaDyn();
    m_pRelaDyn = new OutputRelocSection(pModule, reladyn);
  }
}

void NuPlusGNULDBackend::initTargetSymbols(IRBuilder& pBuilder,
                                          Module& pModule) {
  scratchpadSection = pModule.getSection("scratchpad");
  if (scratchpadSection != NULL) {
    if(scratchpadSection->size() > scratchpadLimit)
      llvm::report_fatal_error("Too many data in scratchpad memory!\n");
    FragmentRef* symFrag = NULL;
    Module::sym_iterator symbol, symEnd = pModule.sym_end();
    for (symbol = pModule.sym_begin(); symbol != symEnd; ++symbol) {
      if ((*symbol)->hasFragRef() && (*symbol)->fragRef()->frag()->getParent()->getSection().name() == "scratchpad") {
        symFrag = (*symbol)->fragRef();
        (*symbol)->setValue((*symbol)->fragRef()->getOutputOffset());
        (*symbol)->resolveInfo()->setBinding(ResolveInfo::Absolute);
      }
    }
    if (symFrag != NULL) {
      beginSym = pBuilder.AddSymbol<IRBuilder::Force, IRBuilder::Unresolve>
                                                   ("__scratchpad_data_start",
                                                    ResolveInfo::NoType,
                                                    ResolveInfo::Define,
                                                    ResolveInfo::Global,
                                                    0,
                                                    0,
                                                    symFrag,
                                                    ResolveInfo::Default);
      endSym = pBuilder.AddSymbol<IRBuilder::Force, IRBuilder::Unresolve>
                                                   ("__scratchpad_data_end",
                                                    ResolveInfo::NoType,
                                                    ResolveInfo::Define,
                                                    ResolveInfo::Global,
                                                    0,
                                                    0,
                                                    symFrag,
                                                    ResolveInfo::Default);
    }
  }
}
										  
bool NuPlusGNULDBackend::initRelocator() {
  if (NULL == m_pRelocator) {
    m_pRelocator = new NuPlusRelocator(*this, config());
  }
  return true;
}

const Relocator* NuPlusGNULDBackend::getRelocator() const {
  assert(NULL != m_pRelocator);
  return m_pRelocator;
}

Relocator* NuPlusGNULDBackend::getRelocator() {
  assert(NULL != m_pRelocator);
  return m_pRelocator;
}

void NuPlusGNULDBackend::doPreLayout(IRBuilder& pBuilder) {
  // initialize .dynamic data
  if (!config().isCodeStatic() && NULL == m_pDynamic)
    m_pDynamic = new NuPlusELFDynamic(*this, config());

  if (LinkerConfig::Object != config().codeGenType()) {
    ELFFileFormat* file_format = getOutputFormat();
    // set .rela.dyn size
    if (!m_pRelaDyn->empty()) {
      assert(
          !config().isCodeStatic() &&
          "static linkage should not result in a dynamic relocation section");
      file_format->getRelaDyn().setSize(m_pRelaDyn->numOfRelocs() *
                                        getRelaEntrySize());
    }
  }
}

void NuPlusGNULDBackend::doPostLayout(Module& pModule, IRBuilder& pBuilder) {}

NuPlusELFDynamic& NuPlusGNULDBackend::dynamic() {
  assert(NULL != m_pDynamic);
  return *m_pDynamic;
}

const NuPlusELFDynamic& NuPlusGNULDBackend::dynamic() const {
  assert(NULL != m_pDynamic);
  return *m_pDynamic;
}

uint64_t NuPlusGNULDBackend::emitSectionData(const LDSection& pSection,
                                            MemoryRegion& pRegion) const {
  assert(pRegion.size() && "Size of MemoryRegion is zero!");

  return pRegion.size();
}

unsigned int NuPlusGNULDBackend::getTargetSectionOrder(
    const LDSection& pSectHdr) const {
  const ELFFileFormat* file_format = getOutputFormat();

  return SHO_UNDEFINED;
}

bool NuPlusGNULDBackend::doRelax(Module& pModule, IRBuilder& pBuilder,
                                bool& pFinished) {
  // TODO
  return false;
}

bool NuPlusGNULDBackend::initTargetStubs() {
  // TODO
  return true;
}

void NuPlusGNULDBackend::doCreateProgramHdrs(Module& pModule) {
  // TODO
}

bool NuPlusGNULDBackend::finalizeTargetSymbols() {
  if (scratchpadSection != NULL && beginSym != NULL && endSym != NULL) {
    uint64_t addr = scratchpadSection->addr(), size = scratchpadSection->size();
    beginSym -> setValue(addr);
    endSym -> setValue(addr + size);
  }
  return true;
}

bool NuPlusGNULDBackend::mergeSection(Module& pModule, const Input& pInput,
                                     LDSection& pSection) {
  // TODO
  return true;
}

bool NuPlusGNULDBackend::readSection(Input& pInput, SectionData& pSD) {
  // TODO
  return true;
}

OutputRelocSection& NuPlusGNULDBackend::getRelaDyn() {
  assert(NULL != m_pRelaDyn && ".rela.dyn section not exist");
  return *m_pRelaDyn;
}

const OutputRelocSection& NuPlusGNULDBackend::getRelaDyn() const {
  assert(NULL != m_pRelaDyn && ".rela.dyn section not exist");
  return *m_pRelaDyn;
}

namespace mcld {

//===----------------------------------------------------------------------===//
//  createNuPlusLDBackend - the help funtion to create corresponding
//  NuPlusLDBackend
//===----------------------------------------------------------------------===//
TargetLDBackend* createNuPlusLDBackend(const LinkerConfig& pConfig) {
  if (pConfig.targets().triple().isOSDarwin()) {
    assert(0 && "MachO linker is not supported");
  }
  if (pConfig.targets().triple().isOSWindows()) {
    assert(0 && "COFF linker is not supported");
  }
  return new NuPlusGNULDBackend(pConfig,
                               new NuPlusGNUInfo(pConfig.targets().triple()));
}

}  // namespace of mcld

//===----------------------------------------------------------------------===//
// Force static initialization.
//===----------------------------------------------------------------------===//
extern "C" void MCLDInitializeNuPlusLDBackend() {
  // Register the linker backend
  mcld::TargetRegistry::RegisterTargetLDBackend(TheNuPlusTarget,
                                                createNuPlusLDBackend);
}

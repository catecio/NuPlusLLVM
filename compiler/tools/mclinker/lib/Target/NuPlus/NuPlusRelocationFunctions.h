//===- NuPlusRelocationFunction.h ----------------------------------------===//
//
//                     The MCLinker Project
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

// Functions definition
#define DECL_NUPLUS_APPLY_RELOC_FUNC(Name)                \
  static NuPlusRelocator::Result Name(Relocation& pEntry, \
                                     NuPlusRelocator& pParent);

#define DECL_NUPLUS_APPLY_RELOC_FUNCS  \
  DECL_NUPLUS_APPLY_RELOC_FUNC(none)   \
  DECL_NUPLUS_APPLY_RELOC_FUNC(abs)    \
  DECL_NUPLUS_APPLY_RELOC_FUNC(branch) \
  DECL_NUPLUS_APPLY_RELOC_FUNC(mem)    \
  DECL_NUPLUS_APPLY_RELOC_FUNC(memext) \
  DECL_NUPLUS_APPLY_RELOC_FUNC(lea) \
  DECL_NUPLUS_APPLY_RELOC_FUNC(absh) \
  DECL_NUPLUS_APPLY_RELOC_FUNC(absl)

// This macro helps with the function-symbol mapping
#define DECL_NUPLUS_APPLY_RELOC_FUNC_PTRS(ValueType, MappedType)         \
  ValueType(0x0, MappedType(&none,   "R_NUPLUS_NONE")),                  \
  ValueType(0x1, MappedType(&abs,    "R_NUPLUS_ABS32", 32)),             \
  ValueType(0x2, MappedType(&branch, "R_NUPLUS_BRANCH", 32)),            \
  ValueType(0x3, MappedType(&mem,    "R_NUPLUS_PCREL_MEM", 32)),         \
  ValueType(0x4, MappedType(&memext, "R_NUPLUS_PCREL_MEM_EXT", 32)),     \
  ValueType(0x5, MappedType(&lea,    "R_NUPLUS_PCREL_LEA", 32)),     \
  ValueType(0x6, MappedType(&absh,    "R_NUPLUS_ABS_HIGH_LEA", 32)),     \
  ValueType(0x7, MappedType(&absl,    "R_NUPLUS_ABS_LOW_LEA", 32))

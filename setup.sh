#!/bin/bash

#
# Build the compiler
#

#PARAMETERS:
#   -l,--lib       : builds the nu+ libraries.
#   -t=,--thread=   : sets the number of threads for compilation.
#   -d,--debug      : sets the building type to debug.
#   -r,--release    : sets the building type to release.
#   -n,--new        : make a new compiler build.
#   -k,--linker    : builds only the linker.

TOP_DIR=`pwd`
COMPILER_PATH=$TOP_DIR/compiler

#default values
BUILDPATHREL=$COMPILER_PATH/build
BUILDPATHDEB=$COMPILER_PATH/buildDebug

DEBUG=0
NEW=0
THREAD=4
LINKER=0

#input variables
LIBRARIES=
NEWDEBUG=
NEWTHREAD=
BUILDPATH=$BUILDPATHREL

set -e

for i in "$@"
do
case $i in
    -l|--lib)
        if [ -z "$LIBRARIES" ]; then    
            LIBRARIES=1
            #echo "LIBRARIES  =   ${LIBRARIES}"
        fi
        shift 
    ;;
    -t=*|--thread=*)
        if [ -z "$NEWTHREAD" ]; then    
            NEWTHREAD="${i#*=}"
            #echo "THREAD  =   ${NEWTHREAD}"
        fi
        shift 
    ;;
    -d|--debug)
        if [ -z "$NEWDEBUG" ]; then
            NEWDEBUG=1
            BUILDPATH=$BUILDPATHDEB
            #echo "DEBUG  =   ${NEWDEBUG}"
        fi
        shift 
    ;;
    -n|--new)
        NEW=1
        #echo "NEW  =   ${NEW}"
        shift  
    ;;
    -k|--linker)
        LINKER=1
        #echo "LINKER  =   ${LINKER}"
        shift  
    ;;
    -h|--help)
        echo "This script helps you to build the nu+ compiler and libraries."
        echo "PARAMETERS:"
        echo "-l,--lib       : builds the nu+ libraries."
        echo "-t=,--thread=   : sets the number of threads for compilation."
        echo "-d,--debug      : sets the building type to debug."
        echo "-n,--new        : make a new compiler build."
        echo "-k,--linker    : builds only the linker."
        exit  
    ;;
    *)
            # unknown option
    ;;
esac
done


if [ ! -z "$NEWDEBUG" ]; then
    DEBUG=$NEWDEBUG
fi

if [ ! -z "$NEWTHREAD" ]; then
    THREAD=$NEWTHREAD
fi

if [ "$LIBRARIES" = 1 ]; then
    echo "Rebuilding nu+ libraries..."    
    echo "make clean"
    make clean
    echo "make"
    make
else

    if [ "$LINKER" = 0 ]; then
        if [ "$NEW" = "1" ]; then
            echo "Creating build directory..."
            echo "mkdir ${BUILDPATH}"
            echo "cd ${BUILDPATH}"

            mkdir -p "${BUILDPATH}"
            cd "${BUILDPATH}"

            if [ "$DEBUG" = "1" ]; then
                echo "Configuring build in Debug mode..."
                echo " cmake -G Unix Makefiles -DCMAKE_BUILD_TYPE=Debug ${COMPILER_PATH}"

                cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX:PATH=${MANGO_ROOT}/usr/local/llvm-nuplus/ -DCMAKE_BUILD_TYPE=Debug "${COMPILER_PATH}"
            else
                echo "Configuring build in Release mode..."
                echo "cmake -G Unix Makefiles -DCMAKE_BUILD_TYPE=Release ${COMPILER_PATH}"

                cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX:PATH=${MANGO_ROOT}/usr/local/llvm-nuplus/ -DCMAKE_BUILD_TYPE=Release "${COMPILER_PATH}"
            fi
        else
            echo "cd {$BUILDPATH}"

            cd "${BUILDPATH}"        
        fi

        echo "Building LLVM..."
        echo "make -j${THREAD} -l4"
        make -j"${THREAD}" -l4
    else
        echo "cd ${BUILDPATH}"
        echo "make ld.mcld"

        cd "${BUILDPATH}"
        make ld.mcld
    fi
    echo "Installing LLVM..."
    echo "sudo make install"

    sudo make install
fi

echo "Done."

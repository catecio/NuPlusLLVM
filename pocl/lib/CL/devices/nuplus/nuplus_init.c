/*
 ============================================================================
 Name        : nuplus_driver.c
 Author      : Luigi Venuso
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "nuplus_driver.h"

void get_nuplus_devices(size_t *n, nuplus_dev *devs, int boaud_rate){
	unsigned int ack;
	unsigned int device_id;
	int general_serial_fd;
	char general_port_name[NUPLUSE_DEVS_NUMBER][256];			//XXX: no more then 256 tty device
	int nuplus_dev_fd[NUPLUSE_DEVS_NUMBER];

	DIR *tty;
	struct dirent *directory;
	tty = opendir("/dev/");
	char tty_char;

	*n = 0;

	while ((directory = readdir(tty)) != NULL)
	{
		strncpy(&tty_char, directory->d_name, 3);
		if(strcmp(&tty_char, "tty") == 0){
			general_serial_fd = open_connection(directory->d_name, boaud_rate);

			///< Send command to ping device
			if(!write_char(general_serial_fd, YOUR_ID_DEVICE_COMMAND))
				break;

			///< Waiting ack from device for write command
			if(!read_long(general_serial_fd, &ack, 1500))
				break;

			///< Check command ack
			if(ack != YOUR_ID_DEVICE_ACK){
				printf("No YOUR_ID_DEVICE_ACK!\n");
				break;
			}

			///< Waiting devise id
			if(!read_long(general_serial_fd, &device_id, 1500))
				break;

			///< Check if id == DATA_PING_MATCH
			if(device_id == DATA_PING_MATCH){
				nuplus_dev_fd[*n] = general_serial_fd;
				strcpy(general_port_name[*n], directory->d_name);
				*n += 1;
			}
		}
	}

	if(*n > 0){
		devs = (nuplus_dev *) calloc (*n, sizeof(nuplus_dev));
		for (int i=0; i<*n; i++){
			devs[i].serial_fd = nuplus_dev_fd[i];
			strcpy(devs[i].port_name, general_port_name[i]);
		}
	}

	closedir(tty);

}

int nuplus_open(nuplus_dev* nuplus_device){
	if (strcmp(nuplus_device->port_name, "") && nuplus_device->baud_rate != 0) {
		nuplus_device->serial_fd = open_connection(nuplus_device->port_name, nuplus_device->baud_rate);
		return 1;
	} else
		return 0;
}

int nuplus_close(nuplus_dev* nuplus_device){
	if(!close_connection(nuplus_device->serial_fd))
		return 0;

	return 1;
}

void create_thread(nuplus_thread* thread, uint32_t pc, uint32_t thread_id){
	thread->pc =  pc;
	thread->thread_id = thread_id;
}

void create_core(nuplus_core* core, nuplus_thread* thread, uint32_t core_id, uint32_t num_threads){
	core->id = core_id;
	core->num_threads = num_threads;
	core->threads = thread;
}

void destroy_device(nuplus_dev* dev){
	if (dev->num_cores > 0){
		for(int i=0; i<dev->num_cores; i++){
			destroy_core(dev[i].cores);
		}
	}

	free(dev);
}

void destroy_core(nuplus_core* core){
	if (core->num_threads > 0){
		for(int i=0; i<core->num_threads; i++){
			free(core[i].threads);
		}
	}
	free(core);
}

/*
 * nuplus_driver.h
 *
 *  Created on: 06 ott 2017
 *      Author: luigi
 */

#ifndef INCLUDE_NUPLUS_DRIVER_H_
#define INCLUDE_NUPLUS_DRIVER_H_

#include <unistd.h>
#include <stdint.h>
#include "uart.h"
#include "dirent.h"

#define SEEK_SET 0
#define SEEK_CUR 1

#define WRITE_MEMORY_COMMAND 	0x01
#define READ_MEMORY_COMMAND 	0x02
#define BOOT_CORE_COMMAND 		0x03
#define ENABLE_THREAD_COMMAND 	0x04
#define YOUR_ID_DEVICE_COMMAND  0x05 //FIXME: change with real command
#define RESET_CORE_COMMAND		0x06
#define FREEZE_CORE_COMMAND		0x07

#define WRITE_MEMORY_ACK 		0x11
#define READ_MEMORY_ACK 		0x12
#define BOOT_CORE_ACK 			0x13
#define ENABLE_THREAD_ACK 		0x14
#define YOUR_ID_DEVICE_ACK		0x15 //FIXME: change with real ack
#define RESET_CORE_ACK			0x16
#define FREEZE_CORE_ACK			0x17

#define END_OF_KERNEL			0x20


#define MEM_LINE 64 //in byte
#define DATA_PING_MATCH 0x206e752b
#define NUPLUSE_DEVS_NUMBER 10	//XXX: Change for more nuplus device


typedef struct {
	uint32_t pc;				  ///< The program Counter associated with the thread.
	uint32_t thread_id; 		  ///< The thread id.
}nuplus_thread;

typedef struct {
	uint32_t id;				  ///< The cores id.
	uint32_t num_threads;    	  ///< The number of the core's enabled thread.
	uint32_t thread_active_mask;  ///< The mask of activated threads
	nuplus_thread *threads; 	  ///< A vector of nuplus_thread struct.
}nuplus_core;


typedef struct {
	char* port_name;		  	  ///< The serial device associated with nu+.
	uint32_t baud_rate;			  ///< The baud rate of the serial interface. 
	uint32_t serial_fd;			  ///< The serial file descriptor.
	uint64_t address; 			  ///< The serial file descriptor.
	uint32_t num_cores;			  ///< The number of enabled cores.
	nuplus_core* cores; 			  ///< A vector of nuplus_core struct.
}nuplus_dev;


/**
 * Open the connection to the nu+ device memory.
 * @param nuplus_device A nuplus_device device struct.
 * @return If success returns 0, a different value otherwise.
 * */
int nuplus_open(nuplus_dev* nuplus_device);
/**
 * Close the connection to the nu+ device memory.
 * @param nuplus_device A nuplus_device device struct.
 * @return If success returns 0, a different value otherwise.
 * */
int nuplus_close(nuplus_dev* nuplus_device);
/**
 * Read n bytes from the nu+ memory.
 * @param nuplus_device A nuplus_device device struct.
 * @param buff A pointer to a variable where to store the bytes read.
 * @param nbyte The number of bytes to read.
 * @return Returns the effective number of bytes read.
 * */
ssize_t nuplus_read(nuplus_dev* nuplus_device, void* buff, size_t nbyte);
/**
 * Write n bytes from the nu+ memory.
 * @param nuplus_device A nuplus_device device struct.
 * @param buff A pointer to a variable which helds the bytes to write.
 * @param nbyte The number of bytes to write.
 * @return Returns the effective number of bytes written.
 * */
ssize_t nuplus_write(nuplus_dev* nuplus_device, const void* buff, size_t nbyte);
/**
 * Reposition read/write file offset.
 * @param nuplus_device A nuplus_device device struct.
 * @param offset The offset used to reposition the file offset. Its use depends on the whence variable.
 * @param whence It can assume the following values:
 * 					SEEK_SET
 *           		  The file offset is set to offset bytes.
 *				    SEEK_CUR
 *		              The file offset is set to its current location plus offset bytes.
 * @return Returns the resulting offset location as measured in bytes from the beginning of the file.
 * */
int nuplus_lseek(nuplus_dev* nuplus_device, uint64_t offset, int whence); 	//	If whence is SEEK_SET, the file offset shall be set to offset bytes.
																				//	If whence is SEEK_CUR, the file offset shall be set to its current location plus offset.


/**
 * Create a nuplus_thread structure with the given inputs.
 * @param nuplus_thread A nuplus_thread struct.
 * @param pc The value of Program Counter to associate with the thread.
 * @param thread_id The thread's thread id.
 * */
void create_thread(nuplus_thread* thread, uint32_t pc, uint32_t thread_id);
/**
 * Create a nuplus_core structure with the given inputs.
 * @param nuplus_core A nuplus_core struct.
 * @param nuplus_thread A vector of nuplus_thread struct.
 * @param core_id The core's core id.
 * @param num_thread The number of threads enabled for the core.
 * */
void create_core(nuplus_core* core, nuplus_thread* thread, uint32_t core_id, uint32_t num_threads);

/**
 * Destroy a nuplus_dev structure with the given inputs.
 * @param nuplus_dev A nuplus_dev struct.
 * */
void destroy_device(nuplus_dev* dev);
/**
 * Destroy a nuplus_core structure with the given inputs.
 * @param nuplus_core A nuplus_core struct.
 * */
void destroy_core(nuplus_core* core);
/**
 * Get a list of nuplus devices.
 * @param n The number of nu+ devices.
 * @param devs A vector of nuplus_device structs.
 * */
void get_nuplus_devices(size_t *n, nuplus_dev *devs, int baud_rate); //FIXME: prima c'era size_t *n ma perché *?


/**
 * Performs the boot operations for the nu+ device.
 * @param nuplus_device A nuplus_device struct.
 * @return If success returns 0, a different value otherwise.
 * */
int nuplus_boot(nuplus_dev* nuplus_device);		//BOOT_COMMAND
/**
 * Start the kernel execution.
 * @param nuplus_device A nuplus_device struct.
 * @return If success returns 0, a different value otherwise.
 * */
int nuplus_run(nuplus_dev* nuplus_device);		//ENABLE_THREAD
/**
 * Reset the nu+ device.
 * @param nuplus_device A nuplus_device struct.
 * @return If success returns 0, a different value otherwise.
 * */
int nuplus_reset(nuplus_dev* nuplus_device);	//RESET_SYSTEM
/**
 * Freeze the nu+ device.
 * @param nuplus_device A nuplus_device struct.
 * @return If success returns 0, a different value otherwise.
 * */
int nuplus_freeze(nuplus_dev* nuplus_device);	//FREEZE_SYSTEM
/**
 * Check the device for the end of the kernel execution.
 * @param nuplus_device A nuplus_device struct.
 * @return If the execution has not ended returns 0, a different value if it has ended.
 * */
int nuplus_get_done(nuplus_dev* nuplus_device);	//GET END OF KERNEL

#endif /* INCLUDE_NUPLUS_DRIVER_H_ */

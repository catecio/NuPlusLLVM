#include "nuplus_compile.h"
#include "nuplusconf.h"

#include <string.h>

#ifndef _MSC_VER
#  include <unistd.h>
#else
#  include "vccompat.hpp"
#endif

#include "pocl_llvm.h"
#include "pocl_file_util.h"
#include "pocl_runtime_config.h"
#include "pocl_util.h"
#include "install-paths.h"

#define COMMAND_LENGTH 2048

/**
 * Generate code from the final bitcode using the LLVM
 * tools.
 *
 * Uses an existing (cached) one, if available.
 *
 * @param tmpdir The directory of the work-group function bitcode.
 * @param return the generated relocatable ELF file. The file needs to be
 * linked again with a correct memory map.
 *
 * Original in devices/common.c
 */
const char*
nuplus_llvm_codegen (const char* tmpdir, cl_kernel kernel, cl_device_id device) {

  int pocl_verbose = pocl_get_bool_option("POCL_VERBOSE", 0);
 
  // sed s/expr/repl => substitute expr with repl if expr is encountered
  static const char* const start_sed = "sed s/:KERN_FUNC:/%s/ %s | %s";

  static const char* const assembly_cmd = "/opt/mango_unina/usr/local/llvm-nuplus/bin/clang --target=nuplus -o %s";  

  char command[COMMAND_LENGTH];
  char command2[COMMAND_LENGTH];
  char bytecode[POCL_FILENAME_LENGTH];
  char objfile[POCL_FILENAME_LENGTH];
  char asmfile[POCL_FILENAME_LENGTH];
  char start_asmfile[POCL_FILENAME_LENGTH];
  char start_objfile[POCL_FILENAME_LENGTH];

  char* module = (char*) malloc(min(POCL_FILENAME_LENGTH,
	   strlen(tmpdir) + strlen(kernel->name) + 5)); // strlen of / .so 4+1

  int error;

  /* NB: Don't change the name of this file. It is checked for in
     clEnqueueNDRangeKernel to determine if the command file should be compiled
     again */
  error = snprintf
    (module, POCL_FILENAME_LENGTH,
     "%s/%s.so", tmpdir, kernel->name);
  assert (error >= 0);

  error = snprintf
    (objfile, POCL_FILENAME_LENGTH,
     "%s/%s_wg.o", tmpdir, kernel->name);
  assert (error >= 0);

  error = snprintf
    (asmfile, POCL_FILENAME_LENGTH,
     "%s/%s_wg.s", tmpdir, kernel->name);
  assert (error >= 0);

  if (pocl_get_bool_option("POCL_BUILDING", 0)) {
    error = snprintf(start_asmfile, POCL_FILENAME_LENGTH, "%s/lib/CL/devices/nuplus/_start.s",
        SRCDIR);
  } else {
    error = snprintf(start_asmfile, POCL_FILENAME_LENGTH, "%s/_start.s",
        PKGDATADIR);
  }

  error = snprintf
    (start_objfile, POCL_FILENAME_LENGTH, "%s/_start.o", tmpdir);
  assert (error >= 0);

  if (access (module, F_OK) != 0)
    {
      error = snprintf (bytecode, POCL_FILENAME_LENGTH,
                        "%s/%s", tmpdir, POCL_PARALLEL_BC_FILENAME);
      assert (error >= 0);

      if (pocl_verbose) {
        fprintf(stderr, "[pocl] generating assembly: %s -> %s\n", bytecode, asmfile);
        fflush(stderr);
      }

      error = pocl_llvm_codegen(kernel, device, bytecode, asmfile);
      assert (error == 0);

      // assemble _start.s
      if (pocl_verbose) {
        fprintf(stderr, "[pocl] assembling files: %s -> %s\n", start_asmfile, start_objfile);
        fflush(stderr);
      }
      error = snprintf (command, COMMAND_LENGTH,
            assembly_cmd, start_objfile);
      assert (error >= 0);

      error = snprintf(command2, COMMAND_LENGTH, start_sed, kernel->name,
          start_asmfile, command);

      if (pocl_verbose) {
        fprintf(stderr, "[pocl] executing [%s]\n", command2);
        fflush(stderr);
      }
      error = system (command2);
      assert (error == 0);

      // link the object files into one object file
      error = snprintf (command, COMMAND_LENGTH,
            "/usr/local/llvm-nuplus/bin/ld.mcld -o %s -L%s %s %s %s/libcompiler.a %s/libc.a --script=%s/lnkrscrpt.ld",
            module, NUPLUS_LIBS_PATH, start_objfile, objfile, NUPLUS_LIBC_PATH, NUPLUS_LIBCOMP_PATH, NUPLUS_MISC_PATH);
      assert (error >= 0);

      if (pocl_verbose) {
        fprintf(stderr, "[pocl] executing [%s]\n", command);
        fflush(stderr);
      }
      error = system (command);
      assert (error == 0);

      /* Save space in kernel cache */
      if (!pocl_get_bool_option("POCL_LEAVE_KERNEL_COMPILER_TEMP_FILES", 0))
        {
          pocl_remove(objfile);
          pocl_remove(asmfile);
          pocl_remove(bytecode);
        }
    }
  return module;
}

/**
 * Link an elf file against a memory map and create binary file that can be
 * uploaded to the target.
 */
const char*
nuplus_link (const char* tmpdir, const char* objfile, size_t start_address, cl_kernel kernel) {
  int error;

  int pocl_verbose = pocl_get_bool_option("POCL_VERBOSE", 0);

  char command[COMMAND_LENGTH];
  char elffile[POCL_FILENAME_LENGTH];

  char* module = (char*) malloc(POCL_FILENAME_LENGTH); // strlen of / .so 4+1

  error = snprintf (module, POCL_FILENAME_LENGTH, "%s/%s_%zu.bin", tmpdir,
          kernel->name, start_address);
  assert (error >= 0);

  error = snprintf (elffile, POCL_FILENAME_LENGTH, "%s/%s_%zu.elf", tmpdir,
          kernel->name, start_address);
  assert (error >= 0);

  if (access (module, F_OK) != 0) {
      /* relocate the obj file at the correct location */
      error = snprintf (command, COMMAND_LENGTH,
        "/usr/local/llvm-nuplus/bin/ld.mcld -Ttext=%zx -o %s -L%s %s %s/libcompiler.a %s/libc.a --script=%s/lnkrscrpt.ld",
        start_address, elffile, NUPLUS_LIBS_PATH, objfile, NUPLUS_LIBC_PATH, NUPLUS_LIBCOMP_PATH, NUPLUS_MISC_PATH);
      assert (error >= 0);

      if (pocl_verbose) {
        fprintf(stderr, "[pocl] executing [%s]\n", command);
        fflush(stderr);
      }
      error = system (command);
      assert (error == 0);

      /* create a binary file */
      error = snprintf (command, COMMAND_LENGTH, "/usr/local/llvm-nuplus/bin/elf2hex -o %s %s",
      module, elffile);
      assert (error >= 0);

      if (pocl_verbose) {
        fprintf(stderr, "[pocl] executing [%s]\n", command);
        fflush(stderr);
      }
      error = system (command);
      assert (error == 0);

      /* Save space in kernel cache */
      if (!pocl_get_bool_option("POCL_LEAVE_KERNEL_COMPILER_TEMP_FILES", 0)) {
          pocl_remove(elffile);
      }
  }

  return module;
}

/* vim: set ts=4 expandtab: */

	.section .text

    # reserve space for pointers to argument 1 and 2
_p_arg0:
    .skip 4, 0
_p_arg1:
    .skip 4, 0

	.proc
_start::
	# Set the start address of the stack 
	load_cr s2, 2         # get my thread ID
	leah  s0, stack_table
	leal  s0, stack_table
	shli  s3, s2, 2
	add_i32 s0, s0, s3
	load32 sp, (s0)
	move_32 fp, sp
	#load the two input arguments
	leah  s0, _p_arg0
	leal  s0, _p_arg0

	load32 s0, (s0)

	leah  s1, _p_arg1
	leal  s1, _p_arg1

	load32 s1, (s1)
	
	jmpsr _:KERN_FUNC:_workgroup_fast
	
__END:
	end
__LOOP:
	jmp __LOOP

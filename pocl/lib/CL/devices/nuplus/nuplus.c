/* nuplus.c - a minimalistic pocl device driver layer implementation

   Copyright (c) 2011-2013 Universidad Rey Juan Carlos and
                 2011-2014 Pekka Jääskeläinen / Tampere University of Technology
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/

#include "nuplusconf.h"
#include "nuplus_compile.h"
#include "nuplus_driver.h"
#include <sys/stat.h>
#include <fcntl.h>

#include "bufalloc.h"
#include "config.h"
#include "nuplus.h"
#include "cpuinfo.h"
#include "topology/pocl_topology.h"
#include "common.h"
#include "utlist.h"
#include "devices.h"
#include "pocl_util.h"

#include <pthread.h>
#include <utlist.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

#include <unistd.h>

#include "pocl_cache.h"
#include "pocl_timing.h"
#include "pocl_file_util.h"

#ifdef OCS_AVAILABLE
#include "pocl_llvm.h"
#endif

#define max(a,b) (((a) > (b)) ? (a) : (b))

#define COMMAND_LENGTH 2048
#define WORKGROUP_STRING_LENGTH 1024

struct data {
  nuplus_dev *nupdev;
  /* Currently loaded kernel. */
  cl_kernel current_kernel;
  /* Loaded kernel dynamic library handle. */
  lt_dlhandle current_dlhandle;
  
  /* List of commands ready to be executed */
  _cl_command_node * volatile ready_list;
  /* List of commands not yet ready to be executed */
  _cl_command_node * volatile command_list;
  pocl_lock_t cq_lock;      /* Lock for command list related operations */
  /* Memory allocation bookkeeping information */
  struct memory_region memory;
};

static const cl_image_format supported_image_formats[] = {
    { CL_R, CL_SNORM_INT8 },
    { CL_R, CL_SNORM_INT16 },
    { CL_R, CL_UNORM_INT8 },
    { CL_R, CL_UNORM_INT16 },
    { CL_R, CL_UNORM_SHORT_565 }, 
    { CL_R, CL_UNORM_SHORT_555 },
    { CL_R, CL_UNORM_INT_101010 }, 
    { CL_R, CL_SIGNED_INT8 },
    { CL_R, CL_SIGNED_INT16 }, 
    { CL_R, CL_SIGNED_INT32 },
    { CL_R, CL_UNSIGNED_INT8 }, 
    { CL_R, CL_UNSIGNED_INT16 },
    { CL_R, CL_UNSIGNED_INT32 }, 
    { CL_R, CL_HALF_FLOAT },
    { CL_R, CL_FLOAT },
    { CL_Rx, CL_SNORM_INT8 },
    { CL_Rx, CL_SNORM_INT16 },
    { CL_Rx, CL_UNORM_INT8 },
    { CL_Rx, CL_UNORM_INT16 },
    { CL_Rx, CL_UNORM_SHORT_565 }, 
    { CL_Rx, CL_UNORM_SHORT_555 },
    { CL_Rx, CL_UNORM_INT_101010 }, 
    { CL_Rx, CL_SIGNED_INT8 },
    { CL_Rx, CL_SIGNED_INT16 }, 
    { CL_Rx, CL_SIGNED_INT32 },
    { CL_Rx, CL_UNSIGNED_INT8 }, 
    { CL_Rx, CL_UNSIGNED_INT16 },
    { CL_Rx, CL_UNSIGNED_INT32 }, 
    { CL_Rx, CL_HALF_FLOAT },
    { CL_Rx, CL_FLOAT },
    { CL_A, CL_SNORM_INT8 },
    { CL_A, CL_SNORM_INT16 },
    { CL_A, CL_UNORM_INT8 },
    { CL_A, CL_UNORM_INT16 },
    { CL_A, CL_UNORM_SHORT_565 }, 
    { CL_A, CL_UNORM_SHORT_555 },
    { CL_A, CL_UNORM_INT_101010 }, 
    { CL_A, CL_SIGNED_INT8 },
    { CL_A, CL_SIGNED_INT16 }, 
    { CL_A, CL_SIGNED_INT32 },
    { CL_A, CL_UNSIGNED_INT8 }, 
    { CL_A, CL_UNSIGNED_INT16 },
    { CL_A, CL_UNSIGNED_INT32 }, 
    { CL_A, CL_HALF_FLOAT },
    { CL_A, CL_FLOAT },
    { CL_RG, CL_SNORM_INT8 },
    { CL_RG, CL_SNORM_INT16 },
    { CL_RG, CL_UNORM_INT8 },
    { CL_RG, CL_UNORM_INT16 },
    { CL_RG, CL_UNORM_SHORT_565 }, 
    { CL_RG, CL_UNORM_SHORT_555 },
    { CL_RG, CL_UNORM_INT_101010 }, 
    { CL_RG, CL_SIGNED_INT8 },
    { CL_RG, CL_SIGNED_INT16 }, 
    { CL_RG, CL_SIGNED_INT32 },
    { CL_RG, CL_UNSIGNED_INT8 }, 
    { CL_RG, CL_UNSIGNED_INT16 },
    { CL_RG, CL_UNSIGNED_INT32 }, 
    { CL_RG, CL_HALF_FLOAT },
    { CL_RG, CL_FLOAT },
    { CL_RGx, CL_SNORM_INT8 },
    { CL_RGx, CL_SNORM_INT16 },
    { CL_RGx, CL_UNORM_INT8 },
    { CL_RGx, CL_UNORM_INT16 },
    { CL_RGx, CL_UNORM_SHORT_565 }, 
    { CL_RGx, CL_UNORM_SHORT_555 },
    { CL_RGx, CL_UNORM_INT_101010 }, 
    { CL_RGx, CL_SIGNED_INT8 },
    { CL_RGx, CL_SIGNED_INT16 }, 
    { CL_RGx, CL_SIGNED_INT32 },
    { CL_RGx, CL_UNSIGNED_INT8 }, 
    { CL_RGx, CL_UNSIGNED_INT16 },
    { CL_RGx, CL_UNSIGNED_INT32 }, 
    { CL_RGx, CL_HALF_FLOAT },
    { CL_RGx, CL_FLOAT },
    { CL_RA, CL_SNORM_INT8 },
    { CL_RA, CL_SNORM_INT16 },
    { CL_RA, CL_UNORM_INT8 },
    { CL_RA, CL_UNORM_INT16 },
    { CL_RA, CL_UNORM_SHORT_565 }, 
    { CL_RA, CL_UNORM_SHORT_555 },
    { CL_RA, CL_UNORM_INT_101010 }, 
    { CL_RA, CL_SIGNED_INT8 },
    { CL_RA, CL_SIGNED_INT16 }, 
    { CL_RA, CL_SIGNED_INT32 },
    { CL_RA, CL_UNSIGNED_INT8 }, 
    { CL_RA, CL_UNSIGNED_INT16 },
    { CL_RA, CL_UNSIGNED_INT32 }, 
    { CL_RA, CL_HALF_FLOAT },
    { CL_RA, CL_FLOAT },
    { CL_RGBA, CL_SNORM_INT8 },
    { CL_RGBA, CL_SNORM_INT16 },
    { CL_RGBA, CL_UNORM_INT8 },
    { CL_RGBA, CL_UNORM_INT16 },
    { CL_RGBA, CL_UNORM_SHORT_565 }, 
    { CL_RGBA, CL_UNORM_SHORT_555 },
    { CL_RGBA, CL_UNORM_INT_101010 }, 
    { CL_RGBA, CL_SIGNED_INT8 },
    { CL_RGBA, CL_SIGNED_INT16 }, 
    { CL_RGBA, CL_SIGNED_INT32 },
    { CL_RGBA, CL_UNSIGNED_INT8 }, 
    { CL_RGBA, CL_UNSIGNED_INT16 },
    { CL_RGBA, CL_UNSIGNED_INT32 }, 
    { CL_RGBA, CL_HALF_FLOAT },
    { CL_RGBA, CL_FLOAT },
    { CL_INTENSITY, CL_UNORM_INT8 }, 
    { CL_INTENSITY, CL_UNORM_INT16 }, 
    { CL_INTENSITY, CL_SNORM_INT8 }, 
    { CL_INTENSITY, CL_SNORM_INT16 }, 
    { CL_INTENSITY, CL_HALF_FLOAT }, 
    { CL_INTENSITY, CL_FLOAT },
    { CL_LUMINANCE, CL_UNORM_INT8 }, 
    { CL_LUMINANCE, CL_UNORM_INT16 }, 
    { CL_LUMINANCE, CL_SNORM_INT8 }, 
    { CL_LUMINANCE, CL_SNORM_INT16 }, 
    { CL_LUMINANCE, CL_HALF_FLOAT }, 
    { CL_LUMINANCE, CL_FLOAT },
    { CL_RGB, CL_UNORM_SHORT_565 }, 
    { CL_RGB, CL_UNORM_SHORT_555 },
    { CL_RGB, CL_UNORM_INT_101010 }, 
    { CL_RGBx, CL_UNORM_SHORT_565 }, 
    { CL_RGBx, CL_UNORM_SHORT_555 },
    { CL_RGBx, CL_UNORM_INT_101010 }, 
    { CL_ARGB, CL_SNORM_INT8 },
    { CL_ARGB, CL_UNORM_INT8 },
    { CL_ARGB, CL_SIGNED_INT8 },
    { CL_ARGB, CL_UNSIGNED_INT8 }, 
    { CL_BGRA, CL_SNORM_INT8 },
    { CL_BGRA, CL_UNORM_INT8 },
    { CL_BGRA, CL_SIGNED_INT8 },
    { CL_BGRA, CL_UNSIGNED_INT8 }
 };


void
pocl_nuplus_init_device_ops(struct pocl_device_ops *ops)
{
  ops->device_name = "nuplus";

  ops->init_device_infos = pocl_nuplus_init_device_infos;
  ops->probe = pocl_nuplus_probe;
  ops->uninit = pocl_nuplus_uninit;
  ops->init = pocl_nuplus_init;
  ops->alloc_mem_obj = pocl_nuplus_alloc_mem_obj;
  ops->free = pocl_nuplus_free;
  ops->free_ptr = pocl_nuplus_free_ptr;
  ops->read = pocl_nuplus_read;
  ops->read_rect = pocl_nuplus_read_rect;
  ops->write = pocl_nuplus_write;
  ops->write_rect = pocl_nuplus_write_rect;
  ops->copy = pocl_nuplus_copy;
  ops->copy_rect = pocl_nuplus_copy_rect;
  ops->fill_rect = pocl_nuplus_fill_rect;
  //ops->memfill = pocl_nuplus_memfill; not implemented in other devices
  ops->map_mem = pocl_nuplus_map_mem;
  ops->compile_kernel = pocl_nuplus_compile_kernel;
  ops->unmap_mem = pocl_nuplus_unmap_mem;
  ops->run = pocl_nuplus_run;
  ops->run_native = pocl_nuplus_run_native;
  ops->get_timer_value = pocl_nuplus_get_timer_value;
  //ops->get_supported_image_formats = pocl_nuplus_get_supported_image_formats; images not supported
  ops->join = pocl_nuplus_join;
  ops->submit = pocl_nuplus_submit;
  ops->broadcast = pocl_nuplus_broadcast;
  ops->notify = pocl_nuplus_notify;
  ops->flush = pocl_nuplus_flush;
  ops->build_hash = pocl_nuplus_build_hash;
  ops->init_build = pocl_nuplus_init_build;
}

char *
pocl_nuplus_build_hash (cl_device_id device)
{
  char* res = calloc(1000, sizeof(char));
  snprintf(res, 1000, "nuplus-manycore-system");
  return res;
}

void
pocl_nuplus_init_device_infos(struct _cl_device_id* dev)
{
  dev->type = CL_DEVICE_TYPE_ACCELERATOR;
  dev->vendor_id = 0;
  // compute units = nu+ cores
  // TODO: get them from the device itself
  dev->max_compute_units = NUPLUS_MAX_COMPUTE_UNITS;
  dev->max_work_item_dimensions = NUPLUS_MAX_ITEM_DIM;

  size_t long_name_size = strlen("nuplus manycore system") + 1;
  char* long_name = malloc(long_name_size);
  snprintf(long_name, long_name_size, "nuplus manycore system");

  dev->short_name = dev->long_name = long_name;
  SETUP_DEVICE_CL_VERSION(NUPLUS_OCL_MAJ_VER, NUPLUS_OCL_MIN_VER)
  /*
    The hard restriction will be the context data which is
    stored in stack that can be as small as 8K in Linux.
    Thus, there should be enough work-items alive to fill up
    the SIMD lanes times the vector units, but not more than
    that to avoid stack overflow and cache trashing.
  */
  dev->max_work_item_sizes[0] = dev->max_work_item_sizes[1] = 
  dev->max_work_group_size = 1024*4;

  dev->preferred_wg_size_multiple = 8;
  dev->preferred_vector_width_char = POCL_DEVICES_PREFERRED_VECTOR_WIDTH_CHAR;
  dev->preferred_vector_width_short = POCL_DEVICES_PREFERRED_VECTOR_WIDTH_SHORT;
  dev->preferred_vector_width_int = POCL_DEVICES_PREFERRED_VECTOR_WIDTH_INT;
  dev->preferred_vector_width_long = POCL_DEVICES_PREFERRED_VECTOR_WIDTH_LONG;
  dev->preferred_vector_width_float = POCL_DEVICES_PREFERRED_VECTOR_WIDTH_FLOAT;
  dev->preferred_vector_width_double = POCL_DEVICES_PREFERRED_VECTOR_WIDTH_DOUBLE;
  dev->preferred_vector_width_half = POCL_DEVICES_PREFERRED_VECTOR_WIDTH_HALF;
  /* TODO: figure out what the difference between preferred and native widths are */
  dev->native_vector_width_char = POCL_DEVICES_PREFERRED_VECTOR_WIDTH_CHAR;
  dev->native_vector_width_short = POCL_DEVICES_PREFERRED_VECTOR_WIDTH_SHORT;
  dev->native_vector_width_int = POCL_DEVICES_PREFERRED_VECTOR_WIDTH_INT;
  dev->native_vector_width_long = POCL_DEVICES_PREFERRED_VECTOR_WIDTH_LONG;
  dev->native_vector_width_float = POCL_DEVICES_PREFERRED_VECTOR_WIDTH_FLOAT;
  dev->native_vector_width_double = POCL_DEVICES_PREFERRED_VECTOR_WIDTH_DOUBLE;
  dev->native_vector_width_half = POCL_DEVICES_PREFERRED_VECTOR_WIDTH_HALF;
  dev->max_clock_frequency = 0;
  dev->address_bits = NUPLUS_ADDRESS_BITS;
  dev->image_support = CL_FALSE;
  /* Use the minimum values until we get a more sensible upper limit from
     somewhere. */
  dev->max_read_image_args = dev->max_write_image_args = dev->max_read_write_image_args = 128;
  dev->image2d_max_width = dev->image2d_max_height = 8192;
  dev->image3d_max_width = dev->image3d_max_height = dev->image3d_max_depth = 2048;
  dev->image_max_buffer_size = 0;
  dev->image_max_array_size = 0;
  dev->max_samplers = 16;
  dev->max_constant_args = 8;

  dev->max_mem_alloc_size = 0;
  dev->max_parameter_size = 1024;
  dev->min_data_type_align_size = MAX_EXTENDED_ALIGNMENT; // this is in bytes
  dev->mem_base_addr_align = MAX_EXTENDED_ALIGNMENT*8; // this is in bits
  dev->half_fp_config = 0;
  dev->single_fp_config = CL_FP_ROUND_TO_NEAREST | CL_FP_INF_NAN;
  dev->double_fp_config = CL_FP_ROUND_TO_NEAREST | CL_FP_INF_NAN;

  dev->global_mem_cache_type = CL_NONE;
  dev->global_mem_cacheline_size = NUPLUS_CACHELINE_SIZE;
  dev->global_mem_cache_size = NUPLUS_CACHE_SIZE;
  dev->global_mem_size = NUPLUS_GLOBAL_MEM_SIZE;

  dev->max_constant_buffer_size = NUPLUS_CONSTANT_BUFFER_SIZE;

  dev->max_constant_args = NUPLUS_CONSTANT_ARGS;

  dev->local_mem_type = CL_GLOBAL;
  dev->local_mem_size = NUPLUS_LOCAL_MEM_SIZE;

  dev->error_correction_support = CL_FALSE;
  dev->host_unified_memory = CL_FALSE;

  dev->profiling_timer_resolution = 0;

  dev->endian_little = CL_TRUE;
  dev->available = CL_TRUE;
  dev->compiler_available = CL_TRUE;
  dev->spmd = CL_FALSE;
  dev->execution_capabilities = CL_EXEC_KERNEL | CL_EXEC_NATIVE_KERNEL;
  dev->platform = 0;

  dev->parent_device = NULL;
  // nuplus does not support partitioning
  dev->max_sub_devices = 1;
  dev->num_partition_properties = 1;
  dev->partition_properties = calloc(dev->num_partition_properties,
    sizeof(cl_device_partition_property));
  dev->num_partition_types = 0;
  dev->partition_type = NULL;

  /* printf buffer size is meaningless for pocl, so just set it to
   * the minimum value required by the spec
   */
  dev->printf_buffer_size = 1024*1024 ;
  dev->vendor = "SecLab";
  dev->profile = "FULL_PROFILE";
  /* Note: The specification describes identifiers being delimited by
     only a single space character. Some programs that check the device's
     extension  string assume this rule. Future extension additions should
     ensure that there is no more than a single space between
     identifiers. */

  dev->global_as_id = dev->local_as_id = dev->constant_as_id = 0;

  dev->should_allocate_svm = 0;
  /* OpenCL 2.0 properties */
  dev->svm_caps = 0;
  /* TODO these are minimums, figure out whats a reasonable value */
  dev->max_events = 1024;
  dev->max_queues = 1;
  dev->max_pipe_args = 16;
  dev->max_pipe_active_res = 1;
  dev->max_pipe_packet_size = 1024;
  dev->dev_queue_pref_size = 16 * 1024;
  dev->dev_queue_max_size = 256 * 1024;
  dev->on_dev_queue_props = CL_QUEUE_PROFILING_ENABLE;
  dev->on_host_queue_props = CL_QUEUE_PROFILING_ENABLE;


  dev->extensions = "";
  dev->llvm_cpu = "nuplus";
  dev->llvm_target_triplet = "nuplus";

  dev->has_64bit_long = 1;
  dev->autolocals_to_args = 1;
}

char *pocl_nuplus_init_build(void *data)
{
  static const char *conf_str = "-O3 -I%s%s -Wall -W";
  static const char *base_dir;
  static const char *path;
  int len;

  base_dir = SRCDIR;
  path = "/../libs/libc/include";

  len = snprintf(NULL, 0, conf_str, base_dir, path);

  char *ret = malloc(len+1);
  snprintf(ret, len+1, conf_str, base_dir, path);

  return ret;
}

unsigned int
pocl_nuplus_probe(struct pocl_device_ops *ops)
{
  int env_count = pocl_device_get_env_count(ops->device_name);

  /* No env specified, so pthread will be used instead of nuplus */
  if(env_count < 0)
    return 0;

  size_t devs_num;
  nuplus_dev *devs;
  get_nuplus_devices(&devs_num, devs, NUPLUS_BAUD_RATE);
  for(int i = 0; i < devs_num; i++)
  {
    destroy_device(&(devs[i]));
  }

  return devs_num;
}

// Copies only the values for communication
void copy_nuplus_dev (nuplus_dev *dst, nuplus_dev *src) {
  strcpy(dst->port_name, src->port_name);
  dst->baud_rate = src->baud_rate; 
  dst->serial_fd = src->serial_fd; 
  dst->address = src->address;

  // Manycore structures uninitialized 
  dst->num_cores = 0; 
  dst->cores = NULL;
}

void
pocl_nuplus_init (cl_device_id device, const char* parameters)
{
  struct data *d;
  static int global_mem_id;
  static int first_nuplus_init = 1;
  static int device_number = 0;
  
  if (first_nuplus_init)
    {
      //pocl_init_dlhandle_cache();
      first_nuplus_init = 0;
      global_mem_id = device->dev_id;
    }
  device->global_mem_id = global_mem_id;
  
  d = (struct data *) calloc (1, sizeof (struct data));
  
  init_mem_region(&d->memory, 0, device->global_mem_size);
  
  d->current_kernel = NULL;
  d->current_dlhandle = 0; //?
  device->data = d;

  // Get the device with global_mem_id id
  size_t devs_num;
  nuplus_dev *devs;
  get_nuplus_devices(&devs_num, devs, NUPLUS_BAUD_RATE);

  d->nupdev = (nuplus_dev *) calloc (1, sizeof(nuplus_dev));
  copy_nuplus_dev(d->nupdev, &(devs[global_mem_id]));

  for(int i = 0; i < devs_num; i++)
  {
    destroy_device(&(devs[i]));
  }
  
  free(devs);

  /* The nuplus driver represents only one "compute unit" as
     it doesn't exploit multiple hardware threads. Multiple
     nuplus devices can be still used for task level parallelism 
     using multiple OpenCL devices. */
  device->max_compute_units = NUPLUS_MAX_COMPUTE_UNITS;

}

cl_int
pocl_nuplus_alloc_mem_obj (cl_device_id device, cl_mem mem_obj, void* host_ptr)
{
  void *b = NULL;
  cl_mem_flags flags = mem_obj->flags;
  unsigned i;
  POCL_MSG_PRINT_INFO("nuplus: alloc_mem_obj, mem %p, dev %d\n", 
                      mem_obj, device->global_mem_id);

  struct data* data = (struct data*)device->data;
  pocl_mem_identifier *dev_ptrs = &mem_obj->device_ptrs[device->global_mem_id];


  assert(! (flags & CL_MEM_USE_HOST_PTR && flags & CL_MEM_COPY_HOST_PTR)
    && "USE_HOST_PTR and COPY_HOST_PTR are both set");

  assert(! (mem_obj->mem_host_ptr == NULL
    && (flags & CL_MEM_USE_HOST_PTR || flags & CL_MEM_COPY_HOST_PTR))
    && "mem_host_ptr should not be NULL");

  /* if memory for this global memory is not yet allocated -> do it */
  if (dev_ptrs->mem_ptr == NULL) {
    chunk_info_t *chunk = alloc_buffer(&data->memory, mem_obj->size);
    if(chunk == NULL) {
      return CL_MEM_OBJECT_ALLOCATION_FAILURE;
    }

    dev_ptrs->mem_ptr = chunk;
    dev_ptrs->global_mem_id = device->global_mem_id;

    if (flags & CL_MEM_USE_HOST_PTR || flags & CL_MEM_COPY_HOST_PTR) {
      pocl_nuplus_write(data, host_ptr, chunk, 0, mem_obj->size); 
          
    }
  }
  /* copy already allocated global mem info to devices own slot */
  mem_obj->device_ptrs[device->dev_id] = mem_obj->device_ptrs[device->global_mem_id];

  return CL_SUCCESS;
}


void
pocl_nuplus_free (cl_device_id device, cl_mem memobj)
{
  cl_mem_flags flags = memobj->flags;

  /* aloocation owner executes freeing */
  if (flags & CL_MEM_USE_HOST_PTR ||
      memobj->shared_mem_allocation_owner != device)
    return;

  void* ptr = memobj->device_ptrs[device->dev_id].mem_ptr;
  size_t size = memobj->size;

  if (memobj->flags | CL_MEM_ALLOC_HOST_PTR)
    memobj->mem_host_ptr = NULL;
  free_chunk((chunk_info_t *)ptr);
}

void pocl_nuplus_free_ptr (cl_device_id device, void* mem_ptr)
{
  /* TODO: check if it works */
  free_chunk((chunk_info_t *)mem_ptr);  
}

void
pocl_nuplus_read (void *data, void *host_ptr, const void *device_ptr, 
                 size_t offset, size_t cb)
{
  struct data *d = (struct data*) data;
  chunk_info_t *chunk = (chunk_info_t*)device_ptr;
  size_t bytes_left = cb;
  struct timespec tw1, tw2;

  if (pocl_get_bool_option("POCL_VERBOSE", 0)) {
    fprintf(stderr, "[pocl] reading %zu bytes from 0x%zx+0x%zx to %p\n", cb,
        chunk->start_address, offset, host_ptr);
    fflush(stderr);

    clock_gettime(CLOCK_MONOTONIC, &tw1);
  }

  int err = nuplus_open(d->nupdev);
  if (err < 0) {
    fprintf(stderr, "Couldn't open nuplus_dev for reading!\n");
    return;
  }

  off_t newoff = nuplus_lseek(d->nupdev, chunk->start_address + offset, SEEK_SET);
  if (newoff == (off_t)-1) {
    nuplus_close(d->nupdev);
    return;
  }

  while (bytes_left > 0) {
    ssize_t read_bytes = nuplus_read(d->nupdev, host_ptr, bytes_left);

    if (read_bytes < 0) {
      fprintf(stderr, "Reading from device failed (addr: 0x%zx, size: %zu)!\n",
          chunk->start_address + offset, bytes_left);
      nuplus_close(d->nupdev);
      return;
    }

    bytes_left -= read_bytes;
    host_ptr += read_bytes;
    offset += read_bytes;
  }
  nuplus_close(d->nupdev);

  if (pocl_get_bool_option("POCL_VERBOSE", 0)) {
    struct tw2;
    double walltime;

    clock_gettime(CLOCK_MONOTONIC, &tw2);
    walltime = 1000.0*tw2.tv_sec + 1e-6*tw2.tv_nsec -
      (1000.0*tw1.tv_sec + 1e-6*tw1.tv_nsec);

    printf("[pocl] Reading %zu bytes took %.2f ms, speed: %.2f MB/s\n", cb,
        walltime, cb / walltime / 1000);
  } 
}

void
pocl_nuplus_write (void *data, const void *host_ptr, void *device_ptr, 
                  size_t offset, size_t cb)
{
  struct data *d = (struct data*) data;
  chunk_info_t *chunk = (chunk_info_t*)device_ptr;
  size_t bytes_left = cb;
  struct timespec tw1, tw2;

  if (pocl_get_bool_option("POCL_VERBOSE", 0)) {
    fprintf(stderr, "[pocl] writing %zu bytes to 0x%zx+0x%zx\n", cb,
        chunk->start_address, offset);
    fflush(stderr);
    uint32_t const *w = host_ptr;

    clock_gettime(CLOCK_MONOTONIC, &tw1);
  }

  int err = nuplus_open(d->nupdev); 
  if (err < 0) {
    fprintf(stderr, "Couldn't open nuplus_dev for writing!\n");
    return;
  }

  off_t newoff = nuplus_lseek(d->nupdev, chunk->start_address + offset, SEEK_SET);
  if (newoff == (off_t)-1) {
    nuplus_close(d->nupdev);
    return;
  }

  while (bytes_left > 0) {
    ssize_t written_bytes = nuplus_write(d->nupdev, host_ptr, bytes_left);

    if (written_bytes < 0) {
      fprintf(stderr, "Writing to device failed (addr: 0x%zx, size: %zu)!\n",
          chunk->start_address + offset, bytes_left);
      nuplus_close(d->nupdev);
      return;
    }

    bytes_left -= written_bytes;
    host_ptr += written_bytes;
    offset += written_bytes;
  }
  nuplus_close(d->nupdev);

  if (pocl_get_bool_option("POCL_VERBOSE", 0)) {
    struct tw2;
    double walltime;

    clock_gettime(CLOCK_MONOTONIC, &tw2);
    walltime = 1000.0*tw2.tv_sec + 1e-6*tw2.tv_nsec -
      (1000.0*tw1.tv_sec + 1e-6*tw1.tv_nsec);

    printf("[pocl] Writing %zu bytes took %.2f ms, speed: %.2f MB/s\n", cb,
        walltime, cb / walltime / 1000);
  }
}

static inline size_t align_size(size_t s, size_t psize) {
  return (s + psize - 1) / psize * psize;
}


// RUN
void pocl_nuplus_run (void *data, _cl_command_node* cmd)
{
  struct data *d;
  struct pocl_argument *al;
  size_t x, y, z;
  unsigned i;
  cl_kernel kernel = cmd->command.run.kernel;
  struct pocl_context *pc = &cmd->command.run.pc;

  assert (data != NULL);
  d = (struct data *) data;

  d->current_kernel = kernel;
 
  //TODO: check if needed alignement 
  const size_t psize = NUPLUS_ADDRESS_BITS/8;

  /* Determine the amount of memory needed for all argument data */
  size_t alloc_size = 0;
  /* The initial argument pointers */
  size_t arg_list_size = psize * (kernel->num_args + kernel->num_locals);
  alloc_size += arg_list_size;
/* Data needed for kernel arguments */
  for (i = 0; i < kernel->num_args; ++i)
  {
    al = &(cmd->command.run.arguments[i]);
    if (kernel->arg_info[i].is_local) {
        alloc_size += align_size(al->size, psize);
    }else {
      switch (kernel->arg_info[i].type) {
        case POCL_ARG_TYPE_POINTER:
          /* The buffer should either be local and handled above, or should
              already be transfered. */
          break;
        case POCL_ARG_TYPE_IMAGE:
          POCL_ABORT_UNIMPLEMENTED("pocl-nuplus: image arguments"
                                  " not implemented.\n");
          break;
        case POCL_ARG_TYPE_SAMPLER:
          POCL_ABORT_UNIMPLEMENTED("pocl-nuplus: sampler arguments"
                                  " not implemented.\n");
          break;
        case POCL_ARG_TYPE_NONE:
          alloc_size += align_size(al->size, psize);
          break;
      }
    }
  }
  
  for (i = kernel->num_args; i < kernel->num_locals; ++i)
    {
      al = &(cmd->command.run.arguments[i]);
      alloc_size += align_size(al->size, psize);
    }

  /* Allocate total argument data memory */
  uint8_t *arguments = malloc(alloc_size);
  chunk_info_t *args_alloc = alloc_buffer(&d->memory, alloc_size);

  /* allocate pocl context (second argument to wg) */
  chunk_info_t *ctxt_alloc = alloc_buffer(&d->memory, sizeof(pc));

  /* The arguments array is located at the beginning of the arg_data array */
  uint32_t *arg_list = (uint32_t*)&arguments[0];
  uint8_t *arg_data = &arguments[arg_list_size];
  uint32_t p_arg_data_dev = args_alloc->start_address + arg_list_size;


  /* Process the kernel arguments. Convert the opaque buffer
  pointers to real device pointers, allocate dynamic local 
  memory buffers, etc. */
  for (i = 0; i < kernel->num_args; ++i) {
    al = &(cmd->command.run.arguments[i]);

    if (kernel->arg_info[i].is_local) {
      arg_list[i] = htole32(p_arg_data_dev);
      p_arg_data_dev += align_size(al->size, psize);

    } else if (kernel->arg_info[i].type == POCL_ARG_TYPE_POINTER) {
      /* It's legal to pass a NULL pointer to clSetKernelArguments. In
          that case we must pass the same NULL forward to the kernel.
          Otherwise, the user must have created a buffer with per device
          pointers stored in the cl_mem. */
      if (al->value == NULL)
        arg_list[i] = 0;
      else {
        cl_mem_t *mem = *(cl_mem_t **) al->value;
        /* rVEX addresses are 32-bit */
        chunk_info_t *chunk = (chunk_info_t*) mem->device_ptrs[cmd->device->dev_id].mem_ptr;
        arg_list[i] = htole32((uint32_t)chunk->start_address);
      }

    } else if (kernel->arg_info[i].type == POCL_ARG_TYPE_IMAGE) {
          POCL_ABORT_UNIMPLEMENTED("pocl-nuplus: image arguments"
                                  " not implemented.\n");
    } else if (kernel->arg_info[i].type == POCL_ARG_TYPE_SAMPLER) {
          POCL_ABORT_UNIMPLEMENTED("pocl-nuplus: sampler arguments"
                                  " not implemented.\n");
    } else {
      memcpy(arg_data, al->value, al->size);
      arg_list[i] = p_arg_data_dev;

      arg_data += align_size(al->size, psize);
      p_arg_data_dev += align_size(al->size, psize);
    }
  }

  for (i = kernel->num_args; i < kernel->num_args + kernel->num_locals; ++i) {
    al = &(cmd->command.run.arguments[i]);

    arg_list[i] = p_arg_data_dev;
    p_arg_data_dev += align_size(al->size, psize);
  }
  
  /* Write argument data to nuplus */
  pocl_nuplus_write(data, arguments, args_alloc, 0, alloc_size);

  /* Reserve space for program and write program */
  /* TODO: determine memory size from elf file */
  chunk_info_t *prog_alloc = alloc_buffer(&d->memory, NUPLUS_ELF_DIM);

  char *binbuffer;

  const char *binfile = nuplus_link (cmd->command.run.tmp_dir,
      (const char*)cmd->command.run.wg, prog_alloc->start_address,
      cmd->command.run.kernel); 
  uint64_t fsize;
  int err = pocl_read_file(binfile, &binbuffer, &fsize);
  assert(fsize > 0);

  /* Write pointers to first and second argument of workgroup to data */
  *(uint32_t*)&binbuffer[0] = htole32(args_alloc->start_address);
  *(uint32_t*)&binbuffer[4] = htole32(ctxt_alloc->start_address);

  pocl_nuplus_write(data, binbuffer, prog_alloc, 0, fsize);

  free(binbuffer);

  if (!pocl_get_bool_option("POCL_LEAVE_KERNEL_COMPILER_TEMP_FILES", 0)) {
    pocl_remove(binfile);
  }
  free((void*)binfile);
  

  struct timespec tw1, tw2, tw3, tw4;
  uint32_t comm_bundles = 0, cycles = 0;
  uint32_t cstalled = 0;
  double walltime = 0.0;

  /*
  pc->local_size[0] = cmd->command.run.local_x;
  pc->local_size[1] = cmd->command.run.local_y;
  pc->local_size[2] = cmd->command.run.local_z;
  
  for (z = 0; z < pc->num_groups[2]; ++z)
  {
    for (y = 0; y < pc->num_groups[1]; ++y)
      {
        for (x = 0; x < pc->num_groups[0]; ++x)
          {
            pc->group_id[0] = x;
            pc->group_id[1] = y;
            pc->group_id[2] = z;

            // Assume that the host system is litle endian. True with Ubuntu 16.04 LTS with x86_64 arch
            pocl_nuplus_write(data, &pc, ctxt_alloc, 0, sizeof(pc));

            nuplus_dev_set_run(d->nupdev, false);
            nuplus_dev_set_reset_vector(d->nupdev, prog_alloc->start_address + 8);
            nuplus_dev_set_reset(d->nupdev, true);
            nuplus_dev_set_reset(d->nupdev, false);
            clock_gettime(CLOCK_MONOTONIC, &tw1);
            nuplus_dev_set_run(d->nupdev, true);

            while(!nuplus_dev_get_done(d->nupdev)) {
                //nanosleep(&(struct timespec){.tv_nsec=10000}, NULL);
            }
            clock_gettime(CLOCK_MONOTONIC, &tw2);
            walltime += 1000.0*tw2.tv_sec + 1e-6*tw2.tv_nsec
                      - (1000.0*tw1.tv_sec + 1e-6*tw1.tv_nsec);

            comm_bundles += nuplus_dev_get_cbun(d->nupdev);
            cycles += nuplus_dev_get_ccyc(d->nupdev);
            cstalled += nuplus_dev_get_cstall(d->nupdev);
          }
      }
  }
  */
  pc->local_size[0] = cmd->command.run.local_x;
  pc->local_size[1] = cmd->command.run.local_y;
  pc->local_size[2] = cmd->command.run.local_z;
  
  for (z = 0; z < pc->num_groups[2]; ++z)
  {
    for (y = 0; y < pc->num_groups[1]; ++y)
      {
        for (x = 0; x < pc->num_groups[0]; ++x)
          {
            pc->group_id[0] = x;
            pc->group_id[1] = y;
            pc->group_id[2] = z;
          }
      }
  }

  pocl_nuplus_write(data, &pc, ctxt_alloc, 0, sizeof(pc));
  
  d->nupdev->cores = (nuplus_core *) calloc (cmd->device->max_compute_units, sizeof(nuplus_core));

  for(int i = 0; i < cmd->device->max_compute_units; i++)
  {
    nuplus_thread *threads = (nuplus_thread *) calloc (NUPLUS_MAX_THREADS, sizeof(nuplus_thread));
    for(int j = 0; j < NUPLUS_MAX_THREADS; j++)
    {
      create_thread(&(threads[j]), prog_alloc->start_address + 8, j);
    }
    
    create_core(&(d->nupdev->cores[i]), threads, i, NUPLUS_MAX_THREADS);
  }
  


  for(int i = 0; i < cmd->device->max_compute_units; i++)
  {
    nuplus_core *core = &(d->nupdev->cores[i]);

    core->id = i;
    core->num_threads = NUPLUS_MAX_THREADS;

    core->threads = (nuplus_thread *) calloc (NUPLUS_MAX_THREADS, sizeof(nuplus_thread));
    for(int j = 0; j < NUPLUS_MAX_THREADS; j++)
    {
      nuplus_thread *thread = &(core->threads[j]);

      thread->pc = prog_alloc->start_address + 8;
      thread-> thread_id = j;
    }
  
  }

  // Open connection with the device
  nuplus_open(d->nupdev);
  // Perform boot operations
  nuplus_boot(d->nupdev);
  // Get start time
  clock_gettime(CLOCK_MONOTONIC, &tw1);
  // Start kernel execution
  nuplus_run(d->nupdev);
  // Wait untill kernel execution ends
  while(!nuplus_get_done(d->nupdev)) {
      //nanosleep(&(struct timespec){.tv_nsec=10000}, NULL);
  }
  // Close connection with the device
  nuplus_close(d->nupdev);

  // Get finish time
  clock_gettime(CLOCK_MONOTONIC, &tw2);
  walltime += 1000.0*tw2.tv_sec + 1e-6*tw2.tv_nsec
            - (1000.0*tw1.tv_sec + 1e-6*tw1.tv_nsec);

  
  if(pocl_get_bool_option("POCL_TIME_EXEC", 0)) {
    printf("%.2f\n", walltime);
  }

  for (i = 0; i < kernel->num_args; ++i)
  {
    if (kernel->arg_info[i].type == POCL_ARG_TYPE_POINTER) {
      al = &(cmd->command.run.arguments[i]);
      /* It is legal to pass NULL */
      if (al->value != NULL) {
        cl_mem_t *mem = *(cl_mem_t **) al->value;

        if(mem->flags & CL_MEM_USE_HOST_PTR) {
          chunk_info_t* chunk = (chunk_info_t*)mem->device_ptrs[cmd->device->dev_id].mem_ptr;
          pocl_nuplus_read(data, mem->mem_host_ptr, chunk, 0, mem->size);
        }
      }
    }
  }
  
    /* Free allocated memory */
  free_chunk(prog_alloc);
  free_chunk(ctxt_alloc);
  free_chunk(args_alloc);
  free(arguments);
}

void
pocl_nuplus_run_native 
(void *data, 
 _cl_command_node* cmd)
{ 
  // unimplemented
  //cmd->command.native.user_func(cmd->command.native.args);
}

void
pocl_nuplus_copy (void *data, const void *src_ptr, size_t src_offset, 
                 void *__restrict__ dst_ptr, size_t dst_offset, size_t cb)
{
  /* unimplemented
  if (src_ptr == dst_ptr)
    return;
  
  memcpy ((char*)dst_ptr + dst_offset, (char*)src_ptr + src_offset, cb);*/
}

void
pocl_nuplus_copy_rect (void *data,
                      const void *__restrict const src_ptr,
                      void *__restrict__ const dst_ptr,
                      const size_t *__restrict__ const src_origin,
                      const size_t *__restrict__ const dst_origin, 
                      const size_t *__restrict__ const region,
                      size_t const src_row_pitch,
                      size_t const src_slice_pitch,
                      size_t const dst_row_pitch,
                      size_t const dst_slice_pitch)
{
  /* unimplemented
  char const *__restrict const adjusted_src_ptr = 
    (char const*)src_ptr +
    src_origin[0] + src_row_pitch * src_origin[1] + src_slice_pitch * src_origin[2];
  char *__restrict__ const adjusted_dst_ptr = 
    (char*)dst_ptr +
    dst_origin[0] + dst_row_pitch * dst_origin[1] + dst_slice_pitch * dst_origin[2];
  
  size_t j, k;
  */
  /* TODO: handle overlaping regions */
  /*
  for (k = 0; k < region[2]; ++k)
    for (j = 0; j < region[1]; ++j)
      memcpy (adjusted_dst_ptr + dst_row_pitch * j + dst_slice_pitch * k,
              adjusted_src_ptr + src_row_pitch * j + src_slice_pitch * k,
              region[0]);*/
}

// write/read rect are used for images operations
void
pocl_nuplus_write_rect (void *data,
                       const void *__restrict__ const host_ptr,
                       void *__restrict__ const device_ptr,
                       const size_t *__restrict__ const buffer_origin,
                       const size_t *__restrict__ const host_origin, 
                       const size_t *__restrict__ const region,
                       size_t const buffer_row_pitch,
                       size_t const buffer_slice_pitch,
                       size_t const host_row_pitch,
                       size_t const host_slice_pitch)
{
  /* unimplemented
  char *__restrict const adjusted_device_ptr = 
    (char*)device_ptr +
    buffer_origin[0] + buffer_row_pitch * buffer_origin[1] + buffer_slice_pitch * buffer_origin[2];
  char const *__restrict__ const adjusted_host_ptr = 
    (char const*)host_ptr +
    host_origin[0] + host_row_pitch * host_origin[1] + host_slice_pitch * host_origin[2];
  
  size_t j, k;
  */
  /* TODO: handle overlaping regions */
  /*
  for (k = 0; k < region[2]; ++k)
    for (j = 0; j < region[1]; ++j)
      memcpy (adjusted_device_ptr + buffer_row_pitch * j + buffer_slice_pitch * k,
              adjusted_host_ptr + host_row_pitch * j + host_slice_pitch * k,
              region[0]);
              */
}

void
pocl_nuplus_read_rect (void *data,
                      void *__restrict__ const host_ptr,
                      void *__restrict__ const device_ptr,
                      const size_t *__restrict__ const buffer_origin,
                      const size_t *__restrict__ const host_origin, 
                      const size_t *__restrict__ const region,
                      size_t const buffer_row_pitch,
                      size_t const buffer_slice_pitch,
                      size_t const host_row_pitch,
                      size_t const host_slice_pitch)
{
    /* unimplemented

  char const *__restrict const adjusted_device_ptr = 
    (char const*)device_ptr +
    buffer_origin[2] * buffer_slice_pitch + buffer_origin[1] * buffer_row_pitch + buffer_origin[0];
  char *__restrict__ const adjusted_host_ptr = 
    (char*)host_ptr +
    host_origin[2] * host_slice_pitch + host_origin[1] * host_row_pitch + host_origin[0];
  
  size_t j, k;
  */
  /* TODO: handle overlaping regions */
  /*
  for (k = 0; k < region[2]; ++k)
    for (j = 0; j < region[1]; ++j)
      memcpy (adjusted_host_ptr + host_row_pitch * j + host_slice_pitch * k,
              adjusted_device_ptr + buffer_row_pitch * j + buffer_slice_pitch * k,
              region[0]);
              */
}

/* origin and region must be in original shape unlike in copy/read/write_rect()
 */
void
pocl_nuplus_fill_rect (void *data,
                      void *__restrict__ const device_ptr,
                      const size_t *__restrict__ const buffer_origin,
                      const size_t *__restrict__ const region,
                      size_t const buffer_row_pitch,
                      size_t const buffer_slice_pitch,
                      void *fill_pixel,
                      size_t pixel_size)                    
{  /* unimplemented  
  char *__restrict const adjusted_device_ptr = (char*)device_ptr 
    + buffer_origin[0] * pixel_size 
    + buffer_row_pitch * buffer_origin[1] 
    + buffer_slice_pitch * buffer_origin[2];
    
  size_t i, j, k;

  for (k = 0; k < region[2]; ++k)
    for (j = 0; j < region[1]; ++j)
      for (i = 0; i < region[0]; ++i)
        memcpy (adjusted_device_ptr + pixel_size * i 
                + buffer_row_pitch * j 
                + buffer_slice_pitch * k, fill_pixel, pixel_size);
                */
}

  /* unimplemented in other devices
void pocl_nuplus_memfill(void *ptr,
                        size_t size,
                        size_t offset,
                        const void* pattern,
                        size_t pattern_size)
{
  size_t i;
  unsigned j;

  /* memfill size is in bytes, we wanto make it into elements */
  /*
  size /= pattern_size;
  offset /= pattern_size;

  switch (pattern_size)
    {
    case 1:
      {
      uint8_t * p = (uint8_t*)ptr + offset;
      for (i = 0; i < size; i++)
        p[i] = *(uint8_t*)pattern;
      }
      break;
    case 2:
      {
      uint16_t * p = (uint16_t*)ptr + offset;
      for (i = 0; i < size; i++)
        p[i] = *(uint16_t*)pattern;
      }
      break;
    case 4:
      {
      uint32_t * p = (uint32_t*)ptr + offset;
      for (i = 0; i < size; i++)
        p[i] = *(uint32_t*)pattern;
      }
      break;
    case 8:
      {
      uint64_t * p = (uint64_t*)ptr + offset;
      for (i = 0; i < size; i++)
        p[i] = *(uint64_t*)pattern;
      }
      break;
    case 16:
      {
      uint64_t * p = (uint64_t*)ptr + offset;
      for (i = 0; i < size; i++)
        for (j = 0; j < 2; j++)
          p[(i<<1) + j] = *((uint64_t*)pattern + j);
      }
      break;
    case 32:
      {
      uint64_t * p = (uint64_t*)ptr + offset;
      for (i = 0; i < size; i++)
        for (j = 0; j < 4; j++)
          p[(i<<2) + j] = *((uint64_t*)pattern + j);
      }
      break;
    case 64:
      {
      uint64_t * p = (uint64_t*)ptr + offset;
      for (i = 0; i < size; i++)
        for (j = 0; j < 8; j++)
          p[(i<<3) + j] = *((uint64_t*)pattern + j);
      }
      break;
    case 128:
      {
      uint64_t * p = (uint64_t*)ptr + offset;
      for (i = 0; i < size; i++)
        for (j = 0; j < 16; j++)
          p[(i<<4) + j] = *((uint64_t*)pattern + j);
      }
      break;
    default:
      assert (0 && "Invalid pattern size");
      break;
    }
}
*/
void *
pocl_nuplus_map_mem (void *data, void *buf_ptr, 
                      size_t offset, size_t size,
                      void *host_ptr) 
{
  /* All global pointers of the pthread/CPU device are in 
     the host address space already, and up to date. */
  /* unimplemented

  if (host_ptr != NULL) return host_ptr;
  return (char*)buf_ptr + offset;
  */
  return NULL;
}

void* pocl_nuplus_unmap_mem(void *data, void *host_ptr,
                           void *device_start_ptr,
                           size_t offset, size_t size)
{
  return host_ptr;
}


void
pocl_nuplus_uninit (cl_device_id device)
{
  struct data *d = (struct data*)device->data;
  destroy_device(d->nupdev);
  POCL_MEM_FREE(d);
  device->data = NULL;
}

cl_ulong
pocl_nuplus_get_timer_value (void *data) 
{
  return pocl_gettimemono_ns();
}

/* images not supported
cl_int 
pocl_nuplus_get_supported_image_formats (cl_mem_flags flags,
                                        const cl_image_format **image_formats,
                                        cl_uint *num_img_formats)
{
    if (num_img_formats == NULL || image_formats == NULL)
      return CL_INVALID_VALUE;
  
    *num_img_formats = sizeof(supported_image_formats)/sizeof(cl_image_format);
    *image_formats = supported_image_formats;
    
    return CL_SUCCESS; 
}
*/
static void nuplus_command_scheduler (struct data *d) 
{
  _cl_command_node *node;
  
  /* execute commands from ready list */
  while ((node = d->ready_list))
    {
      assert (pocl_command_is_ready(node->event));
      CDL_DELETE (d->ready_list, node);

      
      pthread_mutex_unlock (&d->cq_lock);
      assert (node->event->status == CL_SUBMITTED);
      pocl_exec_command(node);
      pthread_mutex_lock (&d->cq_lock);
    }
    
  return;
}

void
pocl_nuplus_submit (_cl_command_node *node, cl_command_queue cq)
{
  struct data *d = node->device->data;
  cl_event *event = &(node->event);
  
  node->device->ops->compile_kernel (node, NULL, NULL);
  POCL_LOCK (d->cq_lock);
  POCL_UPDATE_EVENT_SUBMITTED(event);
  pocl_command_push(node, &d->ready_list, &d->command_list);
  
  nuplus_command_scheduler (d);

  POCL_UNLOCK (d->cq_lock);

  return;
}

void pocl_nuplus_flush (cl_device_id device, cl_command_queue cq)
{
  struct data *d = (struct data*)device->data;

  POCL_LOCK (d->cq_lock);
  nuplus_command_scheduler (d);
  POCL_UNLOCK (d->cq_lock);
}

static void
pocl_nuplus_push_command (_cl_command_node *node)
{
  struct data *d = (struct data*)node->device->data;

  pocl_command_push(node, &d->ready_list, &d->command_list);

}

void
pocl_nuplus_join(cl_device_id device, cl_command_queue cq)
{
  struct data *d = (struct data*)device->data;

  POCL_LOCK (d->cq_lock);
  nuplus_command_scheduler (d);
  POCL_UNLOCK (d->cq_lock);

  return;
}

void
pocl_nuplus_notify (cl_device_id device, cl_event event)
{
  struct data *d = (struct data*)device->data;
  _cl_command_node * volatile node = event->command;
  
  POCL_LOCK_OBJ (event);
  if (!(node->ready) && pocl_command_is_ready(node->event))
    {
      node->ready = 1;
      POCL_UNLOCK_OBJ (event);
      if (node->event->status == CL_SUBMITTED)
        {
          POCL_LOCK (d->cq_lock);
          CDL_DELETE (d->command_list, node);
          CDL_PREPEND (d->ready_list, node);
          nuplus_command_scheduler (d);
          POCL_UNLOCK (d->cq_lock);
        }
      return;
    }
  POCL_UNLOCK_OBJ (event);
}

void
pocl_nuplus_broadcast (cl_event event)
{
  pocl_broadcast (event);
}

typedef struct compiler_cache_item compiler_cache_item;
struct compiler_cache_item
{
  char *tmp_dir;
  char *function_name;
  pocl_workgroup wg;
  compiler_cache_item *next;
};

static compiler_cache_item *compiler_cache;
static pocl_lock_t compiler_cache_lock;

static void check_compiler_cache (_cl_command_node *cmd)
{
  char workgroup_string[WORKGROUP_STRING_LENGTH];
  lt_dlhandle dlhandle;
  compiler_cache_item *ci = NULL;
  
  if (compiler_cache == NULL)
    POCL_INIT_LOCK (compiler_cache_lock);

  POCL_LOCK (compiler_cache_lock);
  LL_FOREACH (compiler_cache, ci)
    {
      if (strcmp (ci->tmp_dir, cmd->command.run.tmp_dir) == 0 &&
          strcmp (ci->function_name, 
                  cmd->command.run.kernel->name) == 0)
        {
          POCL_UNLOCK (compiler_cache_lock);
          cmd->command.run.wg = ci->wg;
          return;
        }
    }
  ci = (compiler_cache_item*) malloc (sizeof (compiler_cache_item));
  ci->next = NULL;
  ci->tmp_dir = strdup(cmd->command.run.tmp_dir);
  ci->function_name = strdup (cmd->command.run.kernel->name);
  const char* module_fn = nuplus_llvm_codegen (cmd->command.run.tmp_dir,
                                        cmd->command.run.kernel,
                                        cmd->device);
  cmd->command.run.wg = (void*)module_fn;

  LL_APPEND (compiler_cache, ci);
  POCL_UNLOCK (compiler_cache_lock);

}

void
pocl_nuplus_compile_kernel (_cl_command_node *cmd, cl_kernel kernel, cl_device_id device)
{
  if (cmd != NULL && cmd->type == CL_COMMAND_NDRANGE_KERNEL)
    check_compiler_cache (cmd);
}

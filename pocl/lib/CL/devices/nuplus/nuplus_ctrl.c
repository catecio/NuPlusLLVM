/*
 * nuplus_boot.c
 *
 *  Created on: 06 ott 2017
 *      Author: luigi
 */

#include "nuplus_driver.h"

//TODO - controlla nuplus_boot e nuplus_run, in particolare capire se le info di destinazione thread id ecc sono date dalla struttura in ingresso, e capire se è utile passare tutto il device o anche solo la struttura core

int nuplus_boot(nuplus_dev* nuplus_device){
	unsigned int ack;
	for (int i = 0; i < nuplus_device->num_cores; i++) {
		for (int j = 0; j < nuplus_device->num_cores; j++) {
			///< Send command to read data from memory
			if(!write_char(nuplus_device->serial_fd, BOOT_CORE_COMMAND))
				return 0;

			///< Waiting ack from device for write command
			if(!read_long(nuplus_device->serial_fd, &ack, 1500))
				return 0;

			///< Check for boot command ack
			if(ack != BOOT_CORE_ACK){
				printf("No BOOT_CORE_ACK!\n");
				return 0;
			}

			///< Send destination
			if(!write_char(nuplus_device->serial_fd, (uint8_t)nuplus_device->cores[i]->id))
				return 0;

			///< Send thread id
			if(!write_char(nuplus_device->serial_fd, (uint8_t)nuplus_device->cores[i]->threads[j]->thread_id))
				return 0;

			///< Send program counter
			if(!write_long(nuplus_device->serial_fd, nuplus_device->cores[i]->threads[j]->pc))
				return 0;


			///< Waiting ack from device
			if(!read_long(nuplus_device->serial_fd, &ack, 1500))
				return 0;

			///< Check for boot ack
			if(ack != BOOT_CORE_ACK){
				printf("No BOOT_CORE_ACK!\n");
				return 0;
			}
		}
	}
	return 1;
}

int nuplus_run(nuplus_dev* nuplus_device){
	unsigned int ack;
	///< Send command to read data from memory
	if(!write_char(nuplus_device->serial_fd, ENABLE_THREAD_COMMAND))
		return 0;

	///< Waiting ack from device for write command
	if(!read_long(nuplus_device->serial_fd, &ack, 1500))
		return 0;

	///< Check for enable thread command ack
	if(ack != ENABLE_THREAD_ACK){
		printf("No BOOT_CORE_ACK!\n");
		return 0;
	}

	///< Send destination
	if(!write_char(nuplus_device->serial_fd, (uint8_t)nuplus_device->cores->id))
		return 0;

	///< Send thread mask
	if(!write_char(nuplus_device->serial_fd, (uint8_t)nuplus_device->cores->thread_active_mask))
		return 0;

	///< Waiting ack from device
	if(!read_long(nuplus_device->serial_fd, &ack, 1500))
		return 0;

	///< Check for boot ack
	if(ack != BOOT_CORE_ACK){
		printf("No BOOT_CORE_ACK!\n");
		return 0;
	}

	return 1;
}

int nuplus_reset(nuplus_dev* nuplus_device){
	unsigned int ack;
	///< Send command to read data from memory
	if(!write_char(nuplus_device->serial_fd, RESET_CORE_COMMAND))
		return 0;

	///< Waiting ack from device for write command
	if(!read_long(nuplus_device->serial_fd, &ack, 1500))
		return 0;

	///< Check command ack
	if(ack != RESET_CORE_ACK){
		printf("No RESET_CORE_ACK!\n");
		return 0;
	}

	return 1;
}

int nuplus_freeze(nuplus_dev* nuplus_device){
	unsigned int ack;
	///< Send command to read data from memory
	if(!write_char(nuplus_device->serial_fd, FREEZE_CORE_COMMAND))
		return 0;

	///< Waiting ack from device for write command
	if(!read_long(nuplus_device->serial_fd, &ack, 1500))
		return 0;

	///< Check command ack
	if(ack != FREEZE_CORE_ACK){
		printf("No FREEZE_CORE_ACK!\n");
		return 0;
	}

	return 1;
}

int nuplus_get_done(nuplus_dev* nuplus_device){
	unsigned int end;

	///< Waiting end from device
	if(!read_long(nuplus_device->serial_fd, &end, 1500))
		return 0;

	///< Check the end of kernel
	if(end != END_OF_KERNEL){
		return 0;
	}

	return 1;
}

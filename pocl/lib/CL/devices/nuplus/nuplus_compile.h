#pragma once
#include "pocl_cl.h"
#include "pocl_cache.h"

const char* nuplus_llvm_codegen (const char* tmpdir,
                          cl_kernel kernel,
                          cl_device_id device);

const char* nuplus_link (const char* tmpdir, const char* objfile,
    size_t start_address, cl_kernel kernel);

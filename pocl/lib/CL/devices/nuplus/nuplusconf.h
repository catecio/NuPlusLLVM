// In this file are defined the values to use in the device configuration
#define NUPLUS_BAUD_RATE 921600

#define NUPLUS_MAX_COMPUTE_UNITS 4
#define NUPLUS_MAX_THREADS 4

#define NUPLUS_MAX_ITEM_DIM 2

#define NUPLUS_OCL_MAJ_VER 2
#define NUPLUS_OCL_MIN_VER 0 

#define NUPLUS_ADDRESS_BITS 32

#define NUPLUS_GLOBAL_MEM_SIZE 512*1024*1024 //bytes (512MB)
#define NUPLUS_CACHELINE_SIZE 0 //bytes
#define NUPLUS_CACHE_SIZE 0 //bytes

#define NUPLUS_LOCAL_MEM_SIZE 0 //bytes
 
#define NUPLUS_CONSTANT_BUFFER_SIZE 0 //bytes
#define NUPLUS_CONSTANT_ARGS 8 

//TODO: This should be fixed with proper elf dimension
#define NUPLUS_ELF_DIM 1*1024*1024

// library paths
#define NUPLUS_LIBS_PATH SRCDIR "/../libs"
#define NUPLUS_LIBC_PATH NUPLUS_LIBS_PATH "/libc"
#define NUPLUS_LIBCOMP_PATH NUPLUS_LIBC_PATH "/libcompiler"
#define NUPLUS_MISC_PATH SRCDIR "/../misc"

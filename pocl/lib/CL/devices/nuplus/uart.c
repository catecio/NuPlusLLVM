/*
 * uart.c
 *
 *  Created on: 05 ott 2017
 *      Author: luigi
 */

#include "uart.h"

int open_connection(const char *path, int uart_baud_rate)
{
	struct termios serialopts;
	int serial_fd;

	    serial_fd = open(path, O_RDWR | O_NOCTTY);
	    if (serial_fd < 0)
	    {
	        perror("couldn't open serial port");
	        return -1;
	    }

	    // Configure serial options
	    memset(&serialopts, 0, sizeof(serialopts));
	    serialopts.c_cflag = CS8 | CLOCAL | CREAD;
	    cfsetspeed(&serialopts, uart_baud_rate);
	    if (tcsetattr(serial_fd, TCSANOW, &serialopts) != 0)
	    {
	        perror("Unable to initialize serial port");
	        return -1;
	    }

	    // Clear out any junk that may already be buffered in the
	    // serial driver (otherwise the ping sequence may fail)
	    tcflush(serial_fd, TCIOFLUSH);

	    return serial_fd;
}

int close_connection(int serial_fd){
	if(!close(serial_fd))
		return 0;
	return 1;
}

void flush(int serial_fd){
	tcflush(serial_fd, TCIOFLUSH);
}



int write_char(int serial_fd, unsigned int b){

	if (write(serial_fd, &b, 1) != 1)
	    {
	        perror("write");
	        return 0;
	    }

	    return 1;
}


int read_char(int serial_fd, unsigned char *ch, int timeout_ms){

		fd_set set;
	    struct timeval tv;
	    int ready_fds;

	    FD_ZERO(&set);
	    FD_SET(serial_fd, &set);

	    tv.tv_sec = timeout_ms / 1000;
	    tv.tv_usec = (timeout_ms % 1000) * 1000;

	    do
	    {
	        ready_fds = select(FD_SETSIZE, &set, NULL, NULL, &tv);
	    }
	    while (ready_fds < 0 && errno == EINTR);

	    if (ready_fds == 0)
	        return 0;

	    if (read(serial_fd, ch, 1) != 1)
	    {
	        perror("read");
	        return 0;
	    }

	    return 1;
}

int write_long(int serial_fd, unsigned int value){
	 unsigned char out[4] =
	 {
			 value & 0xff,
	        (value >> 8) & 0xff,
	        (value >> 16) & 0xff,
	        (value >> 24) & 0xff
	 };

	 if (write(serial_fd, out, 4) != 4)
	 {
		 perror("write");
	     return 0;
	 }

	 return 1;
}

int read_long(int serial_fd, unsigned int *out, int timeout){
	 unsigned int result = 0;
	 unsigned char ch;
	 int i;

	 for (i = 0; i < 4; i++)
	 {
		 if (!read_char(serial_fd, &ch, timeout))
			 return 0;

	     result = (result >> 8) | ((unsigned int) ch << 24);
	 }

	 *out = result;
	 return 1;
}


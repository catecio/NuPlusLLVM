/*
 ============================================================================
 Name        : nuplus_driver.c
 Author      : Luigi Venuso
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include "nuplus_driver.h"
#include <stdlib.h>

void CopyString(char * dest, const char * src, size_t maxLength){
    unsigned int i;
    for(i=0 ; src[i] && ( i < maxLength ) ; i++){
        dest[i] = src[i];
    };
}

void get_nuplus_devices(size_t *n, nuplus_dev **devs){
	unsigned char ack;
	unsigned int device_id;
	int general_serial_fd;
	char general_port_name[NUPLUSE_DEVS_NUMBER][256];			//XXX: no more then 256 tty device

	char port_path[256] = "/dev/";
	nuplus_dev *dev_vec = *devs;

	DIR *tty;
	struct dirent *directory;
	tty = opendir("/dev/");
	*n = 0;

	while ((directory = readdir(tty)) != NULL)
	{

		if(strncmp(directory->d_name, "ttyUSB", 6) == 0){
			strcat(port_path, directory->d_name);
			general_serial_fd = open_connection(port_path, NUPLUS_BAUD_RATE);

			///< Send command to ping device
			if(!write_char(general_serial_fd, READ_ID_COMMAND))
				break;

			///< Waiting ack from device for write command
			if(!read_char(general_serial_fd, &ack, 1500))
				break;


			///< Check command ack
			if(ack != READ_ID_COMMAND){
				printf("No READ_ID_ACK!\n");
				break;
			}

			///< Waiting devise id
			if(!read_long(general_serial_fd, &device_id, 1500))
				break;

			///< Waiting ack from device for write command
			if(!read_char(general_serial_fd, &ack, 1500))
				break;

			///< Check command ack
			if(ack != READ_ID_ACK){
				printf("No READ_ID_ACK!\n");
				break;
			}

			///< Check if id == DATA_PING_MATCH
			if(device_id == DATA_PING_MATCH){
				strcpy(general_port_name[*n], port_path);
				*n += 1;
			}
		}
	}



	if(*n > 0){
		dev_vec = (nuplus_dev *) calloc (*n, sizeof(nuplus_dev));
		for (int i=0; i<*n; i++){
			dev_vec[i].port_name = (char *) calloc (256, sizeof(char));
			strcpy(dev_vec[i].port_name, general_port_name[i]);
		}
	}

	*devs=dev_vec;
	closedir(tty);

}

int nuplus_open(nuplus_dev* nuplus_device){
	if (strcmp(nuplus_device->port_name, "") && nuplus_device->baud_rate != 0) {
		nuplus_device->serial_fd = open_connection(nuplus_device->port_name, nuplus_device->baud_rate);
		return 1;
	} else
		return 0;
}

int nuplus_close(nuplus_dev* nuplus_device){
	if(!close_connection(nuplus_device->serial_fd))
		return 0;

	return 1;
}

int nuplus_set_barrier(nuplus_dev* nuplus_device, unsigned int barrier_counter){
	unsigned char ack;

	///< Send command to read data from memory
	if(!write_char(nuplus_device->serial_fd, BARRIER_COMMAND))
		return 0;

	///< Waiting ack from device for write command
	if(!read_char(nuplus_device->serial_fd, &ack, 1500))
		return 0;

	///< Check command ack
	if(ack != BARRIER_COMMAND){
		printf("No BARRIER_COMMAND!\n");
		return 0;
	}

	///< Send number of barrier
	if(!write_char(nuplus_device->serial_fd, barrier_counter))
		return 0;

	///< Waiting ack from device for write command
		if(!read_char(nuplus_device->serial_fd, &ack, 1500))
			return 0;

	///< Check command ack
	if(ack != BARRIER_ACK){
		printf("No BARRIER_ACK!\n");
		return 0;
	}


	return 1;
}

void create_thread(nuplus_thread* thread, uint32_t pc, uint32_t thread_id){
	thread->pc =  pc;
	thread->thread_id = thread_id;
}

void create_core(nuplus_core* core, nuplus_thread* thread, uint32_t core_id, uint32_t num_threads){
	core->id = core_id;
	core->num_threads = num_threads;
	core->threads = thread;
}

void destroy_device(nuplus_dev* dev){
	if (dev->num_cores > 0){
		for(int i=0; i<dev->num_cores; i++){
			destroy_core(dev[i].cores);
		}
	}

	free(dev);
}

void destroy_core(nuplus_core* core){
	if (core->num_threads > 0){
		for(int i=0; i<core->num_threads; i++){
			free(core[i].threads);
		}
	}
	free(core);
}

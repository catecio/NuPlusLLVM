#include "nuplus_driver.h"

ssize_t nuplus_read(nuplus_dev* nuplus_device, void* buff, size_t nbyte){

	unsigned char data[MEM_LINE];
	unsigned char ack;

	///< Send command to read data from memory
	if(!write_char(nuplus_device->serial_fd, READ_MEMORY_COMMAND))
		return 0;

	///< Waiting ack from device for write command
	if(!read_char(nuplus_device->serial_fd, &ack, 1500))
		return 0;

	///< Check command ack
	if(ack != READ_MEMORY_COMMAND){
		printf("No READ_MEMORY_COMMAND!\n");
		return 0;
	}

	///< Send address
	if(!write_long(nuplus_device->serial_fd, nuplus_device->address))
		return 0;

	///< Read memory line (64 byte)
	for(int i=0; i < 64; i++){
		if(!read_char(nuplus_device->serial_fd, data + i, 1500))
			return 0;
	}

	///< Waiting ack from device
	if(!read_char(nuplus_device->serial_fd, &ack, 1500))
		return 0;

	///< Check ack
	if(ack != READ_MEMORY_ACK){
		printf("No READ_MEMORY_ACK!\n");
		return 0;
	}

	///< If the input nbyte parameter is less than one memory line, the address
	///< must to be increased by nbyte. Instead, if it is larger than a memory line,
	///< the address will be increased by 64 (MEM_LINE).
	if(nbyte <= MEM_LINE) {
		for(int i=0; i < nbyte; i++){
			*(char *)(buff + i) = data[i];
		}
		//buff = data_out;
		nuplus_device->address += nbyte;
	}else {
		buff = data;
		nuplus_device->address += MEM_LINE;
	}

	return nbyte;
}

ssize_t nuplus_write(nuplus_dev* nuplus_device, const void* buff, size_t nbyte){

	unsigned char ack;

	///< Send command to read data from memory
	if(!write_char(nuplus_device->serial_fd, WRITE_MEMORY_COMMAND))
		return 0;

	///< Waiting ack from device for write command
	if(!read_char(nuplus_device->serial_fd, &ack, 1500))
		return 0;

	///< Check command ack
	if(ack != WRITE_MEMORY_COMMAND){
		printf("No WRITE_MEMORY_COMMAND!\n");
		return 0;
	}

	///< Send address
	if(!write_long(nuplus_device->serial_fd, nuplus_device->address))
		return 0;

	///< Send num byte number
	if(nbyte <= MEM_LINE){
		if(!write_char(nuplus_device->serial_fd, nbyte))
			return 0;
	}else {
		if(!write_char(nuplus_device->serial_fd, MEM_LINE))
			return 0;
	}

	///Write memory line
	if(nbyte <= MEM_LINE){
		char data_write[nbyte];
		memcpy(data_write, buff, nbyte);

		for(int i=0; i<nbyte; i++){
			write_char(nuplus_device->serial_fd, data_write[i]);
		}

		nuplus_device->address += nbyte;
		nbyte = 0;
	}else {
		char data_write[MEM_LINE];
		memcpy(data_write, buff, MEM_LINE);

		for(int i=0; i<MEM_LINE; i++){
			write_char(nuplus_device->serial_fd, data_write[i]);
		}

		nuplus_device->address += MEM_LINE;
		nbyte = nbyte - MEM_LINE;
	}

	///< Waiting ack from device
	if(!read_char(nuplus_device->serial_fd, &ack, 1500))
		return 0;

	///< Check ack
	if(ack != WRITE_MEMORY_ACK){
		printf("No WRITE_MEMORY_ACK!\n");
		return 0;
	}

	return nbyte;
}

int nuplus_lseek(nuplus_dev* nuplus_device, uint64_t offset, int whence){
	if(whence == SEEK_SET)
		nuplus_device->address = offset;
	else if(whence == SEEK_CUR)
		nuplus_device->address += offset;
	else
		return 0;

	return nuplus_device->address;
}

/*
 * main.c
 *
 *  Created on: 09 ott 2017
 *      Author: luigi
 */

#include "nuplus_driver.h"

int main(){
	nuplus_dev *devs = NULL;
	char *write_line = (char *)malloc(64 * sizeof(char));
	char *read_line = (char *)malloc(64 * sizeof(char));
	size_t n;


	printf("Ricerca di nu+ systems\n");
	get_nuplus_devices(&n, &devs);
	printf("Trovati %d nu+ system(s)\n", (unsigned int)n);

	devs->serial_fd = open_connection(devs->port_name, NUPLUS_BAUD_RATE);


//	devs->cores = (nuplus_core *) calloc (4, sizeof(nuplus_core));
//
//	  for(int i = 0; i < 4; i++)
//	  {
//	    nuplus_thread *threads = (nuplus_thread *) calloc (4, sizeof(nuplus_thread));
//	    for(int j = 0; j < 4; j++)
//	    {
//	      create_thread(&(threads[j]),0, j);
//	    }
//
//	    create_core(&(devs->cores[i]), threads, i, 4);
//	  }

//	printf("Boot:\n");
//	nuplus_boot(devs);
//	printf("Run:\n");
//	nuplus_run(devs);

	for(int i=0; i<64; i++){
		if(i%2 == 0)
			write_line[i] = 0x20;
		else
			write_line[i] = 0x17;
	}

	printf("Write:\n");
	for(int i=0; i<64; i++){
		printf("%#2x ",write_line[i]);
	}
	printf("\n");


	//Set seek for write
	nuplus_lseek(devs, 0x00000060, SEEK_SET);
	//Write
	nuplus_write(devs, write_line, 70);
	//Set seek for read
	nuplus_lseek(devs, 0x00000060, SEEK_SET);
	printf("\n");
	//Read
	nuplus_read(devs, read_line, 64);

	printf("Read: \n");
		for(int i=0; i<64; i++){
			printf("%#2x ",read_line[i]);
		}

	printf("\n");

	nuplus_set_barrier(devs, 4);

	return 0;

}

PREFIX=$(MANGO_ROOT)

.PHONY: all libs misc libs-header clean

all: misc libs-header libs

libs: 
	cd libs/ && make
	sudo mkdir -p $(PREFIX)/usr/local/llvm-nuplus/libs-nuplus
	sudo cp -r libs/* $(PREFIX)/usr/local/llvm-nuplus/libs-nuplus/

misc: 	
	sudo mkdir -p $(PREFIX)/usr/local/llvm-nuplus/misc-nuplus
	sudo cp -r misc/* $(PREFIX)/usr/local/llvm-nuplus/misc-nuplus/

libs-header:	
	sudo mkdir -p $(PREFIX)/usr/local/llvm-nuplus/libs-nuplus/libc
	sudo cp -r libs/libc/include $(PREFIX)/usr/local/llvm-nuplus/libs-nuplus/libc

clean: 
	cd libs/ && make clean
	sudo rm -rf $(PREFIX)/usr/local/llvm-nuplus/libs-nuplus
	sudo rm -rf $(PREFIX)/usr/local/llvm-nuplus/misc-nuplus

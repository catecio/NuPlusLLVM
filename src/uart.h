/*
 * uart.h
 *
 *  Created on: 05 ott 2017
 *      Author: luigi
 */

#ifndef UART_H_
#define UART_H_

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/fcntl.h>
#include <sys/time.h>
#include <sys/select.h>
#include <termios.h>
#include <unistd.h>
#include <stdint.h>



int open_connection(const char *path, int uart_baud_rate);
int close_connection(int serial_fd);
void flush(int serial_fd);

int write_char(int serial_fd, char b);
int read_char(int serial_fd, unsigned char *ch, int timeout_ms);

int write_long(int serial_fd, unsigned int value);
int read_long(int serial_fd, unsigned int *out, int timeout);


#endif /* UART_H_ */
